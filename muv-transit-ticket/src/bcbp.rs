use std::{
	array,
	fmt::{self, Debug, Display, Formatter},
	str::{self, FromStr},
};

#[derive(Debug, Clone, PartialEq)]
pub struct Bcbp {
	pub passenger_name: AlphaNumeric<PASSENGER_NAME_LEN>,
	pub electronic_ticket: bool,
	pub legs: Vec<Leg>,
}

impl Bcbp {
	pub fn parse(b: &[u8]) -> Result<Self, Error> {
		if b.len() < HEADER_LEN {
			return Err(Error::HeaderTooShort);
		}
		if b[0] != b'M' {
			return Err(Error::InvalidFormatCode);
		}
		if !b[1].is_ascii_digit() {
			return Err(Error::InvalidNumberOfLegs);
		}
		let legs_count = b[1] - b'0';
		let b = &b[2..];

		let (passenger_name, b) = split_to_alphanumeric(b)?;

		let electronic_ticket = match b[0] {
			b'E' => true,
			b' ' => false,
			n => return Err(Error::InvalidElectronicTicketIndicator(n)),
		};
		let mut b = &b[1..];

		let mut legs = Vec::with_capacity(legs_count.into());
		for _ in 0..legs_count {
			let leg;
			(leg, b) = Leg::parse(b)?;
			legs.push(leg);
		}

		Ok(Self {
			passenger_name,
			electronic_ticket,
			legs,
		})
	}
}

impl Bcbp {
	#[must_use]
	pub fn passenger_name(&self) -> (Option<&str>, &str) {
		let name = self.passenger_name.as_str();
		match name.split_once('/') {
			Some((last_name, first_name)) => (Some(first_name), last_name),
			None => (None, name),
		}
	}
}

impl FromStr for Bcbp {
	type Err = Error;
	fn from_str(s: &str) -> Result<Self, Self::Err> {
		Self::parse(s.as_bytes())
	}
}

fn split_to_alphanumeric<const N: usize>(b: &[u8]) -> Result<(AlphaNumeric<N>, &[u8]), Error> {
	let (first, rest) = split_to_array(b);
	let first = AlphaNumeric::new(first).ok_or(Error::InvalidAlphanumeric)?;
	Ok((first, rest))
}

fn split_to_array<const N: usize>(b: &[u8]) -> ([u8; N], &[u8]) {
	let first = array::from_fn(|i| b[i]);
	let rest = &b[N..];
	(first, rest)
}

const PASSENGER_NAME_LEN: usize = 20;
const OPERATING_CARRIER_PNR_LEN: usize = 7;
const AIRPORT_CODE_LEN: usize = 3;
const CARRIER_DESIGNATOR_LEN: usize = 3;
const FLIGHT_NUMBER_LEN: usize = 5;
const DATE_LEN: usize = 3;
const SEAT_NUMBER_LEN: usize = 4;
const CHECKIN_SEQUENCE_NUMBER_LEN: usize = 5;

const HEADER_LEN: usize = 1 + 1 + PASSENGER_NAME_LEN + 1;
const MIN_LEG_LEN: usize = OPERATING_CARRIER_PNR_LEN
	+ AIRPORT_CODE_LEN
	+ AIRPORT_CODE_LEN
	+ CARRIER_DESIGNATOR_LEN
	+ FLIGHT_NUMBER_LEN
	+ DATE_LEN
	+ 1 + SEAT_NUMBER_LEN
	+ CHECKIN_SEQUENCE_NUMBER_LEN
	+ 1 + 2;

#[derive(Debug, Clone, PartialEq)]
pub struct Leg {
	pub operating_carrier_pnr: AlphaNumeric<OPERATING_CARRIER_PNR_LEN>,
	pub from_city_airport_code: AlphaNumeric<AIRPORT_CODE_LEN>,
	pub to_city_airport_code: AlphaNumeric<AIRPORT_CODE_LEN>,
	pub operating_carrier_designator: AlphaNumeric<CARRIER_DESIGNATOR_LEN>,
	pub flight_number: AlphaNumeric<FLIGHT_NUMBER_LEN>,
	pub compartment_code: CompartmentCode,
	pub seat_number: AlphaNumeric<SEAT_NUMBER_LEN>,
	pub checkin_sequence_number: AlphaNumeric<CHECKIN_SEQUENCE_NUMBER_LEN>,
}

impl Leg {
	fn parse(b: &[u8]) -> Result<(Self, &[u8]), Error> {
		if b.len() < MIN_LEG_LEN {
			return Err(Error::HeaderTooShort);
		}

		let (operating_carrier_pnr, b) = split_to_alphanumeric(b)?;
		let (from_city_airport_code, b) = split_to_alphanumeric(b)?;
		let (to_city_airport_code, b) = split_to_alphanumeric(b)?;
		let (operating_carrier_designator, b) = split_to_alphanumeric(b)?;
		let (flight_number, b) = split_to_alphanumeric(b)?;

		let (_date_of_flight, b) = split_to_array::<DATE_LEN>(b);

		let compartment_code = match b[0] {
			b'F' => CompartmentCode::First,
			b'C' => CompartmentCode::Business,
			b'Y' | b'M' | b'B' => CompartmentCode::Economic,
			n => return Err(Error::InvalidCompartmentCode(n)),
		};
		let b = &b[1..];

		let (seat_number, b) = split_to_alphanumeric(b)?;
		let (checkin_sequence_number, b) = split_to_alphanumeric(b)?;

		let _passenger_status = b[0];
		let _field_size = &b[1..3];
		let b = &b[3..];

		let val = Self {
			operating_carrier_pnr,
			from_city_airport_code,
			to_city_airport_code,
			operating_carrier_designator,
			flight_number,
			compartment_code,
			seat_number,
			checkin_sequence_number,
		};

		Ok((val, b))
	}
}

#[derive(Debug, Clone, Copy, PartialEq, Eq)]
pub enum CompartmentCode {
	First,
	Business,
	Economic,
}

#[derive(Clone, Copy)]
pub struct AlphaNumeric<const N: usize>([u8; N]);

impl<const N: usize> AlphaNumeric<N> {
	#[must_use]
	pub const fn new(b: [u8; N]) -> Option<Self> {
		let mut s = b.as_slice();
		while let [rest @ .., last] = s {
			if !matches!(last, b' ' | b'/' | b'0'..=b'9' | b'A'..=b'Z') {
				return None;
			}
			s = rest;
		}

		Some(Self(b))
	}

	#[must_use]
	pub const fn resize<const M: usize>(self) -> AlphaNumeric<M> {
		let mut res = [b' '; M];

		let mut i = 0;
		while i < N && i < M {
			res[i] = self.0[i];
			i += 1;
		}

		AlphaNumeric(res)
	}

	#[must_use]
	pub const fn as_bytes(&self) -> &[u8] {
		let mut b = self.0.as_slice();
		while let [first, rest @ ..] = b {
			if *first != b'0' {
				break;
			}
			b = rest;
		}
		while let [rest @ .., last] = b {
			if *last != b' ' {
				break;
			}
			b = rest;
		}
		b
	}

	#[must_use]
	pub const fn as_str(&self) -> &str {
		let b = self.as_bytes();
		unsafe { str::from_utf8_unchecked(b) }
	}
}

impl<const N: usize, const M: usize> PartialEq<AlphaNumeric<M>> for AlphaNumeric<N> {
	fn eq(&self, other: &AlphaNumeric<M>) -> bool {
		self.as_bytes() == other.as_bytes()
	}
}

impl<const N: usize> Debug for AlphaNumeric<N> {
	fn fmt(&self, f: &mut Formatter<'_>) -> fmt::Result {
		Debug::fmt(self.as_str(), f)
	}
}

impl<const N: usize> Display for AlphaNumeric<N> {
	fn fmt(&self, f: &mut Formatter<'_>) -> fmt::Result {
		f.write_str(self.as_str())
	}
}

#[derive(Debug)]
pub enum Error {
	HeaderTooShort,
	InvalidFormatCode,
	InvalidNumberOfLegs,
	InvalidElectronicTicketIndicator(u8),
	InvalidCompartmentCode(u8),
	InvalidAlphanumeric,
}

#[cfg(test)]
mod tests {
	use crate::bcbp::{AlphaNumeric, Bcbp, CompartmentCode, Leg};

	fn from_str<const N: usize, const M: usize>(b: &[u8; N]) -> AlphaNumeric<M> {
		AlphaNumeric::new(*b).unwrap().resize()
	}

	#[test]
	/// <https://www.iata.org/contentassets/1dccc9ed041b4f3bbdcf8ee8682e75c4/2021_03_02-bcbp-implementation-guide-version-7-.pdf>
	fn iata() {
		// page 39
		assert_eq!(
            Bcbp::parse(b"M1TEST/HIDDEN         E8OQ6FU FRARLGLH 4010 012C004D0001 35C>2180WW6012BLH              2922023642241060 LH                        *30600000K09         ").unwrap(),
            Bcbp {
			    passenger_name: from_str(b"TEST/HIDDEN"),
			    electronic_ticket: true,
			    legs: vec![Leg {
				    operating_carrier_pnr: from_str(b"8OQ6FU"),
				    from_city_airport_code: from_str(b"FRA"),
				    to_city_airport_code: from_str(b"RLG"),
				    operating_carrier_designator: from_str(b"LH"),
				    flight_number: from_str(b"4010"),
				    compartment_code: CompartmentCode::Business,
				    seat_number: from_str(b"4D"),
				    checkin_sequence_number: from_str(b"1"),
			    }],
		    }
        );

		// page 40
		assert_eq!(
            Bcbp::parse(b"M1TEST/PETER          E24Z5RN AMSBRUKL 1733 019M008A0001 316>503  W0D0742497067621").unwrap(),
            Bcbp {
                passenger_name: from_str(b"TEST/PETER"),
                electronic_ticket: true,
                legs: vec![Leg {
                    operating_carrier_pnr: from_str(b"24Z5RN"),
                    from_city_airport_code: from_str(b"AMS"),
                    to_city_airport_code: from_str(b"BRU"),
                    operating_carrier_designator: from_str(b"KL"),
                    flight_number: from_str(b"1733"),
                    compartment_code: CompartmentCode::Economic,
                    seat_number: from_str(b"8A"),
                    checkin_sequence_number: from_str(b"1"),
                }],
            }
        );

		// page 46
		assert_eq!(
            Bcbp::parse(b"M1ASKREN/TEST         EA272SL ORDNRTUA 0881 007F002K0303 15C>3180 M6007BUA              2901624760758980 UA UA EY975897            *30600    09  UAG    ^160MEYCIQCVDy6sskR0zx8Ac5aXCG0hjkejH587woSGHWnbBRbp8QIhAJ790UHbTHG9nZLnllP+JjStGWPLWGR7Ag5on2FPCeRG").unwrap(),
            Bcbp {
                passenger_name: from_str(b"ASKREN/TEST"),
                electronic_ticket: true,
                legs: vec![Leg {
                    operating_carrier_pnr: from_str(b"A272SL"),
                    from_city_airport_code: from_str(b"ORD"),
                    to_city_airport_code: from_str(b"NRT"),
                    operating_carrier_designator: from_str(b"UA"),
                    flight_number: from_str(b"881"),
                    compartment_code: CompartmentCode::First,
                    seat_number: from_str(b"2K"),
                    checkin_sequence_number: from_str(b"303"),
                }],
            }
        );
	}
}
