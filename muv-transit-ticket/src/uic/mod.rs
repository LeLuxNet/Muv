use std::{
	fmt::{self, Debug, Formatter},
	str,
};

use condense::deflate::decompress_zlib;

mod gen_1_3;
mod gen_2_0;
mod gen_3_0;

pub mod flex;
pub mod header;

pub struct Reader {
	body: Vec<u8>,
	pos: usize,
}

impl Reader {
	pub fn new(b: &[u8]) -> Result<Self, Error> {
		const HEADER_LEN: usize = 3 + 2;
		if b.len() < HEADER_LEN {
			return Err(Error::HeaderTooShort);
		}

		if &b[..3] != b"#UT" {
			return Err(Error::UnknownMessageType);
		}
		let header_len = match &b[3..5] {
			b"01" => 5 + 4 + 5 + 50, // https://www.era.europa.eu/system/files/2022-10/Recommendation%20on%20TAP%20TSI%20Revision%20-%20Technical%20Document%20-%20B12.pdf#page=44
			b"02" => 5 + 4 + 5 + 32 + 32, // https://www.era.europa.eu/system/files/2022-10/Recommendation%20on%20TAP%20TSI%20Revision%20-%20Technical%20Document%20-%20B12.pdf#page=45
			_ => return Err(Error::UnknownVersion([b[3], b[4]])),
		};
		if header_len + 4 > b.len() {
			return Err(Error::HeaderTooShort);
		}
		let b = &b[header_len..];

		let body_len: u16 = str::from_utf8(&b[..4]).unwrap().parse().unwrap();
		let body_len = usize::from(body_len);
		let b = &b[4..];

		if body_len > b.len() {
			return Err(Error::BodyTooShort);
		}

		let compressed_body = &b[..body_len];
		let body = decompress_zlib(
			compressed_body,
			Vec::with_capacity(compressed_body.len() * 2),
		)
		.unwrap();

		Ok(Self { body, pos: 0 })
	}
}

impl Iterator for Reader {
	type Item = Result<Record, Error>;

	fn next(&mut self) -> Option<Self::Item> {
		let b = &self.body[self.pos..];
		if b.is_empty() {
			return None;
		}
		if b.len() < 12 {
			todo!()
		}

		// https://www.era.europa.eu/system/files/2022-10/Recommendation%20on%20TAP%20TSI%20Revision%20-%20Technical%20Document%20-%20B12.pdf#page=68
		let id = [b[0], b[1], b[2], b[3], b[4], b[5]];
		let version = [b[6], b[7]];
		let len: u16 = str::from_utf8(&b[8..12]).unwrap().parse().unwrap();
		let len = len.into();
		let data = &b[12..len];
		self.pos += len;

		Some(match (id, version) {
			([b'U', b'_', b'H', b'E', b'A', b'D'], [b'0', b'1']) => header::Header::parse(data)
				.map(Record::UicHeader)
				.map_err(Error::from),
			([b'U', b'_', b'T', b'L', b'A', b'Y'], _) => Ok(Record::UicLayout),
			([b'U', b'_', b'F', b'L', b'E', b'X'], [b'1', b'3']) => flex::Flex::parse_1_3(data)
				.map(Record::UicFlex)
				.map_err(Error::from),
			([b'U', b'_', id @ ..], _) => Ok(Record::UicUnknown {
				id,
				version,
				data: data.into(),
			}),
			(_, _) => Ok(Record::TcoUnknown {
				company: [id[0], id[1], id[2], id[3]],
				r#type: [id[4], id[5]],
				version,
				data: data.into(),
			}),
		})
	}
}

pub enum Record {
	UicHeader(header::Header),
	UicLayout,
	UicFlex(flex::Flex),
	UicUnknown {
		id: [u8; 4],
		version: [u8; 2],
		data: Box<[u8]>,
	},

	TcoUnknown {
		company: [u8; 4],
		r#type: [u8; 2],
		version: [u8; 2],
		data: Box<[u8]>,
	},
}

impl Debug for Record {
	fn fmt(&self, f: &mut Formatter<'_>) -> fmt::Result {
		match self {
			Self::UicHeader(header) => f.debug_tuple("UicHeader").field(&header).finish(),
			Self::UicLayout => f.write_str("UicLayout"),
			Self::UicFlex(flex) => f.debug_tuple("UicFlex").field(&flex).finish(),
			Self::UicUnknown { id, version, data } => f
				.debug_struct("UicUnknown")
				.field("id", &BStr(id))
				.field("version", &BStr(version))
				.field("data", data)
				.finish(),
			Self::TcoUnknown {
				company,
				r#type,
				version,
				data,
			} => f
				.debug_struct("TcoUnknown")
				.field("company", &BStr(company))
				.field("type", &BStr(r#type))
				.field("version", &BStr(version))
				.field("data", data)
				.finish(),
		}
	}
}

struct BStr<'a>(&'a [u8]);
impl Debug for BStr<'_> {
	fn fmt(&self, f: &mut Formatter<'_>) -> fmt::Result {
		f.debug_list()
			.entries(self.0.iter().copied().map(B))
			.finish()
	}
}

struct B(u8);
impl Debug for B {
	fn fmt(&self, f: &mut Formatter<'_>) -> fmt::Result {
		write!(f, "b{:?}", char::from(self.0))
	}
}

#[derive(Debug)]
pub enum Error {
	HeaderTooShort,
	UnknownMessageType,
	UnknownVersion([u8; 2]),
	BodyTooShort,

	UicHeader(header::ParseError),
	UicFlex(flex::ParseError),
}

impl From<header::ParseError> for Error {
	fn from(value: header::ParseError) -> Self {
		Self::UicHeader(value)
	}
}

impl From<flex::ParseError> for Error {
	fn from(value: flex::ParseError) -> Self {
		Self::UicFlex(value)
	}
}
