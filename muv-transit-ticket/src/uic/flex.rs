use rasn::{error::DecodeError, types::Ia5String, uper};

use crate::uic::gen_1_3;

#[derive(Debug)]
pub struct Flex {
	pub traveler: Option<TravelerData>,
	pub document: Vec<Document>,
}

impl Flex {
	pub fn parse_1_3(b: &[u8]) -> Result<Self, ParseError> {
		let data: gen_1_3::UicRailTicketData = uper::decode(b)?;
		Ok(data.into())
	}
}

#[derive(Debug)]
pub enum ParseError {
	Asn(DecodeError),
}

impl From<DecodeError> for ParseError {
	fn from(value: DecodeError) -> Self {
		Self::Asn(value)
	}
}

impl From<gen_1_3::UicRailTicketData> for Flex {
	fn from(value: gen_1_3::UicRailTicketData) -> Self {
		Self {
			traveler: value.traveler_detail.map(TravelerData::from),
			document: value
				.transport_document
				.unwrap_or_default()
				.into_iter()
				.map(Document::from)
				.collect(),
		}
	}
}

#[derive(Debug, Clone, PartialEq, Eq, PartialOrd, Ord)]
pub struct TravelerData {
	pub travelers: Vec<Traveler>,
	pub preferred_language: Option<[u8; 2]>,
	pub group_name: Option<String>,
}

impl From<gen_1_3::TravelerData> for TravelerData {
	fn from(value: gen_1_3::TravelerData) -> Self {
		Self {
			travelers: value
				.traveler
				.unwrap_or_default()
				.into_iter()
				.map(Traveler::from)
				.collect(),
			preferred_language: value.preferred_language.map(|v| [v[0], v[1]]),
			group_name: value.group_name,
		}
	}
}

#[derive(Debug, Clone, PartialEq, Eq, PartialOrd, Ord, Hash)]
pub struct Traveler {
	pub title: Option<String>,
	pub first_name: Option<String>,
	pub second_name: Option<String>,
	pub last_name: Option<String>,

	pub gender: Option<Gender>,

	pub ticket_holder: bool,
}

impl From<gen_1_3::TravelerType> for Traveler {
	fn from(value: gen_1_3::TravelerType) -> Self {
		Self {
			title: value.title.map(String::from),
			first_name: value.first_name,
			second_name: value.second_name,
			last_name: value.last_name,

			gender: value.gender.map(Gender::from),

			ticket_holder: value.ticket_holder,
		}
	}
}

#[derive(Debug, Clone, Copy, PartialEq, Eq, PartialOrd, Ord, Hash)]
#[non_exhaustive]
pub enum Gender {
	Unspecified,
	Female,
	Male,
	Other,
}

impl From<gen_1_3::GenderType> for Gender {
	fn from(value: gen_1_3::GenderType) -> Self {
		match value {
			gen_1_3::GenderType::unspecified => Self::Unspecified,
			gen_1_3::GenderType::female => Self::Female,
			gen_1_3::GenderType::male => Self::Male,
			gen_1_3::GenderType::other => Self::Other,
		}
	}
}

#[derive(Debug, Clone, PartialEq, Eq, PartialOrd, Ord)]
pub struct Document {
	pub ticket: DocumentTicket,
}

impl From<gen_1_3::DocumentData> for Document {
	fn from(value: gen_1_3::DocumentData) -> Self {
		Self {
			ticket: value.ticket.into(),
		}
	}
}

#[derive(Debug, Clone, PartialEq, Eq, PartialOrd, Ord)]
pub enum DocumentTicket {
	Ticket(Ticket),
	CustomerCard(CustomerCard),
}

impl From<gen_1_3::DocumentDataTicket> for DocumentTicket {
	fn from(value: gen_1_3::DocumentDataTicket) -> Self {
		match value {
			gen_1_3::DocumentDataTicket::openTicket(t) => Self::Ticket(t.into()),
			gen_1_3::DocumentDataTicket::customerCard(c) => Self::CustomerCard(c.into()),
			_ => todo!("{value:?}"),
		}
	}
}

#[derive(Debug, Clone, PartialEq, Eq, PartialOrd, Ord)]
pub struct Ticket {
	pub from: Station,
	pub to: Station,

	pub class: Class,

	pub return_included: bool,

	pub valid_regions: Vec<Region>,
}

#[derive(Debug, Clone, PartialEq, Eq, PartialOrd, Ord, Hash)]
pub struct Station {
	pub num: Option<u32>,
	pub name: Option<String>,
}

impl Station {
	fn new(num: Option<u32>, name: Option<Ia5String>, name_utf8: Option<String>) -> Self {
		assert_eq!(name, None);
		Self {
			num,
			name: name_utf8,
		}
	}
}

impl From<gen_1_3::OpenTicketData> for Ticket {
	fn from(value: gen_1_3::OpenTicketData) -> Self {
		Self {
			from: Station::new(
				value.from_station_num,
				value.from_station_ia5,
				value.from_station_name_utf8,
			),
			to: Station::new(
				value.to_station_num,
				value.to_station_ia5,
				value.to_station_name_utf8,
			),

			class: value.class_code.into(),

			return_included: value.return_included,

			valid_regions: value
				.valid_region
				.unwrap_or_default()
				.into_iter()
				.map(Region::from)
				.collect(),
		}
	}
}

#[derive(Debug, Clone, PartialEq, Eq, PartialOrd, Ord, Hash)]
#[non_exhaustive]
pub enum Region {
	TrainLink(TrainLink),
	ViaStations,
	Zones,
	Lines,
	Polygone,
}

impl From<gen_1_3::RegionalValidityType> for Region {
	fn from(value: gen_1_3::RegionalValidityType) -> Self {
		match value {
			gen_1_3::RegionalValidityType::trainLink(t) => Self::TrainLink(t.into()),
			gen_1_3::RegionalValidityType::viaStations(_) => Self::ViaStations,
			gen_1_3::RegionalValidityType::zones(_) => Self::Zones,
			gen_1_3::RegionalValidityType::lines(_) => Self::Lines,
			gen_1_3::RegionalValidityType::polygone(_) => Self::Polygone,
		}
	}
}

#[derive(Debug, Clone, PartialEq, Eq, PartialOrd, Ord, Hash)]
pub struct TrainLink {
	pub name: Option<String>,

	pub from: Station,
	pub to: Station,
}

impl From<gen_1_3::TrainLinkType> for TrainLink {
	fn from(value: gen_1_3::TrainLinkType) -> Self {
		Self {
			name: value.train_ia5.map(String::from),

			from: Station::new(
				value.from_station_num,
				value.from_station_ia5,
				value.from_station_name_utf8,
			),
			to: Station::new(
				value.to_station_num,
				value.to_station_ia5,
				value.to_station_name_utf8,
			),
		}
	}
}

#[derive(Debug, Clone, Copy, PartialEq, Eq, PartialOrd, Ord, Hash)]
#[non_exhaustive]
pub enum Class {
	NotApplicable,
	First,
	Second,
	Tourist,
	Comfort,
	Premium,
	Business,
	All,
}

impl From<gen_1_3::TravelClassType> for Class {
	fn from(value: gen_1_3::TravelClassType) -> Self {
		match value {
			gen_1_3::TravelClassType::notApplicable => Self::NotApplicable,
			gen_1_3::TravelClassType::first => Self::First,
			gen_1_3::TravelClassType::second => Self::Second,
			gen_1_3::TravelClassType::tourist => Self::Tourist,
			gen_1_3::TravelClassType::comfort => Self::Comfort,
			gen_1_3::TravelClassType::premium => Self::Premium,
			gen_1_3::TravelClassType::business => Self::Business,
			gen_1_3::TravelClassType::all => Self::All,
		}
	}
}

#[derive(Debug, Clone, PartialEq, Eq, PartialOrd, Ord, Hash)]
pub struct CustomerCard {
	pub customer: Option<Traveler>,

	pub card_id: Option<String>,
}

impl From<gen_1_3::CustomerCardData> for CustomerCard {
	fn from(value: gen_1_3::CustomerCardData) -> Self {
		Self {
			customer: value.customer.map(Traveler::from),

			card_id: value.card_id_ia5.map(String::from),
		}
	}
}
