use std::array;

use jiff::{civil::DateTime, fmt::strtime::parse};

const COMPANY_CODE_LEN: usize = 4;
const KEY_LEN: usize = 20;
const EDITION_TIME_LEN: usize = 12;
const LANGUAGE_LEN: usize = 2;

const LEN: usize = COMPANY_CODE_LEN + KEY_LEN + EDITION_TIME_LEN + 1 + LANGUAGE_LEN + LANGUAGE_LEN;

#[derive(Debug, Clone, PartialEq, Eq, PartialOrd, Ord, Hash)]
pub struct Header {
	pub company_code: [u8; COMPANY_CODE_LEN],
	pub key: [u8; KEY_LEN],
	pub edition_time: DateTime,
	pub flags: Flags,
	pub edition_language: [u8; LANGUAGE_LEN],
	pub second_language: Option<[u8; LANGUAGE_LEN]>,
}

impl Header {
	pub fn parse(b: &[u8]) -> Result<Self, ParseError> {
		// https://www.era.europa.eu/system/files/2022-10/Recommendation%20on%20TAP%20TSI%20Revision%20-%20Technical%20Document%20-%20B12.pdf#page=39
		if b.len() != LEN {
			return Err(ParseError::Length(b.len()));
		}

		let company_code = array::from_fn(|i| b[i]);
		let b = &b[COMPANY_CODE_LEN..];

		let key = array::from_fn(|i| b[i]);
		let b = &b[KEY_LEN..];

		let edition_time = parse("%d%m%Y%H%M", &b[..EDITION_TIME_LEN])?.to_datetime()?;
		let b = &b[EDITION_TIME_LEN..];

		if !(b'0'..=b'7').contains(&b[0]) {
			return Err(ParseError::Flags(b[0]));
		}
		let flags = Flags(b[0] - b'0');
		let b = &b[1..];

		let edition_language = [b[0], b[1]];
		let b = &b[LANGUAGE_LEN..];

		let second_language = (b[0] != b' ' && b[1] != b' ').then_some([b[0], b[1]]);

		Ok(Self {
			company_code,
			key,
			edition_time,
			flags,
			edition_language,
			second_language,
		})
	}
}

#[derive(Debug, Clone, Copy, PartialEq, Eq, PartialOrd, Ord, Hash)]
pub struct Flags(u8);

impl Flags {
	#[allow(non_upper_case_globals)]
	pub const InternationalTicket: Self = Self(1);
	#[allow(non_upper_case_globals)]
	pub const EditedByAgent: Self = Self(2);
	#[allow(non_upper_case_globals)]
	pub const Specimen: Self = Self(4);
}

#[derive(Debug, Clone)]
pub enum ParseError {
	Length(usize),
	EditionTime(jiff::Error),
	Flags(u8),
}

impl From<jiff::Error> for ParseError {
	fn from(value: jiff::Error) -> Self {
		Self::EditionTime(value)
	}
}
