# Muv

Muv is a collection of libraries for working with geo, transit and mediawiki data.

It's currently being used by the in-development [Muv App](https://gitlab.com/LeLuxNet/Muv-App) and [osm2streets](https://a-b-street.github.io/osm2streets).

## Libraries

- [`muv-geo`](https://leluxnet.gitlab.io/Muv/muv_geo): Geographic types and functions used by all crates.
- [`muv-osm`](https://leluxnet.gitlab.io/Muv/muv_osm): Parsing of [OpenStreetMap tags](https://wiki.openstreetmap.org), with its flagship feature being the ability to extract lanes data.
- [`muv-osm-pbf`](https://leluxnet.gitlab.io/Muv/muv_osm_pbf): Read and write [OSM PBF](https://wiki.openstreetmap.org/wiki/PBF_Format) files in a fast and multithreaded fashion.
- [`muv-mvt`](https://leluxnet.gitlab.io/Muv/muv_mvt): Write geographic data to [Mapbox Vector Tiles](https://docs.mapbox.com/data/tilesets/guides/vector-tiles-standards).
- [`muv-pmtiles`](https://leluxnet.gitlab.io/Muv/muv_pmtiles): Create [PMTiles](https://docs.protomaps.com) files with tiles, using for example MVT data.
- [`muv-vector`](https://leluxnet.gitlab.io/Muv/muv_vector): A simple vector graphic representation with conversions from and to several formats.
- [`muv-transit`](https://leluxnet.gitlab.io/Muv/muv_transit): Read [GTFS](https://gtfs.org) and [NeTeX](https://transmodel-cen.eu/index.php/netex) transit schedules and use [HAFAS](https://www.hacon.de/en/portfolio/information-ticketing) or other transit APIs.
- [`muv-transit-ticket`](https://leluxnet.gitlab.io/Muv/muv_transit_ticket): Parse [UIC](https://www.era.europa.eu/system/files/2022-10/Recommendation%20on%20TAP%20TSI%20Revision%20-%20Technical%20Document%20-%20B12.pdf) and [IATA BCBP](https://www.iata.org/contentassets/1dccc9ed041b4f3bbdcf8ee8682e75c4/2021_03_02-bcbp-implementation-guide-version-7-.pdf) ticket data.
- [`muv-mediawiki`](https://leluxnet.gitlab.io/Muv/muv_mediawiki): Use the [MediaWiki](https://www.mediawiki.org/wiki/MediaWiki) [Action API](https://www.mediawiki.org/wiki/API:Main_page) or the [SparQL query service](https://www.wikidata.org/wiki/Wikidata:SPARQL_query_service).
- [`muv-cdmx-app`](https://leluxnet.gitlab.io/Muv/muv_cdmx_app): Interact with the api that powers the [CDMX App](https://play.google.com/store/apps/details?id=mx.gob.cdmx.adip.apps).

## CLI
`muv-cli` allows doing data processing using the other crates with outputs to human and machine readable formats.

## Licenses

Only `muv-transit-ticket` is licensed under GPL v3.0 or later, all other libraries are licensed under MPL 2.0.
The license for each crate is noted in their specific `Cargo.toml` file.
