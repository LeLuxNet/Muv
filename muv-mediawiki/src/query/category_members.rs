use crate::{Api, Deserialize, Url};

#[must_use]
#[derive(Debug)]
pub struct Request<'a, A> {
	#[allow(dead_code)]
	api: &'a Api<A>,
	url: Url,
}

impl<'a, A> Request<'a, A> {
	pub(crate) fn new(api: &'a Api<A>, mut url: Url, page: Page) -> Self {
		url.query_raw("list", "categorymembers");
		match page {
			Page::Title(title) => url.query("cmtitle", title),
			Page::Id(id) => url.query_raw("cmpageid", id),
		};
		Self { api, url }
	}

	pub fn r#type<I: IntoIterator<Item = Type>>(mut self, r#type: I) -> Self {
		self.url.list(
			"cmtype",
			r#type.into_iter().map(|t| match t {
				Type::File => "file",
				Type::Page => "page",
				Type::Subcat => "subcat",
			}),
		);
		self
	}
}

#[derive(Debug, Clone, Copy, PartialEq, Eq, PartialOrd, Ord, Hash)]
pub enum Page<'a> {
	Title(&'a str),
	Id(u64),
}

impl<'a> From<&'a str> for Page<'a> {
	fn from(value: &'a str) -> Self {
		Self::Title(value)
	}
}

impl From<u64> for Page<'_> {
	fn from(value: u64) -> Self {
		Self::Id(value)
	}
}

#[derive(Debug, Clone, Copy, PartialEq, Eq, PartialOrd, Ord, Hash)]
pub enum Type {
	File,
	Page,
	Subcat,
}

#[derive(Debug, Deserialize)]
pub struct Response {
	pub query: Query,
}

#[derive(Debug, Deserialize)]
pub struct Query {
	#[serde(rename = "categorymembers")]
	pub category_members: Vec<CategoryMember>,
}

#[derive(Debug, Deserialize)]
pub struct CategoryMember {
	#[serde(rename = "pageid")]
	pub page_id: u64,
	pub ns: u64,
	pub title: String,
}

#[cfg(feature = "blocking")]
impl Request<'_, crate::Blocking> {
	pub fn send(&self) -> Result<Response, crate::Error> {
		let res = self.api.get(&self.url).send()?.json()?;
		Ok(res)
	}
}

#[cfg(feature = "tokio")]
impl Request<'_, crate::Tokio> {
	pub async fn send(&self) -> Result<Response, crate::Error> {
		let res = self.api.get(&self.url).send().await?.json()?;
		Ok(res)
	}
}
