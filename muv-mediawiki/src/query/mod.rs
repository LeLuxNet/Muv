use scooter::url;

use crate::{Api, Url};

pub mod category_members;

#[must_use]
#[derive(Debug)]
pub struct Query<'a, A> {
	api: &'a Api<A>,
	url: Url,
}

impl<'a, A> Query<'a, A> {
	pub(crate) fn new(api: &'a Api<A>) -> Self {
		Self {
			api,
			url: Url(url!("{:?}query", api.base_url)),
		}
	}

	pub fn category_members(
		self,
		page: impl Into<category_members::Page<'a>>,
	) -> category_members::Request<'a, A> {
		category_members::Request::new(self.api, self.url, page.into())
	}
}
