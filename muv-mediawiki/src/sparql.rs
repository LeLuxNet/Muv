use std::collections::BTreeMap;

#[cfg(any(feature = "blocking", feature = "tokio"))]
use scooter::url;

use scooter::Agent;
use serde::Deserialize;

use crate::USER_AGENT;

#[derive(Debug)]
#[allow(dead_code)]
pub struct Api<A> {
	agent: Agent<A>,
	query_url: String,
	user_agent: String,
}

impl<A: Default> Api<A> {
	pub fn new<U: Into<String>, S: Into<String>>(user_agent: U, query_url: S) -> Self {
		let mut user_agent = user_agent.into();
		user_agent.push_str(USER_AGENT);
		Self {
			agent: Agent::default(),
			query_url: query_url.into(),
			user_agent,
		}
	}
}

#[derive(Debug, Deserialize)]
pub struct Response {
	pub head: Head,
	pub results: Results,
}

#[derive(Debug, Deserialize)]
pub struct Head {
	pub vars: Vec<String>,
}

#[derive(Debug, Deserialize)]
pub struct Results {
	pub bindings: Vec<BTreeMap<String, Value>>,
}

#[derive(Debug, Deserialize)]
pub struct Value {
	pub datatype: Option<String>,
	pub r#type: Type,
	pub value: String,
}

#[derive(Debug, Deserialize)]
#[serde(rename_all = "lowercase")]
pub enum Type {
	Literal,
	Uri,
}

#[cfg(feature = "blocking")]
impl Api<crate::Blocking> {
	pub fn select(&self, query: &str) -> Result<Response, crate::Error> {
		Ok(self
			.agent
			.post(&self.query_url)
			.header("User-Agent", &self.user_agent)
			.header(
				"Content-Type",
				"application/x-www-form-urlencoded; charset=utf-8",
			)
			.header("Accept", "application/sparql-results+json")
			.text(&url!("query={}", query))
			.send()?
			.json()?)
	}
}

#[cfg(feature = "tokio")]
impl Api<crate::Tokio> {
	pub async fn select(&self, query: &str) -> Result<Response, crate::Error> {
		Ok(self
			.agent
			.post(&self.query_url)
			.header("User-Agent", &self.user_agent)
			.header(
				"Content-Type",
				"application/x-www-form-urlencoded; charset=utf-8",
			)
			.header("Accept", "application/sparql-results+json")
			.text(&url!("query={}", query))
			.send()
			.await?
			.json()?)
	}
}
