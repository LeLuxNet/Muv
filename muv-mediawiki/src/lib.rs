use std::{
	error,
	fmt::{self, Debug, Display, Formatter, Write},
};

use scooter::{Agent, Request, url};
use serde::Deserialize;

#[cfg(feature = "blocking")]
pub use scooter::Blocking;
#[cfg(feature = "tokio")]
pub use scooter::Tokio;

pub mod query;
pub mod sparql;
pub mod wikibase;

pub(crate) const USER_AGENT: &str = " muv-mediawiki/0.1.0";

#[derive(Debug)]
#[allow(dead_code)]
pub struct Api<A> {
	agent: Agent<A>,
	base_url: String,
	user_agent: String,
}

impl<A: Default + Clone> Api<A> {
	#[must_use]
	pub fn new<U: Into<String>>(user_agent: U, base_url: &str) -> Self {
		let mut user_agent = user_agent.into();
		user_agent.push_str(USER_AGENT);
		Self {
			agent: Agent::default(),
			base_url: url!("{:?}?format=json&action=", base_url),
			user_agent,
		}
	}

	#[allow(dead_code)]
	pub(crate) fn get<'a>(&'a self, url: &'a Url) -> Request<'a, A> {
		self.agent
			.get(url.build())
			.header("User-Agent", &self.user_agent)
	}

	pub fn query(&self) -> query::Query<A> {
		query::Query::new(self)
	}

	pub const fn wikibase(&self) -> wikibase::Wikibase<A> {
		wikibase::Wikibase { api: self }
	}
}

#[derive(Debug)]
pub(crate) struct Url(String);

impl Url {
	pub fn new(base_url: &str, action: &str) -> Self {
		Self(url!("{:?}{:?}", base_url, action))
	}

	pub fn query(&mut self, key: &str, val: impl Display) {
		self.query_raw(key, scooter::encode(&val.to_string()));
	}

	pub fn query_raw(&mut self, key: &str, val: impl Display) {
		write!(self.0, "&{key}={val}").unwrap();
	}

	pub fn bool(&mut self, key: &str, val: bool) {
		self.query_raw(key, if val { "yes" } else { "no" });
	}

	pub fn list(&mut self, key: &str, vals: impl IntoIterator<Item = impl Display>) {
		self.query_raw(key, "");

		let mut vals = vals.into_iter();
		let Some(first) = vals.next() else {
			return;
		};
		write!(self.0, "{first}").unwrap();

		for val in vals {
			self.0.push('|');
			write!(self.0, "{val}").unwrap();
		}
	}

	#[allow(dead_code)]
	pub fn build(&self) -> &str {
		&self.0
	}
}

#[derive(Debug)]
pub enum Error {
	Request(scooter::Error),
}

impl From<scooter::Error> for Error {
	fn from(value: scooter::Error) -> Self {
		Self::Request(value)
	}
}

impl Display for Error {
	fn fmt(&self, f: &mut Formatter<'_>) -> fmt::Result {
		match self {
			Self::Request(_) => f.write_str("api request failed"),
		}
	}
}

impl error::Error for Error {
	fn source(&self) -> Option<&(dyn error::Error + 'static)> {
		match self {
			Self::Request(e) => Some(e),
		}
	}
}
