use std::fmt::Debug;

use serde::{Deserialize, Serialize};

use crate::wikibase::{Q, Time};

#[derive(Debug, Clone, Deserialize)]
pub struct Data {
	#[serde(rename = "datavalue")]
	pub value: DataValue,
	#[serde(rename = "datatype")]
	pub r#type: Option<DataType>,
}

#[derive(Debug, Clone, Copy, PartialEq, Eq, PartialOrd, Ord, Hash, Deserialize)]
#[serde(rename_all = "kebab-case")]
pub enum DataType {
	#[serde(rename = "wikibase-item")]
	Item,
	#[serde(rename = "commonsMedia")]
	CommonsMedia,
	GeoShape,
	ExternalId,
	Url,

	String,
	GlobeCoordinate,
	#[serde(rename = "monolingualtext")]
	MonolingualText,
	Time,
	Quantity,
}

#[derive(Debug, Clone, PartialEq, Deserialize)]
#[serde(tag = "type", content = "value", rename_all = "lowercase")]
pub enum DataValue {
	Boolean(()),
	Number(()),
	String(String),
	GlobeCoordinate(GlobeCoordinate),
	MonolingualText(MonolingualText),
	MultilingualText(()),
	Time(Time),
	Quantity(Quantity),

	#[serde(rename = "wikibase-entityid")]
	EntityId {
		id: Q,
	},
}

impl DataValue {
	#[must_use]
	pub fn string(self) -> Option<String> {
		match self {
			Self::String(s) => Some(s),
			_ => None,
		}
	}

	#[must_use]
	pub fn str(&self) -> Option<&str> {
		match self {
			Self::String(s) => Some(s),
			_ => None,
		}
	}

	#[must_use]
	pub const fn time(&self) -> Option<Time> {
		match self {
			Self::Time(v) => Some(*v),
			_ => None,
		}
	}
}

#[derive(Debug, Clone, PartialEq, Eq, Serialize, Deserialize)]
pub struct Quantity {
	pub amount: String,
	pub unit: String,
}

#[derive(Debug, Clone, Copy, PartialEq, Serialize, Deserialize)]
pub struct GlobeCoordinate {
	pub latitude: f64,
	pub longitude: f64,
	pub altitude: Option<f64>,
	pub precision: f64,
	// global: (),
}

#[derive(Debug, Clone, PartialEq, Eq, Serialize, Deserialize)]
pub struct MonolingualText {
	pub text: String,
	pub language: String,
}
