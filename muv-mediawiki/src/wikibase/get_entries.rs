use std::collections::BTreeMap;

use serde::Deserialize;

use crate::{
	Api, Url,
	wikibase::{EntityId, Label, P, Sitelink, Statement},
};

#[must_use]
pub struct Request<'a, A> {
	#[allow(dead_code)]
	api: &'a Api<A>,
	url: Url,
}

#[derive(Debug, Deserialize)]
pub struct Response {
	pub entities: BTreeMap<EntityId, Entity>,
}

#[derive(Debug, Clone, Deserialize)]
pub struct Entity {
	pub labels: Option<BTreeMap<String, Label>>,
	pub statements: Option<BTreeMap<P, Vec<Statement>>>,
	pub claims: Option<BTreeMap<P, Vec<Statement>>>,
	pub sitelinks: Option<BTreeMap<String, Sitelink>>,
}

#[derive(Clone, Copy)]
pub enum Prop {
	Info,
	Sitelinks,
	Aliases,
	Labels,
	Descriptions,
	Claims,
	Datatype,
}

impl<'a, A> Request<'a, A> {
	pub(crate) fn new(api: &'a Api<A>, i: impl Iterator<Item = EntityId>) -> Self {
		let mut url = Url::new(&api.base_url, "wbgetentities");
		url.list("ids", i);
		Self { api, url }
	}

	pub fn redirects(mut self, follow: bool) -> Self {
		self.url.bool("redirects", follow);
		self
	}

	pub fn props<I: IntoIterator<Item = Prop>>(mut self, props: I) -> Self {
		self.url.list(
			"props",
			props.into_iter().map(|p| match p {
				Prop::Info => "info",
				Prop::Sitelinks => "sitelinks",
				Prop::Aliases => "aliases",
				Prop::Labels => "labels",
				Prop::Descriptions => "descriptions",
				Prop::Claims => "claims",
				Prop::Datatype => "datatype",
			}),
		);
		self
	}
}

#[cfg(feature = "blocking")]
impl Request<'_, crate::Blocking> {
	pub fn send(&self) -> Result<Response, crate::Error> {
		Ok(self.api.agent.get(self.url.build()).send()?.json()?)
	}
}

#[cfg(feature = "tokio")]
impl Request<'_, crate::Tokio> {
	pub async fn send(&self) -> Result<Response, crate::Error> {
		Ok(self.api.agent.get(self.url.build()).send().await?.json()?)
	}
}
