use std::{
	error::Error,
	fmt::{self, Debug, Display, Formatter},
	marker::PhantomData,
	num::ParseIntError,
	str::FromStr,
};

use serde::{
	Deserialize, Deserializer, Serialize, Serializer,
	de::{self, Visitor},
};

#[derive(Debug, Clone, Copy, PartialEq, Eq, PartialOrd, Ord, Hash, Serialize, Deserialize)]
#[serde(untagged)]
pub enum EntityId {
	Q(Q),
	M(M),
}

impl Display for EntityId {
	fn fmt(&self, f: &mut Formatter<'_>) -> fmt::Result {
		match self {
			Self::Q(id) => Display::fmt(id, f),
			Self::M(id) => Display::fmt(id, f),
		}
	}
}

macro_rules! id {
	($letter:ident: Entity) => {
		id!($letter);

		impl From<$letter> for EntityId {
			fn from(value: $letter) -> Self {
				Self::$letter(value)
			}
		}
	};
	($letter:ident) => {
		#[derive(Clone, Copy, PartialEq, Eq, PartialOrd, Ord, Hash)]
		pub struct $letter(pub u64);

		impl FromStr for $letter {
			type Err = ParseIdError;
			fn from_str(s: &str) -> Result<Self, Self::Err> {
				let n_str = s
					.strip_prefix(stringify!($letter))
					.ok_or(ParseIdError::MissingPrefix)?;
				n_str.parse().map_err(ParseIdError::InvalidNumber).map(Self)
			}
		}

		impl Debug for $letter {
			fn fmt(&self, f: &mut Formatter<'_>) -> fmt::Result {
				write!(f, concat!(stringify!($letter), "({})"), self.0)
			}
		}

		impl Display for $letter {
			fn fmt(&self, f: &mut Formatter<'_>) -> fmt::Result {
				f.write_str(stringify!($letter))?;
				Display::fmt(&self.0, f)
			}
		}

		impl Serialize for $letter {
			fn serialize<S: Serializer>(&self, serializer: S) -> Result<S::Ok, S::Error> {
				self.to_string().serialize(serializer)
			}
		}

		impl<'de> Deserialize<'de> for $letter {
			fn deserialize<D: Deserializer<'de>>(deserializer: D) -> Result<Self, D::Error> {
				deserializer.deserialize_str(IdVisitor(PhantomData))
			}
		}
	};
}

id!(Q: Entity);
id!(M: Entity);
id!(P);

#[derive(Debug)]
pub enum ParseIdError {
	MissingPrefix,
	InvalidNumber(ParseIntError),
}

impl Display for ParseIdError {
	fn fmt(&self, f: &mut Formatter<'_>) -> fmt::Result {
		f.write_str(match self {
			Self::MissingPrefix => "missing id prefix",
			Self::InvalidNumber(_) => "failed to parse id number",
		})
	}
}

impl Error for ParseIdError {
	fn source(&self) -> Option<&(dyn Error + 'static)> {
		match self {
			Self::MissingPrefix => None,
			Self::InvalidNumber(e) => Some(e),
		}
	}
}

struct IdVisitor<T>(PhantomData<T>);

impl<T: FromStr<Err = ParseIdError>> Visitor<'_> for IdVisitor<T> {
	type Value = T;
	fn expecting(&self, formatter: &mut Formatter) -> fmt::Result {
		formatter.write_str("a wikibase id")
	}

	fn visit_str<E: de::Error>(self, v: &str) -> Result<Self::Value, E> {
		v.parse().map_err(E::custom)
	}
}
