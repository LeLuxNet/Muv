use std::{
	fmt::{self, Formatter},
	num::{NonZero, ParseIntError},
	str::FromStr,
};

use serde::{
	Deserialize, Deserializer, Serialize, Serializer,
	de::{self, Visitor},
};

#[derive(Debug, Clone, Copy, PartialEq, Eq)]
pub struct Time {
	pub time: PreciseTime,
	pub timezone: i16,
	// before
	// after
	// calendar_model
}

impl Serialize for Time {
	fn serialize<S: Serializer>(&self, _serializer: S) -> Result<S::Ok, S::Error> {
		todo!()
	}
}

impl<'de> Deserialize<'de> for Time {
	fn deserialize<D: Deserializer<'de>>(deserializer: D) -> Result<Self, D::Error> {
		#[derive(Deserialize)]
		struct Time {
			pub time: PreciseTime,
			pub timezone: i16,
			pub precision: u8,
		}

		let base = Time::deserialize(deserializer)?;

		Ok(Self {
			time: base.time.with_precision(base.precision).unwrap(),
			timezone: base.timezone,
		})
	}
}

#[derive(Debug, Clone, Copy, PartialEq, Eq, Hash)]
pub enum PreciseTime {
	BillionYears {
		year: i64,
	},
	HundredMillionYears {
		year: i64,
	},
	TenMillionYears {
		year: i64,
	},
	MillionYears {
		year: i64,
	},
	HundredThousandYears {
		year: i64,
	},
	TenThousandYears {
		year: i64,
	},
	Millennium {
		year: i64,
	},
	Century {
		year: i64,
	},
	Decade {
		year: i64,
	},
	Year {
		year: i64,
	},
	Month {
		year: i64,
		month: NonZero<u8>,
	},
	Day {
		year: i64,
		month: NonZero<u8>,
		day: NonZero<u8>,
	},
	Hour {
		year: i64,
		month: NonZero<u8>,
		day: NonZero<u8>,
		hour: u8,
	},
	Minute {
		year: i64,
		month: NonZero<u8>,
		day: NonZero<u8>,
		hour: u8,
		minute: u8,
	},
	Second {
		year: i64,
		month: NonZero<u8>,
		day: NonZero<u8>,
		hour: u8,
		minute: u8,
		second: u8,
	},
}

#[derive(Debug, Clone, PartialEq, Eq)]
pub enum ParseTimeError {
	Missing {
		component: TimeComponent,
	},
	Invalid {
		component: TimeComponent,
		parse: ParseIntError,
	},
	TrailingData,
	PrecisionMismatch,
	InvalidPrecision,
}

#[derive(Debug, Clone, Copy, PartialEq, Eq, PartialOrd, Ord, Hash)]
pub enum TimeComponent {
	Year,
	Month,
	Day,
	Hour,
	Minute,
	Second,
}

fn parse_component<T: FromStr<Err = ParseIntError>>(
	s: &str,
	end: char,
	component: TimeComponent,
) -> Result<(T, &str), ParseTimeError> {
	let (part_s, rest) = s
		.split_once(end)
		.ok_or(ParseTimeError::Missing { component })?;

	let part = part_s
		.parse()
		.map_err(|parse| ParseTimeError::Invalid { component, parse })?;

	Ok((part, rest))
}

impl FromStr for PreciseTime {
	type Err = ParseTimeError;
	fn from_str(s: &str) -> Result<Self, Self::Err> {
		let (year, s) = parse_component(s, '-', TimeComponent::Year)?;
		let (month, s) = parse_component(s, '-', TimeComponent::Month)?;
		let (day, s) = parse_component(s, 'T', TimeComponent::Day)?;

		let (hour, s) = parse_component(s, ':', TimeComponent::Hour)?;
		let (minute, s) = parse_component(s, ':', TimeComponent::Minute)?;
		let (second, s) = parse_component(s, 'Z', TimeComponent::Second)?;

		if !s.is_empty() {
			return Err(ParseTimeError::TrailingData);
		}

		let Some(month) = NonZero::new(month) else {
			return Ok(Self::Year { year });
		};

		let Some(day) = NonZero::new(day) else {
			return Ok(Self::Month { year, month });
		};

		Ok(Self::Second {
			year,
			month,
			day,
			hour,
			minute,
			second,
		})
	}
}

impl PreciseTime {
	fn with_precision(self, precision: u8) -> Option<Self> {
		let year = self.approx_year();
		Some(match precision {
			0 => Self::BillionYears { year },
			1 => Self::HundredMillionYears { year },
			2 => Self::TenMillionYears { year },
			3 => Self::MillionYears { year },
			4 => Self::HundredThousandYears { year },
			5 => Self::TenThousandYears { year },
			6 => Self::Millennium { year },
			7 => Self::Century { year },
			8 => Self::Decade { year },
			9 => Self::Year { year },
			10 => Self::Month {
				year,
				month: self.month()?,
			},
			11 => Self::Day {
				year,
				month: self.month()?,
				day: self.day()?,
			},
			12 => Self::Hour {
				year,
				month: self.month()?,
				day: self.day()?,
				hour: self.hour()?,
			},
			13 => Self::Minute {
				year,
				month: self.month()?,
				day: self.day()?,
				hour: self.hour()?,
				minute: self.minute()?,
			},
			14 => Self::Second {
				year,
				month: self.month()?,
				day: self.day()?,
				hour: self.hour()?,
				minute: self.minute()?,
				second: self.second()?,
			},
			15.. => return None,
		})
	}
}

impl Serialize for PreciseTime {
	fn serialize<S: Serializer>(&self, _serializer: S) -> Result<S::Ok, S::Error> {
		todo!()
	}
}

struct PreciseTimeVisitor;

impl Visitor<'_> for PreciseTimeVisitor {
	type Value = PreciseTime;

	fn expecting(&self, formatter: &mut Formatter) -> fmt::Result {
		formatter.write_str("a time string")
	}

	fn visit_str<E: de::Error>(self, v: &str) -> Result<Self::Value, E> {
		Ok(v.parse().unwrap())
	}
}

impl<'de> Deserialize<'de> for PreciseTime {
	fn deserialize<D: Deserializer<'de>>(deserializer: D) -> Result<Self, D::Error> {
		deserializer.deserialize_str(PreciseTimeVisitor)
	}
}

impl PreciseTime {
	#[must_use]
	pub const fn approx_year(self) -> i64 {
		let (Self::BillionYears { year }
		| Self::HundredMillionYears { year }
		| Self::TenMillionYears { year }
		| Self::MillionYears { year }
		| Self::HundredThousandYears { year }
		| Self::TenThousandYears { year }
		| Self::Millennium { year }
		| Self::Century { year }
		| Self::Decade { year }
		| Self::Year { year }
		| Self::Month { year, .. }
		| Self::Day { year, .. }
		| Self::Hour { year, .. }
		| Self::Minute { year, .. }
		| Self::Second { year, .. }) = self;
		year
	}

	#[must_use]
	pub const fn year(self) -> Option<i64> {
		match self {
			Self::BillionYears { .. }
			| Self::HundredMillionYears { .. }
			| Self::TenMillionYears { .. }
			| Self::MillionYears { .. }
			| Self::HundredThousandYears { .. }
			| Self::TenThousandYears { .. }
			| Self::Millennium { .. }
			| Self::Century { .. }
			| Self::Decade { .. } => None,
			Self::Year { year }
			| Self::Month { year, .. }
			| Self::Day { year, .. }
			| Self::Hour { year, .. }
			| Self::Minute { year, .. }
			| Self::Second { year, .. } => Some(year),
		}
	}

	#[must_use]
	pub const fn month(self) -> Option<NonZero<u8>> {
		match self {
			Self::BillionYears { .. }
			| Self::HundredMillionYears { .. }
			| Self::TenMillionYears { .. }
			| Self::MillionYears { .. }
			| Self::HundredThousandYears { .. }
			| Self::TenThousandYears { .. }
			| Self::Millennium { .. }
			| Self::Century { .. }
			| Self::Decade { .. }
			| Self::Year { .. } => None,
			Self::Month { month, .. }
			| Self::Day { month, .. }
			| Self::Hour { month, .. }
			| Self::Minute { month, .. }
			| Self::Second { month, .. } => Some(month),
		}
	}

	#[must_use]
	pub const fn day(self) -> Option<NonZero<u8>> {
		match self {
			Self::BillionYears { .. }
			| Self::HundredMillionYears { .. }
			| Self::TenMillionYears { .. }
			| Self::MillionYears { .. }
			| Self::HundredThousandYears { .. }
			| Self::TenThousandYears { .. }
			| Self::Millennium { .. }
			| Self::Century { .. }
			| Self::Decade { .. }
			| Self::Year { .. }
			| Self::Month { .. } => None,
			Self::Day { day, .. }
			| Self::Hour { day, .. }
			| Self::Minute { day, .. }
			| Self::Second { day, .. } => Some(day),
		}
	}

	#[must_use]
	pub const fn hour(self) -> Option<u8> {
		match self {
			Self::BillionYears { .. }
			| Self::HundredMillionYears { .. }
			| Self::TenMillionYears { .. }
			| Self::MillionYears { .. }
			| Self::HundredThousandYears { .. }
			| Self::TenThousandYears { .. }
			| Self::Millennium { .. }
			| Self::Century { .. }
			| Self::Decade { .. }
			| Self::Year { .. }
			| Self::Month { .. }
			| Self::Day { .. } => None,
			Self::Hour { hour, .. } | Self::Minute { hour, .. } | Self::Second { hour, .. } => {
				Some(hour)
			}
		}
	}

	#[must_use]
	pub const fn minute(self) -> Option<u8> {
		match self {
			Self::BillionYears { .. }
			| Self::HundredMillionYears { .. }
			| Self::TenMillionYears { .. }
			| Self::MillionYears { .. }
			| Self::HundredThousandYears { .. }
			| Self::TenThousandYears { .. }
			| Self::Millennium { .. }
			| Self::Century { .. }
			| Self::Decade { .. }
			| Self::Year { .. }
			| Self::Month { .. }
			| Self::Day { .. }
			| Self::Hour { .. } => None,
			Self::Minute { minute, .. } | Self::Second { minute, .. } => Some(minute),
		}
	}

	#[must_use]
	pub const fn second(self) -> Option<u8> {
		match self {
			Self::BillionYears { .. }
			| Self::HundredMillionYears { .. }
			| Self::TenMillionYears { .. }
			| Self::MillionYears { .. }
			| Self::HundredThousandYears { .. }
			| Self::TenThousandYears { .. }
			| Self::Millennium { .. }
			| Self::Century { .. }
			| Self::Decade { .. }
			| Self::Year { .. }
			| Self::Month { .. }
			| Self::Day { .. }
			| Self::Hour { .. }
			| Self::Minute { .. } => None,
			Self::Second { second, .. } => Some(second),
		}
	}
}
