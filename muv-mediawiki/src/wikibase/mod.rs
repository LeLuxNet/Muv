use serde::{Deserialize, Serialize};

use crate::Api;

mod id;
pub use id::*;

mod data;
pub use data::*;

mod time;
pub use time::*;

pub mod get_entries;

#[must_use]
#[derive(Debug)]
pub struct Wikibase<'a, A> {
	pub(crate) api: &'a Api<A>,
}

impl<A> Wikibase<'_, A> {
	pub fn get_entities<I: IntoIterator<Item = EntityId>>(
		&self,
		ids: I,
	) -> get_entries::Request<A> {
		get_entries::Request::new(self.api, ids.into_iter())
	}
}

#[derive(Debug, Clone, Serialize, Deserialize)]
pub struct Label {
	pub language: String,
	pub value: String,
}

#[derive(Debug, Clone, Serialize, Deserialize)]
pub struct Sitelink {
	pub site: String,
	pub title: String,
	// badges
}

#[derive(Debug, Clone, Deserialize)]
pub struct Statement {
	pub id: String,
	pub mainsnak: Snak,
	pub rank: Rank,
}

#[derive(Debug, Clone, Copy, PartialEq, Eq, PartialOrd, Ord, Hash, Serialize, Deserialize)]
#[serde(rename_all = "lowercase")]
pub enum Rank {
	Preferred,
	Normal,
	Deprecated,
}

#[derive(Debug, Clone, Deserialize)]
pub struct Snak {
	pub property: P,
	pub hash: String,
	#[serde(flatten)]
	pub data: Value<Data>,
}

#[derive(Debug, Clone, PartialEq, Eq, PartialOrd, Ord, Hash, Deserialize)]
#[serde(tag = "snaktype")]
pub enum Value<T> {
	#[serde(rename = "value")]
	Custom(T),
	#[serde(rename = "somevalue")]
	No,
	#[serde(rename = "novalue")]
	Unknown,
}

impl<T> Value<T> {
	#[must_use]
	pub const fn as_ref(&self) -> Value<&T> {
		match self {
			Self::Custom(v) => Value::Custom(v),
			Self::No => Value::No,
			Self::Unknown => Value::Unknown,
		}
	}

	pub fn custom(self) -> Option<T> {
		match self {
			Self::Custom(v) => Some(v),
			Self::No | Self::Unknown => None,
		}
	}

	pub fn unwrap(self) -> T {
		match self {
			Self::Custom(v) => v,
			Self::No => panic!("called `Value::unwrap()` on an `No` value"),
			Self::Unknown => panic!("called `Value::unwrap()` on an `Unknown` value"),
		}
	}
}
