mod hilbert;

mod header;
use std::ops::{Add, AddAssign, Sub, SubAssign};

pub use header::*;

mod writer;
pub use writer::*;

pub const MIME_TYPE: &str = "application/vnd.pmtiles";

#[derive(Debug, Clone, Copy, PartialEq, Eq, PartialOrd, Ord, Hash)]
pub struct TileId(pub u64);

impl TileId {
	#[must_use]
	pub const fn new(z: u8, x: u32, y: u32) -> Self {
		debug_assert!(z < 32, "zoom level too big");
		debug_assert!(x < (1 << z), "x out of bounds");
		debug_assert!(y < (1 << z), "y out of bounds");

		let base = Self::at_zoom(z);
		let local = hilbert::xy2h(x, y, z);
		Self(base + local)
	}

	const fn at_zoom(z: u8) -> u64 {
		const MASK: u64 = 0x5555_5555_5555_5555;
		let fill = (1 << (2 * z)) - 1;
		fill & MASK
	}

	#[must_use]
	pub const fn get(self) -> (u8, u32, u32) {
		#[allow(clippy::as_conversions, clippy::cast_possible_truncation)]
		let mut z = (65 - self.0.leading_zeros() as u8) / 2;
		let mut base = Self::at_zoom(z);
		if base > self.0 {
			z -= 1;
			base >>= 2;
		}

		let (x, y) = hilbert::h2xy(self.0 - base, z);
		(z, x, y)
	}

	#[must_use]
	pub const fn get_local(z: u8, v: u64) -> (u32, u32) {
		hilbert::h2xy(v, z)
	}

	/*#[must_use]
	pub const fn get2(self) -> (u8, u32, u32) {
		const MASK: u64 = 0xaaaa_aaaa_aaaa_aaaa;
		let mut offset = (self.0 | 1).leading_zeros() as u8 | 1;
		let mut base = MASK >> offset;
		if base > self.0 {
			offset += 1;
			base >>= 2;
		}
		let z = 32 - offset / 2;

		let (x, y) = hilbert::h2xy(self.0 - base, z);
		(z, x, y)
	}*/
}

impl Add<u64> for TileId {
	type Output = Self;
	fn add(self, rhs: u64) -> Self::Output {
		Self(self.0 + rhs)
	}
}
impl AddAssign<u64> for TileId {
	fn add_assign(&mut self, rhs: u64) {
		self.0 += rhs;
	}
}

impl Sub<u64> for TileId {
	type Output = Self;
	fn sub(self, rhs: u64) -> Self::Output {
		Self(self.0 - rhs)
	}
}
impl SubAssign<u64> for TileId {
	fn sub_assign(&mut self, rhs: u64) {
		self.0 -= rhs;
	}
}

#[cfg(test)]
mod tests {
	use crate::TileId;

	fn tile_id(z: u8, x: u32, y: u32, id: u64) {
		let new_id = TileId::new(z, x, y);
		assert_eq!(new_id, TileId(id));
		let (new_z, new_x, new_y) = new_id.get();
		assert_eq!(new_z, z);
		assert_eq!(new_x, x);
		assert_eq!(new_y, y);
	}

	#[test]
	fn roundtrip_tile_id() {
		tile_id(0, 0, 0, 0);
		tile_id(1, 1, 0, 4);
		tile_id(12, 3423, 1763, 19_078_479);

		for i in 0..=1_000_000 {
			let (z, x, y) = TileId(i).get();
			let id = TileId::new(z, x, y);
			assert_eq!(id, TileId(i));
		}
	}
}
