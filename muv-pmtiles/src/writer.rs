use std::io::{self, Write};

use protozer0::Varint;

use crate::{Compression, HEADER_SIZE, Header, Metadata, Position, TileId, TileType};

#[derive(Default)]
pub struct Writer {
	tiles: Vec<Tile>,
	new: Option<Tile>,

	offset: u64,
	addressed: u64,
}

struct Tile {
	data: Vec<u8>,
	run_length: u64,
}

const MAX_TILES_PER_DIR: usize = 2048;

fn encode_tiles_dir(
	compression: Compression,
	id_offset: u64,
	data_offset: u64,
	tiles: &[Tile],
) -> Vec<u8> {
	let mut res = Vec::new();

	// number of entries
	u64::try_from(tiles.len()).unwrap().append_varint(&mut res);

	// tile ids
	id_offset.append_varint(&mut res);
	for tile in &tiles[..tiles.len() - 1] {
		tile.run_length.append_varint(&mut res);
	}

	// run lengths
	for tile in tiles {
		tile.run_length.append_varint(&mut res);
	}

	// lengths
	for tile in tiles {
		u64::try_from(tile.data.len())
			.unwrap()
			.append_varint(&mut res);
	}

	// offsets
	(data_offset + 1).append_varint(&mut res);
	for _ in 1..tiles.len() {
		0u8.append_varint(&mut res);
	}

	compression.compress(res)
}

fn encode_dirs_dir(
	compression: Compression,
	id_offset: u64,
	leaf_dirs: &[(Vec<u8>, u64)],
) -> Vec<u8> {
	let mut res = Vec::new();

	// number of entries
	u64::try_from(leaf_dirs.len())
		.unwrap()
		.append_varint(&mut res);

	// tile ids
	id_offset.append_varint(&mut res);
	for (_, index_delta) in &leaf_dirs[..leaf_dirs.len() - 1] {
		index_delta.append_varint(&mut res);
	}

	// run lengths
	for _ in 0..leaf_dirs.len() {
		0.append_varint(&mut res);
	}

	// lengths
	for (data, _) in leaf_dirs {
		u64::try_from(data.len()).unwrap().append_varint(&mut res);
	}

	// offsets
	1u8.append_varint(&mut res);
	for _ in 1..leaf_dirs.len() {
		0u8.append_varint(&mut res);
	}

	compression.compress(res)
}

impl Writer {
	pub fn add(&mut self, id: TileId, data: Vec<u8>) {
		if self.tiles.is_empty() && self.new.is_none() {
			self.offset = id.0;
		}
		assert_eq!(id.0, self.offset + self.addressed);

		self.addressed += 1;

		if let Some(mut new) = self.new.take() {
			if new.data == data {
				new.run_length += 1;
				self.new = Some(new);
				return;
			}

			self.tiles.push(new);
		}

		self.new = Some(Tile {
			data,
			run_length: 1,
		});
	}

	pub fn finish<W: Write>(
		mut self,
		w: &mut W,
		internal_compression: Compression,
		tile_compression: Compression,
		metadata: &Metadata,
	) -> io::Result<()> {
		if let Some(new) = self.new.take() {
			self.tiles.push(new);
		}

		if tile_compression != Compression::None {
			for tile in &mut self.tiles {
				tile.data = tile_compression.always_compress(&tile.data);
			}
		}

		let (root_dir, leaf_dirs) = if self.tiles.len() <= MAX_TILES_PER_DIR {
			(
				encode_tiles_dir(internal_compression, self.offset, 0, &self.tiles),
				Vec::new(),
			)
		} else {
			let leaf_dirs: Vec<_> = self
				.tiles
				.chunks(MAX_TILES_PER_DIR)
				.scan((self.offset, 0), |(tile_id, offset), tiles| {
					let dir = encode_tiles_dir(internal_compression, *tile_id, *offset, tiles);

					let leaf_count = tiles.iter().map(|tile| tile.run_length).sum::<u64>();
					*tile_id += leaf_count;

					let length = tiles.iter().map(|tile| tile.data.len()).sum::<usize>();
					*offset += u64::try_from(length).unwrap();

					Some((dir, leaf_count))
				})
				.collect();

			(
				encode_dirs_dir(internal_compression, self.offset, &leaf_dirs),
				leaf_dirs,
			)
		};

		let root_dir_offset = HEADER_SIZE;
		let root_dir_length = u64::try_from(root_dir.len()).unwrap();

		let metadata = serde_json::to_vec(metadata).unwrap();
		let metadata = internal_compression.compress(metadata);
		let metadata_offset = root_dir_offset + root_dir_length;
		let metadata_length = u64::try_from(metadata.len()).unwrap();

		let leaf_dirs_offset = metadata_offset + metadata_length;
		let leaf_dirs_length = leaf_dirs.iter().map(|(data, _)| data.len()).sum::<usize>();
		let leaf_dirs_length = u64::try_from(leaf_dirs_length).unwrap();

		let tile_data_offset = leaf_dirs_offset + leaf_dirs_length;
		let tile_data_length = self.tiles.iter().map(|tile| tile.data.len()).sum::<usize>();
		let tile_data_length = u64::try_from(tile_data_length).unwrap();

		let entries = u64::try_from(self.tiles.len()).unwrap();

		let min_zoom = TileId(self.offset).get().0;
		let max_zoom = TileId(self.offset + self.addressed - 1).get().0;

		let header = Header {
			root_dir_offset,
			root_dir_length,
			metadata_offset,
			metadata_length,
			leaf_dirs_offset,
			leaf_dirs_length,
			tile_data_offset,
			tile_data_length,

			addressed_tiles: self.addressed,
			tile_entries: entries,
			tile_contents: entries,

			clustered: true,
			internal_compression,
			tile_compression,
			tile_type: TileType::Mvt,
			min_zoom,
			max_zoom,
			min_position: Position {
				lon: -180 * 10_000_000,
				lat: -85 * 10_000_000,
			},
			max_position: Position {
				lon: 180 * 10_000_000,
				lat: 85 * 10_000_000,
			},
			center_zoom: 0,
			center_position: Position { lon: 0, lat: 0 },
		};

		w.write_all(&header.encode())?;

		w.write_all(&root_dir)?;

		w.write_all(&metadata)?;

		for (dir, _) in leaf_dirs {
			w.write_all(&dir)?;
		}

		for tile in self.tiles {
			w.write_all(&tile.data)?;
		}

		Ok(())
	}
}
