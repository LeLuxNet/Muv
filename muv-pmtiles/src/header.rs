use std::collections::HashMap;

use condense::deflate;
use serde::Serialize;

pub const MAGIC_NUMBER: [u8; 7] = *b"PMTiles";
const MAGIC_NUMBER3: [u8; 8] = *b"PMTiles\x03";

pub(crate) struct Header {
	pub(crate) root_dir_offset: u64,
	pub(crate) root_dir_length: u64,
	pub(crate) metadata_offset: u64,
	pub(crate) metadata_length: u64,
	pub(crate) leaf_dirs_offset: u64,
	pub(crate) leaf_dirs_length: u64,
	pub(crate) tile_data_offset: u64,
	pub(crate) tile_data_length: u64,

	pub(crate) addressed_tiles: u64,
	pub(crate) tile_entries: u64,
	pub(crate) tile_contents: u64,

	pub(crate) clustered: bool,
	pub(crate) internal_compression: Compression,
	pub(crate) tile_compression: Compression,
	pub(crate) tile_type: TileType,
	pub(crate) min_zoom: u8,
	pub(crate) max_zoom: u8,
	pub(crate) min_position: Position,
	pub(crate) max_position: Position,
	pub(crate) center_zoom: u8,
	pub(crate) center_position: Position,
}

macro_rules! array {
    ([] [ $($inner:tt)* ]) => {{
        [ $($inner)* ]
    }};
	([ ..8 $name:ident, $($rest:tt)* ] [ $($inner:tt)* ]) => {
        array!(
            [ $($rest)* ]
            [ $($inner)* $name[0], $name[1], $name[2], $name[3], $name[4], $name[5], $name[6], $name[7], ]
        )
    };
    ([ ..4 $name:ident, $($rest:tt)* ] [ $($inner:tt)* ]) => {
        array!(
            [ $($rest)* ]
            [ $($inner)* $name[0], $name[1], $name[2], $name[3], ]
        )
    };
    ([ $val:expr, $($rest:tt)* ] [ $($inner:tt)* ]) => {
        array!([ $($rest)* ] [ $($inner)* $val, ])
    };
    ($($rest:tt)*) => {
        array!([ $($rest)* ] [])
    }
}

pub(crate) const HEADER_SIZE: u64 = 127;

impl Header {
	pub(crate) fn encode(&self) -> [u8; 127] {
		let root_dir_offset = self.root_dir_offset.to_le_bytes();
		let root_dir_length = self.root_dir_length.to_le_bytes();
		let metadata_offset = self.metadata_offset.to_le_bytes();
		let metadata_length = self.metadata_length.to_le_bytes();
		let leaf_dirs_offset = self.leaf_dirs_offset.to_le_bytes();
		let leaf_dirs_length = self.leaf_dirs_length.to_le_bytes();
		let tile_data_offset = self.tile_data_offset.to_le_bytes();
		let tile_data_length = self.tile_data_length.to_le_bytes();
		let addressed_tiles = self.addressed_tiles.to_le_bytes();
		let tile_entries = self.tile_entries.to_le_bytes();
		let tile_contents = self.tile_contents.to_le_bytes();
		let min_position: [u8; 8] = self.min_position.into();
		let max_position: [u8; 8] = self.max_position.into();
		let center_position: [u8; 8] = self.center_position.into();

		array![
			..8 MAGIC_NUMBER3,
			..8 root_dir_offset,
			..8 root_dir_length,
			..8 metadata_offset,
			..8 metadata_length,
			..8 leaf_dirs_offset,
			..8 leaf_dirs_length,
			..8 tile_data_offset,
			..8 tile_data_length,
			..8 addressed_tiles,
			..8 tile_entries,
			..8 tile_contents,
			self.clustered.into(),
			self.internal_compression.into(),
			self.tile_compression.into(),
			self.tile_type.into(),
			self.min_zoom,
			self.max_zoom,
			..8 min_position,
			..8 max_position,
			self.center_zoom,
			..8 center_position,
		]
	}
}

#[derive(Debug, Clone, Copy, PartialEq, Eq, PartialOrd, Ord, Hash)]
pub enum TileType {
	Unknown = 0,
	Mvt = 1,
	Png = 2,
	Jpeg = 3,
	Webp = 4,
	Avif = 5,
}

impl From<TileType> for u8 {
	fn from(value: TileType) -> Self {
		match value {
			TileType::Unknown => 0,
			TileType::Mvt => 1,
			TileType::Png => 2,
			TileType::Jpeg => 3,
			TileType::Webp => 4,
			TileType::Avif => 5,
		}
	}
}

#[derive(Debug, Clone, Copy, PartialEq, Eq, PartialOrd, Ord, Hash)]
pub enum Compression {
	Unknown = 0,
	None = 1,
	Gzip = 2,
	Brotli = 3,
	Zstd = 4,
}

impl Compression {
	pub(crate) fn always_compress(self, data: &[u8]) -> Vec<u8> {
		match self {
			Self::Unknown => panic!("can't compress with unknown compression"),
			Self::None => unreachable!(),
			Self::Gzip => deflate::compress_gzip(data, (), deflate::Level::SLOWEST).unwrap(),
			_ => panic!("compression {self:?} unsupported"),
		}
	}

	pub(crate) fn compress(self, data: Vec<u8>) -> Vec<u8> {
		if self == Self::None {
			data
		} else {
			self.always_compress(&data)
		}
	}
}

impl From<Compression> for u8 {
	fn from(value: Compression) -> Self {
		match value {
			Compression::Unknown => 0,
			Compression::None => 1,
			Compression::Gzip => 2,
			Compression::Brotli => 3,
			Compression::Zstd => 4,
		}
	}
}

#[derive(Debug, Clone, Copy, PartialEq, Eq, Hash)]
pub struct Position {
	pub lon: i32,
	pub lat: i32,
}

impl From<Position> for [u8; 8] {
	fn from(value: Position) -> Self {
		let lon = value.lon.to_le_bytes();
		let lat = value.lat.to_le_bytes();
		array![
			..4 lon,
			..4 lat,
		]
	}
}

#[cfg(feature = "muv-geo")]
impl From<muv_geo::Location> for Position {
	#[allow(clippy::cast_possible_truncation, clippy::as_conversions)]
	fn from(value: muv_geo::Location) -> Self {
		Self {
			lon: (value.lon * 1e7) as i32,
			lat: (value.lat * 1e7) as i32,
		}
	}
}

#[derive(Serialize)]
pub struct Metadata {
	pub vector_layers: Vec<Layer>,
}

#[derive(Serialize)]
pub struct Layer {
	pub id: String,
	pub fields: HashMap<String, String>,
	pub description: Option<String>,
	pub minzoom: Option<u8>,
	pub maxzoom: Option<u8>,
}
