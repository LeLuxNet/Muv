use std::borrow::Cow;

use protozer0::{PbfWriter, Svarint, VarintField};

pub const MIME_TYPE: &str = "application/vnd.mapbox-vector-tile";

pub struct LayerWriter<'a> {
	w: PbfWriter<'a>,
	keys: u32,
	vals: u32,
}

impl<'a> LayerWriter<'a> {
	pub fn new(b: &'a mut Vec<u8>, name: &str, extent: u32) -> Self {
		let mut w = PbfWriter::new(b);
		w.submessage(3);
		w.uint32(15, 2);
		w.string(1, name);
		w.uint32(5, extent);
		Self {
			w,
			keys: 0,
			vals: 0,
		}
	}

	pub fn key(&mut self, s: &str) -> u32 {
		self.w.string(3, s);
		let i = self.keys;
		self.keys += 1;
		i
	}

	pub fn value<'v, V: Into<Value<'v>>>(&mut self, v: V) -> u32 {
		self.w.submessage(4);
		match v.into() {
			Value::String(s) => self.w.string(1, &s),
			Value::Float(f) => self.w.float(2, f),
			Value::Double(d) => self.w.double(3, d),
			Value::Int(i) => self.w.int64(4, i),
			Value::Uint(i) => self.w.uint64(5, i),
			Value::Sint(i) => self.w.sint64(6, i),
			Value::Bool(b) => self.w.bool(7, b),
		}
		self.w.commit();

		let i = self.vals;
		self.vals += 1;
		i
	}

	pub fn feature<'b>(&'b mut self, id: Option<u64>) -> FeatureWriter<'a, 'b> {
		self.w.submessage(2);
		if let Some(id) = id {
			self.w.uint64(1, id);
		}
		let tags = self.w.packed_uint32(2);
		FeatureWriter { tags: Some(tags) }
	}
}

impl Drop for LayerWriter<'_> {
	fn drop(&mut self) {
		self.w.commit();
	}
}

pub enum Value<'a> {
	String(Cow<'a, str>),
	Float(f32),
	Double(f64),
	Int(i64),
	Uint(u64),
	Sint(i64),
	Bool(bool),
}

impl From<String> for Value<'_> {
	fn from(value: String) -> Self {
		Self::String(Cow::Owned(value))
	}
}

impl<'a> From<&'a String> for Value<'a> {
	fn from(value: &'a String) -> Self {
		Self::String(Cow::Borrowed(&**value))
	}
}

impl<'a> From<&'a str> for Value<'a> {
	fn from(value: &'a str) -> Self {
		Self::String(Cow::Borrowed(value))
	}
}

impl From<f32> for Value<'_> {
	fn from(value: f32) -> Self {
		Self::Float(value)
	}
}

impl From<f64> for Value<'_> {
	fn from(value: f64) -> Self {
		Self::Double(value)
	}
}

impl From<bool> for Value<'_> {
	fn from(value: bool) -> Self {
		Self::Bool(value)
	}
}

#[expect(dead_code)]
enum Command {
	MoveTo = 1,
	LineTo = 2,
	ClosePath = 7,
}

impl Command {
	const fn int(self, count: u32) -> u32 {
		let id = match self {
			Self::MoveTo => 1,
			Self::LineTo => 2,
			Self::ClosePath => 7,
		};
		id | (count << 3)
	}
}

pub struct FeatureWriter<'a, 'b> {
	tags: Option<VarintField<'a, 'b, u32>>,
}

impl<'a, 'b> FeatureWriter<'a, 'b> {
	pub fn property(&mut self, key: u32, val: u32) {
		let tags = self.tags.as_mut().unwrap();
		tags.add(key);
		tags.add(val);
	}

	fn geometry(mut self, r#type: i32) -> VarintField<'a, 'b, u32> {
		let mut tags = self.tags.take().unwrap();
		tags.commit();

		let w = tags.writer;
		w.r#enum(3, r#type);
		w.packed_uint32(4)
	}

	pub fn point(self, x: u32, y: u32) {
		self.points(1).add(x, y);
	}

	#[must_use]
	pub fn points(self, count: u32) -> PointFeatureWriter<'a, 'b> {
		let mut geometry = self.geometry(1);
		geometry.add(Command::MoveTo.int(count));
		PointFeatureWriter {
			geometry,
			x: 0,
			y: 0,
		}
	}
}

impl Drop for FeatureWriter<'_, '_> {
	fn drop(&mut self) {
		if let Some(mut tags) = self.tags.take() {
			tags.rollback();
			tags.writer.rollback();
		}
	}
}

pub struct PointFeatureWriter<'a, 'b> {
	geometry: VarintField<'a, 'b, u32>,
	x: u32,
	y: u32,
}

impl PointFeatureWriter<'_, '_> {
	pub fn add(&mut self, x: u32, y: u32) {
		#[allow(clippy::as_conversions, clippy::cast_possible_wrap)]
		let dx = x.wrapping_sub(self.x) as i32;
		#[allow(clippy::as_conversions, clippy::cast_possible_wrap)]
		let dy = y.wrapping_sub(self.y) as i32;
		self.geometry.add(dx.encode_zigzag());
		self.geometry.add(dy.encode_zigzag());
		self.x = x;
		self.y = y;
	}
}

impl Drop for PointFeatureWriter<'_, '_> {
	fn drop(&mut self) {
		self.geometry.commit();
		self.geometry.writer.commit();
	}
}
