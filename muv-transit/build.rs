use std::env;

const DB_CLIENT_ID_ENV: &str = "MUV_DB_CLIENT_ID";
const DB_CLIENT_SECRET_ENV: &str = "MUV_DB_CLIENT_SECRET";
const DB_API_SKIP: &str = "muv_db_api_skip";

fn main() {
	println!("cargo::rerun-if-env-changed={DB_CLIENT_ID_ENV}");
	println!("cargo::rerun-if-env-changed={DB_CLIENT_SECRET_ENV}");
	println!("cargo::rustc-check-cfg=cfg({DB_API_SKIP})");
	if env::var(DB_CLIENT_ID_ENV).is_err() || env::var(DB_CLIENT_SECRET_ENV).is_err() {
		println!("cargo::rustc-cfg={DB_API_SKIP}");
	}
}
