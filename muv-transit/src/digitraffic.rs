use chrono::{DateTime, Utc};
use scooter::Agent;
use serde::Deserialize;

#[cfg(any(feature = "blocking", feature = "tokio"))]
use serde::{Serialize, de::DeserializeOwned};

use crate::{Id, Mode};

#[cfg(any(feature = "blocking", feature = "tokio"))]
use crate::Result;

#[derive(Debug, Default)]
pub struct Graphql<A> {
	pub agent: Agent<A>,
}

#[cfg(any(feature = "blocking", feature = "tokio"))]
#[derive(Serialize)]
struct Request<'a> {
	query: &'a str,
}

#[cfg(any(feature = "blocking", feature = "tokio"))]
#[derive(Deserialize)]
struct Response<R> {
	data: R,
}

#[cfg(any(feature = "blocking", feature = "tokio"))]
const CURRENT_RUNNING_TRAINS: &str = "{
  currentlyRunningTrains {
    commuterLineid
    trainNumber
    trainType {
      name
      trainCategory {
        name
      }
    }
    trainLocations(orderBy: {timestamp: DESCENDING}, take: 1) {
      location
      speed
      accuracy
      timestamp
    }
  }
}";

#[cfg(feature = "blocking")]
impl Graphql<crate::Blocking> {
	pub fn request<R: DeserializeOwned>(&self, query: &'static str) -> Result<R> {
		let res: Response<R> = self
			.agent
			.post("https://rata.digitraffic.fi/api/v2/graphql/graphql")
			.header("Content-Type", "application/json")
			.json(&Request { query })?
			.send()?
			.json()?;
		Ok(res.data)
	}

	pub fn currently_running_trains(&self) -> Result<Vec<Train>> {
		let res: Query = self.request(CURRENT_RUNNING_TRAINS)?;
		Ok(res.currently_running_trains)
	}
}

#[cfg(feature = "tokio")]
impl Graphql<crate::Tokio> {
	pub async fn request<R: DeserializeOwned>(&self, query: &'static str) -> Result<R> {
		let res: Response<R> = self
			.agent
			.post("https://rata.digitraffic.fi/api/v2/graphql/graphql")
			.header("Content-Type", "application/json")
			.json(&Request { query })?
			.send()
			.await?
			.json()?;
		Ok(res.data)
	}

	pub async fn currently_running_trains(&self) -> Result<Vec<Train>> {
		let res: Query = self.request(CURRENT_RUNNING_TRAINS).await?;
		Ok(res.currently_running_trains)
	}
}

#[derive(Debug, Deserialize)]
#[serde(rename_all = "camelCase")]
pub struct Query {
	pub currently_running_trains: Vec<Train>,
}

#[derive(Debug, Deserialize)]
#[serde(rename_all = "camelCase")]
pub struct Train {
	pub commuter_lineid: String,
	pub train_number: u32,
	pub train_type: TrainType,
	pub train_locations: Option<Vec<TrainLocation>>,
}

#[allow(clippy::fallible_impl_from)]
impl From<Train> for crate::Vehicle {
	fn from(value: Train) -> Self {
		let code = if value.commuter_lineid.is_empty() {
			format!("{} {}", value.train_type.name, value.train_number)
		} else {
			value.commuter_lineid.clone()
		};
		let loc = value.train_locations.as_ref().and_then(|locs| locs.first());
		Self {
			id: Id::new(
				"digitraffic",
				format!("{}{}", value.train_type.name, value.train_number),
			),
			uic: None,
			code: Some(code),
			line_name: None,
			trip: None,
			mode: Some(Mode::from(&value.train_type.train_category)),

			location: loc.map(|loc| loc.location.into()),
			heading: None,
			speed: loc.map(|loc| loc.speed.into()),

			carriages: Vec::new(), // TODO
		}
	}
}

#[derive(Debug, Deserialize)]
#[serde(rename_all = "camelCase")]
pub struct TrainType {
	pub name: String,
	pub train_category: TrainCategory,
}

#[derive(Debug, Deserialize)]
pub struct TrainCategory {
	pub name: String,
}

impl From<&TrainCategory> for Mode {
	fn from(value: &TrainCategory) -> Self {
		match &*value.name {
			"Commuter" => Self::RegionalRail,
			"Long-distance" => Self::LongDistanceRail,
			"Cargo" => Self::FreightRail,
			"Shunting" | "Locomotive" => Self::SwitcherRail,
			"On-track machines" | "Test drive" => Self::SpecialRail,
			_ => todo!(),
		}
	}
}

#[derive(Debug, Deserialize)]
pub struct TrainLocation {
	pub speed: i32,
	pub accuracy: Option<i32>,
	pub location: Location,
	pub timestamp: DateTime<Utc>,
}

#[derive(Debug, Clone, Copy, PartialEq, Deserialize)]
pub struct Location(f64, f64);

impl From<Location> for crate::Location {
	fn from(value: Location) -> Self {
		Self::new(value.1, value.0)
	}
}

impl From<crate::Location> for Location {
	fn from(value: crate::Location) -> Self {
		Self(value.lon, value.lat)
	}
}
