use scooter::Agent;
use serde::Deserialize;

use crate::{Id, Location, Mode, Vehicle};

#[cfg(any(feature = "blocking", feature = "tokio"))]
use crate::Result;
#[cfg(any(feature = "blocking", feature = "tokio"))]
use serde::de::DeserializeOwned;

#[derive(Debug)]
pub struct Api<A> {
	pub agent: Agent<A>,
	pub key: String,
}

impl<A: Default> Api<A> {
	#[must_use]
	pub fn new(key: impl Into<String>) -> Self {
		Self {
			agent: Agent::default(),
			key: key.into(),
		}
	}
}

#[cfg(any(feature = "blocking", feature = "tokio"))]
#[derive(Deserialize)]
struct Response<T> {
	payload: T,
}

#[cfg(feature = "blocking")]
impl Api<crate::Blocking> {
	fn request<R: DeserializeOwned>(&self, url: &str) -> Result<R> {
		let res: Response<R> = self
			.agent
			.get(url)
			.header("Ocp-Apim-Subscription-Key", &self.key)
			.send()?
			.json()?;
		Ok(res.payload)
	}

	pub fn vehicles(&self) -> Result<Vec<Trein>> {
		let treinen: Treinen =
			self.request("https://gateway.apiportal.ns.nl/virtual-train-api/vehicle")?;
		Ok(treinen.treinen)
	}
}

#[cfg(feature = "tokio")]
impl Api<crate::Tokio> {
	async fn request<R: DeserializeOwned>(&self, url: &str) -> Result<R> {
		let res: Response<R> = self
			.agent
			.get(url)
			.header("Ocp-Apim-Subscription-Key", &self.key)
			.send()
			.await?
			.json()?;
		Ok(res.payload)
	}

	pub async fn vehicles(&self) -> Result<Vec<Trein>> {
		let treinen: Treinen = self
			.request("https://gateway.apiportal.ns.nl/virtual-train-api/vehicle")
			.await?;
		Ok(treinen.treinen)
	}
}

#[cfg(any(feature = "blocking", feature = "tokio"))]
#[derive(Deserialize)]
struct Treinen {
	treinen: Vec<Trein>,
}

#[derive(Debug, Deserialize)]
#[serde(rename_all = "camelCase")]
pub struct Trein {
	pub trein_nummer: u32,
	pub rit_id: String,
	pub lat: f64,
	pub lng: f64,
	pub snelheid: f64,
	pub richting: f64,
	pub horizontale_nauwkeurigheid: f64,
	pub r#type: String,
	pub bron: String,
}

impl From<Trein> for Vehicle {
	fn from(value: Trein) -> Self {
		Self {
			id: Id::new("ns", value.rit_id),
			uic: None,
			code: Some(format!("{} {}", value.r#type, value.trein_nummer)),
			line_name: None,
			trip: None,
			mode: Some(mode_from_type(&value.r#type)),

			location: Some(Location::new(value.lat, value.lng)),
			heading: Some(value.richting),
			speed: Some(value.snelheid),

			carriages: Vec::new(),
		}
	}
}

fn mode_from_type(r#type: &str) -> Mode {
	match r#type {
		"IC" => Mode::LongDistanceRail,
		"SPR" | "ARR" => Mode::RegionalRail,
		_ => todo!("{type}"),
	}
}
