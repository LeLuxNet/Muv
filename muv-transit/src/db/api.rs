use scooter::Agent;

#[cfg(any(feature = "blocking", feature = "tokio"))]
use crate::Result;
#[cfg(any(feature = "blocking", feature = "tokio"))]
use serde::de::DeserializeOwned;
#[cfg(any(feature = "blocking", feature = "tokio"))]
use std::fmt::Display;

#[cfg(feature = "tokio")]
use std::future::Future;

#[derive(Debug, Clone)]
pub struct Api<A> {
	pub agent: Agent<A>,
	pub client_id: String,
	pub client_secret: String,
}

impl<A: Default> Api<A> {
	#[must_use]
	pub fn new(client_id: impl Into<String>, client_secret: impl Into<String>) -> Self {
		Self {
			agent: Agent::default(),
			client_id: client_id.into(),
			client_secret: client_secret.into(),
		}
	}
}

#[cfg(feature = "blocking")]
impl Api<crate::Blocking> {
	pub fn get<R: DeserializeOwned>(&self, api: &str, path: impl Display) -> Result<R> {
		Ok(self
			.agent
			.get(format!(
				"https://apis.deutschebahn.com/db-api-marketplace/apis/{api}/{path}"
			))
			.debug(
				format!("db-api-{}", api.replace('/', "_")),
				path.to_string().replace('/', "_"),
			)
			.header("DB-Client-ID", &self.client_id)
			.header("DB-Api-Key", &self.client_secret)
			.send()?
			.json()?)
	}
}

#[cfg(feature = "tokio")]
impl Api<crate::Tokio> {
	pub fn get<'a, R: DeserializeOwned>(
		&'a self,
		api: &'static str,
		path: impl Display,
	) -> impl Future<Output = Result<R>> + 'a {
		let req = self
			.agent
			.get(format!(
				"https://apis.deutschebahn.com/db-api-marketplace/apis/{api}/{path}"
			))
			.debug(
				format!("db-api-{}", api.to_string().replace('/', "_")),
				path.to_string().replace('/', "_"),
			)
			.header("DB-Client-ID", &self.client_id)
			.header("DB-Api-Key", &self.client_secret)
			.send();

		async { Ok(req.await?.json()?) }
	}
}

#[cfg(test)]
#[must_use]
pub fn api<H: Default>() -> Api<H> {
	Api::new(
		std::env::var("MUV_DB_CLIENT_ID").unwrap(),
		std::env::var("MUV_DB_CLIENT_SECRET").unwrap(),
	)
}
