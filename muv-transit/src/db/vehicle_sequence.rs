use scooter::Agent;
use serde::Deserialize;

use crate::UicMarking;

#[cfg(any(feature = "blocking", feature = "tokio"))]
use crate::Result;
#[cfg(any(feature = "blocking", feature = "tokio"))]
use chrono::{DateTime, NaiveDate, Utc};
#[cfg(any(feature = "blocking", feature = "tokio"))]
use scooter::url;

#[derive(Debug, Default)]
pub struct WebApi<A> {
	pub agent: Agent<A>,
}

#[cfg(feature = "blocking")]
impl WebApi<crate::Blocking> {
	pub fn vehicle_sequence(
		&self,
		category: &str,
		number: &str,
		first_date: NaiveDate,
		time: DateTime<Utc>,
		eva_number: &str,
	) -> Result<VehicleSequence> {
		let url = url!(
			"https://www.bahn.de/web/api/reisebegleitung/wagenreihung/vehicle-sequence?category={}&number={}&date={:?}&time={:?}&evaNumber={}",
			category,
			number,
			first_date.format("%Y-%m-%d"),
			time.to_rfc3339(),
			eva_number
		);
		Ok(self.agent.get(&url).send()?.json()?)
	}
}

#[cfg(feature = "tokio")]
impl WebApi<crate::Tokio> {
	pub async fn vehicle_sequence(
		&self,
		category: &str,
		number: &str,
		first_date: NaiveDate,
		time: DateTime<Utc>,
		eva_number: &str,
	) -> Result<VehicleSequence> {
		let url = url!(
			"https://www.bahn.de/web/api/reisebegleitung/wagenreihung/vehicle-sequence?category={}&number={}&date={:?}&time={:?}&evaNumber={}",
			category,
			number,
			first_date.format("%Y-%m-%d"),
			time.to_rfc3339(),
			eva_number
		);
		Ok(self.agent.get(&url).send().await?.json()?)
	}
}

#[derive(Debug, PartialEq, Eq, Hash, Deserialize)]
#[serde(rename_all = "camelCase")]
pub struct VehicleSequence {
	#[serde(rename = "departureID")]
	departure_id: String,
	departure_platform: String,
	departure_platform_schedule: String,

	groups: Vec<Group>,

	#[serde(rename = "journeyID")]
	journey_id: String,
}

#[derive(Debug, PartialEq, Eq, Hash, Deserialize)]
pub struct Group {
	pub name: String,
	pub vehicles: Vec<Vehicle>,
}

#[derive(Debug, PartialEq, Eq, Hash, Deserialize)]
pub struct Vehicle {
	pub amenities: Vec<Amenity>,
	pub orientation: Orientation,
	pub status: Status,
	pub r#type: VehicleType,
	#[serde(rename = "vehicleID")]
	pub vehicle_id: UicMarking,
}

#[derive(Debug, PartialEq, Eq, PartialOrd, Ord, Hash, Deserialize)]
#[serde(rename_all = "UPPERCASE")]
pub enum Orientation {
	Forwards,
	Backwards,
}

#[derive(Debug, PartialEq, Eq, PartialOrd, Ord, Hash, Deserialize)]
#[serde(rename_all = "UPPERCASE")]
pub enum Status {
	Open,
}

#[derive(Debug, PartialEq, Eq, PartialOrd, Ord, Hash, Deserialize)]
#[serde(rename_all = "camelCase")]
pub struct VehicleType {
	pub category: VehicleCategory,
	pub construction_type: String,
	pub has_economy_class: bool,
	pub has_first_class: bool,
}

#[derive(Debug, PartialEq, Eq, PartialOrd, Ord, Hash, Deserialize)]
#[serde(rename_all = "SCREAMING_SNAKE_CASE")]
pub enum VehicleCategory {
	DoubledeckFirstEconomyClass,
	DoubledeckFirstClass,
	DoubledeckEconomyClass,
	DoubledeckControlcarFirstEconomoyClass,
	DoubledeckControlcarFirstClass,
	DoubledeckControlcarEconomyClass,
	DoubledeckCarcarrierPassengertrain,

	PassengercarriageFirstEconomyClass,
	PassengercarriageFirstClass,
	PassengercarriageEconomyClass,

	ControlcarFirstClass,
	ControlcarEconomyClass,
	ControlcarFirstEconomyClass,

	DoublecontrolcarEconomyClass,
	DoublecontrolcarFirstEconomyClass,

	Diningcar,

	HalfdiningcarFirstClass,
	HalfdiningcarEconomyClass,

	SleeperFirstClass,
	SleeperFirstEconomyClass,
	SleeperEconomyClass,

	CouchetteFirstClass,
	CouchetteEconomyClass,

	Baggagecar,
	Halfbaggagecar,
	Locomotive,
	Powercar,
	Undefined,
}

#[derive(Debug, PartialEq, Eq, PartialOrd, Ord, Hash, Deserialize)]
pub struct Amenity {
	pub amount: u16,
	pub r#type: AmenityType,
}

#[derive(Debug, PartialEq, Eq, PartialOrd, Ord, Hash, Deserialize)]
#[serde(rename_all = "SCREAMING_SNAKE_CASE")]
pub enum AmenityType {
	Bistro,
	AirCondition,
	BikeSpace,
	WheelchairSpace,
	ToiletWheelchair,
	ZoneMultiPurpose,
	BoardingAid,
	CabinInfant,
	ZoneQuiet,
	ZoneFamily,
	Info,
	SeatsSeverelyDisabled,
	SeatsBahnComfort,
	SeatsLufthansaExpressRail,
	Wifi,
}
