use serde::{Deserialize, Serialize};

use scooter::Agent;

#[cfg(any(feature = "blocking", feature = "tokio"))]
use crate::Result;

#[derive(Debug, Default)]
pub struct Api<A> {
	pub agent: Agent<A>,
}

#[cfg(feature = "blocking")]
impl Api<crate::Blocking> {
	pub fn status(&self) -> Result<Status> {
		Ok(self
			.agent
			.get("https://iceportal.de/api1/rs/status")
			.send()?
			.json()?)
	}
}

#[cfg(feature = "tokio")]
impl Api<crate::Tokio> {
	pub async fn status(&self) -> Result<Status> {
		Ok(self
			.agent
			.get("https://iceportal.de/api1/rs/status")
			.send()
			.await?
			.json()?)
	}
}

#[derive(Debug, Clone, PartialEq, Serialize, Deserialize)]
#[serde(rename_all = "camelCase")]
pub struct Status {
	pub connection: bool,
	pub service_level: ServiceLevel,
	pub gps_status: GpsStatus,
	pub internet: Internet,

	pub latitude: f64,
	pub longitude: f64,
	pub tile_y: i32,
	pub tile_x: i32,

	pub series: String,
	// pub server_time: DateTime<Utc>,
	pub speed: u16,
	pub train_type: String,
	pub tzn: String,
	pub wagon_class: WagonClass,
	pub connectivity: Connectivity,
	pub bap_installed: bool,
}

#[derive(Debug, Clone, Copy, PartialEq, Eq, PartialOrd, Ord, Hash, Serialize, Deserialize)]
#[serde(rename_all = "SCREAMING_SNAKE_CASE")]
pub enum ServiceLevel {
	AvailableService,
}

#[derive(Debug, Clone, Copy, PartialEq, Eq, PartialOrd, Ord, Hash, Serialize, Deserialize)]
#[serde(rename_all = "SCREAMING_SNAKE_CASE")]
pub enum GpsStatus {
	Valid,
}

#[derive(Debug, Clone, Copy, PartialEq, Eq, PartialOrd, Ord, Hash, Serialize, Deserialize)]
#[serde(rename_all = "SCREAMING_SNAKE_CASE")]
pub enum Internet {
	High,
}

#[derive(Debug, Clone, Copy, PartialEq, Eq, PartialOrd, Ord, Hash, Serialize, Deserialize)]
#[serde(rename_all = "SCREAMING_SNAKE_CASE")]
pub enum WagonClass {
	First,
	Second,
}

#[derive(Debug, Clone, PartialEq, Eq, Serialize, Deserialize)]
#[serde(rename_all = "camelCase")]
pub struct Connectivity {
	pub current_state: ConnectivityState,
	pub next_state: ConnectivityState,
	pub remaining_time_seconds: u32,
}

#[derive(Debug, Clone, Copy, PartialEq, Eq, PartialOrd, Ord, Hash, Serialize, Deserialize)]
#[serde(rename_all = "SCREAMING_SNAKE_CASE")]
pub enum ConnectivityState {
	High,
	Middle,
	Weak,
	Unstable,
	NoInternet,
	NoInfo,
}
