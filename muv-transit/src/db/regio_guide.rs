use chrono::{DateTime, FixedOffset};
use scooter::Agent;
use serde::{Deserialize, Serialize};

use crate::Location;

#[cfg(feature = "blocking")]
use crate::{Result, UicMarking};
#[cfg(feature = "blocking")]
use scooter::url;

#[derive(Debug, Default)]
pub struct Api<A> {
	pub agent: Agent<A>,
}

#[cfg(feature = "blocking")]
impl Api<crate::Blocking> {
	pub fn journey(&self, id: &str) -> Result<Journey> {
		let url = url!(
			"https://regio-guide.de/@prd/zupo-travel-information/api/public/ri/journey/{}",
			id
		);
		Ok(self.agent.get(&url).send()?.json()?)
	}

	pub fn journey_by_uic(&self, uic: UicMarking) -> Result<Journey> {
		let url = format!(
			"https://regio-guide.de/@prd/zupo-travel-information/api/public/ri/journey?uic={:02}{:02}{:04}{:03}{}",
			uic.model,
			uic.country_code,
			uic.r#type,
			uic.serial,
			uic.check_digit(),
		);
		Ok(self.agent.get(&url).send()?.json()?)
	}
}

#[derive(Debug, Clone, PartialEq, Serialize, Deserialize)]
#[serde(rename_all = "camelCase")]
pub struct Journey {
	pub name: String,
	pub no: u16,
	pub journey_id: String,
	pub tenant_id: String,
	pub administration_id: String,
	pub operator_name: String,
	pub operator_code: String,
	pub category: String,
	pub r#type: Type,
	pub date: DateTime<FixedOffset>,
	pub stops: Vec<Stop>,
	pub started: bool,
	pub finished: bool,
	pub hims: Vec<Him>,
	pub valid_until: DateTime<FixedOffset>,
	pub valid_from: DateTime<FixedOffset>,
	pub is_loyalty_case_eligible: bool,
}

#[derive(Debug, Clone, Copy, PartialEq, Eq, PartialOrd, Ord, Hash, Serialize, Deserialize)]
#[serde(rename_all = "SCREAMING_SNAKE_CASE")]
pub enum Type {
	RegionalTrain,
}

#[derive(Debug, Clone, PartialEq, Serialize, Deserialize)]
#[serde(rename_all = "camelCase")]
pub struct Stop {
	pub status: String,
	pub departure_id: Option<String>,
	pub arrival_id: Option<String>,
	pub station: Station,
	pub track: Track,
	pub departure_time: Option<Time>,
	pub arrival_time: Option<Time>,
	pub occupancy: Option<ClassOccupancy>,
}

#[derive(Debug, Clone, PartialEq, Serialize, Deserialize)]
#[serde(rename_all = "camelCase")]
pub struct Station {
	pub eva_no: String,
	pub name: String,
	pub position: Position,
}

#[derive(Debug, Clone, PartialEq, Eq, PartialOrd, Ord, Hash, Serialize, Deserialize)]
pub struct Track {
	pub target: String,
	pub prediction: String,
}

#[derive(Debug, Clone, Copy, PartialEq, Eq, Serialize, Deserialize)]
#[serde(rename_all = "camelCase")]
pub struct Time {
	pub target: DateTime<FixedOffset>,
	pub predicted: DateTime<FixedOffset>,
	pub time_type: TimeType,
}

#[derive(Debug, Clone, Copy, PartialEq, Eq, PartialOrd, Ord, Hash, Serialize, Deserialize)]
#[serde(rename_all = "UPPERCASE")]
pub enum TimeType {
	Real,
	Preview,
}

#[derive(Debug, Clone, Copy, PartialEq, Serialize, Deserialize)]
pub struct Position {
	pub latitude: f64,
	pub longitude: f64,
}

impl From<Position> for Location {
	fn from(value: Position) -> Self {
		Self {
			lat: value.latitude,
			lon: value.longitude,
		}
	}
}

#[derive(Debug, Clone, Copy, PartialEq, Eq, PartialOrd, Ord, Serialize, Deserialize, Hash)]
#[serde(rename_all = "camelCase")]
pub struct ClassOccupancy {
	economy_class: Occupancy,
}

#[derive(Debug, Clone, Copy, PartialEq, Eq, PartialOrd, Ord, Hash, Serialize, Deserialize)]
#[serde(rename_all = "UPPERCASE")]
pub enum Occupancy {
	Middle,
	Low,
}

#[derive(Debug, Clone, PartialEq, Eq, Serialize, Deserialize)]
#[serde(rename_all = "camelCase")]
pub struct Him {
	pub id: String,
	pub caption: String,
	pub short_text: String,
	pub caption_html: String,
	pub short_text_html: String,
}

#[cfg(feature = "blocking")]
#[cfg(test)]
mod tests {
	use std::str::FromStr;

	use crate::{UicMarking, db::regio_guide::Api};

	#[test]
	fn journey() {
		let api: Api<crate::Blocking> = Api::default();
		api.journey_by_uic(UicMarking::from_str("948014620439").unwrap())
			.unwrap();
	}
}
