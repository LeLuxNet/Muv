#[cfg(any(feature = "blocking", feature = "tokio"))]
use crate::{Result, db::WebApi};
#[cfg(any(feature = "blocking", feature = "tokio"))]
use chrono::{DateTime, TimeZone, Utc};
#[cfg(any(feature = "blocking", feature = "tokio"))]
use serde::{Deserialize, Serialize};

#[cfg(any(feature = "blocking", feature = "tokio"))]
#[derive(Serialize)]
#[serde(rename_all = "camelCase")]
struct TeilenReq<'a> {
	start_ort: &'a str,
	ziel_ort: &'a str,
	hinfahrt_datum: DateTime<Utc>,
	hinfahrt_recon: &'a str,
}

#[cfg(any(feature = "blocking", feature = "tokio"))]
#[derive(Deserialize)]
struct Teilen {
	vbid: String,
}

#[cfg(feature = "blocking")]
impl WebApi<crate::Blocking> {
	pub fn teilen<Tz: TimeZone>(
		&self,
		from: &str,
		to: &str,
		date_time: &DateTime<Tz>,
		recon_ctx: &str,
	) -> Result<String> {
		let teilen: Teilen = self
			.agent
			.post("https://www.bahn.de/web/api/angebote/verbindung/teilen")
			.json(&TeilenReq {
				start_ort: from,
				ziel_ort: to,
				hinfahrt_datum: date_time.to_utc(),
				hinfahrt_recon: recon_ctx,
			})?
			.send()?
			.json()?;
		Ok(teilen.vbid)
	}
}

#[cfg(feature = "tokio")]
impl WebApi<crate::Tokio> {
	pub fn teilen<Tz: TimeZone>(
		&self,
		from: &str,
		to: &str,
		date_time: &DateTime<Tz>,
		recon_ctx: &str,
	) -> impl std::future::Future<Output = Result<String>> {
		let req = self
			.agent
			.post("https://www.bahn.de/web/api/angebote/verbindung/teilen")
			.json(&TeilenReq {
				start_ort: from,
				ziel_ort: to,
				hinfahrt_datum: date_time.to_utc(),
				hinfahrt_recon: recon_ctx,
			})
			.map(|r| r.send());

		async {
			let teilen: Teilen = req?.await?.json()?;
			Ok(teilen.vbid)
		}
	}
}
