use serde::{Deserialize, Serialize};

#[cfg(any(feature = "blocking", feature = "tokio"))]
use crate::{Result, db::Api};

#[cfg(feature = "blocking")]
use crate::Blocking;

#[cfg(feature = "tokio")]
use crate::Tokio;

#[derive(Debug, Clone, PartialEq, PartialOrd, Serialize, Deserialize)]
#[serde(rename_all = "camelCase")]
pub struct Station {
	#[serde(rename = "DBinformation")]
	pub db_information: Option<Schedule>,
	pub aufgabentraeger: Aufgabentraeger,
	pub category: i8,
	pub eva_numbers: Vec<EvaNumber>,
	pub federal_state: String,

	pub has_bicycle_parking: bool,
	pub has_car_rental: bool,
	#[serde(rename = "hasDBLounge")]
	pub has_db_lounge: bool,
	pub has_local_public_transport: bool,
	pub has_locker_system: bool,
	pub has_lost_and_found: bool,
	pub has_mobility_service: String,
	pub has_parking: bool,
	pub has_public_facilities: bool,
	pub has_railway_mission: bool,
	pub has_stepless_access: Option<Partial>,
	pub has_taxi_rank: bool,
	pub has_travel_center: bool,
	pub has_travel_necessities: bool,
	#[serde(rename = "hasWiFi")]
	pub has_wifi: bool,

	pub ifopt: Option<String>,
	pub local_service_staff: Option<Schedule>,
	pub mailing_address: MailingAddress,
	pub mobility_service_staff: Option<MobilityServiceStaff>,
	pub name: String,
	pub number: u16,
	pub price_category: u8,
	pub regionalbereich: RegionalBereich,
	pub ril100_identifiers: Vec<Ril100Identifier>,
	pub station_management: StationManagement,
	pub szentrale: Szentrale,
	pub time_table_office: TimetableOffice,
	pub wireless_lan: Option<WirelessLan>,
}

#[derive(Debug, Clone, PartialEq, Eq, PartialOrd, Ord, Hash, Serialize, Deserialize)]
#[serde(rename_all = "camelCase")]
pub struct Szentrale {
	pub address: Option<MailingAddress>,
	pub internal_fax_number: Option<String>,
	pub internal_phone_number: Option<String>,
	pub mobile_phone_number: Option<String>,
	pub name: String,
	pub number: u16,
	pub public_fax_number: Option<String>,
	pub public_phone_number: String,
}

#[derive(Debug, Clone, PartialEq, PartialOrd, Serialize, Deserialize)]
#[serde(rename_all = "camelCase")]
pub struct EvaNumber {
	pub geographic_coordinates: Option<GeojsonPoint>,
	pub is_main: bool,
	pub number: u32,
}

#[derive(Debug, Clone, PartialEq, PartialOrd, Serialize, Deserialize)]
#[serde(rename_all = "camelCase")]
pub struct Ril100Identifier {
	pub geographic_coordinates: Option<GeojsonPoint>,
	pub is_main: bool,
	pub primary_location_code: Option<String>,
	pub ril_identifier: String,
	pub steam_permission: SteamPermission,
}

#[derive(Debug, Clone, Copy, PartialEq, Eq, PartialOrd, Ord, Hash, Serialize, Deserialize)]
#[serde(rename_all = "camelCase")]
pub enum SteamPermission {
	Restricted,
	Unrestricted,
	EntryBan,
}

#[derive(Debug, Clone, PartialEq, Eq, PartialOrd, Ord, Hash, Serialize, Deserialize)]
#[serde(rename_all = "camelCase")]
pub struct WirelessLan {
	pub amount: Option<u32>,
	// TODO
	// pub install_date: Option<()>,
	pub product: Option<String>,
}

#[derive(Debug, Clone, PartialEq, Eq, PartialOrd, Ord, Hash, Serialize, Deserialize)]
#[serde(rename_all = "camelCase")]
pub struct Aufgabentraeger {
	pub name: String,
	pub short_name: String,
}

#[derive(Debug, Clone, PartialEq, Eq, PartialOrd, Ord, Hash, Serialize, Deserialize)]
#[serde(rename_all = "camelCase")]
pub struct TimetableOffice {
	pub email: Option<String>,
	pub name: String,
}

#[derive(Debug, Clone, PartialEq, Eq, PartialOrd, Ord, Hash, Serialize, Deserialize)]
#[serde(rename_all = "camelCase")]
pub struct MailingAddress {
	pub city: String,
	pub house_number: Option<String>,
	pub street: String,
	pub zipcode: String,
}

#[derive(Debug, Clone, PartialEq, Eq, PartialOrd, Ord, Hash, Serialize, Deserialize)]
#[serde(rename_all = "camelCase")]
pub struct RegionalBereich {
	pub name: String,
	pub number: u8,
	pub short_name: String,
}

#[derive(Debug, Clone, PartialEq, Eq, PartialOrd, Ord, Hash, Serialize, Deserialize)]
#[serde(rename_all = "camelCase")]
pub struct StationManagement {
	pub name: String,
	pub number: u8,
}

#[derive(Debug, Clone, PartialEq, Eq, PartialOrd, Ord, Hash, Serialize, Deserialize)]
#[serde(rename_all = "camelCase")]
pub struct MobilityServiceStaff {
	pub availability: DoubleSchedule,
	pub staff_on_site: Option<bool>,
}

#[derive(Debug, Clone, PartialEq, Eq, PartialOrd, Ord, Hash, Serialize, Deserialize)]
pub struct DoubleSchedule {
	// TODO
}

#[derive(Debug, Clone, PartialEq, Eq, PartialOrd, Ord, Hash, Serialize, Deserialize)]
pub struct Schedule {
	// TODO
}

#[derive(Debug, Clone, Copy, PartialEq, Eq, PartialOrd, Ord, Hash, Serialize, Deserialize)]
#[serde(rename_all = "lowercase")]
#[allow(clippy::enum_variant_names)]
pub enum Partial {
	Yes,
	No,
	Partial,
}

#[derive(Debug, Clone, PartialEq, PartialOrd, Serialize, Deserialize)]
pub struct GeojsonPoint {
	pub coordinates: (f64, f64),
	pub r#type: String,
}

#[cfg(any(feature = "blocking", feature = "tokio"))]
#[derive(Deserialize)]
struct Query<T> {
	result: (T,),
}

#[cfg(any(feature = "blocking", feature = "tokio"))]
#[derive(Deserialize)]
struct Querys<T> {
	result: Vec<T>,
}

#[cfg(feature = "blocking")]
impl Api<Blocking> {
	pub fn stada_stations(&self) -> Result<Vec<Station>> {
		let res: Querys<_> = self.get("station-data/v2", "stations")?;
		Ok(res.result)
	}

	pub fn stada_station(&self, id: u16) -> Result<Station> {
		let res: Query<_> = self.get("station-data/v2", format_args!("stations/{id}"))?;
		Ok(res.result.0)
	}

	pub fn stada_szentralen(&self) -> Result<Vec<Szentrale>> {
		let res: Querys<_> = self.get("station-data/v2", "szentralen")?;
		Ok(res.result)
	}

	pub fn stada_szentrale(&self, id: u16) -> Result<Station> {
		let res: Query<_> = self.get("station-data/v2", format_args!("szentralen/{id}"))?;
		Ok(res.result.0)
	}
}

#[cfg(feature = "tokio")]
impl Api<Tokio> {
	pub async fn stada_stations(&self) -> Result<Vec<Station>> {
		let res: Querys<_> = self.get("station-data/v2", "stations").await?;
		Ok(res.result)
	}

	pub async fn stada_station(&self, id: u16) -> Result<Station> {
		let res: Query<_> = self
			.get("station-data/v2", format!("stations/{id}"))
			.await?;
		Ok(res.result.0)
	}

	pub async fn stada_szentralen(&self) -> Result<Vec<Szentrale>> {
		let res: Querys<_> = self.get("station-data/v2", "szentralen").await?;
		Ok(res.result)
	}

	pub async fn stada_szentrale(&self, id: u16) -> Result<Station> {
		let res: Query<_> = self
			.get("station-data/v2", format!("szentralen/{id}"))
			.await?;
		Ok(res.result.0)
	}
}

#[cfg(test)]
mod tests {
	use crate::db::api;

	#[test]
	#[cfg(feature = "blocking")]
	#[cfg_attr(muv_db_api_skip, ignore)]
	fn stations_blocking() {
		let f = api::<crate::Blocking>().stada_stations().unwrap();
		assert!(!f.is_empty());
	}

	#[test]
	#[cfg(feature = "blocking")]
	#[cfg_attr(muv_db_api_skip, ignore)]
	fn szentralen_blocking() {
		let f = api::<crate::Blocking>().stada_szentralen().unwrap();
		assert!(!f.is_empty());
	}
}
