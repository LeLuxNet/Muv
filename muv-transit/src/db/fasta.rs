use serde::{Deserialize, Serialize};

#[cfg(any(feature = "blocking", feature = "tokio"))]
use crate::{Result, db::Api};

#[cfg(feature = "blocking")]
use crate::Blocking;

#[cfg(feature = "tokio")]
use crate::Tokio;

#[derive(Debug, Clone, PartialEq, Serialize, Deserialize)]
#[serde(rename_all = "camelCase")]
pub struct Facility {
	pub description: Option<String>,
	pub equipmentnumber: u32,
	/// Longitude of this facility in WGS 84.
	/// This value might be invalid or a placeholder value.
	/// To filter these out use [`geocoord_x()`](Self::geocoord_x).
	pub geocoord_x: Option<f64>,
	/// Latitude of this facility in WGS 84.
	/// This value might be invalid or a placeholder value.
	/// To filter these out use [`geocoord_y()`](Self::geocoord_y).
	pub geocoord_y: Option<f64>,
	/// Operator of this facility. In most cases `"DB InfraGO"`.
	pub operatorname: Option<String>,
	pub state: FacilityState,
	pub state_explanation: Option<String>,
	/// The station number this facility is located in.
	pub stationnumber: u16,
	pub r#type: FacilityType,
}

impl Facility {
	/// Filter out placeholder or invalid longitudes returned by some facilities.
	///
	/// ```no_run
	/// use muv_transit::{Blocking, db::Api};
	///
	/// let api = Api::<Blocking>::new("<secret client id>", "<secret client secret>");
	///
	/// let f = api.fasta_facility(10_814_436).unwrap();
	/// assert_eq!(f.geocoord_x, Some(9.16417335));
	/// assert_eq!(f.geocoord_x(), Some(9.16417335));
	///
	/// let f = api.fasta_facility(5_010_010).unwrap();
	/// assert_eq!(f.geocoord_x, Some(12345.0));
	/// assert_eq!(f.geocoord_x(), None);
	/// ```
	#[must_use]
	pub fn geocoord_x(&self) -> Option<f64> {
		self.geocoord_x.filter(|c| (0.0..20.0).contains(c))
	}

	/// Filter out placeholder or invalid latitudes returned by some facilities.
	///
	/// ```no_run
	/// use muv_transit::{Blocking, db::Api};
	///
	/// let api = Api::<Blocking>::new("<secret client id>", "<secret client secret>");
	///
	/// let f = api.fasta_facility(10_814_436).unwrap();
	/// assert_eq!(f.geocoord_y, Some(48.77239005));
	/// assert_eq!(f.geocoord_y(), Some(48.77239005));
	///
	/// let f = api.fasta_facility(5_010_010).unwrap();
	/// assert_eq!(f.geocoord_y, Some(12345.0));
	/// assert_eq!(f.geocoord_y(), None);
	/// ```
	#[must_use]
	pub fn geocoord_y(&self) -> Option<f64> {
		self.geocoord_y.filter(|c| (40.0..60.0).contains(c))
	}
}

#[derive(Debug, Clone, Copy, PartialEq, Eq, PartialOrd, Ord, Hash, Serialize, Deserialize)]
#[serde(rename_all = "UPPERCASE")]
pub enum FacilityState {
	Active,
	Inactive,
	Unknown,
}

#[derive(Debug, Clone, Copy, PartialEq, Eq, PartialOrd, Ord, Hash, Serialize, Deserialize)]
#[serde(rename_all = "UPPERCASE")]
pub enum FacilityType {
	Escalator,
	Elevator,
}

#[derive(Debug, Clone, PartialEq, Serialize, Deserialize)]
pub struct Station {
	pub facilities: Vec<Facility>,
	pub name: String,
	pub stationnumber: u16,
}

#[cfg(feature = "blocking")]
impl Api<Blocking> {
	pub fn fasta_facilities(&self) -> Result<Vec<Facility>> {
		self.get("fasta/v2", "facilities")
	}

	pub fn fasta_facility(&self, id: u32) -> Result<Facility> {
		self.get("fasta/v2", format_args!("facilities/{id}"))
	}

	pub fn fasta_station(&self, id: u16) -> Result<Station> {
		self.get("fasta/v2", format_args!("stations/{id}"))
	}
}

#[cfg(feature = "tokio")]
impl Api<Tokio> {
	pub async fn fasta_facilities(&self) -> Result<Vec<Facility>> {
		self.get("fasta/v2", "facilities").await
	}

	pub async fn fasta_facility(&self, id: u32) -> Result<Facility> {
		self.get("fasta/v2", format!("facilities/{id}")).await
	}

	pub async fn fasta_station(&self, id: u16) -> Result<Station> {
		self.get("fasta/v2", format!("stations/{id}")).await
	}
}

#[cfg(test)]
mod tests {
	#[cfg(any(feature = "blocking", feature = "tokio"))]
	use crate::db::{api, fasta::FacilityType};

	#[test]
	#[cfg(feature = "blocking")]
	#[cfg_attr(muv_db_api_skip, ignore)]
	fn facilities_blocking() {
		let f = api::<crate::Blocking>().fasta_facilities().unwrap();
		assert!(!f.is_empty());
	}

	#[test]
	#[cfg(feature = "blocking")]
	#[cfg_attr(muv_db_api_skip, ignore)]
	fn facility_blokcing() {
		let f = api::<crate::Blocking>().fasta_facility(10_867_869).unwrap();
		assert_eq!(f.equipmentnumber, 10_867_869);
		assert_eq!(f.stationnumber, 1528);
		assert_eq!(f.description, Some("zu Gleis 3/4".into()));
		assert_eq!(f.r#type, FacilityType::Elevator);
		assert_eq!(f.geocoord_x, Some(10.332_061_7));
		assert_eq!(f.geocoord_y, Some(50.977_016_3));
	}

	#[test]
	#[cfg(feature = "blocking")]
	#[cfg_attr(muv_db_api_skip, ignore)]
	fn station_blocking() {
		let f = api::<crate::Blocking>().fasta_station(3329).unwrap();
		assert_eq!(f.stationnumber, 3329);
		assert_eq!(f.name, "Köln Messe/Deutz");
		assert!(!f.facilities.is_empty());
	}

	#[tokio::test]
	#[cfg(feature = "tokio")]
	#[cfg_attr(muv_db_api_skip, ignore)]
	async fn facilities_tokio() {
		let f = api::<crate::Tokio>().fasta_facilities().await.unwrap();
		assert!(!f.is_empty());
	}

	#[tokio::test]
	#[cfg(feature = "tokio")]
	#[cfg_attr(muv_db_api_skip, ignore)]
	async fn facility_tokio() {
		let f = api::<crate::Tokio>()
			.fasta_facility(10_867_869)
			.await
			.unwrap();
		assert_eq!(f.equipmentnumber, 10_867_869);
		assert_eq!(f.stationnumber, 1528);
		assert_eq!(f.description, Some("zu Gleis 3/4".into()));
		assert_eq!(f.r#type, FacilityType::Elevator);
		assert_eq!(f.geocoord_x, Some(10.332_061_7));
		assert_eq!(f.geocoord_y, Some(50.977_016_3));
	}

	#[tokio::test]
	#[cfg(feature = "tokio")]
	#[cfg_attr(muv_db_api_skip, ignore)]
	async fn station_tokio() {
		let f = api::<crate::Tokio>().fasta_station(3329).await.unwrap();
		assert_eq!(f.stationnumber, 3329);
		assert_eq!(f.name, "Köln Messe/Deutz");
		assert!(!f.facilities.is_empty());
	}
}
