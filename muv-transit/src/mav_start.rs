use std::{
	error,
	fmt::{self, Display, Formatter},
	iter::Map,
	time::Duration,
	vec::IntoIter,
};

use scooter::Agent;
use serde::Deserialize;

#[cfg(feature = "tokio")]
use crate::Result;
#[cfg(feature = "tokio")]
use scooter::Response;
#[cfg(feature = "tokio")]
use std::io::Cursor;

use crate::{Id, Location, Mode, UicMarking, Vehicle};

#[cfg(feature = "tokio")]
use futures_util::future::join;

#[derive(Debug, Default)]
pub struct Emig<A> {
	pub agent: Agent<A>,
	pub session_id: Option<String>,
	pub sql_id: Option<String>,
}

#[cfg(feature = "tokio")]
impl Emig<crate::Tokio> {
	async fn get_session(&mut self) -> Result<(&Agent<crate::Tokio>, &str, &str)> {
		if self.session_id.is_none() {
			let body = self
				.agent
				.get("https://iemig.mav-trakcio.hu/emig7/emig.aspx?v=7.1")
				.send()
				.await?
				.text()?;
			let (_, body) = body.split_once("gSessionId=").unwrap();
			let (session_id, _) = body.split_once(';').unwrap();

			let body_none_body = format!("s={session_id}&u=public&t=crb&key=BODY-NONE-7.1");
			let body_none = self
				.agent
				.post("https://iemig.mav-trakcio.hu/emig7/emig.aspx")
				.header("Content-Type", "application/x-www-form-urlencoded")
				.text(&body_none_body)
				.send();
			let map_public_body = format!("s={session_id}&u=public&t=crb&key=MAP_PUBLIC");
			let map_public = self
				.agent
				.post("https://iemig.mav-trakcio.hu/emig7/emig.aspx")
				.header("Content-Type", "application/x-www-form-urlencoded")
				.text(&map_public_body)
				.send();

			let (body_none, map_public) = join(body_none, map_public).await;
			body_none?;
			map_public?;

			self.session_id = Some(session_id.to_owned());
		}
		let session_id = self.session_id.as_ref().unwrap();

		if self.sql_id.is_none() {
			let body = self
				.agent
				.post("https://iemig.mav-trakcio.hu/emig7/emig.aspx")
				.header("Content-Type", "application/x-www-form-urlencoded")
				.text(&format!(
					"u=public&s={session_id}&t=publicsandr&q=Q5&lt=SqlCreate"
				))
				.send()
				.await?
				.bytes()?;
			let res: StatusResponse = quick_xml::de::from_reader(Cursor::new(body))?;

			Self::mozdonyok_res(&self.agent, session_id, &res.status.sqlid).await?;

			self.sql_id = Some(res.status.sqlid);
		};
		let sql_id = self.sql_id.as_ref().unwrap();

		Ok((&self.agent, session_id, sql_id))
	}

	pub async fn mozdonyok(&mut self) -> Result<Mozdonyok> {
		let (agent, session_id, sql_id) = self.get_session().await?;
		let body = Self::mozdonyok_res(agent, session_id, sql_id)
			.await?
			.bytes()?;
		let res: MozdonyokResponse = quick_xml::de::from_reader(Cursor::new(body))?;
		Ok(res.mozdonyok)
	}

	async fn mozdonyok_res(
		agent: &Agent<crate::Tokio>,
		session_id: &str,
		sql_id: &str,
	) -> Result<Response> {
		let body = format!("u=public&s={session_id}&t=publicrspec&q={sql_id}&f=publicmlist");
		Ok(agent
			.post("https://iemig.mav-trakcio.hu/emig7/emig.aspx")
			.header("Content-Type", "application/x-www-form-urlencoded")
			.text(&body)
			.send()
			.await?)
	}
}

#[derive(Deserialize)]
pub struct StatusResponse {
	pub status: Status,
}

#[derive(Deserialize)]
pub struct Status {
	pub sqlid: String,
}

#[derive(Deserialize)]
pub struct MozdonyokResponse {
	pub mozdonyok: Mozdonyok,
}

#[derive(Debug, Clone, PartialEq, Eq, Deserialize)]
pub struct Mozdonyok {
	#[serde(rename = "Mozdony", default)]
	pub mozdony: Vec<Mozdony>,
}

pub type IntoVehicles = Map<IntoIter<Mozdony>, fn(Mozdony) -> Vehicle>;

impl Mozdonyok {
	pub const UPDATE_CYCLE: Duration = Duration::from_secs(60);

	pub fn vehicles(self) -> IntoVehicles {
		self.mozdony.into_iter().map(Into::into)
	}
}

#[derive(Debug, Clone, PartialEq, Eq, Deserialize)]
pub struct Mozdony {
	#[serde(rename = "@id")]
	pub id: String,
	#[serde(rename = "@lat")]
	pub lat: i32,
	#[serde(rename = "@lng")]
	pub lng: i32,
	#[serde(rename = "@icon")]
	pub icon: String,
	#[serde(rename = "@title")]
	pub title: String,
	#[serde(rename = "@tipus")]
	pub tipus: Tipus,
	#[serde(rename = "@vonatszam")]
	pub vonatszam: String,
	#[serde(rename = "@uic")]
	pub uic: String,
	#[serde(rename = "$text")]
	pub label: String,
}

#[derive(Debug, Clone, Copy, PartialEq, Eq, PartialOrd, Ord, Hash, Deserialize)]
pub enum Tipus {
	#[serde(rename = "S")]
	Szemely,
	M,
	#[serde(rename = "T")]
	Teher,
}

impl From<Tipus> for Option<Mode> {
	fn from(value: Tipus) -> Self {
		match value {
			Tipus::Szemely => Some(Mode::LongDistanceRail),
			Tipus::M => None,
			Tipus::Teher => Some(Mode::FreightRail),
		}
	}
}

const fn mode_from_uic(model: u8) -> Mode {
	#[allow(clippy::manual_range_patterns)]
	match model {
		93 | 94 | 95 => Mode::LongDistanceRail,
		97 | 98 => Mode::SwitcherRail,
		99 => Mode::SpecialRail,
		_ => Mode::Rail,
	}
}

#[allow(clippy::fallible_impl_from)]
impl From<Mozdony> for Vehicle {
	fn from(value: Mozdony) -> Self {
		let has_title = value.title != ".";
		let uic: UicMarking = value.uic.parse().unwrap();
		let mode = Option::from(value.tipus).unwrap_or_else(|| mode_from_uic(uic.model));
		let location = Location::new(f64::from(value.lat) / 1e6, f64::from(value.lng) / 1e6);
		let heading = value.icon.rsplit('-').next().unwrap().parse().ok();
		Self {
			id: Id::new("mav_start", value.id),
			uic: Some(uic),
			code: has_title.then_some(value.title),
			line_name: None,
			trip: None,
			mode: Some(mode),

			location: Some(location),
			heading,
			speed: None,

			carriages: Vec::new(),
		}
	}
}

#[derive(Debug)]
#[non_exhaustive]
pub enum Error {
	QuickXml(quick_xml::DeError),
	#[cfg(feature = "reqwest")]
	Reqwest(reqwest::Error),
}

impl From<quick_xml::DeError> for Error {
	fn from(value: quick_xml::DeError) -> Self {
		Self::QuickXml(value)
	}
}

#[cfg(feature = "reqwest")]
impl From<reqwest::Error> for Error {
	fn from(value: reqwest::Error) -> Self {
		Self::Reqwest(value)
	}
}

impl Display for Error {
	fn fmt(&self, f: &mut Formatter<'_>) -> fmt::Result {
		match self {
			Self::QuickXml(e) => write!(f, "failed to parse XML: {e}"),
			#[cfg(feature = "reqwest")]
			Self::Reqwest(e) => write!(f, "{e}"),
		}
	}
}

impl error::Error for Error {}

#[cfg(test)]
mod tests {
	#[cfg(feature = "tokio")]
	use crate::mav_start::Emig;

	#[tokio::test]
	#[cfg(feature = "tokio")]
	async fn vehicles_tokio() {
		let res = Emig::<crate::Tokio>::default().mozdonyok().await.unwrap();
		assert!(!res.mozdony.is_empty());
	}
}
