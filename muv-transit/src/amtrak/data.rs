use chrono::NaiveDateTime;
use chrono_tz::{Tz, US};
use muv_geo::Location;
use serde::{
	Deserialize, Deserializer,
	de::{self, MapAccess, Visitor},
};

use std::{
	collections::BTreeMap,
	fmt::{self, Formatter},
	iter::Map,
	vec::IntoIter,
};

use crate::{Id, Mode, Vehicle};

#[derive(Debug, Deserialize)]
pub struct TrainsData {
	pub features: Vec<TrainsFeature>,
}

#[derive(Debug, Deserialize)]
pub struct TrainStations {
	pub features: Vec<StationsFeature>,
}

pub type IntoVehicles = Map<IntoIter<TrainsFeature>, fn(TrainsFeature) -> Vehicle>;

impl TrainsData {
	pub fn vehicles(self) -> IntoVehicles {
		self.features.into_iter().map(Into::into)
	}
}

#[derive(Debug, Clone, PartialEq, Deserialize)]
pub struct TrainsFeature {
	pub geometry: Geometry,
	pub properties: TrainsProperties,
}

#[derive(Debug, Clone, PartialEq, Deserialize)]
pub struct StationsFeature {
	pub geometry: Geometry,
	pub properties: StationsProperties,
}

impl From<TrainsFeature> for Vehicle {
	fn from(value: TrainsFeature) -> Self {
		let props = value.properties;
		Self {
			id: Id::new("amtrak", props.gx_id),
			uic: None,
			code: Some(props.train_num),
			line_name: props.route_name,
			trip: None,
			mode: Some(Mode::LongDistanceRail),

			location: Some(value.geometry.into()),
			heading: props.heading.map(|h| u16::from(h).into()),
			speed: props.velocity.map(|v| v * 1.609_344),

			carriages: Vec::new(),
		}
	}
}

#[derive(Debug, Clone, Copy, PartialEq, Deserialize)]
pub struct Geometry {
	pub coordinates: (f64, f64),
}

impl From<Geometry> for Location {
	fn from(value: Geometry) -> Self {
		Self::new(value.coordinates.1, value.coordinates.0)
	}
}

#[derive(Debug, Clone, PartialEq, Deserialize)]
#[serde(rename_all = "PascalCase")]
pub struct TrainsProperties {
	#[serde(rename = "gx_id")]
	pub gx_id: String,
	pub status_msg: Option<String>,
	pub heading: Option<Heading>,
	#[serde(rename = "LastValTS", with = "datetime_am")]
	pub last_val_ts: NaiveDateTime,
	pub event_code: Option<String>,
	pub dest_code: String,
	pub orig_code: String,
	pub route_name: Option<String>,
	#[serde(rename = "OriginTZ")]
	pub origin_tz: Timezone,
	#[serde(with = "datetime_am")]
	pub orig_sch_dep: NaiveDateTime,

	#[serde(rename = "CMSID")]
	pub cmsid: Option<String>,
	#[serde(rename = "ID")]
	pub id: u32,
	pub train_num: String,
	#[serde(with = "opt_from_str")]
	pub velocity: Option<f64>,

	#[serde(flatten, deserialize_with = "deserialize_stations")]
	pub stations: BTreeMap<u8, Station>,
}

#[derive(Debug, Clone, PartialEq, Eq, Deserialize)]
pub struct Station {
	pub code: String,
	pub tz: Timezone,
	pub bus: bool,

	#[serde(rename = "scharr", default, with = "opt_datetime")]
	pub sch_arr: Option<NaiveDateTime>,
	#[serde(rename = "schdep", default, with = "opt_datetime")]
	pub sch_dep: Option<NaiveDateTime>,
	#[serde(rename = "schcmnt")]
	pub sch_cmnt: String,

	#[serde(rename = "autoarr")]
	pub auto_arr: bool,
	#[serde(rename = "autodep")]
	pub auto_dep: bool,

	#[serde(rename = "estarr", default, with = "opt_datetime")]
	pub est_arr: Option<NaiveDateTime>,
	#[serde(rename = "estdep", default, with = "opt_datetime")]
	pub est_dep: Option<NaiveDateTime>,
	#[serde(rename = "estarrcmnt")]
	pub est_arr_cmnt: Option<String>,
	#[serde(rename = "estdepcmnt")]
	pub est_dep_cmnt: Option<String>,

	#[serde(rename = "postarr", default, with = "opt_datetime")]
	pub post_arr: Option<NaiveDateTime>,
	#[serde(rename = "postdep", default, with = "opt_datetime")]
	pub post_dep: Option<NaiveDateTime>,
	#[serde(rename = "postcmnt")]
	pub post_cmnt: Option<String>,
}

impl From<Station> for crate::Stop {
	fn from(value: Station) -> Self {
		Self {
			name: None,
			code: Some(value.code),
			r#ref: None,
			ifopt: None,
			uic: None,
			location: None,
			parent: None,
		}
	}
}

struct StationsVisitor;

impl<'de> Visitor<'de> for StationsVisitor {
	type Value = BTreeMap<u8, Station>;

	fn expecting(&self, formatter: &mut Formatter) -> fmt::Result {
		formatter.write_str("map keys starting with 'Station'")
	}

	fn visit_map<A: MapAccess<'de>>(self, mut map: A) -> Result<Self::Value, A::Error> {
		let mut res = BTreeMap::new();
		while let Some(key) = map.next_key::<String>()? {
			let Some(key) = key.strip_prefix("Station") else {
				continue;
			};
			let id: u8 = key.parse().map_err(de::Error::custom)?;

			let Some(val) = map.next_value::<Option<String>>()? else {
				continue;
			};
			let station: Station = serde_json::from_str(&val).map_err(de::Error::custom)?;

			res.insert(id, station);
		}
		Ok(res)
	}
}

fn deserialize_stations<'de, D: Deserializer<'de>>(
	deserializer: D,
) -> Result<BTreeMap<u8, Station>, D::Error> {
	deserializer.deserialize_map(StationsVisitor)
}

#[derive(Debug, Clone, PartialEq, Deserialize)]
#[serde(rename_all = "PascalCase")]
pub struct StationsProperties {
	#[serde(rename = "OBJECTID")]
	pub objectid: u16,

	#[serde(rename = "lon")]
	pub lon: f64,
	#[serde(rename = "lat")]
	pub lat: f64,

	pub is_train_st: String,
	#[serde(with = "from_str")]
	pub map_zm_lvl: u8,
	#[serde(rename = "gx_id")]
	pub gx_id: String,
	pub date_modif: NaiveDateTime,
	pub sta_type: StationType,

	pub zipcode: String,
	pub state: String,
	pub city: String,
	pub address2: String,
	pub address1: String,
	pub name: String,
	pub code: String,

	#[serde(rename = "created_at", with = "datetime_am")]
	pub created_at: NaiveDateTime,
	#[serde(rename = "updated_at", with = "datetime_am")]
	pub updated_at: NaiveDateTime,

	pub station_name: String,
	pub station_facility_name: String,
	pub station_aliases: String,
	pub station_rank: String,
}

#[derive(Debug, Clone, PartialEq, Eq, PartialOrd, Ord, Hash, Deserialize)]
pub enum StationType {
	#[serde(rename = "Station Building (with waiting room)")]
	StationBuildingWithWaitingRoom,
	#[serde(rename = "Platform with Shelter")]
	PlatformWithShelter,
	#[serde(rename = "Platform only (no shelter)")]
	PlatformOnlyNoShelter,
	#[serde(rename = "Curbside Bus Stop only (no shelter)")]
	CurbsideBusStopOnlyNoShelter,
}

mod opt_datetime {
	use chrono::NaiveDateTime;
	use serde::{Deserialize, Deserializer, de};

	pub fn deserialize<'de, D: Deserializer<'de>>(
		deserializer: D,
	) -> Result<Option<NaiveDateTime>, D::Error> {
		if let Some(s) = Deserialize::deserialize(deserializer)? {
			NaiveDateTime::parse_from_str(s, "%m/%d/%Y %T")
				.map(Some)
				.map_err(de::Error::custom)
		} else {
			Ok(None)
		}
	}
}

mod datetime_am {
	use chrono::NaiveDateTime;
	use serde::{Deserialize, Deserializer, de};

	pub fn deserialize<'de, D: Deserializer<'de>>(
		deserializer: D,
	) -> Result<NaiveDateTime, D::Error> {
		let s: &str = Deserialize::deserialize(deserializer)?;
		NaiveDateTime::parse_from_str(s, "%_m/%_d/%Y %_I:%M:%S %p").map_err(de::Error::custom)
	}
}

#[derive(Debug, Clone, Copy, PartialEq, Eq, PartialOrd, Ord, Hash, Deserialize)]
pub enum Heading {
	#[serde(rename = "NW")]
	NorthWest,
	#[serde(rename = "N")]
	North,
	#[serde(rename = "NE")]
	NorthEast,
	#[serde(rename = "E")]
	East,
	#[serde(rename = "SE")]
	SouthEast,
	#[serde(rename = "S")]
	South,
	#[serde(rename = "SW")]
	SouthWest,
	#[serde(rename = "W")]
	West,
}

impl From<Heading> for u16 {
	fn from(value: Heading) -> Self {
		match value {
			Heading::North => 0,
			Heading::NorthEast => 45,
			Heading::East => 90,
			Heading::SouthEast => 135,
			Heading::South => 180,
			Heading::SouthWest => 225,
			Heading::West => 270,
			Heading::NorthWest => 315,
		}
	}
}

#[derive(Debug, Clone, Copy, PartialEq, Eq, PartialOrd, Ord, Hash, Deserialize)]
pub enum Timezone {
	#[serde(rename = "P")]
	Pacific,
	#[serde(rename = "M")]
	Mountain,
	#[serde(rename = "C")]
	Central,
	#[serde(rename = "E")]
	Eastern,
}

impl From<Timezone> for Tz {
	fn from(value: Timezone) -> Self {
		match value {
			Timezone::Pacific => US::Pacific,
			Timezone::Mountain => US::Mountain,
			Timezone::Central => US::Central,
			Timezone::Eastern => US::Eastern,
		}
	}
}

mod from_str {
	use std::{fmt::Display, str::FromStr};

	use serde::{Deserialize, Deserializer, de};

	pub(crate) fn deserialize<'de, F: FromStr, D: Deserializer<'de>>(
		deserializer: D,
	) -> Result<F, D::Error>
	where
		F::Err: Display,
	{
		let s = String::deserialize(deserializer)?;
		s.parse().map_err(de::Error::custom)
	}
}

mod opt_from_str {
	use std::{fmt::Display, str::FromStr};

	use serde::{Deserialize, Deserializer, de};

	pub(crate) fn deserialize<'de, F: FromStr, D: Deserializer<'de>>(
		deserializer: D,
	) -> Result<Option<F>, D::Error>
	where
		F::Err: Display,
	{
		if let Some(s) = Option::<String>::deserialize(deserializer)? {
			s.parse().map(Some).map_err(de::Error::custom)
		} else {
			Ok(None)
		}
	}
}
