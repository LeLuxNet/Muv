use aes::{
	Aes128,
	cipher::{
		BlockDecryptMut, KeyIvInit,
		block_padding::{Pkcs7, UnpadError},
	},
};
use base64_simd::STANDARD;
use cbc::Decryptor;
use pbkdf2::pbkdf2_hmac_array;
use sha1::Sha1;

use std::{
	error,
	fmt::{self, Display, Formatter},
};

const MASTER_SEGMENT: usize = 88;
const PUBLIC_KEY: &str = "69af143c-e8cf-47f8-bf09-fc1f61e5cc33";
const GUID_LEN: usize = 36;

const SALT: [u8; 4] = [0x9a, 0x36, 0x86, 0xac];
const ITERATIONS: u32 = 1000;
const KEY_LEN: usize = 16;

const IV: [u8; 16] = [
	0xc6, 0xeb, 0x2f, 0x7f, 0x5c, 0x47, 0x40, 0xc1, 0xa2, 0xf7, 0x08, 0xfe, 0xfd, 0x94, 0x7d, 0x39,
];

pub fn decrypt_body(body: &mut [u8]) -> Result<&[u8], DecryptError> {
	let (content, private_key_hash) = body.split_at_mut(body.len() - MASTER_SEGMENT);

	let private_key = decrypt(private_key_hash, PUBLIC_KEY.as_bytes())?;
	let private_key = &private_key[..GUID_LEN];

	let content = decrypt(content, private_key)?;
	Ok(content)
}

pub fn decrypt<'a>(content: &'a mut [u8], password: &[u8]) -> Result<&'a [u8], DecryptError> {
	let content = STANDARD.decode_inplace(content)?;

	let key = pbkdf2_hmac_array::<Sha1, KEY_LEN>(password, &SALT, ITERATIONS);
	let decryptor = Decryptor::<Aes128>::new(&key.into(), &IV.into());
	let decrypted_content = decryptor.decrypt_padded_mut::<Pkcs7>(content)?;

	Ok(decrypted_content)
}

#[derive(Debug, Clone, Copy, PartialEq, Eq)]
pub enum DecryptError {
	Base64,
	AesPadding,
}

impl From<base64_simd::Error> for DecryptError {
	fn from(_: base64_simd::Error) -> Self {
		Self::Base64
	}
}

impl From<UnpadError> for DecryptError {
	fn from(_: UnpadError) -> Self {
		Self::AesPadding
	}
}

impl Display for DecryptError {
	fn fmt(&self, f: &mut Formatter<'_>) -> fmt::Result {
		match self {
			Self::Base64 => write!(f, "invalid base64 encoded data"),
			Self::AesPadding => write!(f, "invalid padding in AES data"),
		}
	}
}

impl error::Error for DecryptError {}
