use std::time::Duration;

use scooter::Agent;

use crate::amtrak::{TrainStations, TrainsData};

#[cfg(any(feature = "blocking", feature = "tokio"))]
use crate::{Error, amtrak::decrypt_body};
#[cfg(any(feature = "blocking", feature = "tokio"))]
use scooter::debug;
#[cfg(any(feature = "blocking", feature = "tokio"))]
use serde::Deserialize;

#[derive(Debug, Default)]
pub struct Maps<A> {
	pub agent: Agent<A>,
}

impl TrainsData {
	pub const URL: &'static str =
		"https://maps.amtrak.com/services/MapDataService/trains/getTrainsData";

	pub const UPDATE_CYCLE: Duration = Duration::from_secs(3 * 60);
}

impl TrainStations {
	pub const URL: &'static str =
		"https://maps.amtrak.com/services/MapDataService/stations/trainStations";
}

#[cfg(any(feature = "blocking", feature = "tokio"))]
#[derive(Deserialize)]
#[serde(rename_all = "PascalCase")]
struct StationsResponse {
	stations_data_response: TrainStations,
}

#[cfg(feature = "blocking")]
impl Maps<crate::Blocking> {
	pub fn trains_data(&self) -> Result<TrainsData, Error> {
		let mut body = self.agent.get(TrainsData::URL).send()?.bytes()?;
		let content = decrypt_body(&mut body)?;
		debug("amtrak", "trains_data", "json", content);
		serde_json::from_slice(content).map_err(|e| Error::Request(scooter::Error::Json(e)))
	}

	pub fn train_stations(&self) -> Result<TrainStations, Error> {
		let mut body = self.agent.get(TrainStations::URL).send()?.bytes()?;
		let content = decrypt_body(&mut body)?;
		debug("amtrak", "train_stations", "json", content);
		let res: StationsResponse =
			serde_json::from_slice(content).map_err(|e| Error::Request(scooter::Error::Json(e)))?;
		Ok(res.stations_data_response)
	}
}

#[cfg(feature = "tokio")]
impl Maps<crate::Tokio> {
	pub async fn trains_data(&self) -> Result<TrainsData, Error> {
		let mut body = self.agent.get(TrainsData::URL).send().await?.bytes()?;
		let content = decrypt_body(&mut body)?;
		debug("amtrak", "trains_data", "json", content);
		serde_json::from_slice(content).map_err(|e| Error::Request(scooter::Error::Json(e)))
	}

	pub async fn train_stations(&self) -> Result<TrainStations, Error> {
		let mut body = self.agent.get(TrainStations::URL).send().await?.bytes()?;
		let content = decrypt_body(&mut body)?;
		debug("amtrak", "train_stations", "json", content);
		let res: StationsResponse =
			serde_json::from_slice(content).map_err(|e| Error::Request(scooter::Error::Json(e)))?;
		Ok(res.stations_data_response)
	}
}

#[cfg(test)]
mod tests {
	#[cfg(any(feature = "blocking", feature = "tokio"))]
	use super::Maps;

	#[test]
	#[cfg(feature = "blocking")]
	fn trains_data_blocking() {
		let maps: Maps<crate::Blocking> = Maps::default();
		maps.trains_data().unwrap();
	}

	#[test]
	#[cfg(feature = "blocking")]
	fn train_stations_blocking() {
		let maps: Maps<crate::Blocking> = Maps::default();
		maps.train_stations().unwrap();
	}

	#[tokio::test]
	#[cfg(feature = "tokio")]
	async fn trains_data_tokio() {
		let maps: Maps<crate::Tokio> = Maps::default();
		maps.trains_data().await.unwrap();
	}

	#[tokio::test]
	#[cfg(feature = "tokio")]
	async fn train_stations_tokio() {
		let maps: Maps<crate::Tokio> = Maps::default();
		maps.train_stations().await.unwrap();
	}
}
