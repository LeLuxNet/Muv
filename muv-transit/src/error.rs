use std::{
	error,
	fmt::{self, Display, Formatter},
	result,
};

#[cfg(feature = "amtrak")]
use crate::amtrak::DecryptError;

#[cfg(feature = "hafas")]
use crate::hafas;

pub type Result<T> = result::Result<T, Error>;

#[derive(Debug)]
#[non_exhaustive]
pub enum Error {
	#[cfg(feature = "amtrak")]
	AmtrakDecrypt(DecryptError),
	#[cfg(feature = "hafas")]
	Hafas(hafas::Error),
	#[cfg(feature = "mav-start")]
	ParseXml(quick_xml::DeError),

	#[cfg(any(feature = "zsr", feature = "sprava-zeleznic"))]
	ParseJson(serde_json::Error),
	#[cfg(feature = "reqwest")]
	Reqwest(reqwest::Error),

	#[cfg(any(
		feature = "amtrak",
		feature = "viarail",
		feature = "hafas",
		feature = "db",
		feature = "ns",
		feature = "digitraffic",
		feature = "mav-start",
		feature = "taxi-de"
	))]
	Request(scooter::Error),
}

#[cfg(feature = "amtrak")]
impl From<DecryptError> for Error {
	fn from(value: DecryptError) -> Self {
		Self::AmtrakDecrypt(value)
	}
}

#[cfg(feature = "hafas")]
impl From<hafas::Error> for Error {
	fn from(value: hafas::Error) -> Self {
		Self::Hafas(value)
	}
}

#[cfg(feature = "mav-start")]
impl From<quick_xml::DeError> for Error {
	fn from(value: quick_xml::DeError) -> Self {
		Self::ParseXml(value)
	}
}

#[cfg(any(feature = "zsr", feature = "sprava-zeleznic"))]
impl From<serde_json::Error> for Error {
	fn from(value: serde_json::Error) -> Self {
		Self::ParseJson(value)
	}
}

#[cfg(feature = "reqwest")]
impl From<reqwest::Error> for Error {
	fn from(value: reqwest::Error) -> Self {
		Self::Reqwest(value)
	}
}

#[cfg(any(
	feature = "amtrak",
	feature = "viarail",
	feature = "hafas",
	feature = "db",
	feature = "ns",
	feature = "digitraffic",
	feature = "mav-start",
	feature = "taxi-de"
))]
impl From<scooter::Error> for Error {
	fn from(value: scooter::Error) -> Self {
		Self::Request(value)
	}
}

impl Display for Error {
	fn fmt(&self, f: &mut Formatter<'_>) -> fmt::Result {
		#[allow(unreachable_code)]
		f.write_str(match self {
			#[cfg(feature = "amtrak")]
			Self::AmtrakDecrypt(_) => "decrypting amtrak response error",
			#[cfg(feature = "hafas")]
			Self::Hafas(_) => "hafas server error",
			#[cfg(feature = "mav-start")]
			Self::ParseXml(_) => "parse XML error",

			#[cfg(any(feature = "zsr", feature = "sprava-zeleznic"))]
			Self::ParseJson(_) => "parse JSON error",
			#[cfg(feature = "reqwest")]
			Self::Reqwest(_) => "reqwest http error",
			#[cfg(any(
				feature = "amtrak",
				feature = "viarail",
				feature = "hafas",
				feature = "db",
				feature = "ns",
				feature = "digitraffic",
				feature = "mav-start",
				feature = "taxi-de"
			))]
			Self::Request(_) => "api request failed",
			#[cfg(not(any(
				feature = "amtrak",
				feature = "viarail",
				feature = "hafas",
				feature = "db",
				feature = "ns",
				feature = "zsr",
				feature = "digitraffic",
				feature = "mav-start",
				feature = "sprava-zeleznic",
				feature = "reqwest",
				feature = "taxi-de",
			)))]
			_ => {
				let _ = f;
				unreachable!()
			}
		})
	}
}

impl error::Error for Error {
	fn source(&self) -> Option<&(dyn error::Error + 'static)> {
		#[allow(unreachable_code)]
		Some(match self {
			#[cfg(feature = "amtrak")]
			Self::AmtrakDecrypt(e) => e,
			#[cfg(feature = "hafas")]
			Self::Hafas(e) => e,
			#[cfg(feature = "mav-start")]
			Self::ParseXml(e) => e,

			#[cfg(any(feature = "zsr", feature = "sprava-zeleznic"))]
			Self::ParseJson(e) => e,
			#[cfg(feature = "reqwest")]
			Self::Reqwest(e) => e,
			#[cfg(any(
				feature = "amtrak",
				feature = "viarail",
				feature = "hafas",
				feature = "db",
				feature = "ns",
				feature = "digitraffic",
				feature = "mav-start",
				feature = "taxi-de"
			))]
			Self::Request(e) => e,
			#[cfg(not(any(
				feature = "amtrak",
				feature = "viarail",
				feature = "hafas",
				feature = "db",
				feature = "ns",
				feature = "zsr",
				feature = "digitraffic",
				feature = "mav-start",
				feature = "sprava-zeleznic",
				feature = "reqwest",
				feature = "taxi-de",
			)))]
			_ => return None,
		})
	}
}
