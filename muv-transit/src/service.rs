use chrono::NaiveDate;

#[cfg(feature = "serde")]
use serde::{Deserialize, Serialize};

use std::{
	fmt::{self, Debug, Formatter},
	iter::FlatMap,
	slice::Iter,
	vec::IntoIter,
};

#[cfg(feature = "gtfs")]
use crate::gtfs::Weekdays;
#[cfg(feature = "gtfs")]
use chrono::Datelike;
#[cfg(feature = "gtfs")]
use std::collections::HashSet;

pub type ServiceId = String;

#[non_exhaustive]
#[derive(Debug, Clone, PartialEq)]
#[cfg_attr(feature = "serde", derive(Deserialize))]
pub enum Service {
	#[cfg(feature = "gtfs")]
	Gtfs {
		start_date: NaiveDate,
		end_date: NaiveDate,
		weekdays: Weekdays,

		added: HashSet<NaiveDate>,
		removed: HashSet<NaiveDate>,
	},
	Bits {
		from_date: NaiveDate,
		to_date: Option<NaiveDate>,
		days: DayBits,
	},
}

impl Service {
	#[must_use]
	pub fn allowed(&self, date: NaiveDate) -> bool {
		match self {
			#[cfg(feature = "gtfs")]
			Self::Gtfs {
				start_date,
				end_date,
				weekdays,

				added,
				removed,
			} => {
				if date < *start_date || date > *end_date {
					return false;
				}

				let mut allowed = weekdays.get(start_date.weekday());

				let exceptions = if allowed { &removed } else { &added };
				if exceptions.contains(start_date) {
					allowed = !allowed;
				}

				allowed
			}
			Self::Bits {
				from_date,
				to_date: _,
				days,
			} => {
				let offset = date.signed_duration_since(*from_date).num_days();
				offset.try_into().is_ok_and(|i| days.contains(i))
			}
		}
	}
}

impl<'a> IntoIterator for &'a Service {
	type Item = NaiveDate;
	type IntoIter = ServiceIter<'a>;

	fn into_iter(self) -> Self::IntoIter {
		match self {
			#[cfg(feature = "gtfs")]
			Service::Gtfs {
				start_date,
				end_date,
				weekdays,
				added,
				removed,
			} => ServiceIter::Gtfs {
				start_date: *start_date,
				end_date: *end_date,
				weekdays: *weekdays,
				added,
				removed,
			},
			Service::Bits {
				from_date,
				to_date,
				days: valid_days,
			} => ServiceIter::Bits {
				from_date: *from_date,
				to_date: *to_date,
				valid_days_iter: valid_days.into_iter(),
			},
		}
	}
}

impl Service {
	pub fn iter(&self) -> ServiceIter<'_> {
		self.into_iter()
	}
}

#[must_use]
#[derive(Debug)]
pub enum ServiceIter<'a> {
	#[cfg(feature = "gtfs")]
	Gtfs {
		start_date: NaiveDate,
		end_date: NaiveDate,
		weekdays: Weekdays,

		added: &'a HashSet<NaiveDate>,
		removed: &'a HashSet<NaiveDate>,
	},
	Bits {
		from_date: NaiveDate,
		to_date: Option<NaiveDate>,

		valid_days_iter: IterDayBits<'a>,
	},
}

impl Iterator for ServiceIter<'_> {
	type Item = NaiveDate;

	fn next(&mut self) -> Option<Self::Item> {
		#[cfg(any(feature = "gtfs", feature = "netex"))]
		match self {
			#[cfg(feature = "gtfs")]
			ServiceIter::Gtfs {
				start_date,
				end_date,
				weekdays,

				added,
				removed,
			} => {
				while start_date <= end_date {
					let mut allowed = weekdays.get(start_date.weekday());

					let exceptions = if allowed { &removed } else { &added };
					if exceptions.contains(start_date) {
						allowed = !allowed;
					}

					let now = *start_date;
					*start_date = now.succ_opt().unwrap();
					if allowed {
						return Some(now);
					}
				}
			}
			ServiceIter::Bits {
				from_date,
				to_date,
				valid_days_iter,
			} => {
				while to_date.map_or(true, |end_date| *from_date <= end_date) {
					if valid_days_iter.next()? {
						return Some(*from_date);
					}

					*from_date = from_date.succ_opt().unwrap();
				}
			}
		};
		None
	}

	fn size_hint(&self) -> (usize, Option<usize>) {
		match self {
			#[cfg(feature = "gtfs")]
			ServiceIter::Gtfs {
				start_date,
				end_date,
				..
			} => {
				let max = end_date.signed_duration_since(*start_date).num_days();
				let max = if max < 0 {
					None
				} else {
					Some(max.try_into().unwrap_or(usize::MAX))
				};
				(0, max)
			}
			ServiceIter::Bits { .. } => {
				(0, None) // TODO
			}
		}
	}
}

#[derive(Clone, PartialEq, Eq, PartialOrd, Ord, Hash)]
#[cfg_attr(feature = "serde", derive(Serialize, Deserialize))]
pub struct DayBits(pub Vec<u8>);

impl DayBits {
	#[must_use]
	pub fn contains(&self, index: usize) -> bool {
		self.0.get(index / 8).is_some_and(|block| {
			let mask = 1 << (7 - index % 8);
			block & mask != 0
		})
	}
}

impl Debug for DayBits {
	fn fmt(&self, f: &mut Formatter<'_>) -> fmt::Result {
		for v in &self.0 {
			write!(f, "{v:b}")?;
		}
		Ok(())
	}
}

pub type IterDayBits<'a> = FlatMap<Iter<'a, u8>, [bool; 8], fn(&u8) -> [bool; 8]>;
impl<'a> IntoIterator for &'a DayBits {
	type Item = bool;
	type IntoIter = IterDayBits<'a>;

	fn into_iter(self) -> Self::IntoIter {
		self.0.iter().flat_map(|b| {
			[
				b & 1 << 7 != 0,
				b & 1 << 6 != 0,
				b & 1 << 5 != 0,
				b & 1 << 4 != 0,
				b & 1 << 3 != 0,
				b & 1 << 2 != 0,
				b & 1 << 1 != 0,
				b & 1 << 0 != 0,
			]
		})
	}
}

impl DayBits {
	pub fn iter(&self) -> IterDayBits<'_> {
		self.into_iter()
	}
}

pub type IntoDayBits = FlatMap<IntoIter<u8>, [bool; 8], fn(u8) -> [bool; 8]>;
impl IntoIterator for DayBits {
	type Item = bool;
	type IntoIter = IntoDayBits;

	fn into_iter(self) -> Self::IntoIter {
		self.0.into_iter().flat_map(|b| {
			[
				b & 1 << 7 != 0,
				b & 1 << 6 != 0,
				b & 1 << 5 != 0,
				b & 1 << 4 != 0,
				b & 1 << 3 != 0,
				b & 1 << 2 != 0,
				b & 1 << 1 != 0,
				b & 1 << 0 != 0,
			]
		})
	}
}
