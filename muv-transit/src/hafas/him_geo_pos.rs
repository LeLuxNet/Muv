use chrono::NaiveDate;
use serde::{Deserialize, Serialize};

use crate::hafas::{Common, Crd, PolyG, Time, date};

#[cfg(any(feature = "blocking", feature = "tokio"))]
use crate::{Result, hafas::Client};
#[cfg(any(feature = "blocking", feature = "tokio"))]
use chrono::NaiveDateTime;

#[derive(Debug, Deserialize)]
#[serde(rename_all = "camelCase")]
pub struct HimGeoPos {
	pub common: Common,
	pub msg_ref_l: Vec<usize>,
	#[serde(default)]
	pub edge_ref_l: Vec<usize>,
}

#[derive(Debug, Deserialize)]
#[serde(rename_all = "camelCase")]
pub struct Him {
	pub hid: String,
	pub act: bool,

	pub head: String,
	pub text: Option<String>,

	pub ico_x: usize,
	pub prio: u32,
	pub prod: u32,

	#[serde(with = "date")]
	pub s_date: NaiveDate,
	pub s_time: Time,

	#[serde(with = "date")]
	pub l_mod_date: NaiveDate,
	pub l_mod_time: Time,

	#[serde(with = "date")]
	pub e_date: NaiveDate,
	pub e_time: Time,

	pub cat_ref_l: Vec<usize>,
	#[serde(default)]
	pub edge_ref_l: Vec<usize>,
	#[serde(default)]
	pub event_ref_l: Vec<usize>,
	#[serde(default)]
	pub r_ref_l: Vec<usize>,
}

#[derive(Debug, Deserialize)]
#[serde(rename_all = "camelCase")]
pub struct HimMsgEdge {
	pub dir: Option<usize>,
	pub ico_x: Option<usize>,
	pub ico_crd: Crd,

	pub f_loc_x: Option<usize>,
	pub t_loc_x: Option<usize>,

	#[serde(default)]
	pub msg_ref_l: Vec<usize>,
	pub poly_g: Option<PolyG>,
}

#[cfg(any(feature = "blocking", feature = "tokio"))]
#[derive(Serialize)]
#[serde(rename_all = "camelCase")]
struct HimGeoPosReq {
	rect: Option<Rectangle>,

	get_poly_line: bool,

	#[serde(with = "date")]
	date_b: NaiveDate,
	time_b: Time,

	#[serde(with = "date")]
	date_e: NaiveDate,
	time_e: Time,
}

#[derive(Debug, Serialize)]
#[serde(rename_all = "camelCase")]
pub struct Rectangle {
	pub ll_crd: Crd,
	pub ur_crd: Crd,
}

impl Rectangle {
	pub const GLOBE: Self = Self {
		ll_crd: Crd {
			floor: None,
			x: -180_000_000,
			y: -90_000_000,
			z: None,
		},
		ur_crd: Crd {
			floor: None,
			x: 180_000_000,
			y: 90_000_000,
			z: None,
		},
	};
}

#[cfg(feature = "blocking")]
impl Client<crate::Blocking> {
	pub fn him_geo_pos(
		&self,
		rect: Option<Rectangle>,
		from: NaiveDateTime,
		to: NaiveDateTime,
		polyline: bool,
	) -> Result<HimGeoPos> {
		self.request(
			"HimGeoPos",
			&HimGeoPosReq {
				rect,
				get_poly_line: polyline,

				date_b: from.date(),
				time_b: from.time().into(),

				date_e: to.date(),
				time_e: to.time().into(),
			},
		)
	}
}

#[cfg(feature = "tokio")]
impl Client<crate::Tokio> {
	pub async fn him_geo_pos(
		&self,
		rect: Option<Rectangle>,
		from: NaiveDateTime,
		to: NaiveDateTime,
		polyline: bool,
	) -> Result<HimGeoPos> {
		self.request(
			"HimGeoPos",
			&HimGeoPosReq {
				rect,
				get_poly_line: polyline,

				date_b: from.date(),
				time_b: from.time().into(),

				date_e: to.date(),
				time_e: to.time().into(),
			},
		)
		.await
	}
}
