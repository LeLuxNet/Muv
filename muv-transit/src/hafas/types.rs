use std::{
	error::Error,
	fmt::{self, Debug, Display, Formatter, Write},
	hash::Hash,
	num::ParseIntError,
	str::FromStr,
};

use chrono::{DateTime, Datelike, NaiveDate, Utc};
use serde::{
	Deserialize, Deserializer, Serialize, Serializer,
	de::{self, Visitor},
	ser::SerializeStruct,
};

use crate::{
	DayBits,
	hafas::{ParseDateError, Time, opt_date, parse_date},
};

const fn hex_digit(b: u8) -> Option<u8> {
	Some(match b {
		b'0'..=b'9' => b - b'0',
		b'a'..=b'f' => b - b'a' + 10,
		b'A'..=b'F' => b - b'A' + 10,
		_ => return None,
	})
}

pub(crate) struct HexVisitor;

impl Visitor<'_> for HexVisitor {
	type Value = DayBits;

	fn expecting(&self, formatter: &mut Formatter) -> fmt::Result {
		formatter.write_str("a hex string")
	}

	fn visit_str<E: de::Error>(self, v: &str) -> Result<Self::Value, E> {
		let b = v.as_bytes();
		if b.len() % 2 != 0 {
			return Err(de::Error::custom("invalid length of hex string"));
		}

		b.chunks(2)
			.map(|c| {
				let left = hex_digit(c[0]).ok_or_else(|| {
					de::Error::custom(format!("invalid hex digit {:?}", char::from(c[0])))
				})?;
				let right = hex_digit(c[1]).ok_or_else(|| {
					de::Error::custom(format!("invalid hex digit {:?}", char::from(c[1])))
				})?;
				Ok(left << 4 | right)
			})
			.collect::<Result<_, _>>()
			.map(DayBits)
	}
}

mod hex {
	use std::fmt::Write;

	use serde::{Deserializer, Serialize, Serializer};

	use crate::{DayBits, hafas::HexVisitor};

	pub fn serialize<S: Serializer>(bits: &DayBits, serializer: S) -> Result<S::Ok, S::Error> {
		let mut s = String::with_capacity(bits.0.len() * 2);
		for b in &bits.0 {
			write!(&mut s, "{b:02x}").unwrap();
		}
		s.serialize(serializer)
	}

	pub fn deserialize<'de, D: Deserializer<'de>>(deserializer: D) -> Result<DayBits, D::Error> {
		deserializer.deserialize_str(HexVisitor)
	}
}

#[derive(Debug, Default)]
pub struct Location {
	pub name: Option<String>,
	pub id: Option<String>,
	pub gid: Option<Gid>,
	pub x: Option<i32>,
	pub y: Option<i32>,
}

impl Location {
	#[must_use]
	pub fn new_name<S: Into<String>>(name: S) -> Self {
		Self {
			name: Some(name.into()),
			id: None,
			gid: None,
			x: None,
			y: None,
		}
	}

	#[must_use]
	pub fn new_id<S: Into<String>>(id: S) -> Self {
		Self {
			name: None,
			id: Some(id.into()),
			gid: None,
			x: None,
			y: None,
		}
	}

	#[must_use]
	pub const fn new_xy(x: i32, y: i32) -> Self {
		Self {
			name: None,
			id: None,
			gid: None,
			x: Some(x),
			y: Some(y),
		}
	}

	#[must_use]
	pub const fn new_gid(gid: Gid) -> Self {
		Self {
			name: None,
			id: None,
			gid: Some(gid),
			x: None,
			y: None,
		}
	}
}

impl From<crate::Location> for Location {
	fn from(value: crate::Location) -> Self {
		Self::new_xy(to_xy(value.lon), to_xy(value.lat))
	}
}

impl Serialize for Location {
	fn serialize<S: Serializer>(&self, serializer: S) -> Result<S::Ok, S::Error> {
		let mut res = String::new();
		if let Some(id) = self.id.as_ref() {
			write!(res, "L={id}").unwrap();
		}
		if let Some(gid) = self.gid.as_ref() {
			if !res.is_empty() {
				res.push('@');
			}
			write!(res, "i={gid}").unwrap();
		}
		if let Some(name) = self.name.as_ref() {
			if !res.is_empty() {
				res.push('@');
			}
			write!(res, "O={name}").unwrap();
		}
		if let Some(x) = self.x {
			if !res.is_empty() {
				res.push('@');
			}
			write!(res, "X={x}").unwrap();
		}
		if let Some(y) = self.y {
			if !res.is_empty() {
				res.push('@');
			}
			write!(res, "Y={y}").unwrap();
		}
		res.serialize(serializer)
	}
}

struct LocationVisitor;

impl Visitor<'_> for LocationVisitor {
	type Value = Location;

	fn expecting(&self, formatter: &mut Formatter) -> fmt::Result {
		formatter.write_str("a hafas location string")
	}

	fn visit_str<E: de::Error>(self, v: &str) -> Result<Self::Value, E> {
		let mut res = Location::default();
		for (key, val) in v.split('@').filter_map(|s| s.split_once('=')) {
			match key {
				"O" => res.name = Some(val.to_owned()),
				"L" => res.id = Some(val.to_owned()),
				"i" => res.gid = Some(Gid::parse(val)?),
				"X" => res.x = Some(val.parse().map_err(de::Error::custom)?),
				"Y" => res.y = Some(val.parse().map_err(de::Error::custom)?),
				_ => {}
			}
		}
		Ok(res)
	}
}

impl<'de> Deserialize<'de> for Location {
	fn deserialize<D: Deserializer<'de>>(deserializer: D) -> Result<Self, D::Error> {
		deserializer.deserialize_str(LocationVisitor)
	}
}

#[derive(Debug, Deserialize)]
#[serde(rename_all = "camelCase")]
pub struct Station {
	pub lid: Location,
	pub name: String,
	pub ico_x: usize,
	pub ext_id: Option<String>,
	pub crd: Crd,
	pub p_cls: Option<u16>,

	#[serde(default)]
	pub meta: bool,
	pub m_mast_loc_x: Option<usize>,
	#[serde(default)]
	pub is_main_mast: bool,

	#[serde(default)]
	pub gid_l: Vec<Gid>,
	#[serde(default)]
	pub global_id_l: Vec<Gid>,

	#[serde(rename = "TZOffset")]
	pub tz_offset: Option<i16>,
	pub chg_time: Option<Time>, // TODO: TimeDelta

	pub map_selectable: Option<bool>,
	#[serde(rename = "isFavrbl")]
	pub is_favoriteable: Option<bool>,
	pub is_hstrbl: Option<bool>,

	#[serde(default)]
	pub country_code_l: Vec<String>,
}

#[derive(Debug, Clone, Copy, PartialEq, Eq, Hash, Serialize, Deserialize)]
#[serde(rename_all = "camelCase")]
pub struct Crd {
	pub floor: Option<i32>,
	pub x: i32,
	pub y: i32,
	pub z: Option<i32>,
}

impl Crd {
	#[must_use]
	pub fn lat(self) -> f64 {
		f64::from(self.y) / 1e6
	}

	#[must_use]
	pub fn lon(self) -> f64 {
		f64::from(self.x) / 1e6
	}
}

impl From<Crd> for crate::Location {
	fn from(value: Crd) -> Self {
		Self {
			lat: value.lat(),
			lon: value.lon(),
		}
	}
}

#[allow(clippy::as_conversions, clippy::cast_possible_truncation)]
fn to_xy(v: f64) -> i32 {
	(v * 1e6) as i32
}

impl From<crate::Location> for Crd {
	fn from(value: crate::Location) -> Self {
		Self {
			floor: None,
			x: to_xy(value.lon),
			y: to_xy(value.lat),
			z: None,
		}
	}
}

#[derive(Debug, Clone, PartialEq, Eq, PartialOrd, Ord, Hash, Deserialize)]
#[serde(tag = "type", content = "id")]
pub enum Gid {
	#[serde(rename = "U")]
	Uic(String),
	#[serde(rename = "A")]
	Ifopt(String),
	#[serde(rename = "B")]
	Bst(String),
}

impl Gid {
	fn parse<E: de::Error>(s: &str) -> Result<Self, E> {
		let b = s.as_bytes();
		if b.len() < 3 || &b[1..=2] != b"\xc3\x97" {
			return Err(de::Error::custom(
				"expected gid to start with a single character followed by a `×` (multiplication sign)",
			));
		}

		let value = s[3..].to_owned();
		Ok(match b[0] {
			b'U' => Self::Uic(value),
			b'A' => Self::Ifopt(value),
			b'B' => Self::Bst(value),
			_ => {
				return Err(de::Error::custom(format!(
					"unknown gid type {:?}: expected 'U', 'A' or 'B'",
					char::from(b[0])
				)));
			}
		})
	}
}

impl Display for Gid {
	fn fmt(&self, f: &mut Formatter<'_>) -> fmt::Result {
		let (prefix, value) = match self {
			Self::Uic(v) => ('U', v),
			Self::Ifopt(v) => ('A', v),
			Self::Bst(v) => ('B', v),
		};
		write!(f, "{prefix}×{value}")
	}
}

impl Serialize for Gid {
	fn serialize<S: Serializer>(&self, serializer: S) -> Result<S::Ok, S::Error> {
		self.to_string().serialize(serializer)
	}
}

pub(crate) struct StationInput(pub(crate) Location);

impl Serialize for StationInput {
	fn serialize<S: Serializer>(&self, serializer: S) -> Result<S::Ok, S::Error> {
		let mut s = serializer.serialize_struct("Station", 2)?;
		s.serialize_field("type", "S")?;
		s.serialize_field("lid", &self.0)?;
		s.end()
	}
}

#[derive(Debug, PartialEq, Eq, Hash, Serialize, Deserialize)]
#[serde(rename_all = "camelCase")]
pub struct Message {
	pub f_idx: Option<isize>,
	pub f_loc_x: Option<usize>,

	pub prio: Option<u32>,
	pub rem_x: Option<usize>,
	pub him_x: Option<usize>,
	pub sort: u32,

	pub t_idx: Option<isize>,
	pub t_loc_x: Option<usize>,
}

#[derive(Debug, PartialEq, Eq, Serialize, Deserialize)]
#[serde(rename_all = "camelCase")]
pub struct Journey {
	pub ani: Option<Ani>,

	#[serde(default, with = "opt_date")]
	pub date: Option<NaiveDate>,
	pub dir_txt: Option<String>,
	pub dir_geo: Option<u8>,
	pub is_part_cncl: Option<bool>,
	pub is_cncl: Option<bool>,
	pub is_rchbl: Option<bool>,
	pub is_redir: Option<bool>,
	pub pos: Option<Crd>,

	pub jid: JourneyId,
	#[serde(default)]
	pub par_jny_seg_l: Vec<ParJourney>,
	#[serde(default)]
	pub msg_l: Vec<Message>,
	pub poly_g: Option<PolyG>,

	#[serde(default)]
	pub prod_l: Vec<ProdRange>,
	pub prod_x: usize,

	#[serde(default)]
	pub s_days_l: Vec<ScheduleDay>,

	pub stb_stop: Option<Stop>,
	#[serde(default)]
	pub stop_l: Vec<Stop>,

	#[serde(default, with = "opt_date")]
	pub train_start_date: Option<NaiveDate>,
}

#[derive(Debug, PartialEq, Eq, Serialize, Deserialize)]
#[serde(rename_all = "camelCase")]
pub struct Ani {
	pub m_sec: Vec<u32>,
	pub proc: Vec<u8>,
	pub proc_abs: Vec<u16>,

	pub f_loc_x: Vec<usize>,
	pub t_loc_x: Vec<usize>,

	pub dir_geo: Vec<i8>,
	pub stc_output_x: Vec<i8>,
	pub poly_g: PolyG,
	pub state: Vec<char>,
}

#[derive(Debug, PartialEq, Eq, Serialize, Deserialize)]
#[serde(rename_all = "camelCase")]
pub struct ParJourney {
	pub jid: JourneyId,
	pub f_loc_x: usize,
	pub link_type: Option<LinkType>,
	pub t_loc_x: usize,
	pub r#type: ParJourneyType,
}

#[derive(Debug, PartialEq, Eq, PartialOrd, Ord, Hash, Serialize, Deserialize)]
#[serde(rename_all = "SCREAMING_SNAKE_CASE")]
pub enum LinkType {
	Planned,
	LinkedRt,
	UnlinkedRt,
}

#[derive(Debug, PartialEq, Eq, PartialOrd, Ord, Hash, Serialize, Deserialize)]
#[serde(rename_all = "UPPERCASE")]
pub enum ParJourneyType {
	Union,
	Throughcoach,
}

#[derive(Debug, PartialEq, Eq, Hash, Serialize)]
pub struct JourneyId {
	pub id: u32,
	pub cycle: u8,
	pub pool: u8,
	pub date: NaiveDate,

	pub start_time: Option<DateTime<Utc>>,

	pub first_time: Option<Time>,
	pub last_time: Option<Time>,
}

impl JourneyId {
	#[must_use]
	pub const fn new(id: u32, cycle: u8, pool: u8, date: NaiveDate) -> Self {
		Self {
			id,
			cycle,
			pool,
			date,

			start_time: None,

			first_time: None,
			last_time: None,
		}
	}

	#[must_use]
	pub fn to_v1_string(&self) -> String {
		format!(
			"1|{}|{}|{}|{}{:02}{:04}",
			self.id,
			self.cycle,
			self.pool,
			self.date.day(),
			self.date.month(),
			self.date.year(),
		)
	}
}

#[derive(Debug, Clone)]
pub enum ParseJourneyIdError {
	MissingVersion,
	UnknownVersion(String),
	MissingId,
	InvalidId(ParseIntError),
	MissingCycle,
	InvalidCycle(ParseIntError),
	MissingPool,
	InvalidPool(ParseIntError),
	MissingDate,
	InvalidDate(ParseDateError),
}

impl Display for ParseJourneyIdError {
	fn fmt(&self, f: &mut Formatter<'_>) -> fmt::Result {
		match self {
			Self::MissingVersion => f.write_str("missing version"),
			Self::UnknownVersion(v) => write!(f, "unknown version `{v}`"),
			Self::MissingId => write!(f, "missing id parameter"),
			Self::InvalidId(_) => write!(f, "invalid id parameter"),
			Self::MissingCycle => write!(f, "missing cycle parameter"),
			Self::InvalidCycle(_) => write!(f, "invalid cycle parameter"),
			Self::MissingPool => write!(f, "missing pool parameter"),
			Self::InvalidPool(_) => write!(f, "invalid pool parameter"),
			Self::MissingDate => write!(f, "missing date parameter"),
			Self::InvalidDate(_) => write!(f, "invalid date parameter"),
		}
	}
}

impl Error for ParseJourneyIdError {
	fn source(&self) -> Option<&(dyn Error + 'static)> {
		Some(match self {
			Self::InvalidId(e) | Self::InvalidCycle(e) | Self::InvalidPool(e) => e,
			Self::InvalidDate(e) => e,
			_ => return None,
		})
	}
}

impl FromStr for JourneyId {
	type Err = ParseJourneyIdError;

	fn from_str(s: &str) -> Result<Self, Self::Err> {
		let (version, rest) = s
			.split_once('|')
			.ok_or(ParseJourneyIdError::MissingVersion)?;

		let mut id = None;
		let mut cycle = None;
		let mut pool = None;
		let mut date = None;

		let mut start_time = None;

		let mut first_time = None;
		let mut last_time = None;

		match version {
			"1" => {
				let mut iter = rest.split('|');
				id = iter.next();
				cycle = iter.next();
				pool = iter.next();
				date = iter
					.next()
					.map(|s| parse_date(s, true))
					.transpose()
					.map_err(ParseJourneyIdError::InvalidDate)?;
			}
			"2" => {
				let mut i = s.split('#').skip(1);
				loop {
					let key = i.next().unwrap();
					if key.is_empty() {
						break;
					}

					let val = i.next().unwrap();

					#[allow(clippy::match_same_arms)]
					match key {
						"VN" => {} // version
						"ST" => {
							start_time = val
								.parse()
								.ok()
								.and_then(|secs| DateTime::from_timestamp(secs, 0));
						}
						"PI" => {}
						"ZI" => id = Some(val),
						"TA" => cycle = Some(val),
						"DA" => {
							date = Some(
								parse_date(val, false).map_err(ParseJourneyIdError::InvalidDate)?,
							);
						}
						"1S" => {} // 1st stop
						"1T" => first_time = Time::from_str(val, false).ok(),
						"LS" => {} // last stop
						"LT" => last_time = Time::from_str(val, false).ok(),
						"PU" => pool = Some(val),
						"RT" => {}
						"CA" => {} // category
						"ZE" => {} // line
						"ZB" => {} // product
						"PC" => {} // category code
						"FR" => {} // from
						"FT" => {} // from time
						"TO" => {} // to
						"TT" => {} // to time
						_ => todo!("unknown jid key {key:?} => {val:?}"),
					}
				}
			}
			_ => return Err(ParseJourneyIdError::UnknownVersion(version.to_owned())),
		}

		let id = id
			.ok_or(ParseJourneyIdError::MissingId)?
			.parse()
			.map_err(ParseJourneyIdError::InvalidId)?;
		let cycle = cycle
			.ok_or(ParseJourneyIdError::MissingCycle)?
			.parse()
			.map_err(ParseJourneyIdError::InvalidCycle)?;
		let pool = pool
			.ok_or(ParseJourneyIdError::MissingPool)?
			.parse()
			.map_err(ParseJourneyIdError::InvalidPool)?;
		let date = date.ok_or(ParseJourneyIdError::MissingDate)?;

		Ok(Self {
			id,
			cycle,
			pool,
			date,

			start_time,

			first_time,
			last_time,
		})
	}
}

struct JourneyIdVisitor;

impl Visitor<'_> for JourneyIdVisitor {
	type Value = JourneyId;

	fn expecting(&self, formatter: &mut Formatter) -> fmt::Result {
		formatter.write_str("a hafas jid")
	}

	fn visit_str<E: de::Error>(self, v: &str) -> Result<Self::Value, E> {
		v.parse().map_err(de::Error::custom)
	}
}

impl<'de> Deserialize<'de> for JourneyId {
	fn deserialize<D: Deserializer<'de>>(deserializer: D) -> Result<Self, D::Error> {
		deserializer.deserialize_str(JourneyIdVisitor)
	}
}

#[derive(Debug, PartialEq, Eq, PartialOrd, Ord, Hash, Serialize, Deserialize)]
pub struct PolyG {
	#[serde(rename = "polyXL")]
	pub poly_xl: (usize,),
}

#[derive(Debug, PartialEq, Eq, Hash, Serialize, Deserialize)]
#[serde(rename_all = "camelCase")]
pub struct ProdRange {
	pub f_idx: usize,
	pub f_loc_x: usize,
	pub prod_x: usize,
	pub t_idx: usize,
	pub t_loc_x: usize,
}

#[derive(Debug, PartialEq, Eq, Hash, Serialize, Deserialize)]
#[serde(rename_all = "camelCase")]
pub struct ScheduleDay {
	pub f_loc_idx: Option<usize>,
	pub f_loc_x: Option<usize>,
	pub s_days_r: Option<String>,
	pub s_days_i: Option<String>,
	#[serde(with = "hex")]
	pub s_days_b: DayBits,
	pub t_loc_idx: Option<usize>,
	pub t_loc_x: Option<usize>,
}

#[derive(Debug, PartialEq, Eq, Serialize, Deserialize)]
#[serde(rename_all = "camelCase")]
pub struct Stop {
	pub a_out_r: Option<bool>,
	pub a_out_s: Option<bool>,
	#[serde(default)]
	pub a_platf_ch: bool,
	pub a_pltf_r: Option<Platform>,
	pub a_pltf_s: Option<Platform>,
	pub a_prod_x: Option<usize>,
	pub a_prog_type: Option<ProgType>,
	#[serde(rename = "aTZOffset")]
	pub a_tz_offset: Option<i32>,
	pub a_time_r: Option<Time>,
	pub a_time_s: Option<Time>,

	pub d_in_r: Option<bool>,
	pub d_in_s: Option<bool>,
	pub d_dir_txt: Option<String>,
	#[serde(default)]
	pub d_pltf_ch: bool,
	pub d_pltf_r: Option<Platform>,
	pub d_pltf_s: Option<Platform>,
	pub d_prod_x: Option<usize>,
	pub d_prog_type: Option<ProgType>,
	#[serde(rename = "dTZOffset")]
	pub d_tz_offset: Option<i32>,
	pub d_time_r: Option<Time>,
	pub d_time_s: Option<Time>,

	#[serde(default)]
	pub border: bool,
	pub idx: Option<usize>,
	pub loc_x: Option<usize>,

	#[serde(default)]
	pub msg_l: Vec<Message>,
}

impl Stop {
	#[must_use]
	pub fn a_time(&self) -> Option<Time> {
		self.a_time_r.or(self.a_time_s)
	}

	#[must_use]
	pub fn d_time(&self) -> Option<Time> {
		self.d_time_r.or(self.d_time_s)
	}
}

#[derive(Debug, PartialEq, Eq, PartialOrd, Ord, Hash, Serialize, Deserialize)]
#[serde(rename_all = "UPPERCASE")]
pub enum ProgType {
	Reported,
	Prognosed,
	Calculated,
	Corrected,
}

#[derive(Debug, PartialEq, Eq, PartialOrd, Ord, Hash, Serialize, Deserialize)]
pub struct Platform {
	pub txt: String,
	pub r#type: Option<PlatformType>,
}

#[derive(Debug, PartialEq, Eq, PartialOrd, Ord, Hash, Serialize, Deserialize)]
#[serde(rename_all = "UPPERCASE")]
pub enum PlatformType {
	Pl,
	St,
}

#[derive(Debug, PartialEq, Eq, PartialOrd, Ord, Hash, Serialize, Deserialize)]
pub struct JourneyFilter {
	pub mode: JourneyFilterMode,
	pub r#type: JourneyFilterType,
	pub value: String,
}

impl JourneyFilter {
	#[must_use]
	pub const fn inc(r#type: JourneyFilterType, value: String) -> Self {
		Self {
			mode: JourneyFilterMode::Inc,
			r#type,
			value,
		}
	}
}

#[derive(Debug, PartialEq, Eq, PartialOrd, Ord, Hash, Serialize, Deserialize)]
#[serde(rename_all = "UPPERCASE")]
pub enum JourneyFilterMode {
	Inc,
}

#[derive(Debug, PartialEq, Eq, PartialOrd, Ord, Hash, Serialize, Deserialize)]
#[serde(rename_all = "UPPERCASE")]
pub enum JourneyFilterType {
	Prod,
	Group,
}
