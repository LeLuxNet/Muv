use std::{borrow::Cow, collections::HashMap};

use crate::{
	Board, Location, Network,
	hafas::{
		Common, Gid, Journey, JourneyDetails, Operator, Product, ProductCtx, Remark, Section,
		Station, Stop, TripSearch,
	},
};

use chrono::{NaiveDate, TimeDelta};
use chrono_tz::Tz;
use polyline::decode_polyline;

impl JourneyDetails {
	pub fn insert_into(self, network: &mut Network, tz: Tz, from_cls: fn(u16) -> crate::Mode) {
		self.journey
			.insert_into(&self.common, network, tz, self.fp_b, self.fp_e);
		self.common.insert_into(network, from_cls);
	}
}

impl TripSearch {
	pub fn insert_into(self, network: &mut Network, tz: Tz, from_cls: fn(u16) -> crate::Mode) {
		for con in self.out_con_l {
			for sec in con.sec_l {
				let Section::Journey { jny, .. } = sec else {
					continue;
				};

				jny.insert_into(&self.common, network, tz, self.fp_b, self.fp_e);
			}
		}

		self.common.insert_into(network, from_cls);
	}
}

impl Common {
	pub fn insert_into(self, network: &mut Network, from_cls: fn(u16) -> crate::Mode) {
		for prod in self.prod_l {
			if let Some((id, line)) = prod.into_line(&self.op_l, from_cls) {
				network.lines.insert(id, line);
			}
		}

		for op in self.op_l {
			network.organizations.insert(op.name.clone(), op.into());
		}

		for poly in self.poly_l {
			let shape = decode_polyline(&poly.crd_enc_yx, 5)
				.unwrap()
				.into_iter()
				.map(|c| Location::new(c.y, c.x))
				.collect();
			network.shapes.insert(poly.crd_enc_yx, shape);
		}
	}
}

impl From<Operator> for crate::Organization {
	fn from(value: Operator) -> Self {
		Self {
			name: Some(value.name),
			url: None,
			email: None,
			phone: None,
		}
	}
}

fn add_line_spaces(name: &str) -> String {
	let mut s = String::with_capacity(name.len() + 2);
	let mut digit = false;
	for c in name.chars() {
		if digit != c.is_ascii_digit() {
			digit = !digit;
			s.push(' ');
		}
		s.push(c);
	}
	s
}

impl Product {
	pub fn into_line(
		self,
		operators: &[Operator],
		from_cls: fn(u16) -> crate::Mode,
	) -> Option<(String, crate::Line)> {
		let id = self.prod_ctx.as_ref().unwrap().id()?;

		let line = crate::Line {
			authority: self.opr_x.map(|i| operators[i].name.clone()),
			operators: Vec::new(),
			mode: self.cls.map(from_cls),
			name: self.normalized_name().into_owned(),
			color: None,
			text_color: None,
		};

		Some((id, line))
	}

	#[must_use]
	pub fn normalized_name(&self) -> Cow<str> {
		self.normalized_name_special()
			.or_else(|| self.add_name.as_deref().map(Cow::Borrowed))
			.unwrap_or(Cow::Borrowed(&self.name))
	}

	fn normalized_name_special(&self) -> Option<Cow<str>> {
		let ctx = self.prod_ctx.as_ref()?;

		if ctx.cat_out_l.as_deref() == Some("Mitteldeutsche Regiobahn") {
			return ctx
				.line
				.as_deref()
				.filter(|line| line.starts_with('R'))
				.map(add_line_spaces)
				.map(Cow::Owned);
		}

		match ctx.cat_out.as_deref() {
			Some("STR" | "Bus" | "Fähre") => return ctx.line.as_deref().map(Cow::Borrowed),
			Some("FEX") => return Some(Cow::Borrowed("FEX")),
			Some("RE" | "RB") => return None,
			_ => {}
		}

		if ctx.cat_out_s.as_deref() == Some("DPN") {
			return ctx.line.as_deref().map(add_line_spaces).map(Cow::Owned);
		}

		None
	}
}

impl ProductCtx {
	fn id(&self) -> Option<String> {
		Some(format!(
			"{}-{}{}",
			self.admin.as_ref()?,
			self.cat_out.as_ref().unwrap(),
			self.match_id.as_ref().unwrap()
		))
	}
}

impl From<Station> for crate::Stop {
	fn from(value: Station) -> Self {
		Self {
			name: Some(value.name),
			code: None,
			r#ref: None,
			ifopt: None,
			uic: None,
			location: Some(value.crd.into()),
			parent: None,
		}
	}
}

impl Stop {
	#[must_use]
	pub fn schedule_stop(
		&self,
		locations: &[Station],
		remarks: &[Remark],
		tz: Tz,
	) -> (crate::Stop, crate::RouteStop) {
		let arrival = self.a_time_s.map(|t| t.to_time(self.a_tz_offset, tz));
		let departure = self.d_time_s.map(|t| t.to_time(self.d_tz_offset, tz));
		let platform = self
			.d_pltf_s
			.as_ref()
			.or(self.a_pltf_s.as_ref())
			.map(|p| p.txt.clone());

		let loc = &locations[self.loc_x.unwrap()];
		let ext_id = loc.ext_id.as_deref().unwrap_or_default();

		let route_stop = crate::RouteStop {
			id: platform
				.as_ref()
				.map_or_else(|| ext_id.into(), |p| format!("{ext_id}-{p}")),
			arrival,
			departure,
			board: self.d_in_s.unwrap_or(true).into(),
			alight: self.a_out_s.unwrap_or(true),
		};

		let mut route_stops = [route_stop];
		for msg in &self.msg_l {
			if let Some(rem_x) = msg.rem_x {
				apply_remark(&remarks[rem_x], &mut route_stops);
			}
		}
		let [route_stop] = route_stops;

		let mut uic = None;
		let mut ifopt = None;
		for gid in loc.global_id_l.iter().chain(&loc.gid_l) {
			match gid {
				Gid::Uic(s) => uic = Some(s.clone()),
				Gid::Ifopt(s) => ifopt = Some(s.clone()),
				Gid::Bst(_) => {}
			};
		}

		let stop = crate::Stop {
			name: Some(loc.name.clone()),
			code: None,
			r#ref: platform,
			ifopt,
			uic,
			location: Some(loc.crd.into()),
			parent: loc.ext_id.clone(),
		};

		(stop, route_stop)
	}

	#[must_use]
	pub fn realtime_stop(
		&self,
		locations: &[Station],
		remarks: &[Remark],
		tz: Tz,
	) -> (crate::Stop, crate::RouteStop) {
		let arrival = self.a_time_r.map(|t| t.to_time(self.a_tz_offset, tz));
		let departure = self.d_time_r.map(|t| t.to_time(self.d_tz_offset, tz));
		let platform = self
			.d_pltf_r
			.as_ref()
			.or(self.a_pltf_r.as_ref())
			.map(|p| p.txt.clone());

		let loc = &locations[self.loc_x.unwrap()];
		let ext_id = loc.ext_id.as_deref().unwrap_or_default();

		let route_stop = crate::RouteStop {
			id: platform
				.as_ref()
				.map_or_else(|| ext_id.into(), |p| format!("{ext_id}-{p}")),
			arrival,
			departure,
			board: self.d_in_r.or(self.d_in_s).unwrap_or(true).into(),
			alight: self.a_out_r.or(self.a_out_s).unwrap_or(true),
		};

		let mut route_stops = [route_stop];
		for msg in &self.msg_l {
			if let Some(rem_x) = msg.rem_x {
				apply_remark(&remarks[rem_x], &mut route_stops);
			}
		}
		let [route_stop] = route_stops;

		let mut uic = None;
		let mut ifopt = None;
		for gid in loc.global_id_l.iter().chain(&loc.gid_l) {
			match gid {
				Gid::Uic(s) => uic = Some(s.clone()),
				Gid::Ifopt(s) => ifopt = Some(s.clone()),
				Gid::Bst(_) => {}
			};
		}

		let stop = crate::Stop {
			name: Some(loc.name.clone()),
			code: None,
			r#ref: platform,
			ifopt,
			uic,
			location: Some(loc.crd.into()),
			parent: loc.ext_id.clone(),
		};

		(stop, route_stop)
	}
}

fn apply_remark(remark: &Remark, stops: &mut [crate::RouteStop]) {
	match remark.code.as_deref().unwrap_or_default() {
		"r?" | "kw" => {
			let offset = remark
				.txt_n
				.split_once(" Min.")
				.map(|(pre_min, _)| pre_min.rsplit(' ').next().unwrap())
				.and_then(|min| min.parse().ok())
				.map(TimeDelta::minutes);

			let number = remark.txt_n.rsplit_once(": ").map(|(_, phone)| phone);

			for stop in stops {
				if stop.board == Board::Yes {
					stop.board = Board::Phone {
						offset,
						number: number.map(ToOwned::to_owned),
					}
				}
			}
		}
		_ => {}
	}
}

impl Journey {
	pub fn insert_into(
		self,
		common: &Common,
		network: &mut Network,
		tz: Tz,
		from_date: NaiveDate,
		to_date: NaiveDate,
	) {
		let mut indicies = HashMap::new();

		let mut stops: Vec<_> = self
			.stop_l
			.into_iter()
			.enumerate()
			.map(|(i, s)| {
				indicies.insert(s.idx.unwrap(), i);
				let (schedule_stop, schedule) = s.schedule_stop(&common.loc_l, &common.rem_l, tz);
				network.stops.insert(schedule.id.clone(), schedule_stop);
				schedule
			})
			.collect();

		let mut restaurant = false;
		let mut ac = None;
		let mut outlets = None;
		for msg in self.msg_l {
			let (Some(from), Some(to)) = (
				msg.f_idx.and_then(|i| usize::try_from(i).ok()),
				msg.t_idx.and_then(|i| usize::try_from(i).ok()),
			) else {
				continue;
			};

			let Some(rem_x) = msg.rem_x else {
				continue;
			};

			let remark = &common.rem_l[rem_x];
			apply_remark(remark, &mut stops[indicies[&from]..=indicies[&to]]);

			if from != 0 || to != stops.len() - 1 {
				continue;
			}
			match remark.code.as_deref().unwrap_or_default() {
				"BR" => restaurant = true,
				"KL" => ac = Some(true),
				"LS" => outlets = Some(true),
				_ => {}
			};
		}

		let shape = self
			.poly_g
			.map(|poly| common.poly_l[poly.poly_xl.0].crd_enc_yx.clone());

		let s_day = self.s_days_l.into_iter().next().unwrap();
		let s_day_r = s_day.s_days_r.unwrap();
		network
			.services
			.insert(s_day_r.clone(), crate::Service::Bits {
				from_date,
				to_date: Some(to_date),
				days: s_day.s_days_b,
			});

		let route = crate::Route {
			service: s_day_r,
			line: Some(
				common.prod_l[self.prod_x]
					.prod_ctx
					.as_ref()
					.unwrap()
					.id()
					.unwrap(),
			),
			shape,
			stops,

			restaurant: Some(restaurant),
			ac,
			outlets,
		};

		network.routes.insert(self.jid.to_v1_string(), route);
	}
}
