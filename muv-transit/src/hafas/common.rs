use serde::{Deserialize, Deserializer, Serialize, de};

use crate::{
	Mode,
	hafas::{Him, HimMsgEdge, JourneyId, Station},
};

#[derive(Debug, Default, Deserialize)]
#[serde(rename_all = "camelCase")]
pub struct Common {
	#[serde(default)]
	pub dir_l: Vec<Direction>,
	#[serde(default)]
	pub loc_l: Vec<Station>,
	#[serde(default)]
	pub op_l: Vec<Operator>,
	#[serde(default)]
	pub ico_l: Vec<Icon>,
	#[serde(default)]
	pub poly_l: Vec<Polyline>,
	#[serde(default)]
	pub prod_l: Vec<Product>,
	#[serde(default)]
	pub rem_l: Vec<Remark>,
	#[serde(default)]
	pub him_l: Vec<Him>,
	#[serde(default)]
	pub him_msg_edge_l: Vec<HimMsgEdge>,
	#[serde(default)]
	pub tcoc_l: Vec<Tcoc>,
}

#[derive(Debug, Deserialize)]
pub struct Direction {
	pub txt: String,
	pub flg: Option<String>,
}

#[derive(Debug, Deserialize)]
#[serde(rename_all = "camelCase")]
pub struct Operator {
	pub id: Option<String>,
	pub ico_x: usize,
	pub name: String,
}

#[derive(Debug, Deserialize)]
#[serde(rename_all = "camelCase")]
pub struct Icon {
	pub res: Option<String>,
	pub txt: Option<String>,
	pub txt_a: Option<String>,
	pub fg: Option<Color>,
	pub bg: Option<Color>,
	pub shp: Option<String>,
}

#[derive(Debug, Deserialize)]
pub struct Color {
	pub r: u8,
	pub b: u8,
	pub g: u8,
}

#[derive(Debug, Deserialize)]
#[serde(rename_all = "camelCase")]
pub struct Polyline {
	pub crd_enc_dist: Option<String>,
	pub crd_enc_f: String,
	pub crd_enc_s: String,
	#[serde(rename = "crdEncYX")]
	pub crd_enc_yx: String,
	pub crd_enc_z: Option<String>,

	pub dim: u32,
	pub delta: bool,
	#[serde(default)]
	pub pp_loc_ref_l: Vec<PolylineLocationRef>,
}

#[derive(Debug, Deserialize)]
#[serde(rename_all = "camelCase")]
pub struct PolylineLocationRef {
	pub loc_x: usize,
	pub pp_idx: usize,
}

#[derive(Debug, Serialize, Deserialize)]
#[serde(rename_all = "camelCase")]
pub struct Product {
	pub add_name: Option<String>,
	pub cls: Option<u16>,
	#[serde(default)]
	pub him_id_l: Vec<String>,
	pub ico_x: usize,
	pub name: String,
	pub name_s: Option<String>,
	pub number: Option<String>,
	pub opr_x: Option<usize>,
	pub pid: Option<String>,
	pub prod_ctx: Option<ProductCtx>,
}

#[derive(Debug, Clone, Copy, Serialize, Deserialize)]
pub struct ProductClass(pub u16);

impl From<ProductClass> for Mode {
	fn from(value: ProductClass) -> Self {
		match value.0 {
			1 | 2 => Self::LongDistanceRail,
			4 | 8 => Self::RegionalRail,
			16 => Self::UrbanRail,
			32 => Self::Bus,
			64 => Self::Ferry,
			128 => Self::Metro,
			256 => Self::Tram,
			512 => Self::Taxi,
			_ => todo!("{value:?}"),
		}
	}
}

#[derive(Debug, Serialize, Deserialize)]
#[serde(rename_all = "camelCase")]
pub struct ProductCtx {
	pub admin: Option<String>,
	pub cat_code: Option<String>,
	pub cat_in: Option<String>,
	pub cat_out: Option<String>,
	pub cat_out_l: Option<String>,
	pub cat_out_s: Option<String>,
	pub line: Option<String>,
	pub line_id: Option<String>,
	pub match_id: Option<String>,
	pub name: String,
	pub num: Option<String>,
}

#[derive(Debug, Deserialize)]
#[serde(rename_all = "camelCase")]
pub struct Remark {
	pub code: Option<String>,
	pub prio: Option<u32>,
	pub jid: Option<JourneyId>,
	pub txt_n: String,
	pub txt_s: Option<String>,
	pub r#type: RemarkType,
	pub url: Option<String>,
}

#[derive(Debug, Deserialize)]
pub enum RemarkType {
	#[serde(rename = "A")]
	Attribute,
	C,
	#[serde(rename = "D")]
	Delay,
	#[serde(rename = "G")]
	PlatformChange,
	#[serde(rename = "H")]
	Hint,
	I,
	K,
	L,
	N,
	#[serde(rename = "M")]
	HimMessage,
	O,
	#[serde(rename = "P")]
	Canceled,
	Q,
	#[serde(rename = "R")]
	Realtime,
	#[serde(rename = "Y")]
	PartCanceled,
	#[serde(rename = "TAR")]
	Tarif,
}

#[derive(Debug, Deserialize)]
pub struct Tcoc {
	#[serde(rename = "c")]
	pub class: Class,
	pub r: Demand,
}

#[derive(Debug, Deserialize)]
#[serde(rename_all = "UPPERCASE")]
pub enum Class {
	First,
	Second,
}

#[derive(Debug)]
pub enum Demand {
	Low,
	Medium,
	High,
	ExceptionallyHigh,
}

impl<'de> Deserialize<'de> for Demand {
	fn deserialize<D: Deserializer<'de>>(deserializer: D) -> Result<Self, D::Error> {
		let n = u8::deserialize(deserializer)?;
		Ok(match n {
			1 => Self::Low,
			2 => Self::Medium,
			3 => Self::High,
			4 => Self::ExceptionallyHigh,
			_ => {
				return Err(de::Error::unknown_variant(&n.to_string(), &[
					"1", "2", "3", "4",
				]));
			}
		})
	}
}
