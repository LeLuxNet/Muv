use chrono::NaiveDate;
use serde::Deserialize;

use crate::hafas::{Common, Journey, Time, date};

#[cfg(any(feature = "blocking", feature = "tokio"))]
use chrono::TimeDelta;
#[cfg(any(feature = "blocking", feature = "tokio"))]
use serde::Serialize;

#[cfg(any(feature = "blocking", feature = "tokio"))]
use crate::{
	Result,
	hafas::{Client, JourneyFilter, Rectangle},
};

#[derive(Debug, Deserialize)]
#[serde(rename_all = "camelCase")]
pub struct JourneyGeoPos {
	pub common: Common,

	#[serde(with = "date")]
	pub date: NaiveDate,
	pub time: Time,

	pub jny_l: Vec<Journey>,

	#[serde(with = "date")]
	pub fp_b: NaiveDate,
	#[serde(with = "date")]
	pub fp_e: NaiveDate,
}

#[cfg(any(feature = "blocking", feature = "tokio"))]
#[derive(Serialize)]
#[serde(rename_all = "camelCase")]
struct JourneyGeoPosReq<'a> {
	rect: Rectangle,
	jny_fltr_l: &'a [JourneyFilter],
	per_size: Option<i64>,
	per_step: Option<i64>,
}

#[cfg(feature = "blocking")]
impl Client<crate::Blocking> {
	pub fn journey_geo_pos(
		&self,
		bbox: Rectangle,
		filter: &[JourneyFilter],
		frames: Option<(TimeDelta, u16)>,
	) -> Result<JourneyGeoPos> {
		self.request(
			"JourneyGeoPos",
			&JourneyGeoPosReq {
				rect: bbox,
				jny_fltr_l: filter,
				per_size: frames.map(|(d, _)| d.num_milliseconds()),
				per_step: frames.map(|(d, f)| d.num_milliseconds() / i64::from(f)),
			},
		)
	}
}

#[cfg(feature = "tokio")]
impl Client<crate::Tokio> {
	pub async fn journey_geo_pos(
		&self,
		bbox: Rectangle,
		filter: &[JourneyFilter],
		frames: Option<(TimeDelta, u16)>,
	) -> Result<JourneyGeoPos> {
		self.request(
			"JourneyGeoPos",
			&JourneyGeoPosReq {
				rect: bbox,
				jny_fltr_l: filter,
				per_size: frames.map(|(d, _)| d.num_milliseconds()),
				per_step: frames.map(|(d, f)| d.num_milliseconds() / i64::from(f)),
			},
		)
		.await
	}
}
