use serde::Deserialize;

use crate::hafas::{Common, Station};

#[cfg(any(feature = "blocking", feature = "tokio"))]
use serde::Serialize;

#[cfg(any(feature = "blocking", feature = "tokio"))]
use crate::{
	Result,
	hafas::{Client, Location, StationInput},
};

#[derive(Debug, Deserialize)]
pub struct LocMatch {
	#[serde(default)]
	pub common: Common,
	pub r#match: LocMatchMatch,
}

#[derive(Debug, Deserialize)]
#[serde(rename_all = "camelCase")]
pub struct LocMatchMatch {
	#[serde(default)]
	pub loc_l: Vec<Station>,
}

#[cfg(any(feature = "blocking", feature = "tokio"))]
#[derive(Serialize)]
struct LocMatchRequest {
	input: LocMatchInput,
}

#[cfg(any(feature = "blocking", feature = "tokio"))]
#[derive(Serialize)]
#[serde(rename_all = "camelCase")]
struct LocMatchInput {
	loc: StationInput,
	max_loc: Option<u8>,
	field: LocMatchField,
}

#[cfg(any(feature = "blocking", feature = "tokio"))]
#[derive(Serialize)]
enum LocMatchField {
	S,
}

#[cfg(feature = "blocking")]
impl Client<crate::Blocking> {
	pub fn loc_match(&self, query: Location, results: Option<u8>) -> Result<LocMatch> {
		self.request(
			"LocMatch",
			&LocMatchRequest {
				input: LocMatchInput {
					loc: StationInput(query),
					max_loc: results,
					field: LocMatchField::S,
				},
			},
		)
	}
}

#[cfg(feature = "tokio")]
impl Client<crate::Tokio> {
	pub async fn loc_match(&self, query: Location, results: Option<u8>) -> Result<LocMatch> {
		self.request(
			"LocMatch",
			&LocMatchRequest {
				input: LocMatchInput {
					loc: StationInput(query),
					max_loc: results,
					field: LocMatchField::S,
				},
			},
		)
		.await
	}
}
