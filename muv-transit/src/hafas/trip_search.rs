use chrono::{NaiveDate, NaiveDateTime};
use serde::{Deserialize, Serialize};

use crate::hafas::{
	Common, Journey, Location, Message, PolyG, ScheduleDay, StationInput, Stop, Time, date,
	opt_date,
};

#[cfg(any(feature = "blocking", feature = "tokio"))]
use crate::{Result, hafas::Client};

#[must_use]
#[derive(Serialize)]
#[serde(rename_all = "camelCase")]
pub struct TripSearchRequest {
	dep_loc_l: (StationInput,),
	via_loc_l: Vec<Via>,
	arr_loc_l: (StationInput,),

	#[serde(with = "opt_date")]
	out_date: Option<NaiveDate>,
	out_time: Option<Time>,

	num_f: Option<u8>,
	max_chg: Option<i16>,

	get_polyline: bool,
}

impl TripSearchRequest {
	pub const fn new(dep: Location, arr: Location) -> Self {
		Self {
			dep_loc_l: (StationInput(dep),),
			via_loc_l: Vec::new(),
			arr_loc_l: (StationInput(arr),),

			out_date: None,
			out_time: None,

			num_f: None,
			max_chg: None,

			get_polyline: false,
		}
	}

	pub fn at(mut self, time: NaiveDateTime) -> Self {
		self.out_date = Some(time.date());
		self.out_time = Some(time.time().into());
		self
	}

	pub fn via<I: IntoIterator<Item = Location>>(mut self, via: I) -> Self {
		self.via_loc_l = via
			.into_iter()
			.map(|l| Via {
				loc: StationInput(l),
			})
			.collect();
		self
	}

	pub const fn results(mut self, n: u8) -> Self {
		self.num_f = Some(n);
		self
	}

	pub fn max_changes<C: Into<Changes>>(mut self, changes: C) -> Self {
		self.max_chg = Some(i16::from(changes.into()));
		self
	}

	pub const fn polyline(mut self, polyline: bool) -> Self {
		self.get_polyline = polyline;
		self
	}
}

pub enum Changes {
	Unlimited,
	Limited(u8),
}

impl From<Changes> for i16 {
	fn from(value: Changes) -> Self {
		match value {
			Changes::Unlimited => -1,
			Changes::Limited(n) => n.into(),
		}
	}
}

impl<I: Into<u8>> From<I> for Changes {
	fn from(value: I) -> Self {
		Self::Limited(value.into())
	}
}

#[derive(Serialize)]
pub struct Via {
	loc: StationInput,
}

#[derive(Debug, Deserialize)]
#[serde(rename_all = "camelCase")]
pub struct TripSearch {
	pub common: Common,

	pub out_con_l: Vec<Con>,
	pub out_ctx_scr_b: String,
	pub out_ctx_scr_f: String,

	#[serde(with = "date")]
	pub fp_b: NaiveDate,
	#[serde(with = "date")]
	pub fp_e: NaiveDate,
}

#[derive(Debug, Deserialize)]
#[serde(rename_all = "camelCase")]
pub struct Con {
	pub cid: String,
	#[serde(with = "date")]
	pub date: NaiveDate,
	pub dur: Time,
	pub dur_s: Option<Time>,
	pub dur_r: Option<Time>,
	pub chg: u8,
	pub s_days: ScheduleDay,
	#[serde(default)]
	pub is_alt: bool,

	pub dep: Stop,
	pub arr: Stop,
	pub sec_l: Vec<Section>,
	pub freq: Option<Freq>,
	pub trf_res: Option<TarifRes>,

	pub recon: Recon,
}

#[derive(Debug, Deserialize)]
#[serde(rename_all = "camelCase")]
pub struct Freq {
	pub min_c: u8,
}

#[derive(Debug, Deserialize)]
pub struct Recon {
	pub ctx: String,
}

#[derive(Debug, Deserialize)]
#[serde(rename_all = "camelCase")]
pub struct TarifRes {
	pub total_price: Option<Price>,
}

#[derive(Debug, Deserialize)]
pub struct Price {
	pub amount: u32,
	pub currency: String,
}

#[derive(Debug, Deserialize)]
#[serde(tag = "type", rename_all = "UPPERCASE")]
pub enum Section {
	Walk {
		dep: Stop,
		arr: Stop,

		gis: Gis,
	},
	#[serde(rename = "TRSF")]
	Transfer {
		dep: Stop,
		arr: Stop,

		gis: Gis,
	},
	#[serde(rename = "JNY")]
	Journey {
		dep: Stop,
		arr: Stop,

		jny: Box<Journey>,
	},
}

#[derive(Debug, Deserialize)]
#[serde(rename_all = "camelCase")]
pub struct Gis {
	pub ctx: Option<String>,
	pub dir_geo: Option<u8>,
	pub dist: Option<u32>,
	pub dur_s: Option<Time>, // TODO: TimeDelta
	pub get_descr: Option<bool>,
	pub get_poly: Option<bool>,
	#[serde(default)]
	pub msg_l: Vec<Message>,
	pub poly_g: Option<PolyG>,
	pub prod_x: usize,
}

#[cfg(feature = "blocking")]
impl Client<crate::Blocking> {
	pub fn trip_search(&self, req: &TripSearchRequest) -> Result<TripSearch> {
		self.request("TripSearch", req)
	}
}

#[cfg(feature = "tokio")]
impl Client<crate::Tokio> {
	pub async fn trip_search(&self, req: &TripSearchRequest) -> Result<TripSearch> {
		self.request("TripSearch", req).await
	}
}
