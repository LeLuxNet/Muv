use serde::{Deserialize, Serialize};

use crate::hafas::{Common, Journey};

#[cfg(any(feature = "blocking", feature = "tokio"))]
use crate::{
	Result,
	hafas::{Client, Location, StationInput, Time, date},
};
#[cfg(any(feature = "blocking", feature = "tokio"))]
use chrono::{NaiveDate, NaiveDateTime, TimeDelta};

#[derive(Debug, Serialize)]
pub enum StationBoardType {
	#[serde(rename = "ARR")]
	Arrival,
	#[serde(rename = "DEP")]
	Departure,
}

#[derive(Debug, Deserialize)]
#[serde(rename_all = "camelCase")]
pub struct StationBoard {
	pub common: Common,
	#[serde(default)]
	pub jny_l: Vec<Journey>,
}

#[cfg(any(feature = "blocking", feature = "tokio"))]
#[derive(Serialize)]
#[serde(rename_all = "camelCase")]
struct StationBoardRequest {
	r#type: StationBoardType,
	time: Time,
	#[serde(with = "date")]
	date: NaiveDate,
	dur: i64,
	stb_loc: StationInput,
}

#[cfg(feature = "blocking")]
impl Client<crate::Blocking> {
	pub fn station_board(
		&self,
		r#type: StationBoardType,
		station: Location,
		time: NaiveDateTime,
		duration: TimeDelta,
	) -> Result<StationBoard> {
		self.request(
			"StationBoard",
			&StationBoardRequest {
				r#type,
				time: time.time().into(),
				date: time.date(),
				dur: duration.num_minutes(),
				stb_loc: StationInput(station),
			},
		)
	}
}

#[cfg(feature = "tokio")]
impl Client<crate::Tokio> {
	pub async fn station_board(
		&self,
		r#type: StationBoardType,
		station: Location,
		time: NaiveDateTime,
		duration: TimeDelta,
	) -> Result<StationBoard> {
		self.request(
			"StationBoard",
			&StationBoardRequest {
				r#type,
				time: time.time().into(),
				date: time.date(),
				dur: duration.num_minutes(),
				stb_loc: StationInput(station),
			},
		)
		.await
	}
}

#[cfg(any(feature = "blocking", feature = "tokio"))]
#[cfg(test)]
mod tests {
	use chrono::{Duration, Utc};

	use crate::hafas::{Config, Location, OEBB, StationBoardType, VMT};

	const STATIONS: &[(Config, &str)] = &[
		(OEBB, "Wien"),
		(OEBB, "Salzburg Hbf"),
		(OEBB, "Linz/Donau Hbf"),
		(VMT, "Erfurt, Hauptbahnhof"),
		(VMT, "Weimar, Goetheplatz / Zentrum"),
		(VMT, "Jena, Stadtzentrum Löbdergraben"),
	];

	#[test]
	#[cfg(feature = "blocking")]
	fn fetch_blocking() {
		for (config, name) in STATIONS {
			config
				.client::<crate::Blocking>()
				.station_board(
					StationBoardType::Departure,
					Location::new_name(*name),
					config.time(&Utc::now()),
					Duration::minutes(30),
				)
				.unwrap_or_else(|e| panic!("{:?} {name:?}: {e:?}", config.endpoint));
		}
	}

	#[tokio::test]
	#[cfg(feature = "tokio")]
	async fn fetch_tokio() {
		for (config, name) in STATIONS {
			config
				.client::<crate::Tokio>()
				.station_board(
					StationBoardType::Arrival,
					Location::new_name(*name),
					config.time(&Utc::now()),
					Duration::minutes(30),
				)
				.await
				.unwrap_or_else(|e| panic!("{:?} {name:?}: {e:?}", config.endpoint));
		}
	}
}
