use chrono_tz::Europe;

use crate::{
	Mode, cls_map,
	hafas::{ClsMap, Config, RequestAuth, RequestClient, RequestClientType, RequestV},
};

pub const VMT: Config = Config {
	endpoint: "https://vmt.hafas.cloud/mct/mgate",
	timezone: Europe::Berlin,

	client: RequestClient {
		r#type: RequestClientType::Web,
		id: "HAFAS",
		v: RequestV::String("1.0.0"),
		name: "MCT",
	},
	ext: None,
	ver: "1.59",
	auth: RequestAuth::Aid {
		aid: "I7mCt5tAtI0n8oArD",
	},
	salt: None,

	cfg_hash: None,
	rt_mode: false,

	cls_map: cls_map! {
		LongDistanceRail => 1 | 2 | 4,
		RegionalRail => 8 | 16,
		Tram => 32,
		Bus => 256 | 512,
	},
};

pub const OEBB: Config = Config {
	endpoint: "https://fahrplan.oebb.at/bin/mgate.exe",
	timezone: Europe::Vienna,

	client: RequestClient {
		r#type: RequestClientType::Web,
		id: "OEBB",
		v: RequestV::Number(21_402),
		name: "webapp",
	},
	ext: Some("OEBB.14"),
	ver: "1.72",
	auth: RequestAuth::Aid {
		aid: "5vHavmuWPWIfetEe",
	},
	salt: None,

	cfg_hash: None,
	rt_mode: false,

	cls_map: ClsMap {
		from: |cls| match cls {
			1 | 2 | 4 | 8 | 4096 => Mode::LongDistanceRail,
			16 | 48 => Mode::RegionalRail,
			32 => Mode::UrbanRail,
			64 => Mode::Bus,
			128 => Mode::Ferry,
			256 => Mode::Metro,
			512 => Mode::Tram,
			_ => todo!("{cls}"),
		},
		to: |mode| match mode {
			Mode::LongDistanceRail => 1 | 2 | 4 | 8 | 4096,
			Mode::RegionalRail => 16,
			Mode::UrbanRail => 32,
			Mode::Bus => 64,
			Mode::Ferry => 128,
			Mode::Metro => 256,
			Mode::Tram => 512,
			_ => 0,
		},
	},
};
