use chrono::NaiveDate;
use serde::Deserialize;

use crate::hafas::{Common, Journey, date};

#[cfg(any(feature = "blocking", feature = "tokio"))]
use crate::{
	Result,
	hafas::{Client, JourneyId},
};
#[cfg(any(feature = "blocking", feature = "tokio"))]
use serde::Serialize;

#[derive(Debug, Deserialize)]
#[serde(rename_all = "camelCase")]
pub struct JourneyDetails {
	pub common: Common,
	#[serde(with = "date")]
	pub fp_b: NaiveDate,
	#[serde(with = "date")]
	pub fp_e: NaiveDate,
	pub journey: Journey,
}

#[cfg(any(feature = "blocking", feature = "tokio"))]
#[derive(Serialize)]
struct JourneyDetailsRequest {
	jid: String,
}

#[cfg(feature = "blocking")]
impl Client<crate::Blocking> {
	pub fn journey_details(&self, jid: &JourneyId) -> Result<JourneyDetails> {
		self.request(
			"JourneyDetails",
			&JourneyDetailsRequest {
				jid: jid.to_v1_string(),
			},
		)
	}
}

#[cfg(feature = "tokio")]
impl Client<crate::Tokio> {
	pub async fn journey_details(&self, jid: &JourneyId) -> Result<JourneyDetails> {
		self.request(
			"JourneyDetails",
			&JourneyDetailsRequest {
				jid: jid.to_v1_string(),
			},
		)
		.await
	}
}
