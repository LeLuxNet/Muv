use std::{
	error,
	fmt::{self, Display, Formatter},
};

use chrono::{DateTime, NaiveDateTime, TimeZone};
use chrono_tz::Tz;
use scooter::Agent;
use serde::Serialize;

mod types;
pub use types::*;

mod network;

mod common;
pub use common::*;

mod time;
pub use time::*;

mod station_board;
pub use station_board::*;
mod journey_details;
pub use journey_details::*;
mod trip_search;
pub use trip_search::*;
mod him_geo_pos;
pub use him_geo_pos::*;
mod journey_geo_pos;
pub use journey_geo_pos::*;
mod loc_match;
pub use loc_match::*;

mod profiles;
pub use profiles::*;

#[cfg(any(feature = "blocking", feature = "tokio"))]
use crate::Result;
#[cfg(any(feature = "blocking", feature = "tokio"))]
use scooter::DEBUG;
#[cfg(any(feature = "blocking", feature = "tokio"))]
use serde::{Deserialize, de::DeserializeOwned};
#[cfg(any(feature = "blocking", feature = "tokio"))]
use std::{borrow::Cow, result};

#[cfg(feature = "tokio")]
use std::future::Future;

#[derive(Debug, Clone)]
pub struct Client<A> {
	pub agent: Agent<A>,
	pub config: &'static Config,
}

#[derive(Debug, Clone, Copy)]
pub struct Config {
	pub endpoint: &'static str,
	pub timezone: Tz,

	pub client: RequestClient,
	pub ext: Option<&'static str>,
	pub ver: &'static str,
	pub auth: RequestAuth,
	pub salt: Option<&'static [u8]>,

	pub cfg_hash: Option<&'static str>,
	pub rt_mode: bool,

	pub cls_map: ClsMap,
}

impl Config {
	#[must_use]
	pub fn client<A: Default>(&'static self) -> Client<A> {
		Client {
			agent: Agent::default(),
			config: self,
		}
	}

	#[must_use]
	pub fn time<Tz: TimeZone>(&self, time: &DateTime<Tz>) -> NaiveDateTime {
		time.with_timezone(&self.timezone).naive_local()
	}
}

#[derive(Debug, Clone, Copy)]
pub struct ClsMap {
	pub from: fn(u16) -> crate::Mode,
	pub to: fn(crate::Mode) -> u16,
}

#[macro_export]
macro_rules! cls_map {
    ($( $mode:ident => $($val:literal)|+ , )+) => {
        $crate::hafas::ClsMap {
            from: |cls| match cls {
                $( $($val)|+ => $crate::Mode::$mode, )+
                _ => todo!("{cls}")
            },
            to: |mode| match mode {
                $( $crate::Mode::$mode => $($val)|+, )+
                _ => 0,
            },
        }
    };
}

#[derive(Debug, Clone, Copy, Serialize)]
#[serde(untagged)]
pub enum RequestV {
	String(&'static str),
	Number(u32),
}

#[derive(Debug, Clone, Copy, Serialize)]
pub struct RequestClient {
	pub r#type: RequestClientType,
	pub id: &'static str,
	pub v: RequestV,
	pub name: &'static str,
}

#[derive(Debug, Clone, Copy, PartialEq, Eq, Hash, Serialize)]
#[serde(rename_all = "UPPERCASE")]
pub enum RequestClientType {
	Web,
	#[serde(rename = "AND")]
	Android,
	#[serde(rename = "IPH")]
	Iphone,
}

#[derive(Debug, Clone, Copy, Serialize)]
#[serde(tag = "type", rename_all = "UPPERCASE")]
pub enum RequestAuth {
	Aid { aid: &'static str },
}

#[cfg(any(feature = "blocking", feature = "tokio"))]
#[derive(Serialize)]
#[serde(rename_all = "camelCase")]
struct Request<'a, T> {
	client: RequestClient,
	lang: String,
	#[serde(skip_serializing_if = "Option::is_none")]
	ext: Option<&'static str>,
	ver: &'static str,
	formatted: bool,
	svc_req_l: (RequestL<'a, T>,),
	auth: RequestAuth,
}

#[cfg(any(feature = "blocking", feature = "tokio"))]
#[derive(Serialize)]
struct RequestL<'a, T> {
	cfg: RequestCfg,
	meth: &'a str,
	req: T,
}

#[cfg(any(feature = "blocking", feature = "tokio"))]
#[derive(Serialize)]
#[serde(rename_all = "camelCase")]
struct RequestCfg {
	#[serde(skip_serializing_if = "Option::is_none")]
	cfg_hash: Option<&'static str>,
	#[serde(skip_serializing_if = "Option::is_none")]
	rt_mode: Option<RtMode>,
}

#[cfg(any(feature = "blocking", feature = "tokio"))]
#[derive(Serialize)]
#[serde(rename_all = "UPPERCASE")]
enum RtMode {
	Hybrid,
}

#[cfg(any(feature = "blocking", feature = "tokio"))]
#[derive(Deserialize)]
#[serde(rename_all = "camelCase")]
struct Response<T> {
	svc_res_l: Option<(ResponseL<T>,)>,
	err: String,
	err_txt: Option<String>,
}

#[cfg(any(feature = "blocking", feature = "tokio"))]
impl<T> Response<T> {
	fn ok(self) -> result::Result<T, Error> {
		let Some((res_l,)) = self.svc_res_l else {
			return Err(Error::Global {
				err: self.err,
				txt: self.err_txt.unwrap(),
			});
		};

		let Some(res) = res_l.res else {
			return Err(Error::Request {
				err: res_l.err,
				txt: res_l.err_txt.unwrap(),
				txt_out: res_l.err_txt_out.unwrap(),
			});
		};

		Ok(res)
	}
}

#[cfg(any(feature = "blocking", feature = "tokio"))]
#[derive(Deserialize)]
#[serde(rename_all = "camelCase")]
struct ResponseL<T> {
	res: Option<T>,
	err: String,
	err_txt: Option<String>,
	err_txt_out: Option<String>,
}

#[cfg(feature = "blocking")]
impl Client<crate::Blocking> {
	pub fn request<Q: Serialize, S: DeserializeOwned>(&self, method: &str, req: &Q) -> Result<S> {
		let (url, req_body) = self.config.format_request(method, req);

		let res: Response<S> = self
			.agent
			.post(url)
			.debug(
				format!(
					"hafas_{}_{}",
					self.config.client.id, self.config.client.name,
				),
				method,
			)
			.bytes(req_body)
			.send()?
			.json()?;

		Ok(res.ok()?)
	}
}

#[cfg(feature = "tokio")]
impl Client<crate::Tokio> {
	pub fn request<Q: Serialize, S: DeserializeOwned>(
		&self,
		method: &str,
		req: &Q,
	) -> impl Future<Output = Result<S>> {
		let (url, req_body) = self.config.format_request(method, req);

		let req = self
			.agent
			.post(url)
			.debug(
				format!(
					"hafas_{}_{}",
					self.config.client.id, self.config.client.name,
				),
				method,
			)
			.header("User-Agent", "muv-transit/0.1")
			.bytes(req_body)
			.send();

		async {
			let res: Response<S> = req.await?.json()?;
			Ok(res.ok()?)
		}
	}
}

#[cfg(any(feature = "blocking", feature = "tokio"))]
impl Config {
	fn format_request<'a, Q: Serialize>(
		&'a self,
		method: &str,
		req: &Q,
	) -> (Cow<'a, str>, Vec<u8>) {
		let body = serde_json::to_vec(&Request {
			client: self.client,
			lang: "de".into(),
			ext: self.ext,
			ver: self.ver,
			formatted: DEBUG,
			svc_req_l: (RequestL {
				cfg: RequestCfg {
					cfg_hash: self.cfg_hash,
					rt_mode: self.rt_mode.then_some(RtMode::Hybrid),
				},
				meth: method,
				req,
			},),
			auth: self.auth,
		})
		.unwrap();

		let url = if let Some(salt) = self.salt {
			let mut checksum = md5::Context::new();
			checksum.consume(&body);
			checksum.consume(salt);
			Cow::Owned(format!(
				"{}?checksum={:x}",
				self.endpoint,
				checksum.compute()
			))
		} else {
			Cow::Borrowed(self.endpoint)
		};

		(url, body)
	}
}

#[derive(Debug)]
pub enum Error {
	Global {
		err: String,
		txt: String,
	},
	Request {
		err: String,
		txt: String,
		txt_out: String,
	},
}

impl Display for Error {
	fn fmt(&self, f: &mut Formatter<'_>) -> fmt::Result {
		match self {
			Self::Global { err, txt } => write!(f, "global {err} error: {txt}"),
			Self::Request {
				err,
				txt,
				txt_out: _,
			} => write!(f, "request {err} error: {txt}"),
		}
	}
}

impl error::Error for Error {}
