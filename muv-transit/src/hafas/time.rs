use std::{
	error::Error,
	fmt::{self, Debug, Display, Formatter},
	str::FromStr,
};

use chrono::{FixedOffset, NaiveDate, NaiveTime, Timelike};
use chrono_tz::Tz;
use serde::{
	Deserialize, Deserializer, Serialize, Serializer,
	de::{self, Visitor},
};

use crate::{PrimitiveTime, Timezone};

fn last_num<T: FromStr + Default>(s: &str, width: usize) -> Option<(&str, T)> {
	let len = s.len();
	if len == 0 {
		Some(("", T::default()))
	} else if len < width {
		let n = s.parse().ok()?;
		Some(("", n))
	} else {
		let (rest, n_str) = s.split_at(s.len() - width);
		let n = n_str.parse().ok()?;
		Some((rest, n))
	}
}

#[derive(Clone, Copy, PartialEq, Eq, PartialOrd, Ord, Hash)]
pub struct Time {
	pub days: u8,
	pub hours: u8,
	pub minutes: u8,
	pub seconds: Option<u8>,
}

impl Debug for Time {
	fn fmt(&self, f: &mut Formatter<'_>) -> fmt::Result {
		if self.days != 0 {
			write!(f, "{}-", self.days)?;
		}
		write!(f, "{:02}:{:02}", self.hours, self.minutes)?;
		if let Some(seconds) = self.seconds {
			write!(f, ":{seconds:02}")?;
		}
		Ok(())
	}
}

impl From<Time> for PrimitiveTime {
	fn from(value: Time) -> Self {
		Self {
			hours: value.days * 24 + value.hours,
			minutes: value.minutes,
			seconds: value.seconds.unwrap_or_default(),
		}
	}
}

#[allow(clippy::fallible_impl_from)]
impl From<NaiveTime> for Time {
	fn from(value: NaiveTime) -> Self {
		Self {
			days: 0,
			hours: value.hour().try_into().unwrap(),
			minutes: value.minute().try_into().unwrap(),
			seconds: Some(value.second().try_into().unwrap()),
		}
	}
}

impl Time {
	pub fn from_str(s: &str, with_seconds: bool) -> Result<Self, ParseTimeError> {
		let s = s.strip_prefix('-').unwrap_or(s);

		let (rest, seconds) = if with_seconds {
			let (rest, seconds) = last_num(s, 2).ok_or(ParseTimeError::Second)?;
			(rest, Some(seconds))
		} else {
			(s, None)
		};

		let (rest, minutes) = last_num(rest, 2).ok_or(ParseTimeError::Minute)?;
		let (rest, hours) = last_num(rest, 2).ok_or(ParseTimeError::Hour)?;
		let days = if rest.is_empty() {
			0
		} else {
			rest.parse().map_err(|_| ParseTimeError::Day)?
		};

		Ok(Self {
			days,
			hours,
			minutes,
			seconds,
		})
	}

	pub fn to_time(self, offset: Option<i32>, tz: Tz) -> crate::Time {
		let tz = if let Some(offset) = offset {
			Timezone::Fixed(FixedOffset::east_opt(offset * 60).unwrap())
		} else {
			Timezone::Named(tz)
		};
		crate::Time::new(self.into(), tz)
	}
}

#[derive(Debug, Clone, Copy)]
pub enum ParseTimeError {
	Day,
	Hour,
	Minute,
	Second,
}

impl Display for ParseTimeError {
	fn fmt(&self, f: &mut Formatter<'_>) -> fmt::Result {
		match self {
			Self::Day => f.write_str("day invalid"),
			Self::Hour => f.write_str("hour invalid"),
			Self::Minute => f.write_str("minute invalid"),
			Self::Second => f.write_str("second invalid"),
		}
	}
}

impl Error for ParseTimeError {}

impl Serialize for Time {
	fn serialize<S: Serializer>(&self, serializer: S) -> Result<S::Ok, S::Error> {
		if self.days == 0 {
			format!(
				"{:02}{:02}{:02}",
				self.hours,
				self.minutes,
				self.seconds.unwrap()
			)
		} else {
			format!(
				"{:02}{:02}{:02}{:02}",
				self.days,
				self.hours,
				self.minutes,
				self.seconds.unwrap()
			)
		}
		.serialize(serializer)
	}
}

struct TimeVisitor;

impl Visitor<'_> for TimeVisitor {
	type Value = Time;

	fn expecting(&self, formatter: &mut Formatter) -> fmt::Result {
		formatter.write_str("a time")
	}

	fn visit_str<E: de::Error>(self, v: &str) -> Result<Self::Value, E> {
		Time::from_str(v, true).map_err(de::Error::custom)
	}
}

impl<'de> Deserialize<'de> for Time {
	fn deserialize<D: Deserializer<'de>>(deserializer: D) -> Result<Self, D::Error> {
		deserializer.deserialize_str(TimeVisitor)
	}
}

pub(crate) fn parse_date(s: &str, full_year: bool) -> Result<NaiveDate, ParseDateError> {
	let (rest, year) = if full_year {
		last_num(s, 4)
	} else {
		last_num(s, 2).map(|(rest, year): (&str, i32)| (rest, year + 2000))
	}
	.ok_or(ParseDateError::Year)?;

	let (rest, month) = last_num(rest, 2).ok_or(ParseDateError::Month)?;
	let day = rest.parse().map_err(|_| ParseDateError::Day)?;

	Ok(NaiveDate::from_ymd_opt(year, month, day).unwrap())
}

#[derive(Debug, Clone, Copy)]
pub enum ParseDateError {
	Year,
	Month,
	Day,
}

impl Display for ParseDateError {
	fn fmt(&self, f: &mut Formatter<'_>) -> fmt::Result {
		match self {
			Self::Year => f.write_str("year invalid"),
			Self::Month => f.write_str("month invalid"),
			Self::Day => f.write_str("day invalid"),
		}
	}
}

impl Error for ParseDateError {}

pub(crate) struct DateVisitor;

impl<'de> Visitor<'de> for DateVisitor {
	type Value = Option<NaiveDate>;

	fn expecting(&self, formatter: &mut Formatter) -> fmt::Result {
		formatter.write_str("a date")
	}

	fn visit_some<D: Deserializer<'de>>(self, deserializer: D) -> Result<Self::Value, D::Error> {
		deserializer.deserialize_str(self)
	}

	fn visit_str<E: de::Error>(self, v: &str) -> Result<Self::Value, E> {
		NaiveDate::parse_from_str(v, date::FORMAT)
			.map(Some)
			.map_err(de::Error::custom)
	}
}

pub mod date {
	use chrono::NaiveDate;
	use serde::{Deserializer, Serialize, Serializer};

	use crate::hafas::DateVisitor;

	pub(crate) const FORMAT: &str = "%Y%m%d";

	#[allow(clippy::trivially_copy_pass_by_ref)]
	pub fn serialize<S: Serializer>(date: &NaiveDate, serializer: S) -> Result<S::Ok, S::Error> {
		date.format(FORMAT).to_string().serialize(serializer)
	}

	pub fn deserialize<'de, D: Deserializer<'de>>(deserializer: D) -> Result<NaiveDate, D::Error> {
		deserializer
			.deserialize_str(DateVisitor)
			.map(Option::unwrap)
	}
}

pub(crate) mod opt_date {
	use chrono::NaiveDate;
	use serde::{Deserializer, Serializer};

	use crate::hafas::{DateVisitor, date};

	#[allow(clippy::trivially_copy_pass_by_ref)]
	pub fn serialize<S: Serializer>(
		d: &Option<NaiveDate>,
		serializer: S,
	) -> Result<S::Ok, S::Error> {
		if let Some(d) = d {
			date::serialize(d, serializer)
		} else {
			serializer.serialize_none()
		}
	}

	pub fn deserialize<'de, D: Deserializer<'de>>(
		deserializer: D,
	) -> Result<Option<NaiveDate>, D::Error> {
		deserializer.deserialize_option(DateVisitor)
	}
}
