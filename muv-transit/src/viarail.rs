use std::{
	collections::{BTreeMap, btree_map::IntoIter},
	iter::Map,
	result,
	time::Duration,
};

use chrono::{DateTime, NaiveDate, Utc};
use scooter::Agent;
use serde::{Deserialize, Deserializer, de};

use crate::{Id, Mode, Vehicle};

#[cfg(any(feature = "blocking", feature = "tokio"))]
use crate::Result;

#[derive(Debug, Default)]
pub struct TsiMobile<A> {
	pub agent: Agent<A>,
}

#[derive(Debug, Deserialize)]
pub struct AllData(pub BTreeMap<String, Train>);

pub type IntoVehicles = Map<IntoIter<String, Train>, fn((String, Train)) -> Vehicle>;

impl AllData {
	pub const URL: &'static str = "https://tsimobile.viarail.ca/data/allData.json";
	pub const UPDATE_CYCLE: Duration = Duration::from_secs(15);

	pub fn vehicles(self) -> IntoVehicles {
		self.0.into_iter().map(Into::into)
	}
}

#[cfg(feature = "blocking")]
impl TsiMobile<crate::Blocking> {
	pub fn all_data(&self) -> Result<AllData> {
		Ok(self
			.agent
			.get(AllData::URL)
			.header("User-Agent", "muv-transit/0.1")
			.send()?
			.json()?)
	}
}

#[cfg(feature = "tokio")]
impl TsiMobile<crate::Tokio> {
	pub async fn all_data(&self) -> Result<AllData> {
		Ok(self
			.agent
			.get(AllData::URL)
			.header("User-Agent", "muv-transit/0.1")
			.send()
			.await?
			.json()?)
	}
}

#[allow(clippy::fallible_impl_from)]
impl From<(String, Train)> for Vehicle {
	fn from((id, train): (String, Train)) -> Self {
		let code = id.split(' ').next().unwrap().to_owned();
		Self {
			id: Id::new("viarail", id),
			uic: None,
			code: Some(code),
			line_name: None,
			trip: None,
			mode: Some(Mode::LongDistanceRail),

			location: train.lat.zip(train.lng).map(Into::into),
			heading: train.direction,
			speed: train.speed,

			carriages: Vec::new(),
		}
	}
}

#[derive(Debug, Deserialize)]
#[serde(rename_all = "camelCase")]
pub struct Train {
	pub lat: Option<f64>,
	pub lng: Option<f64>,
	pub speed: Option<f64>,
	pub direction: Option<f64>,
	pub poll: Option<DateTime<Utc>>,
	pub departed: bool,
	pub arrived: bool,
	pub from: String,
	pub to: String,
	pub instance: NaiveDate,
	pub poll_min: Option<u16>,
	pub poll_radius: Option<u32>,
	pub times: Vec<Station>,
}

#[derive(Debug, Deserialize)]
#[serde(rename_all = "camelCase")]
pub struct Station {
	pub station: String,
	pub code: String,
	#[serde(deserialize_with = "deserialize_optional_datetime")]
	pub estimated: Option<DateTime<Utc>>,
	#[serde(deserialize_with = "deserialize_optional_datetime")]
	pub scheduled: Option<DateTime<Utc>>,
	pub eta: String,
	pub departure: Option<Time>,
	pub arrival: Option<Time>,
	pub diff: Option<Diff>,
	pub diff_min: Option<i16>,
}

fn deserialize_optional_datetime<'de, D: Deserializer<'de>>(
	deserializer: D,
) -> result::Result<Option<DateTime<Utc>>, D::Error> {
	let s = String::deserialize(deserializer)?;
	if s == "&mdash;" {
		Ok(None)
	} else {
		s.parse().map(Some).map_err(de::Error::custom)
	}
}

#[derive(Debug, Deserialize)]
#[serde(rename_all = "lowercase")]
pub enum Diff {
	#[serde(rename = "goo")]
	Good,
	#[serde(rename = "med")]
	Medium,
	Bad,
}

#[derive(Debug, Deserialize)]
pub struct Time {
	pub estimated: Option<DateTime<Utc>>,
	pub scheduled: DateTime<Utc>,
}
