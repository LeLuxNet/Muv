use serde::Deserialize;

use crate::{Id, Mode, Result};

#[cfg(feature = "reqwest")]
use crate::Reqwest;
#[cfg(feature = "reqwest")]
use reqwest::ClientBuilder;

#[derive(Debug)]
pub struct Mapa<H> {
	pub http: H,
	cookies_loaded: bool,
}

#[cfg(feature = "reqwest")]
impl Default for Mapa<Reqwest> {
	fn default() -> Self {
		let client = ClientBuilder::new().cookie_store(true).build().unwrap();
		Self {
			http: Reqwest(client),
			cookies_loaded: false,
		}
	}
}

#[cfg(feature = "reqwest")]
impl Mapa<Reqwest> {
	async fn load_cookies(&mut self) -> Result<()> {
		if !self.cookies_loaded {
			self.http
				.get("https://mapa.zsr.sk/index.aspx")
				.send()
				.await?;
			self.cookies_loaded = true;
		}

		Ok(())
	}

	pub async fn gtall(&mut self) -> Result<Vec<Entry>> {
		self.load_cookies().await?;

		let body = self
			.http
			.post("https://mapa.zsr.sk/api/action")
			.header("Content-Type", "application/x-www-form-urlencoded")
			.body("action=gtall")
			.send()
			.await?
			.bytes()
			.await?;
		Ok(serde_json::from_slice(&body)?)
	}
}

pub type Position = (f64, f64);

#[derive(Deserialize)]
#[serde(rename_all = "PascalCase")]
pub struct Entry {
	pub stanica_z_cislo: String,
	pub stanica_do_cislo: String,
	pub nazov: String,
	pub typ_vlaku: String,
	pub cislo_vlaku: String,
	pub nazov_vlaku: String,
	pub popis: String,
	pub meska: i32,
	pub meska_skutocne: u16,
	// TODO
	pub position: Position,
	// TODO
	pub angle: i16,
	// TODO
}

impl From<Entry> for crate::Vehicle {
	fn from(value: Entry) -> Self {
		Self {
			id: Id::new("zsr", value.nazov), // TODO
			uic: None,
			code: Some(format!("{} {}", value.typ_vlaku, value.cislo_vlaku)),
			line_name: None,
			trip: None,
			mode: Some(map_mode(&value.typ_vlaku)),

			location: Some(value.position.into()),
			heading: Some(value.angle.into()),
			speed: None,

			carriages: Vec::new(),
		}
	}
}

fn map_mode(s: &str) -> Mode {
	match s {
		"Os" | "REX" | "Zr" => Mode::RegionalRail,
		"R" | "Ex" | "IC" | "EC" | "SC" | "RJ" => Mode::LongDistanceRail,
		"EN" => Mode::SleeperRail,
		_ => todo!("{s}"),
	}
}
