use std::{
	fmt::{self, Formatter},
	result,
};

use scooter::Agent;
use serde::{
	Deserialize, Serialize,
	de::{self, Visitor},
};

#[cfg(any(feature = "blocking", feature = "tokio"))]
use crate::Location;

use crate::Result;

#[derive(Debug, Default)]
pub struct TaxiButton<H> {
	pub agent: Agent<H>,
}

#[cfg(feature = "blocking")]
impl TaxiButton<crate::Blocking> {
	pub fn radar(&self, loc: Location, distance: u16) -> Result<Radar> {
		let url = format!(
			"http://mein.taxibutton.de/radar.json?lat={}&long={}&distance={distance}",
			loc.lat, loc.lon,
		);
		Ok(self.agent.get(&url).send()?.json()?)
	}
}

#[cfg(feature = "tokio")]
impl TaxiButton<crate::Tokio> {
	pub async fn radar(&self, loc: Location, distance: u16) -> Result<Radar> {
		let url = format!(
			"http://mein.taxibutton.de/radar.json?lat={}&long={}&distance={distance}",
			loc.lat, loc.lon,
		);
		Ok(self.agent.get(&url).send().await?.json()?)
	}
}

#[derive(Debug, Clone, PartialEq, Serialize, Deserialize)]
pub struct Radar {
	pub taxis: Vec<Taxi>,
	pub controls: Vec<Control>,
	pub config: Config,
	pub objects: Vec<Object>,
}

#[derive(Debug, Clone, PartialEq, Serialize, Deserialize)]
pub struct Taxi {
	pub lat: f64,
	pub lon: f64,
	#[serde(with = "bool")]
	pub busy: bool,
	pub id: u32,
	pub message: String,
}

struct BoolVisitor;

impl Visitor<'_> for BoolVisitor {
	type Value = bool;

	fn expecting(&self, formatter: &mut Formatter) -> fmt::Result {
		formatter.write_str("0 or 1 as a string")
	}

	fn visit_str<E: de::Error>(self, v: &str) -> result::Result<Self::Value, E> {
		match v {
			"0" => Ok(false),
			"1" => Ok(true),
			_ => Err(de::Error::unknown_variant(v, &["0", "1"])),
		}
	}
}

mod bool {
	use serde::{Deserializer, Serializer};

	use crate::taxi_de::BoolVisitor;

	#[allow(clippy::trivially_copy_pass_by_ref)]
	pub fn serialize<S: Serializer>(value: &bool, serializer: S) -> Result<S::Ok, S::Error> {
		serializer.serialize_str(if *value { "1" } else { "0" })
	}

	pub fn deserialize<'de, D: Deserializer<'de>>(deserializer: D) -> Result<bool, D::Error> {
		deserializer.deserialize_str(BoolVisitor)
	}
}

#[derive(Debug, Clone, PartialEq, Serialize, Deserialize)]
pub struct Control {
	pub lat: f64,
	pub lon: f64,
	// pub active
}

#[derive(Debug, Clone, PartialEq, Eq, PartialOrd, Ord, Hash, Serialize, Deserialize)]
pub struct Config {
	pub info: Info,
	// pub radar_interval
}

#[derive(Debug, Clone, PartialEq, Eq, PartialOrd, Ord, Hash, Serialize, Deserialize)]
pub struct Info {
	pub text: String,
	// pub speed
	// pub interval
}

#[derive(Debug, Clone, PartialEq, Serialize, Deserialize)]
pub struct Object {}
