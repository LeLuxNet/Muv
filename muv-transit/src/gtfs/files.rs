use std::num::{NonZeroIsize, NonZeroUsize};

use chrono_tz::Tz;

use crate::{
	PrimitiveTime,
	gtfs::{
		Color, CurrencyAmount, CurrencyCode, Email, LanguageCode, PhoneNumber, PickupDropOff, Url,
		Weekdays, allowed,
	},
};

id!(AgencyId);
fields!(Agency(Eq) {
	"agency_id" id: Option<AgencyId>,
	"agency_name" name: String,
	"agency_url" url: Url,
	"agency_timezone" timezone: Tz,
	"agency_lang" lang: Option<LanguageCode>,
	"agency_phone" phone: Option<PhoneNumber>,
	"agency_fare_url" fare_url: Option<Url>,
	"agency_email" email: Option<Email>,
});

id!(StopId);
fields!(Stop {
	"stop_id" id: StopId,
	"stop_code" code: Option<String>,
	"stop_name" name: Option<String>,
	"tts_stop_name" tts_name: Option<String>,
	"stop_desc" desc: Option<String>,
	"stop_lat" lat: Option<f64>,
	"stop_lon" lon: Option<f64>,
	"zone_id" zone: Option<ZoneId>,
	"stop_url" url: Option<Url>,
	#[serde(default)]
	location_type: LocationType,
	parent_station: Option<StopId>,
	"stop_timezone" timezone: Option<Tz>,
	#[serde(default, deserialize_with = "allowed::deserialize")]
	wheelchair_boarding: Option<bool>,
	"level_id" level: Option<LevelId>,
	platform_code: Option<String>,
});
id!(ZoneId);

r#enum! {LocationType
	Stop! = 0 "0",
	Station = 1 "1",
	EntranceExit = 2 "2",
	GenericNode = 3 "3",
	BoardingArea = 4 "4",
}

id!(RouteId);
fields!(Route(Eq) {
	"route_id" id: RouteId,
	"agency_id" agency: Option<AgencyId>,
	"route_short_name" short_name: Option<String>,
	"route_long_name" long_name: Option<String>,
	"route_desc" desc: Option<String>,
	"route_type" r#type: RouteType,
	"route_url" url: Option<Url>,
	"route_color" color: Option<Color>,
	"route_text_color" text_color: Option<Color>,
	"route_sort_order" sort_order: Option<usize>,
	#[serde(default = "PickupDropOff::no", deserialize_with = "PickupDropOff::deserialize_no")]
	continuous_pickup: PickupDropOff,
	#[serde(default = "PickupDropOff::no", deserialize_with = "PickupDropOff::deserialize_no")]
	continuous_drop_off: PickupDropOff,
	"network_id" network: Option<NetworkId>,
});

r#enum! {RouteType
	Tram = 0 "0" | "900",
	Subway = 1 "1",
	Rail = 2 "2" | "100",
	Bus = 3 "3" | "700",
	Ferry = 4 "4" | "1200",
	CableTram = 5 "5",
	AerialLift = 6 "6" | "1300",
	Funicular = 7 "7" | "1400",
	Trolleybus = 11 "11" | "800",
	Monorail = 12 "12" | "405",

	HighSpeedRail = 101 "101",
	LongDistanceRail = 102 "102",
	InterRegionalRail = 103 "103",
	CarTransportRail = 104 "104",
	SleeperRail = 105 "105",
	RegionalRail = 106 "106",
	TouristRail = 107 "107",
	RailShuttle = 108 "108",
	SururbanRail = 109 "109",
	ReplacementRail = 110 "110",
	SpecialRail = 111 "111",
	LorryTransportRail = 112 "112",
	AllRail = 113 "113",
	CrossCountryRail = 114 "114",
	VehicleTransportRail = 115 "115",
	RackAndPinionRail = 116 "116",
	AdditionalRail = 117 "117",

	Coach = 200 "200",
	InternationalCoach = 201 "201",
	NationalCoach = 202 "202",
	ShuttleCoach = 203 "203",
	RegionalCoach = 204 "204",
	SpecialCoach = 205 "205",
	SightseeingCoach = 206 "206",
	TouristCoach = 207 "207",
	CommuterCoach = 208 "208",
	AllCoach = 209 "209",

	UrbanRailway = 400 "400",
	Metro = 401 "401",
	Underground = 402 "402",
	UrbanRail = 403 "403",
	AllUrbanRail = 404 "404",

	RegionalBus = 701 "701",
	ExpressBus = 702 "702",
	StoppingBus = 703 "703",
	LocalBus = 704 "704",
	NightBus = 705 "705",
	PostBus = 706 "706",
	SpecialNeedsBus = 707 "707",
	MobilityBus = 708 "708",
	MobilityBusForRegisteredDisabled = 709 "709",
	SightseeingBus = 710 "710",
	ShuttleBus = 711 "711",
	SchoolBus = 712 "712",
	SchoolAndPublicServiceBus = 713 "713",
	RailReplacementBus = 714 "714",
	DemandAndResponseBus = 715 "715",
	AllBus = 716 "716",

	CityTram = 901 "901",
	LocalTram = 902 "902",
	RegionalTram = 903 "903",
	SightseeingTram = 904 "904",
	ShuttleTram = 905 "905",
	AllTram = 906 "906",

	WaterTransport = 1000 "1000",

	Air = 1100 "1100",

	Telecabin = 1301 "1301",
	CableCar = 1302 "1302",
	Elevator = 1303 "1303",
	ChairLift = 1304 "1304",
	DragLift = 1305 "1305",
	SmallTelecabin = 1306 "1306",
	AllTelecabin = 1307 "1307",

	Taxi = 1500 "1500",
	CommunalTaxi = 1501 "1501",
	WaterTaxi = 1502 "1502",
	RailTaxi = 1503 "1503",
	BikeTaxi = 1504 "1504",
	LicensedTaxi = 1505 "1505",
	PrivateHireVehicle = 1506 "1506",
	AllTaxi = 1507 "1507",

	Miscellaneous = 1700 "1700",
	HorseDrawnCarriage = 1702 "1702",
}

id!(TripId);
fields!(Trip(Eq) {
	"route_id" route: RouteId,
	"service_id" service: ServiceId,
	"trip_id" id: TripId,
	"trip_headsign" headsign: Option<String>,
	"trip_short_name" short_name: Option<String>,
	"direction_id" direction: Option<Direction>,
	"block_id" block: Option<BlockId>,
	"shape_id" shape: Option<ShapeId>,
	#[serde(default, deserialize_with = "allowed::deserialize")]
	wheelchair_accessible: Option<bool>,
	#[serde(default, deserialize_with = "allowed::deserialize")]
	bikes_allowed: Option<bool>,
});
id!(BlockId);

r#enum! {Direction
	Forward = 0 "0",
	Backward = 1 "1",
}

fields!(StopTime {
	"trip_id" trip: TripId,
	arrival_time: Option<PrimitiveTime>,
	departure_time: Option<PrimitiveTime>,
	"stop_id" stop: Option<StopId>,
	"location_group_id" location_group: Option<LocationGroupId>,
	"location_id" location: Option<LocationId>,
	stop_sequence: usize,
	stop_headsign: Option<String>,
	start_pickup_drop_off_window: Option<PrimitiveTime>,
	end_pickup_drop_off_window: Option<PrimitiveTime>,
	#[serde(default = "PickupDropOff::yes", deserialize_with = "PickupDropOff::deserialize_yes")]
	pickup_type: PickupDropOff,
	#[serde(default = "PickupDropOff::yes", deserialize_with = "PickupDropOff::deserialize_yes")]
	drop_off_type: PickupDropOff,
	#[serde(default = "PickupDropOff::no", deserialize_with = "PickupDropOff::deserialize_no")]
	continuous_pickup: PickupDropOff,
	#[serde(default = "PickupDropOff::no", deserialize_with = "PickupDropOff::deserialize_no")]
	continuous_drop_off: PickupDropOff,
	shape_dist_traveled: Option<f64>,
	#[serde(default)]
	timepoint: Timepoint,
	"pickup_booking_rule_id" pickup_booking_rule: Option<BookingRuleId>,
	"drop_off_booking_rule_id" drop_off_booking_rule: Option<BookingRuleId>,
});
id!(LocationId);

r#enum! {Timepoint
	Approximate = 0 "0",
	Exact! = 1 "1",
}

id!(ServiceId);
fields!(Service(Eq) {
	"service_id" id: ServiceId,

	#[serde(flatten)]
	weekdays: Weekdays,

	start_date: Date,
	end_date: Date,
});

fields!(ServiceDate(Eq) {
	"service_id" service: ServiceId,
	date: Date,
	exception_type: ExceptionType,
});

r#enum! {ExceptionType
	Added = 1 "1",
	Removed = 2 "2",
}

id!(FareId);
fields!(Fare {
	"fare_id" id: FareId,
	price: f64,
	currency_type: CurrencyCode,
	payment_method: PaymentMethod,
	transfers: Option<usize>,
	"agency_id" agency: Option<AgencyId>,
	transfer_duration: Option<usize>,
});

r#enum! {PaymentMethod
	OnBoard = 0 "0",
	BeforeBoarding = 1 "1",
}

fields!(FareRule(Eq) {
	"fare_id" fare: FareId,
	"route_id" route: Option<RouteId>,
	"origin_id" origin: Option<ZoneId>,
	"destination_id" destination: Option<ZoneId>,
	"contains_id" contains: Option<ZoneId>,
});

id!(TimeframeGroupId);
fields!(Timeframe(Eq) {
	"timeframe_group_id" group: TimeframeGroupId,
	start_time: Option<PrimitiveTime>,
	end_time: Option<PrimitiveTime>,
	"service_id" service: ServiceId,
});

id!(FareMediaId);
fields!(FareMedia(Eq) {
	"fare_media_id" id: FareMediaId,
	"fare_media_name" name: Option<String>,
	"fare_media_type" r#type: FareMediaType,
});

r#enum! {FareMediaType
	None = 0 "0",
	PaperTicket = 1 "1",
	TransitCard = 2 "2",
	Contactless = 3 "3",
	MobileApp = 4 "4",
}

id!(FareProductId);
fields!(FareProduct(Eq) {
	"fare_product_id" id: FareProductId,
	"fare_product_name" name: Option<String>,
	"fare_media_id" fare_media: Option<FareMediaId>,
	amount: CurrencyAmount,
	currency: CurrencyCode,
});

fields!(FareLegRule(Eq) {
	"leg_group_id" leg_group: Option<FareLegGroupId>,
	"network_id" network: Option<NetworkId>,
	"from_area_id" from_area: Option<AreaId>,
	"to_area_id" to_area: Option<AreaId>,
	"from_timeframe_group_id" from_timeframe_group: Option<TimeframeGroupId>,
	"to_timeframe_group_id" to_timeframe_group: Option<TimeframeGroupId>,
	"fare_product_id" fare_product: FareProductId,
});
id!(FareLegGroupId);

fields!(FareTransferRule(Eq) {
	"from_leg_group_id" from_leg_group: Option<FareLegGroupId>,
	"to_leg_group_id" to_leg_group: Option<FareLegGroupId>,
	transfer_count: Option<NonZeroIsize>,
	duration_limit: Option<NonZeroUsize>,
	duration_limit_type: Option<DurationLimitType>,
	// fare_transfer_type
	"fare_product_id" fare_product: Option<FareProductId>,
});

r#enum! {DurationLimitType
	DepartureArrival = 0 "0",
	DepartureDeparture = 1 "1",
	ArrivalDeparture = 2 "2",
	ArrivalArrival = 3 "3",
}

id!(AreaId);
fields!(Area(Eq) {
	"area_id" id: AreaId,
	"area_name" name: Option<String>,
});

fields!(StopArea(Eq) {
	"area_id" area: AreaId,
	"stop_id" stop: StopId,
});

id!(NetworkId);
fields!(Network(Eq) {
	"network_id" id: NetworkId,
	"network_name" name: Option<String>,
});

fields!(RouteNetwork(Eq) {
	"network_id" network: NetworkId,
	"route_id" route: RouteId,
});

id!(ShapeId);
fields!(Shape {
	"shape_id" id: ShapeId,
	"shape_pt_lat" point_lat: f64,
	"shape_pt_lon" point_lon: f64,
	"shape_pt_sequence" point_sequence: usize,
	"shape_dist_traveled" dist_traveled: Option<f64>,
});

fields!(Frequency(Eq) {
	"trip_id" trip: TripId,
	start_time: PrimitiveTime,
	end_time: PrimitiveTime,
	headway_secs: NonZeroUsize,
	#[serde(default)]
	exact_times: ServiceType,
});

r#enum! {ServiceType
	FrequencyBased! = 0 "0",
	ScheduleBased = 1 "1",
}

fields!(Transfer(Eq) {
	"from_stop_id" from_stop: Option<StopId>,
	"to_stop_id" to_stop: Option<StopId>,
	"from_route_id" from_route: Option<RouteId>,
	"to_route_id" to_route: Option<RouteId>,
	"from_trip_id" from_trip: Option<TripId>,
	"to_trip_id" to_trip: Option<TripId>,
	#[serde(default)]
	"transfer_type" r#type: TransferType,
	"min_transfer_time" min_time: Option<usize>,
});

r#enum! {TransferType
	Recommended! = 0 "0",
	Timed = 1 "1",
	MinimumTime = 2 "2",
	NotPossible = 3 "3",
	Linked = 4 "4",
	LinkedReboard = 5 "5",
}

id!(PathwayId);
fields!(Pathway {
	"pathway_id" id: PathwayId,
	"from_stop_id" from_stop: StopId,
	"to_stop_id" to_stop: StopId,
	"pathway_mode" mode: PathwayMode,
	is_bidirectional: bool,
	length: Option<f64>,
	traversal_time: Option<NonZeroUsize>,
	stair_count: Option<usize>,
	max_slope: Option<f64>,
	min_width: Option<f64>,
	signposted_as: Option<String>,
	reversed_signposted_as: Option<String>,
});

r#enum! {PathwayMode
	Walkway = 1 "1",
	Stairs = 2 "2",
	Travelator = 3 "3",
	Escalator = 4 "4",
	Elevator = 5 "5",
	FareGate = 6 "6",
	ExitGate = 7 "7",
}

id!(LevelId);
fields!(Level {
	"level_id" id: LevelId,
	"level_index" index: f64,
	"level_name" name: Option<String>,
});

id!(LocationGroupId);
fields!(LocationGroup(Eq) {
	"location_group_id" id: LocationGroupId,
	"location_group_name" name: Option<String>,
});

fields!(LocationGroupStop(Eq) {
	"location_group_id" location_group: LocationGroupId,
	"stop_id" stop: StopId,
});

id!(BookingRuleId);
fields!(BookingRule(Eq) {
	"booking_rule_id" id: BookingRuleId,
	booking_type: BookingType,
	prior_notice_duration_min: Option<isize>,
	prior_notice_duration_max: Option<isize>,
	prior_notice_last_day: Option<isize>,
	prior_notice_last_time: Option<PrimitiveTime>,
	prior_notice_start_day: Option<isize>,
	prior_notice_start_time: Option<PrimitiveTime>,
	"prior_notice_service_id" prior_notice_service: Option<ServiceId>,
	message: Option<String>,
	pickup_message: Option<String>,
	drop_off_message: Option<String>,
	phone_number: Option<PhoneNumber>,
	info_url: Option<Url>,
	booking_url: Option<Url>,
});

r#enum! {BookingType
	RealTime = 0 "0",
	SameDay = 1 "1",
	PriorDays = 2 "2",
}

fields!(Translation(Eq) {
	table_name: TableName,
	field_name: String,
	language: LanguageCode,
	translation: String,
	record_id: Option<String>,
	record_sub_id: Option<String>,
	field_value: Option<String>,
});

r#enum! {
#[serde(rename_all = "snake_case")]
TableName
	Agency,
	Stops,
	Routes,
	Trips,
	StopTimes,
	Pathways,
	Levels,
	FeedInfo,
	Attributions,
}

fields!(FeedInfo(Eq) {
	"feed_publisher_name" publisher_name: String,
	"feed_publisher_url" publisher_url: Url,
	"feed_lang" lang: LanguageCode,
	default_lang: Option<LanguageCode>,
	"feed_start_date" start_date: Option<Date>,
	"feed_end_date" end_date: Option<Date>,
	"feed_version" version: Option<String>,
	"feed_contact_email" contact_email: Option<Email>,
	"feed_contact_url" contact_url: Option<Url>,
});

id!(AttributionId);
fields!(Attribution(Eq) {
	"attribution_id" id: Option<AttributionId>,
	"agency_id" agency: Option<AgencyId>,
	"route_id" route: Option<RouteId>,
	"trip_id" trip: Option<TripId>,
	organization_name: String,
	#[serde(default)]
	is_producer: bool,
	#[serde(default)]
	is_operator: bool,
	#[serde(default)]
	is_authority: bool,
	"attribution_url" url: Option<Url>,
	"attribution_email" email: Option<Email>,
	"attribution_phone" phone: Option<PhoneNumber>,
});
