use chrono::Weekday;
use serde::{Deserialize, Deserializer, de};

use crate::Board;

macro_rules! id {
	($name:ident) => {
		pub type $name = String;
	};
}

macro_rules! fields {
	($name:ident $(($($derive:tt)*))? $(#[$($attr:tt)*])* $([$($body:tt)*])? {}) => {
        $(#[$($attr)*])*
		#[derive(Debug, Clone, PartialEq, serde::Serialize, serde::Deserialize, $($($derive)*)? )]
        #[allow(clippy::struct_field_names)]
		pub struct $name {
            $($($body)*)?
        }
	};
    ($name:ident $(($($derive:tt)*))? $(#[$($attr:tt)*])* $([$($body:tt)*])? { $(#[$($fieldattr:tt)*])* $($rename:literal)? $key:ident : bool, $($rest:tt)* }) => {
        fields! { $name $(($($derive)*))? $(#[$($attr)*])* [
            $($($body)*)?
            $(#[$($fieldattr)*])*
            $(#[serde(rename = $rename)])?
            #[serde(with = "crate::gtfs::bool")]
            pub $key: bool,
        ] { $($rest)* } }
	};
    ($name:ident $(($($derive:tt)*))? $(#[$($attr:tt)*])* $([$($body:tt)*])? { $(#[$($fieldattr:tt)*])* $($rename:literal)? $key:ident : Option<Date>, $($rest:tt)* }) => {
        fields! { $name $(($($derive)*))? $(#[$($attr)*])* [
            $($($body)*)?
            $(#[$($fieldattr)*])*
            $(#[serde(rename = $rename)])?
            #[serde(with = "crate::gtfs::opt_date")]
            pub $key: Option<chrono::NaiveDate>,
        ] { $($rest)* } }
	};
    ($name:ident $(($($derive:tt)*))? $(#[$($attr:tt)*])* $([$($body:tt)*])? { $(#[$($fieldattr:tt)*])* $($rename:literal)? $key:ident : Date, $($rest:tt)* }) => {
        fields! { $name $(($($derive)*))? $(#[$($attr)*])* [
            $($($body)*)?
            $(#[$($fieldattr)*])*
            $(#[serde(rename = $rename)])?
            #[serde(with = "crate::gtfs::date")]
            pub $key: chrono::NaiveDate,
        ] { $($rest)* } }
	};
    ($name:ident $(($($derive:tt)*))? $(#[$($attr:tt)*])* $([$($body:tt)*])? { $(#[$($fieldattr:tt)*])* $($rename:literal)? $key:ident : $type:path, $($rest:tt)* }) => {
        fields! { $name $(($($derive)*))? $(#[$($attr)*])* [
            $($($body)*)?
            $(#[$($fieldattr)*])*
            $(#[serde(rename = $rename)])?
            pub $key: $type,
        ] { $($rest)* } }
	};
}

macro_rules! r#enum {
	($(#[$($attr:tt)*])* $name:ident $(($($body:tt)*))? ) => {
        #[derive(Debug, Clone, Copy, PartialEq, Eq, PartialOrd, Ord, Hash, serde::Serialize, serde::Deserialize)]
        $(#[$($attr)*])*
        pub enum $name {
            $($($body)*)?
        }
    };
	($(#[$($attr:tt)*])* $name:ident $(($($body:tt)*))? $(#[$($vattr:tt)*])* $variant:ident! = $value:literal $valuestr:literal, $($rest:tt)* ) => {
        r#enum! { $(#[$($attr)*])* $name (
            $($($body)*)?
            $(#[$($vattr)*])*
            #[serde(rename = $valuestr, alias = "")]
            $variant = $value,
        ) $($rest)* }

        impl Default for $name {
            fn default() -> Self {
                Self::$variant
            }
        }
    };
    ($(#[$($attr:tt)*])* $name:ident $(($($body:tt)*))? $(#[$($vattr:tt)*])* $variant:ident, $($rest:tt)* ) => {
        r#enum! { $(#[$($attr)*])* $name (
            $($($body)*)?
            $(#[$($vattr)*])*
            $variant,
        ) $($rest)* }
    };
    ($(#[$($attr:tt)*])* $name:ident $(($($body:tt)*))? $(#[$($vattr:tt)*])* $variant:ident = $value:literal $valuestr:literal $(| $alias:literal)* , $($rest:tt)* ) => {
        r#enum! { $(#[$($attr)*])* $name (
            $($($body)*)?
            $(#[$($vattr)*])*
            #[serde(rename = $valuestr, $(alias = $alias),* )]
            $variant = $value,
        ) $($rest)* }
    };
}

pub(crate) mod bool {
	use serde::{
		Deserialize, Deserializer, Serializer,
		de::{self, Unexpected},
	};

	#[allow(clippy::trivially_copy_pass_by_ref)]
	pub fn serialize<S: Serializer>(value: &bool, serializer: S) -> Result<S::Ok, S::Error> {
		serializer.serialize_u8((*value).into())
	}

	pub fn deserialize<'de, D: Deserializer<'de>>(deserializer: D) -> Result<bool, D::Error> {
		let n = u8::deserialize(deserializer)?;
		match n {
			1 => Ok(true),
			0 => Ok(false),
			_ => Err(de::Error::invalid_value(
				Unexpected::Unsigned(n.into()),
				&"1 or 0",
			)),
		}
	}
}

pub(crate) mod allowed {
	use serde::{Deserialize, Deserializer, de};

	pub fn deserialize<'de, D: Deserializer<'de>>(
		deserializer: D,
	) -> Result<Option<bool>, D::Error> {
		let s: &str = Deserialize::deserialize(deserializer)?;
		match s {
			"" | "0" => Ok(None),
			"1" => Ok(Some(true)),
			"2" => Ok(Some(false)),
			_ => Err(de::Error::unknown_variant(s, &["", "0", "1", "2"])),
		}
	}
}

pub type Color = String;
pub type CurrencyCode = String;
pub type CurrencyAmount = String;
pub type Email = String;
pub type LanguageCode = String;
pub type PhoneNumber = String;
pub type Url = String;

pub(crate) mod date {
	use chrono::NaiveDate;
	use serde::{Deserialize, Deserializer, Serialize, Serializer, de};

	#[allow(clippy::trivially_copy_pass_by_ref)]
	pub fn serialize<S: Serializer>(date: &NaiveDate, serializer: S) -> Result<S::Ok, S::Error> {
		date.format("%Y%m%d").to_string().serialize(serializer)
	}

	pub fn deserialize<'de, D: Deserializer<'de>>(deserializer: D) -> Result<NaiveDate, D::Error> {
		let s: &str = Deserialize::deserialize(deserializer)?;
		NaiveDate::parse_from_str(s, "%Y%m%d").map_err(de::Error::custom)
	}
}

pub(crate) mod opt_date {
	use chrono::NaiveDate;
	use serde::{Deserialize, Deserializer, Serialize, Serializer, de};

	#[allow(clippy::trivially_copy_pass_by_ref)]
	pub fn serialize<S: Serializer>(
		d: &Option<NaiveDate>,
		serializer: S,
	) -> Result<S::Ok, S::Error> {
		d.map(|d| d.format("%Y%m%d").to_string())
			.serialize(serializer)
	}

	pub fn deserialize<'de, D: Deserializer<'de>>(
		deserializer: D,
	) -> Result<Option<NaiveDate>, D::Error> {
		let s: &str = Deserialize::deserialize(deserializer)?;
		if s.is_empty() {
			Ok(None)
		} else {
			NaiveDate::parse_from_str(s, "%Y%m%d")
				.map_err(de::Error::custom)
				.map(Some)
		}
	}
}

fields!(Weekdays(Copy, Eq) {
	monday: bool,
	tuesday: bool,
	wednesday: bool,
	thursday: bool,
	friday: bool,
	saturday: bool,
	sunday: bool,
});

impl Weekdays {
	#[must_use]
	pub const fn get(self, w: Weekday) -> bool {
		match w {
			Weekday::Mon => self.monday,
			Weekday::Tue => self.tuesday,
			Weekday::Wed => self.wednesday,
			Weekday::Thu => self.thursday,
			Weekday::Fri => self.friday,
			Weekday::Sat => self.saturday,
			Weekday::Sun => self.sunday,
		}
	}
}

r#enum! {PickupDropOff
	Yes = 0 "0",
	No = 1 "1",
	Phone = 2 "2",
	Coordinate = 3 "3",
}

impl From<PickupDropOff> for Board {
	fn from(value: PickupDropOff) -> Self {
		match value {
			PickupDropOff::Yes => Self::Yes,
			PickupDropOff::No => Self::No,
			PickupDropOff::Phone => Self::Phone {
				offset: None,
				number: None,
			},
			PickupDropOff::Coordinate => todo!(),
		}
	}
}

impl PickupDropOff {
	pub(crate) const fn yes() -> Self {
		Self::Yes
	}
	pub(crate) const fn no() -> Self {
		Self::No
	}

	pub(crate) fn deserialize_yes<'de, D: Deserializer<'de>>(
		deserializer: D,
	) -> Result<Self, D::Error> {
		Self::deserialize(deserializer, Self::Yes)
	}

	pub(crate) fn deserialize_no<'de, D: Deserializer<'de>>(
		deserializer: D,
	) -> Result<Self, D::Error> {
		Self::deserialize(deserializer, Self::No)
	}

	fn deserialize<'de, D: Deserializer<'de>>(
		deserializer: D,
		default: Self,
	) -> Result<Self, D::Error> {
		let s: &str = Deserialize::deserialize(deserializer)?;
		match s {
			"" => Ok(default),
			"0" => Ok(Self::Yes),
			"1" => Ok(Self::No),
			"2" => Ok(Self::Phone),
			"3" => Ok(Self::Coordinate),
			_ => Err(de::Error::unknown_variant(s, &["", "0", "1", "2", "3"])),
		}
	}
}
