use std::{
	error,
	fmt::{self, Display, Formatter},
	io::{Read, Seek},
};

use csv::DeserializeRecordsIntoIter;
use serde::de::DeserializeOwned;
use zip::{ZipArchive, read::ZipFile, result::ZipError};

use crate::gtfs::files;

macro_rules! read_files {
	() => {};
	($file:literal $name:ident? -> $t:ident, $($rest:tt)*) => {
        #[inline]
		pub fn $name(&mut self) -> Result<Records<files::$t>, Error> {
			self.read_optional($file)
		}

        read_files!($($rest)*);
	};
	($file:literal $name:ident -> $t:ident, $($rest:tt)*) => {
        #[inline]
		pub fn $name(&mut self) -> Result<Records<files::$t>, Error> {
			self.read($file)
		}

        read_files!($($rest)*);
	};
}

pub struct DatasetReader<R>(ZipArchive<R>);

impl<R: Read + Seek> DatasetReader<R> {
	pub fn new(r: R) -> Result<Self, Error> {
		Ok(Self(ZipArchive::new(r)?))
	}

	pub fn read<D: DeserializeOwned>(&mut self, file: &'static str) -> Result<Records<D>, Error> {
		let r = self.0.by_name(file)?;
		let iter = csv::Reader::from_reader(r).into_deserialize();
		Ok(Records {
			file,
			iter: Some(iter),
		})
	}

	pub fn read_optional<D: DeserializeOwned>(
		&mut self,
		file: &'static str,
	) -> Result<Records<D>, Error> {
		match self.read(file) {
			Err(Error::Zip(ZipError::FileNotFound)) => Ok(Records { file, iter: None }),
			file => file,
		}
	}
}

impl<R: Read + Seek> DatasetReader<R> {
	read_files! {
		"agency.txt" agencies -> Agency,
		"stops.txt" stops -> Stop,
		"routes.txt" routes -> Route,
		"trips.txt" trips -> Trip,
		"stop_times.txt" stop_times -> StopTime,
		"calendar.txt" calendar? -> Service,
		"calendar_dates.txt" calendar_dates? -> ServiceDate,
		"fare_attributes.txt" fares? -> Fare,
		"fare_rules.txt" fare_rules? -> FareRule,
		"timeframes.txt" timeframes? -> Timeframe,
		"fare_media.txt" fare_media? -> FareMedia,
		"fare_products.txt" fare_products? -> FareProduct,
		"fare_leg_rules.txt" fare_leg_rules? -> FareLegRule,
		"fare_transfer_rules.txt" fare_transfer_rules? -> FareTransferRule,
		"areas.txt" areas? -> Area,
		"stop_areas.txt" stop_areas? -> StopArea,
		"networks.txt" networks? -> Network,
		"route_networks.txt" route_networks? -> RouteNetwork,
		"shapes.txt" shapes? -> Shape,
		"frequencies.txt" frequencies? -> Frequency,
		"transfers.txt" transfers? -> Transfer,
		"pathways.txt" pathways? -> Pathway,
		"levels.txt" levels? -> Level,
		"location_groups.txt" location_groups? -> LocationGroup,
		"location_group_stops.txt" location_group_stops? -> LocationGroupStop,
		"booking_rules.txt" booking_rules? -> BookingRule,
		"translations.txt" translations? -> Translation,
		"feed_info.txt" feed_info? -> FeedInfo,
		"attributions.txt" attributions? -> Attribution,
	}
}

#[must_use]
pub struct Records<'r, D> {
	pub file: &'static str,
	pub iter: Option<DeserializeRecordsIntoIter<ZipFile<'r>, D>>,
}

impl<D: DeserializeOwned> Iterator for Records<'_, D> {
	type Item = Result<D, Error>;

	fn next(&mut self) -> Option<Self::Item> {
		let v = self.iter.as_mut()?.next()?;
		Some(v.map_err(|error| Error::Csv {
			file: self.file,
			error,
		}))
	}
}

#[derive(Debug)]
pub enum Error {
	Zip(ZipError),
	Csv {
		file: &'static str,
		error: csv::Error,
	},
}

impl Display for Error {
	fn fmt(&self, f: &mut Formatter<'_>) -> fmt::Result {
		match self {
			Self::Zip(_) => write!(f, "failed to read zip"),
			Self::Csv { file, error: _ } => write!(f, "failed to read {file:?}"),
		}
	}
}

impl error::Error for Error {
	fn source(&self) -> Option<&(dyn error::Error + 'static)> {
		match self {
			Self::Zip(e) => Some(e),
			Self::Csv { file: _, error } => Some(error),
		}
	}
}

impl From<ZipError> for Error {
	fn from(value: ZipError) -> Self {
		Self::Zip(value)
	}
}
