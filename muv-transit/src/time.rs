use std::fmt::{self, Display, Formatter};

use chrono::{
	DateTime, FixedOffset, MappedLocalTime, NaiveDate, NaiveDateTime, NaiveTime, Offset, TimeDelta,
	TimeZone, Timelike,
};
use chrono_tz::Tz;

#[cfg(feature = "serde")]
use serde::{Deserialize, Deserializer, Serialize, Serializer, de};

#[derive(Debug, Clone, Copy, PartialEq, Eq, Hash)]
#[cfg_attr(feature = "serde", derive(Serialize, Deserialize))]
pub struct Time {
	pub time: PrimitiveTime,
	pub tz: Timezone,
}

impl Time {
	#[must_use]
	pub const fn new(time: PrimitiveTime, tz: Timezone) -> Self {
		Self { time, tz }
	}

	#[must_use]
	pub fn on(self, date: NaiveDate) -> DateTime<Timezone> {
		let noon = NaiveTime::from_hms_opt(12, 0, 0).unwrap();
		let local_noon = date
			.and_time(noon)
			.and_local_timezone(self.tz)
			.single()
			.unwrap();
		let local_midnight = local_noon - TimeDelta::hours(12);
		local_midnight + self.time.after_midnight()
	}
}

#[derive(Debug, Clone, Copy, PartialEq, Eq, PartialOrd, Ord, Hash)]
pub struct PrimitiveTime {
	pub hours: u8,
	pub minutes: u8,
	pub seconds: u8,
}

#[allow(clippy::fallible_impl_from)]
impl From<NaiveTime> for PrimitiveTime {
	fn from(value: NaiveTime) -> Self {
		Self {
			hours: value.hour().try_into().unwrap(),
			minutes: value.minute().try_into().unwrap(),
			seconds: value.second().try_into().unwrap(),
		}
	}
}

#[cfg(any(feature = "serde", feature = "gtfs-rt"))]
impl PrimitiveTime {
	pub(crate) fn parse(s: &str) -> Option<Self> {
		let mut i = s.splitn(3, ':');

		let hours = i.next()?.parse().ok()?;
		let minutes = i.next()?.parse().ok()?;
		let seconds = i.next()?.parse().ok()?;

		Some(Self {
			hours,
			minutes,
			seconds,
		})
	}
}

impl PrimitiveTime {
	fn after_midnight(self) -> TimeDelta {
		TimeDelta::hours(self.hours.into())
			+ TimeDelta::minutes(self.minutes.into())
			+ TimeDelta::seconds(self.seconds.into())
	}
}

impl Display for PrimitiveTime {
	fn fmt(&self, f: &mut Formatter<'_>) -> fmt::Result {
		write!(
			f,
			"{:02}:{:02}:{:02}",
			self.hours, self.minutes, self.seconds
		)
	}
}

#[cfg(feature = "serde")]
impl Serialize for PrimitiveTime {
	fn serialize<S: Serializer>(&self, serializer: S) -> Result<S::Ok, S::Error> {
		self.to_string().serialize(serializer)
	}
}

#[cfg(feature = "serde")]
impl<'de> Deserialize<'de> for PrimitiveTime {
	fn deserialize<D: Deserializer<'de>>(deserializer: D) -> Result<Self, D::Error> {
		let s: &str = Deserialize::deserialize(deserializer)?;
		Self::parse(s).ok_or_else(|| de::Error::custom("failed to parse Time"))
	}
}

#[derive(Debug, Clone, Copy, PartialEq, Eq, Hash)]
pub enum Timezone {
	Fixed(FixedOffset),
	Named(Tz),
}

impl From<FixedOffset> for Timezone {
	fn from(value: FixedOffset) -> Self {
		Self::Fixed(value)
	}
}

impl From<Tz> for Timezone {
	fn from(value: Tz) -> Self {
		Self::Named(value)
	}
}

impl Display for Timezone {
	fn fmt(&self, f: &mut Formatter<'_>) -> fmt::Result {
		match self {
			Self::Fixed(t) => write!(f, "{t}"),
			Self::Named(t) => write!(f, "{t}"),
		}
	}
}

#[cfg(feature = "serde")]
impl Serialize for Timezone {
	fn serialize<S: Serializer>(&self, serializer: S) -> Result<S::Ok, S::Error> {
		self.to_string().serialize(serializer)
	}
}

#[cfg(feature = "serde")]
impl<'de> Deserialize<'de> for Timezone {
	fn deserialize<D: Deserializer<'de>>(deserializer: D) -> Result<Self, D::Error> {
		let s: &str = Deserialize::deserialize(deserializer)?;
		if let Ok(t) = s.parse() {
			Ok(Self::Named(t))
		} else {
			s.parse().map(Self::Fixed).map_err(de::Error::custom)
		}
	}
}

impl TimeZone for Timezone {
	type Offset = TimezoneOffset;

	fn from_offset(offset: &Self::Offset) -> Self {
		match offset {
			TimezoneOffset::Fixed(t) => Self::Fixed(*t),
			TimezoneOffset::Named(t) => Self::Named(Tz::from_offset(t)),
		}
	}

	fn offset_from_local_date(&self, local: &NaiveDate) -> MappedLocalTime<Self::Offset> {
		match self {
			Self::Fixed(t) => MappedLocalTime::Single(TimezoneOffset::Fixed(*t)),
			Self::Named(t) => t.offset_from_local_date(local).map(TimezoneOffset::Named),
		}
	}

	fn offset_from_local_datetime(&self, local: &NaiveDateTime) -> MappedLocalTime<Self::Offset> {
		match self {
			Self::Fixed(t) => MappedLocalTime::Single(TimezoneOffset::Fixed(*t)),
			Self::Named(t) => t
				.offset_from_local_datetime(local)
				.map(TimezoneOffset::Named),
		}
	}

	fn offset_from_utc_date(&self, utc: &NaiveDate) -> Self::Offset {
		match self {
			Self::Fixed(t) => TimezoneOffset::Fixed(*t),
			Self::Named(t) => TimezoneOffset::Named(t.offset_from_utc_date(utc)),
		}
	}

	fn offset_from_utc_datetime(&self, utc: &NaiveDateTime) -> Self::Offset {
		match self {
			Self::Fixed(t) => TimezoneOffset::Fixed(*t),
			Self::Named(t) => TimezoneOffset::Named(t.offset_from_utc_datetime(utc)),
		}
	}
}

#[derive(Debug, Clone, Copy, PartialEq, Eq)]
pub enum TimezoneOffset {
	Fixed(FixedOffset),
	Named(<Tz as TimeZone>::Offset),
}

impl Offset for TimezoneOffset {
	fn fix(&self) -> FixedOffset {
		match self {
			Self::Fixed(t) => *t,
			Self::Named(t) => t.fix(),
		}
	}
}
