use muv_geo::Location;
use serde::{Deserialize, Serialize};

use crate::{Id, Mode, Result, Vehicle};

#[cfg(feature = "reqwest")]
use crate::Reqwest;
#[cfg(feature = "reqwest")]
use reqwest::ClientBuilder;

#[derive(Debug)]
pub struct Grapp<H> {
	pub http: H,
	pub key: Option<String>,
}

#[cfg(feature = "reqwest")]
impl Default for Grapp<Reqwest> {
	fn default() -> Self {
		let client = ClientBuilder::new().cookie_store(true).build().unwrap();
		Self {
			http: Reqwest(client),
			key: None,
		}
	}
}

#[cfg(feature = "reqwest")]
impl Grapp<Reqwest> {
	async fn load_cookies(&mut self) -> Result<&str> {
		if self.key.is_none() {
			let body = self
				.http
				.get("https://grapp.spravazeleznic.cz")
				.send()
				.await?
				.text()
				.await?;
			let (_, end) = body.split_once("id=\"token\" value=\"").unwrap();
			let (token, _) = end.split_once('"').unwrap();

			self.key = Some(token.to_owned());
		}

		Ok(self.key.as_ref().unwrap())
	}

	pub async fn get_trains_with_filter(&mut self) -> Result<GetTrainsWithFilter> {
		let key = self.load_cookies().await?;

		let url = format!("https://grapp.spravazeleznic.cz/post/trains/GetTrainsWithFilter/{key}");
		let body = serde_json::to_vec(&GetTrainsWithFilterReq {
			carrier_code: vec![
				"991919",
				"992230",
				"992719",
				"993030",
				"990010",
				"993188",
				"991943",
				"993246",
				"991950",
				"993196",
				"992693",
				"991638",
				"991976",
				"993089",
				"993162",
				"991257",
				"991935",
				"991562",
				"991125",
				"992644",
				"992842",
				"991927",
				"993170",
				"991810",
				"992909",
				"991612",
				"f_o_r_e_i_g_n",
			],
			public_kind_of_train: vec![
				"LE", "Ex", "Sp", "rj", "TL", "EC", "SC", "AEx", "Os", "Rx", "TLX", "IC", "EN",
				"R", "RJ", "NJ", "LET", "ES",
			],
			freight_kind_of_train: vec![],
			delay: vec!["0", "60", "5", "61", "15", "-1", "30"],
			kind_of_extraordinary: vec![],

			delay_min: -99_999,
			delay_max: -99_999,

			train_no_change: 0,
			train_running: false,
			pmd: false,
			train_out_of_order: false,

			search_by_train_number: true,
			search_by_train_name: true,
			search_by_trip: false,
			search_by_vehicle_number: false,

			search_text_type: "0",
			search_phrase: "",
			selected_train: -1,
		})
		.unwrap();

		let body = self
			.http
			.post(url)
			.header("Content-Type", "application/json")
			.body(body)
			.send()
			.await?
			.bytes()
			.await?;
		Ok(serde_json::from_slice(&body)?)
	}
}

#[derive(Serialize)]
#[serde(rename_all = "PascalCase")]
struct GetTrainsWithFilterReq {
	pub carrier_code: Vec<&'static str>,
	pub public_kind_of_train: Vec<&'static str>,
	pub freight_kind_of_train: Vec<&'static str>,
	pub delay: Vec<&'static str>,
	pub kind_of_extraordinary: Vec<&'static str>,

	pub delay_min: i32,
	pub delay_max: i32,

	pub train_no_change: u8,
	pub train_running: bool,
	#[serde(rename = "PMD")]
	pub pmd: bool,
	pub train_out_of_order: bool,

	pub search_by_train_number: bool,
	pub search_by_train_name: bool,
	#[serde(rename = "SearchByTRID")]
	pub search_by_trip: bool,
	pub search_by_vehicle_number: bool,

	pub search_text_type: &'static str,
	pub search_phrase: &'static str,
	pub selected_train: i8,
}

#[derive(Deserialize)]
#[serde(rename_all = "PascalCase")]
pub struct GetTrainsWithFilter {
	pub copyright_notice: String,
	pub trains: Vec<Train>,
}

#[derive(Deserialize)]
#[serde(rename_all = "PascalCase")]
pub struct Train {
	pub id: u32,
	pub angle: i16,
	#[serde(rename = "GPS")]
	pub gps: (f64, f64),
	pub order: u8,
	pub layer: u8,
	pub title: String,
	pub search_val: String,
	pub icon: u16,
	pub priority: u8,
}

fn mode(prefix: &str) -> Mode {
	match prefix {
		"SC" | "IC" | "EC" | "R" | "RJ" | "rj" | "LE" | "LET" => Mode::LongDistanceRail,
		"Sp" | "Os" | "TLX" | "TL" => Mode::RegionalRail,
		"NJ" | "EN" | "Ex" | "ES" => Mode::SleeperRail,
		_ => todo!("{prefix}"),
	}
}

#[allow(clippy::fallible_impl_from)]
impl From<Train> for Vehicle {
	fn from(value: Train) -> Self {
		let mode = mode(value.title.split(' ').next().unwrap());
		let heading = (value.angle != i16::MIN).then(|| value.angle.into());
		Self {
			id: Id::new("sprava_zeleznic", value.id.to_string()),
			uic: None,
			code: Some(value.title),
			line_name: None,
			trip: None,
			mode: Some(mode),

			location: Some(Location::new(value.gps.0, value.gps.1)),
			heading,
			speed: None,

			carriages: Vec::new(),
		}
	}
}

#[cfg(test)]
#[cfg(feature = "reqwest")]
mod tests {
	use crate::{Vehicle, sprava_zeleznic::Grapp};

	#[ignore]
	#[tokio::test]
	async fn trains() {
		let mut c = Grapp::default();

		let res = c.get_trains_with_filter().await.unwrap();
		let vehicles: Vec<_> = res.trains.into_iter().map(Vehicle::from).collect();
		assert!(!vehicles.is_empty());
	}
}
