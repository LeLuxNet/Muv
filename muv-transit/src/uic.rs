use std::{
	fmt::{self, Display, Formatter},
	str::FromStr,
};

#[cfg(feature = "serde")]
use serde::{Deserialize, Serialize};

/// UIC marking for [tractive stock](https://en.wikipedia.org/wiki/UIC_identification_marking_for_tractive_stock) and [wagons](https://en.wikipedia.org/wiki/UIC_wagon_numbers)
#[derive(Debug, Clone, Copy, PartialEq, Eq, Hash)]
#[cfg_attr(feature = "serde", derive(Serialize, Deserialize))]
pub struct UicMarking {
	pub model: u8,
	pub country_code: u8,
	pub r#type: u16,
	pub serial: u16,
}

/// Parse the UIC Marking, verifying it's length and check digit
///
/// ```
/// # use std::str::FromStr;
/// # use muv_transit::{UicMarking, ParseUicMarkingError};
/// assert_eq!(
/// 	UicMarking::from_str("918544650040"),
/// 	Ok(UicMarking {
/// 		model: 91,
/// 		country_code: 85,
/// 		r#type: 4465,
/// 		serial: 004,
/// 	})
/// );
/// assert_eq!(
/// 	UicMarking::from_str("918544650047"),
/// 	Err(ParseUicMarkingError::InvalidCheckDigit)
/// );
/// ```
impl FromStr for UicMarking {
	type Err = ParseUicMarkingError;
	fn from_str(s: &str) -> Result<Self, Self::Err> {
		let Ok(bytes) = <[u8; 12]>::try_from(s.as_bytes()) else {
			return Err(ParseUicMarkingError::InvalidLength);
		};

		let [digits @ .., check] = bytes.map(|b| b - b'0');
		let sum = Self::luhn_sum(digits);
		if (sum + check) % 10 != 0 {
			return Err(ParseUicMarkingError::InvalidCheckDigit);
		}

		Ok(Self {
			model: digits[0] * 10 + digits[1],
			country_code: digits[2] * 10 + digits[3],
			r#type: u16::from(digits[4]) * 1000
				+ u16::from(digits[5]) * 100
				+ u16::from(digits[6]) * 10
				+ u16::from(digits[7]),
			serial: u16::from(digits[8]) * 100 + u16::from(digits[9]) * 10 + u16::from(digits[10]),
		})
	}
}

impl UicMarking {
	#[must_use]
	#[allow(clippy::as_conversions, clippy::cast_possible_truncation)]
	const fn digits(self) -> [u8; 11] {
		[
			self.model / 10,
			self.model % 10,
			self.country_code / 10,
			self.country_code % 10,
			(self.r#type / 1000) as u8,
			(self.r#type / 100 % 10) as u8,
			(self.r#type / 10 % 10) as u8,
			(self.r#type % 10) as u8,
			(self.serial / 100) as u8,
			(self.serial / 10 % 10) as u8,
			(self.serial % 10) as u8,
		]
	}

	/// Calculates the check digit using the [Luhn algorithm](https://en.wikipedia.org/wiki/Luhn_algorithm)
	///
	/// ```
	/// # use std::str::FromStr;
	/// # use muv_transit::{UicMarking, ParseUicMarkingError};
	/// assert_eq!(
	/// 	UicMarking::from_str("928120160173").unwrap().check_digit(),
	/// 	3 //                             ^
	/// );
	/// ```
	#[must_use]
	pub const fn check_digit(self) -> u8 {
		9 - (Self::luhn_sum(self.digits()) + 9) % 10
	}

	#[must_use]
	const fn luhn_sum(digits: [u8; 11]) -> u8 {
		const fn luhn_map(even: bool, mut n: u8) -> u8 {
			if even {
				n *= 2;
				if n > 9 {
					n -= 9;
				}
			}
			n
		}

		let mut i = 0;
		let mut sum = 0;
		while i < digits.len() {
			sum += luhn_map(i % 2 == 0, digits[i]);
			i += 1;
		}
		sum
	}
}

/// ```
/// # use std::str::FromStr;
/// # use muv_transit::UicMarking;
/// assert_eq!(
/// 	UicMarking::from_str("918061439587").unwrap().to_string(),
/// 	"91 80 6143 958-7"
/// );
/// ```
impl Display for UicMarking {
	fn fmt(&self, f: &mut Formatter<'_>) -> fmt::Result {
		write!(
			f,
			"{:02} {:02} {:04} {:03}-{}",
			self.model,
			self.country_code,
			self.r#type,
			self.serial,
			self.check_digit(),
		)
	}
}

#[derive(Debug, PartialEq, Eq)]
pub enum ParseUicMarkingError {
	/// Did not receive a string of length 12
	InvalidLength,
	/// The calculated check digit did not match the last digit
	InvalidCheckDigit,
}

impl Display for ParseUicMarkingError {
	fn fmt(&self, f: &mut Formatter<'_>) -> fmt::Result {
		match self {
			Self::InvalidLength => write!(f, "expected a string of length 12"),
			Self::InvalidCheckDigit => write!(f, "invalid check digit"),
		}
	}
}
