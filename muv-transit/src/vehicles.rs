use crate::UicMarking;

pub struct VehicleInfo {
	pub r#type: VehicleType,
	pub code: Option<String>,
	pub name: Option<&'static str>,
}

pub enum VehicleType {
	DbIc2Twindexx,
	DbIc2Kiss6Part,
	DbIc2Kiss4Part,
}

#[must_use]
pub fn vehicle_info(uic: UicMarking) -> Option<VehicleInfo> {
	let serial = uic.serial;
	Some(match (uic.model, uic.country_code, uic.r#type, serial) {
		(50, 80, 8681, 850..=876) => VehicleInfo {
			r#type: VehicleType::DbIc2Twindexx,
			code: Some(format!("2{serial}")),
			name: db_ic2_twindexx_name(serial),
		},
		(50, 80, 8681, 877..=919) => VehicleInfo {
			r#type: VehicleType::DbIc2Twindexx,
			code: Some(format!("4{serial}")),
			name: db_ic2_twindexx_name(serial),
		},
		(93, 85, 4010, 1..=8) => VehicleInfo {
			r#type: VehicleType::DbIc2Kiss6Part,
			code: Some(format!("41{serial:02}")),
			name: db_ic2_kiss_name(serial),
		},
		(93, 85, 4110, 9..=17) => VehicleInfo {
			r#type: VehicleType::DbIc2Kiss4Part,
			code: Some(format!("41{serial:02}")),
			name: db_ic2_kiss_name(serial),
		},
		_ => return None,
	})
}

#[must_use]
const fn db_ic2_twindexx_name(serial: u16) -> Option<&'static str> {
	Some(match serial {
		853 => "Nationalpark Sächsische Schweiz",
		865 => "Remstal",
		868 => "Nationalpark Niedersächsisches Wattenmeer",
		871 => "Leipziger Neuseenland",
		874 => "Oberer Neckar",
		875 => "Magdeburger Börde",
		893 => "Bodetal",
		898 => "Lahntal",
		_ => return None,
	})
}

#[must_use]
const fn db_ic2_kiss_name(serial: u16) -> Option<&'static str> {
	Some(match serial {
		2 => "Naturpark Schönbuch",
		3 => "Allgäu",
		8 => "Hegau",
		11 => "Gäu",
		14 => "Dresden Elbland",
		17 => "Mecklenburgische Ostseeküste",
		_ => return None,
	})
}
