use std::fmt::Debug;

use chrono_tz::Tz;
use serde::Deserialize;

use crate::{
	Timezone,
	netex::{
		InfrastructureFrame, MultilingualString, Ref, ResourceFrame, ServiceCalendarFrame,
		ServiceFrame, SiteFrame, TimetableFrame, deserialize_optional_from_str, unwrap_list,
	},
};

#[derive(Debug, Deserialize)]
pub struct CompositeFrame {
	#[serde(rename = "@id")]
	pub id: String,
	#[serde(flatten, default)]
	pub version_frame: VersionFrame,

	#[serde(deserialize_with = "unwrap_list")]
	pub frames: Vec<CommonFrame>,
}

#[derive(Debug, Deserialize)]
pub enum CommonFrame {
	#[serde(rename = "SiteFrame")]
	Site(SiteFrame),
	#[serde(rename = "InfrastructureFrame")]
	Infrastructure(InfrastructureFrame),
	#[serde(rename = "ServiceCalendarFrame")]
	ServiceCalendar(ServiceCalendarFrame),
	#[serde(rename = "ServiceFrame")]
	Service(ServiceFrame),
	#[serde(rename = "ResourceFrame")]
	Resource(ResourceFrame),
	#[serde(rename = "TimetableFrame")]
	Timetable(TimetableFrame),
}

#[derive(Debug, Default, Deserialize)]
#[serde(rename_all = "PascalCase")]
pub struct VersionFrame {
	pub name: Option<MultilingualString>,
	pub description: Option<MultilingualString>,

	pub frame_defaults: Option<FrameDefaults>,
}

#[derive(Debug, Default, Deserialize)]
pub struct FrameDefaults {
	#[serde(rename = "DefaultCodespaceRef")]
	pub codespace: Option<Ref>,
	#[serde(rename = "DefaultDataSourceRef")]
	pub data_source: Option<Ref>,
	#[serde(rename = "DefaultResponsibilitySetRef")]
	pub responsibility_set: Option<Ref>,
	#[serde(rename = "DefaultLocale")]
	pub locale: Option<Locale>,
	#[serde(
		rename = "DefaultLocationSystem",
		deserialize_with = "deserialize_optional_from_str",
		default
	)]
	pub location_system: Option<String>,
	#[serde(rename = "DefaultSystemOfUnits", default)]
	pub system_of_units: SystemOfUnits,
	#[serde(rename = "DefaultCurrency")]
	pub currency: Option<String>,
}

#[derive(Debug, Default, Deserialize)]
pub enum SystemOfUnits {
	#[default]
	SiMetres,
	SiKilometresAndMetres,
	Other,
}

#[derive(Debug, Deserialize)]
#[serde(rename_all = "PascalCase")]
pub struct Locale {
	pub time_zone_offset: Option<f64>,
	#[serde(deserialize_with = "deserialize_optional_from_str", default)]
	pub time_zone: Option<Tz>,
	pub summer_time_zone_offset: Option<f64>,
	#[serde(deserialize_with = "deserialize_optional_from_str", default)]
	pub summer_time_zone: Option<Tz>,
	#[serde(deserialize_with = "deserialize_optional_from_str", default)]
	pub default_language: Option<String>,
}

#[allow(clippy::fallible_impl_from)]
impl From<Locale> for Timezone {
	fn from(value: Locale) -> Self {
		Self::Named(value.time_zone.unwrap()) // TODO
	}
}
