use std::collections::HashMap;

use crate::{
	Mode, Network, Timezone,
	netex::{
		CommonFrame, CompositeFrame, Journey, PublicationDelivery, QuayRel, ResourceFrame,
		RouteView, ServiceCalendarFrame, ServiceFrame, SiteFrame, TimetableFrame, TransportMode,
	},
};

pub struct Converter<'a> {
	network: &'a mut Network,
	journey_patterns: HashMap<String, CServicePattern>,
}

impl<'a> Converter<'a> {
	pub fn new(network: &'a mut Network) -> Self {
		Self {
			network,
			journey_patterns: HashMap::new(),
		}
	}
}

struct CServicePattern {
	line: Option<String>,
	stops: HashMap<String, crate::RouteStop>,
}

impl PublicationDelivery {
	pub fn insert_into(self, converter: &mut Converter) {
		for frame in self.data_objects {
			frame.insert_into(converter);
		}
	}
}

impl CompositeFrame {
	pub fn insert_into(self, converter: &mut Converter) {
		let tz = self
			.version_frame
			.frame_defaults
			.unwrap()
			.locale
			.unwrap()
			.into();

		for frame in self.frames {
			match frame {
				CommonFrame::Site(frame) => frame.insert_into(converter),
				CommonFrame::Infrastructure(_) => {}
				CommonFrame::ServiceCalendar(frame) => frame.insert_into(converter),
				CommonFrame::Service(frame) => frame.insert_into(converter),
				CommonFrame::Resource(frame) => frame.insert_into(converter),
				CommonFrame::Timetable(frame) => frame.insert_into(converter, tz),
			}
		}
	}
}

impl SiteFrame {
	pub fn insert_into(self, converter: &mut Converter) {
		for mut place in self.stop_places {
			let location = place.centroid.and_then(|c| c.location).map(Into::into);
			let cplace = crate::Stop {
				name: place.name.map(|s| s.content),
				code: None,
				r#ref: None,
				ifopt: place.key_list.remove("GlobalID"),
				uic: None,
				location,
				parent: None,
			};

			for quay in place.quays {
				let QuayRel::Owned(mut quay) = quay else {
					continue;
				};

				let location = quay.centroid.and_then(|c| c.location).map(Into::into);
				let cstop = crate::Stop {
					name: quay.name.map(|s| s.content),
					code: None,
					r#ref: None,
					ifopt: quay.key_list.remove("GlobalID"),
					uic: None,
					location,
					parent: Some(place.id.clone()),
				};
				converter.network.stops.insert(quay.id, cstop);
			}

			converter.network.stops.insert(place.id, cplace);
		}
	}
}

impl ServiceCalendarFrame {
	pub fn insert_into(self, converter: &mut Converter) {
		let Some(calendar) = self.service_calendar else {
			return;
		};

		let op: HashMap<_, _> = calendar
			.operating_periods
			.into_iter()
			.map(|op| {
				let service = crate::Service::Bits {
					from_date: op.from_date.date(),
					to_date: op.to_date.map(|d| d.date()),
					days: op.valid_day_bits,
				};
				(op.id, service)
			})
			.collect();

		for dta in calendar.day_type_assignments {
			converter.network.services.insert(
				dta.day_type_ref.r#ref,
				op.get(&dta.operating_period_ref.r#ref).unwrap().clone(),
			);
		}
	}
}

impl ServiceFrame {
	pub fn insert_into(self, converter: &mut Converter) {
		for line in self.lines {
			let (color, text_color) = if let Some(p) = line.presentation {
				(p.colour, p.text_colour)
			} else {
				(None, None)
			};

			let l = crate::Line {
				authority: line.authority_ref.map(|r| r.r#ref),
				operators: line
					.operator_ref
					.into_iter()
					.chain(line.additional_operators)
					.map(|r| r.r#ref)
					.collect(),
				mode: line.transport_mode.and_then(Into::into),
				name: line.name.content,
				color,
				text_color,
			};
			converter.network.lines.insert(line.id, l);
		}

		let ssp_to_quay: HashMap<_, _> = self
			.stop_assignments
			.into_iter()
			.map(|sa| {
				(
					sa.scheduled_stop_point.unwrap().into_id(),
					sa.quay.unwrap().into_id(),
				)
			})
			.collect();

		for ssp in self.scheduled_stop_points {
			let cstop = crate::Stop {
				name: ssp.name.map(|s| s.content),
				code: None,
				r#ref: None,
				ifopt: None,
				uic: None,
				location: ssp.location.map(Into::into),
				parent: ssp_to_quay.get(&ssp.id).cloned(),
			};
			converter.network.stops.insert(ssp.id, cstop);
		}

		let routes: HashMap<_, _> = self
			.routes
			.into_iter()
			.filter_map(|r| Some((r.id, r.line_ref?.r#ref)))
			.collect();

		for jp in self.journey_patterns {
			let jp = jp.journey_pattern();

			let line = match jp.route_view {
				Some(RouteView::Owned(r)) => r.line_ref.map(|r| r.r#ref),
				Some(RouteView::Ref(r)) => routes.get(&r.r#ref).cloned(),
				None => None,
			};

			let stops = jp
				.points_in_sequence
				.into_iter()
				.map(|s| {
					let stop = crate::RouteStop {
						id: s.scheduled_stop_point_ref.r#ref,
						arrival: None,
						departure: None,
						board: s.for_boarding.into(),
						alight: s.for_alighting,
					};
					(s.id, stop)
				})
				.collect();
			converter
				.journey_patterns
				.insert(jp.id, CServicePattern { line, stops });
		}
	}
}

impl ResourceFrame {
	pub fn insert_into(self, converter: &mut Converter) {
		for o in self.organisations {
			let o = o.operator();
			let (url, email, phone) = if let Some(cd) = o.contact_details {
				(cd.url, cd.email, cd.phone)
			} else {
				(None, None, None)
			};

			converter
				.network
				.organizations
				.insert(o.id, crate::Organization {
					name: o.name.map(|s| s.content),
					url,
					email,
					phone,
				});
		}
	}
}

impl TimetableFrame {
	pub fn insert_into(self, converter: &mut Converter, tz: Timezone) {
		let tz = self
			.version_frame
			.frame_defaults
			.and_then(|fd| fd.locale)
			.map_or(tz, Into::into);

		for vj in self.vehicle_journeys {
			let Journey::ServiceJourney(sj) = vj;
			let Some(sj_ref) = sj.service_journey_pattern_ref else {
				continue;
			};
			let jp = converter.journey_patterns.get(&sj_ref.r#ref).unwrap();

			let day_type = sj.day_types.into_iter().next().unwrap();

			let stops = sj
				.passing_times
				.into_iter()
				.map(|pt| {
					let mut sp = jp
						.stops
						.get(&pt.stop_point_in_journey_pattern.unwrap().r#ref)
						.unwrap()
						.clone();

					sp.arrival = pt
						.arrival_time
						.map(|t| t.to_global(pt.arrival_day_offset, tz));
					sp.departure = pt
						.departure_time
						.map(|t| t.to_global(pt.departure_day_offset, tz));
					sp
				})
				.collect();
			converter.network.routes.insert(sj.id, crate::Route {
				service: day_type.r#ref,
				line: jp.line.clone(),
				shape: None,
				stops,

				restaurant: None,
				ac: None,
				outlets: None,
			});
		}
	}
}

impl From<TransportMode> for Option<Mode> {
	fn from(value: TransportMode) -> Self {
		Some(match value {
			TransportMode::All
			| TransportMode::Unknown
			| TransportMode::AnyMode
			| TransportMode::Other => return None,
			TransportMode::Bus => Mode::Bus,
			TransportMode::TrolleyBus => Mode::TrolleyBus,
			TransportMode::Tram => Mode::Tram,
			TransportMode::Coach => Mode::Coach,
			TransportMode::Rail => Mode::Rail,
			TransportMode::IntercityRail => Mode::LongDistanceRail,
			TransportMode::UrbanRail => Mode::UrbanRail,
			TransportMode::Metro => Mode::Metro,
			TransportMode::Air => Mode::Air,
			TransportMode::Water | TransportMode::Ferry => Mode::Ferry,
			TransportMode::Cablecar => Mode::Cablecar,
			TransportMode::Funicular => Mode::Funicular,
			TransportMode::SnowAndIce => todo!(),
			TransportMode::Taxi => Mode::Taxi,
			TransportMode::Lift => todo!(),
			TransportMode::SelfDrive => todo!(),
		})
	}
}
