use std::fmt::Debug;

use chrono::NaiveDateTime;
use serde::Deserialize;

use crate::{
	DayBits,
	netex::{MultilingualString, Ref, VersionFrame, const_true, unwrap_list},
};

#[derive(Debug, Deserialize)]
#[serde(rename_all = "PascalCase")]
pub struct ServiceCalendarFrame {
	#[serde(rename = "@id")]
	pub id: String,
	#[serde(flatten, default)]
	pub version_frame: VersionFrame,

	pub service_calendar: Option<ServiceCalendar>,
}

#[derive(Debug, Deserialize)]
#[serde(rename_all = "camelCase")]
pub struct ServiceCalendar {
	#[serde(deserialize_with = "unwrap_list", default)]
	pub day_types: Vec<DayType>,
	#[serde(deserialize_with = "unwrap_list", default)]
	pub operating_periods: Vec<UicOperatingPeriod>,
	#[serde(deserialize_with = "unwrap_list", default)]
	pub day_type_assignments: Vec<DayTypeAssignment>,
}

#[derive(Debug, Deserialize)]
pub struct DayType {
	#[serde(rename = "@id")]
	pub id: String,
}

#[derive(Debug, Deserialize)]
#[serde(rename_all = "PascalCase")]
pub struct UicOperatingPeriod {
	#[serde(rename = "@id")]
	pub id: String,

	pub name: Option<MultilingualString>,
	pub short_name: Option<MultilingualString>,

	pub from_date: NaiveDateTime,
	pub to_date: Option<NaiveDateTime>,
	#[serde(with = "day_bits")]
	pub valid_day_bits: DayBits,
}

mod day_bits {
	use serde::{Deserialize, Deserializer, de};

	use crate::DayBits;

	pub fn deserialize<'de, D: Deserializer<'de>>(deserializer: D) -> Result<DayBits, D::Error> {
		String::deserialize(deserializer)?
			.as_bytes()
			.chunks(8)
			.map(|c| {
				c.iter()
					.map(|b| match b {
						b'0' => Ok(0),
						b'1' => Ok(1),
						_ => Err("unknown byte in day bits"),
					})
					.enumerate()
					.try_fold(0, |byte, (i, bit)| bit.map(|bit| byte | bit << (7 - i)))
			})
			.collect::<Result<_, _>>()
			.map_err(de::Error::custom)
			.map(DayBits)
	}
}

#[derive(Debug, Deserialize)]
#[serde(rename_all = "PascalCase")]
pub struct DayTypeAssignment {
	pub operating_period_ref: Ref,
	pub day_type_ref: Ref,

	#[serde(rename = "isAvailable", default = "const_true")]
	pub is_available: bool,
}
