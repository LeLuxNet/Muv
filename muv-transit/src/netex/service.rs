use serde::Deserialize;

use crate::netex::{
	Location, MultilingualString, QuayRel, Ref, StopPlaceRel, TransportMode, VersionFrame,
	const_true, deserialize_from_str, unwrap_enum_list, unwrap_list,
};

#[derive(Debug, Deserialize)]
#[serde(rename_all = "camelCase")]
pub struct ServiceFrame {
	#[serde(rename = "@id")]
	pub id: String,
	#[serde(flatten, default)]
	pub version_frame: VersionFrame,

	#[serde(deserialize_with = "unwrap_list", default)]
	pub routes: Vec<Route>,
	#[serde(deserialize_with = "unwrap_list", default)]
	pub lines: Vec<Line>,
	#[serde(deserialize_with = "unwrap_list", default)]
	pub scheduled_stop_points: Vec<ScheduledStopPoint>,
	#[serde(deserialize_with = "unwrap_list", default)]
	pub service_links: Vec<ServiceLink>,
	#[serde(deserialize_with = "unwrap_list", default)]
	pub journey_patterns: Vec<JourneyPattern_>,
	#[serde(deserialize_with = "unwrap_list", default)]
	pub stop_assignments: Vec<PassengerStopAssignment>,
}

#[derive(Debug, Deserialize)]
#[serde(rename_all = "PascalCase")]
pub struct Route {
	#[serde(rename = "@id")]
	pub id: String,

	pub line_ref: Option<Ref>,
	pub direction_type: Option<DirectionType>,
	pub direction_ref: Option<Ref>,
}
rel!(RouteView(Route) => "RouteView" + "RouteRef");

#[derive(Debug, Deserialize)]
#[serde(rename_all = "lowercase")]
pub enum DirectionType {
	Inbound,
	Outbound,
	Clockwise,
	Anticlockwise,
}

#[derive(Debug, Deserialize)]
#[serde(rename_all = "PascalCase")]
pub struct Line {
	#[serde(rename = "@id")]
	pub id: String,

	pub name: MultilingualString,
	pub short_name: Option<MultilingualString>,
	pub description: Option<MultilingualString>,
	pub transport_mode: Option<TransportMode>,
	pub url: Option<String>,

	pub public_code: Option<String>,
	pub private_code: Option<String>,

	pub authority_ref: Option<Ref>,
	pub operator_ref: Option<Ref>,
	#[serde(
		rename = "additionalOperators",
		deserialize_with = "unwrap_list",
		default
	)]
	pub additional_operators: Vec<Ref>,

	pub presentation: Option<Presentation>,
	pub alternative_presentation: Option<Presentation>,
	pub printed_presentation: Option<Presentation>,
}

#[derive(Debug, Deserialize)]
#[serde(rename_all = "PascalCase")]
pub struct Presentation {
	pub colour: Option<Colour>,
	pub background_colour: Option<Colour>,
	pub text_colour: Option<Colour>,
}

pub type Colour = String;

#[derive(Debug, Deserialize)]
#[serde(rename_all = "PascalCase")]
pub struct ScheduledStopPoint {
	#[serde(rename = "@id")]
	pub id: String,

	pub name: Option<MultilingualString>,
	pub short_name: Option<MultilingualString>,
	pub description: Option<MultilingualString>,
	pub presentation: Option<Presentation>,

	pub short_stop_code: Option<String>,
	pub public_code: Option<String>,
	pub private_code: Option<String>,

	pub location: Option<Location>,
}
rel!(ScheduledStopPointRel(ScheduledStopPoint) => "ScheduledStopPoint" + "ScheduledStopPointRef");

#[derive(Debug, Deserialize)]
#[serde(rename_all = "PascalCase")]
pub struct ServiceLink {
	#[serde(rename = "@id")]
	pub id: String,

	pub from_point_ref: Ref,
	pub to_point_ref: Ref,
}

#[derive(Debug, Deserialize)]
pub enum JourneyPattern_ {
	JourneyPattern(JourneyPattern),
	ServiceJourneyPattern(ServiceJourneyPattern),
	DeadRunJourneyPattern(DeadRunJourneyPattern),
}

impl JourneyPattern_ {
	#[must_use]
	pub fn journey_pattern(self) -> JourneyPattern {
		match self {
			Self::JourneyPattern(j) => j,
			Self::ServiceJourneyPattern(j) => j.journey_pattern,
			Self::DeadRunJourneyPattern(j) => j.journey_pattern,
		}
	}
}

#[derive(Debug, Deserialize)]
#[serde(rename_all = "PascalCase")]
pub struct JourneyPattern {
	#[serde(rename = "@id")]
	pub id: String,

	#[serde(flatten)]
	pub route_view: Option<RouteView>,

	#[serde(
		rename = "pointsInSequence",
		deserialize_with = "unwrap_enum_list",
		default
	)]
	pub points_in_sequence: Vec<StopPointInJourneyPattern>,
}

#[derive(Debug, Deserialize)]
#[serde(rename_all = "PascalCase")]
pub struct ServiceJourneyPattern {
	#[serde(flatten)]
	pub journey_pattern: JourneyPattern,
}

#[derive(Debug, Deserialize)]
#[serde(rename_all = "PascalCase")]
pub struct DeadRunJourneyPattern {
	#[serde(flatten)]
	pub journey_pattern: JourneyPattern,
}

#[derive(Debug, Deserialize)]
#[serde(rename_all = "PascalCase")]
pub struct StopPointInJourneyPattern {
	#[serde(rename = "@id")]
	pub id: String,

	pub scheduled_stop_point_ref: Ref,

	#[serde(deserialize_with = "deserialize_from_str", default = "const_true")]
	pub for_alighting: bool,
	#[serde(deserialize_with = "deserialize_from_str", default = "const_true")]
	pub for_boarding: bool,
}

#[derive(Debug, Deserialize)]
#[serde(rename_all = "PascalCase")]
pub struct PassengerStopAssignment {
	#[serde(rename = "@id")]
	pub id: String,

	#[serde(default = "const_true")]
	pub boarding_use: bool,
	#[serde(default = "const_true")]
	pub alighting_use: bool,

	#[serde(flatten)]
	pub scheduled_stop_point: Option<ScheduledStopPointRel>,
	#[serde(flatten)]
	pub stop_place: Option<StopPlaceRel>,
	#[serde(flatten)]
	pub quay: Option<QuayRel>,
	#[serde(flatten)]
	pub boarding_position: Option<BoardingPositionRel>,
}

#[derive(Debug, Deserialize)]
pub struct BoardingPosition {
	#[serde(rename = "@id")]
	pub id: String,
}
rel!(BoardingPositionRel(BoardingPosition) => "BoardingPosition" + "BoardingPositionRef");
