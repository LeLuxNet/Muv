use std::str::FromStr;

use chrono::{
	FixedOffset, NaiveTime, ParseError,
	format::{Fixed, Item, Parsed, StrftimeItems, parse, parse_and_remainder},
};
use serde::{Deserialize, Deserializer, de};

use crate::Timezone;

macro_rules! rel {
	($relident:ident($ident:ident) => $str:literal + $refstr:literal) => {
		#[derive(Debug, Deserialize)]
		pub enum $relident {
			#[serde(rename = $str)]
			Owned(Box<$ident>),
			#[serde(rename = $refstr)]
			Ref($crate::netex::Ref),
		}

		impl $relident {
			#[must_use]
			pub fn into_id(self) -> String {
				match self {
					Self::Owned(v) => v.id,
					Self::Ref(r) => r.r#ref,
				}
			}

			#[must_use]
			pub fn id(&self) -> &str {
				match self {
					Self::Owned(v) => &v.id,
					Self::Ref(r) => &r.r#ref,
				}
			}
		}
	};
}

#[derive(Debug, Deserialize)]
pub struct MultilingualString {
	#[serde(rename = "@lang")]
	pub lang: Option<String>,
	#[serde(rename = "$text")]
	pub content: String,
}

#[derive(Debug, Deserialize)]
#[serde(rename_all = "snake_case")]
pub enum TransportMode {
	All,
	Unknown,
	Bus,
	TrolleyBus,
	Tram,
	Coach,
	Rail,
	IntercityRail,
	UrbanRail,
	Metro,
	Air,
	Water,
	Cablecar,
	Funicular,
	SnowAndIce,
	Taxi,
	Ferry,
	Lift,
	SelfDrive,
	AnyMode,
	Other,
}

#[derive(Debug, Clone, Copy, Deserialize)]
#[serde(rename_all = "PascalCase")]
pub struct Location {
	pub latitude: f64,
	pub longitude: f64,
}

impl From<Location> for crate::Location {
	fn from(value: Location) -> Self {
		Self {
			lat: value.latitude,
			lon: value.longitude,
		}
	}
}

#[derive(Debug, Deserialize)]
pub struct Ref {
	#[serde(rename = "@ref")]
	pub r#ref: String,
}

pub(crate) const fn const_true() -> bool {
	true
}

#[derive(Debug, Clone, Copy, PartialEq, Eq)]
pub struct Time {
	pub time: NaiveTime,
	pub tz: Option<FixedOffset>,
}

impl Time {
	pub(crate) fn to_global(self, days_offset: i32, tz: Timezone) -> crate::Time {
		let tz = self.tz.map_or(tz, Timezone::Fixed);
		let mut time: crate::PrimitiveTime = self.time.into();
		time.hours += u8::try_from(days_offset).unwrap() * 24;
		crate::Time::new(time, tz)
	}
}

impl FromStr for Time {
	type Err = ParseError;

	fn from_str(s: &str) -> Result<Self, Self::Err> {
		let mut parsed = Parsed::new();
		let s = parse_and_remainder(&mut parsed, s, StrftimeItems::new("%T%.f"))?;
		let time = parsed.to_naive_time()?;

		let tz = if s.is_empty() {
			None
		} else {
			const TZ_FORMAT: &[Item<'static>] = &[Item::Fixed(Fixed::TimezoneOffsetColonZ)];
			parse(&mut parsed, s, TZ_FORMAT.iter())?;
			Some(parsed.to_fixed_offset()?)
		};

		Ok(Self { time, tz })
	}
}

impl<'de> Deserialize<'de> for Time {
	fn deserialize<D: Deserializer<'de>>(deserializer: D) -> Result<Self, D::Error> {
		let s = String::deserialize(deserializer)?;
		s.parse().map_err(de::Error::custom)
	}
}

#[cfg(test)]
mod tests {
	use chrono::{FixedOffset, NaiveTime};

	use crate::netex::Time;

	#[test]
	fn parse_time() {
		assert_eq!(
			"12:34:56.789".parse(),
			Ok(Time {
				time: NaiveTime::from_hms_milli_opt(12, 34, 56, 789).unwrap(),
				tz: None,
			})
		);
		assert_eq!(
			"00:00:00Z".parse(),
			Ok(Time {
				time: NaiveTime::from_hms_opt(0, 0, 0).unwrap(),
				tz: FixedOffset::east_opt(0),
			})
		);
		assert_eq!(
			"02:30:00-02:00".parse(),
			Ok(Time {
				time: NaiveTime::from_hms_opt(2, 30, 0).unwrap(),
				tz: FixedOffset::west_opt(2 * 3600),
			})
		);
	}
}
