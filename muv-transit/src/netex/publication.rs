use chrono::NaiveDateTime;
use serde::Deserialize;

use crate::netex::{CompositeFrame, unwrap_list};

#[derive(Debug, Deserialize)]
#[serde(rename_all = "PascalCase")]
pub struct PublicationDelivery {
	pub publication_timestamp: NaiveDateTime,

	#[serde(rename = "dataObjects", deserialize_with = "unwrap_list")]
	pub data_objects: Vec<CompositeFrame>,
}
