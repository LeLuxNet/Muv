use std::collections::HashMap;

use serde::Deserialize;

use crate::netex::{Location, MultilingualString, VersionFrame, deserialize_map, unwrap_list};

#[derive(Debug, Deserialize)]
pub struct SiteFrame {
	#[serde(rename = "@id")]
	pub id: String,
	#[serde(flatten, default)]
	pub version_frame: VersionFrame,

	#[serde(rename = "stopPlaces", deserialize_with = "unwrap_list", default)]
	pub stop_places: Vec<StopPlace>,
}

#[derive(Debug, Deserialize)]
#[serde(rename_all = "PascalCase")]
pub struct StopPlace {
	#[serde(rename = "@id")]
	pub id: String,
	#[serde(rename = "keyList", deserialize_with = "deserialize_map", default)]
	pub key_list: HashMap<String, String>,

	pub name: Option<MultilingualString>,

	pub centroid: Option<SimplePoint>,

	#[serde(rename = "quays", deserialize_with = "unwrap_list", default)]
	pub quays: Vec<QuayRel>,
}
rel!(StopPlaceRel(StopPlace) => "StopPlace" + "StopPlaceRef");

#[derive(Debug, Deserialize)]
#[serde(rename_all = "PascalCase")]
pub struct SimplePoint {
	pub name: Option<MultilingualString>,
	pub location: Option<Location>,
}

#[derive(Debug, Deserialize)]
#[serde(rename_all = "PascalCase")]
pub struct Quay {
	#[serde(rename = "@id")]
	pub id: String,
	#[serde(rename = "keyList", deserialize_with = "deserialize_map", default)]
	pub key_list: HashMap<String, String>,

	pub name: Option<MultilingualString>,
	pub short_name: Option<MultilingualString>,
	pub description: Option<MultilingualString>,

	pub centroid: Option<SimplePoint>,
}
rel!(QuayRel(Quay) => "Quay" + "QuayRef");
