#![cfg_attr(docsrs, feature(doc_auto_cfg))]
#![allow(clippy::missing_panics_doc, clippy::missing_errors_doc)]

use std::{
	collections::HashMap,
	fmt::{self, Debug, Display, Formatter},
};

#[cfg(feature = "serde")]
use std::result;

#[cfg(feature = "reqwest")]
use std::ops::Deref;

use chrono::TimeDelta;

mod service;
pub use service::*;
mod time;
pub use time::*;
mod error;
pub use error::*;
mod uic;
pub use uic::*;
mod vehicles;
pub use vehicles::*;

pub use muv_geo::Location;

#[cfg(feature = "serde")]
use serde::{Deserialize, Serialize, Serializer};

#[cfg(feature = "amtrak")]
pub mod amtrak;
#[cfg(feature = "db")]
pub mod db;
#[cfg(feature = "digitraffic")]
pub mod digitraffic;
#[cfg(feature = "gtfs")]
pub mod gtfs;
#[cfg(feature = "gtfs-rt")]
pub mod gtfs_rt;
#[cfg(feature = "hafas")]
pub mod hafas;
#[cfg(feature = "mav-start")]
pub mod mav_start;
#[cfg(feature = "netex")]
pub mod netex;
#[cfg(feature = "ns")]
pub mod ns;
#[cfg(feature = "sprava-zeleznic")]
pub mod sprava_zeleznic;
#[cfg(feature = "taxi-de")]
pub mod taxi_de;
#[cfg(feature = "viarail")]
pub mod viarail;
#[cfg(feature = "zsr")]
pub mod zsr;

#[derive(Debug, Default, Clone, PartialEq)]
pub struct Network {
	pub organizations: HashMap<OrganizationId, Organization>,
	pub stops: HashMap<StopId, Stop>,
	pub routes: HashMap<RouteId, Route>,
	pub services: HashMap<ServiceId, Service>,
	pub lines: HashMap<LineId, Line>,
	pub shapes: HashMap<ShapeId, Vec<Location>>,
}

pub type OrganizationId = String;
#[derive(Debug, Clone, PartialEq, Eq)]
#[cfg_attr(feature = "serde", derive(Serialize, Deserialize))]
pub struct Organization {
	pub name: Option<String>,
	pub url: Option<String>,
	pub email: Option<String>,
	pub phone: Option<String>,
}

pub type StopId = String;
#[derive(Debug, Clone, PartialEq)]
#[cfg_attr(feature = "serde", derive(Serialize, Deserialize))]
pub struct Stop {
	pub name: Option<String>,
	pub code: Option<String>,
	pub r#ref: Option<String>,
	pub ifopt: Option<String>,
	pub uic: Option<String>,

	pub location: Option<Location>,

	pub parent: Option<StopId>,
}

pub type LineId = String;
#[derive(Debug, Clone, PartialEq, Eq)]
#[cfg_attr(feature = "serde", derive(Serialize, Deserialize))]
pub struct Line {
	pub authority: Option<OrganizationId>,
	pub operators: Vec<OrganizationId>,
	pub mode: Option<Mode>,
	pub name: String,
	pub color: Option<String>,
	pub text_color: Option<String>,
}

#[derive(Debug, Clone, Copy, PartialEq, Eq, PartialOrd, Ord, Hash)]
#[cfg_attr(feature = "serde", derive(Serialize, Deserialize))]
pub enum Mode {
	Bus,
	TrolleyBus,
	Coach,

	Tram,
	Metro,

	Rail,
	UrbanRail,
	RegionalRail,
	LongDistanceRail,
	SleeperRail,
	FreightRail,
	SwitcherRail,
	SpecialRail,

	Monorail,
	Cablecar,
	Funicular,
	HorseDrawnCarriage,
	Ferry,
	Taxi,
	Air,
}

pub type RouteId = String;
#[derive(Debug, Clone, PartialEq, Eq)]
#[cfg_attr(feature = "serde", derive(Serialize, Deserialize))]
pub struct Route {
	pub service: ServiceId,
	pub line: Option<LineId>,
	pub shape: Option<ShapeId>,
	pub stops: Vec<RouteStop>,

	pub restaurant: Option<bool>,
	pub ac: Option<bool>,
	pub outlets: Option<bool>,
}

#[derive(Debug, Clone, PartialEq, Eq)]
#[cfg_attr(feature = "serde", derive(Serialize, Deserialize))]
pub struct RouteStop {
	pub id: StopId,

	pub arrival: Option<Time>,
	pub departure: Option<Time>,

	pub board: Board,
	pub alight: bool,
}

#[derive(Debug, Clone, PartialEq, Eq, PartialOrd, Ord, Hash)]
#[cfg_attr(feature = "serde", derive(Serialize, Deserialize))]
pub enum Board {
	Yes,
	Phone {
		#[cfg_attr(feature = "serde", serde(skip))] // TODO
		offset: Option<TimeDelta>,
		number: Option<String>,
	},
	No,
}

impl From<bool> for Board {
	fn from(value: bool) -> Self {
		if value { Self::Yes } else { Self::No }
	}
}

pub type ShapeId = String;

#[derive(Debug, Clone, PartialEq, Eq)]
pub struct Id {
	pub namespace: &'static str,
	pub id: String,
}

impl Id {
	#[must_use]
	pub const fn new(namespace: &'static str, id: String) -> Self {
		Self { namespace, id }
	}
}

impl Display for Id {
	fn fmt(&self, f: &mut Formatter<'_>) -> fmt::Result {
		write!(f, "{}:{}", self.namespace, self.id)
	}
}

#[cfg(feature = "serde")]
impl Serialize for Id {
	fn serialize<S: Serializer>(&self, serializer: S) -> result::Result<S::Ok, S::Error> {
		serializer.serialize_str(&self.to_string())
	}
}

pub type VehicleId = Id;
#[derive(Debug, Clone, PartialEq)]
#[cfg_attr(feature = "serde", derive(Serialize))]
pub struct Vehicle {
	pub id: VehicleId,
	#[cfg_attr(feature = "serde", serde(skip_serializing_if = "Option::is_none"))]
	pub uic: Option<UicMarking>,
	#[cfg_attr(feature = "serde", serde(skip_serializing_if = "Option::is_none"))]
	pub code: Option<String>,
	#[cfg_attr(feature = "serde", serde(skip_serializing_if = "Option::is_none"))]
	pub trip: Option<TripSelector>,
	#[cfg_attr(feature = "serde", serde(skip_serializing_if = "Option::is_none"))]
	pub line_name: Option<String>,
	#[cfg_attr(feature = "serde", serde(skip_serializing_if = "Option::is_none"))]
	pub mode: Option<Mode>,

	#[cfg_attr(feature = "serde", serde(skip_serializing_if = "Option::is_none"))]
	pub location: Option<Location>,
	#[cfg_attr(feature = "serde", serde(skip_serializing_if = "Option::is_none"))]
	pub heading: Option<f64>,
	#[cfg_attr(feature = "serde", serde(skip_serializing_if = "Option::is_none"))]
	pub speed: Option<f64>,

	pub carriages: Vec<Carriage>,
}

#[derive(Debug, Clone, PartialEq, Eq)]
#[cfg_attr(feature = "serde", derive(Serialize))]
pub struct Carriage {
	pub r#ref: Option<String>,
	pub seats: &'static [Seat],
}

#[derive(Debug, Clone, Copy, PartialEq, Eq)]
#[cfg_attr(feature = "serde", derive(Serialize, Deserialize))]
pub struct Seat {
	pub seats: u8,
	pub across: bool,
	pub table: bool,
	pub charger: bool,

	pub count: u8,
}

#[derive(Debug, Clone, PartialEq, Eq)]
#[cfg_attr(feature = "serde", derive(Serialize, Deserialize))]
pub struct TripSelector {
	#[cfg_attr(feature = "serde", serde(skip_serializing_if = "Option::is_none"))]
	pub line: Option<LineId>,
	#[cfg_attr(feature = "serde", serde(skip_serializing_if = "Option::is_none"))]
	pub route: Option<RouteId>,
	#[cfg_attr(feature = "serde", serde(skip_serializing_if = "Option::is_none"))]
	pub start_time: Option<PrimitiveTime>,
}

#[cfg(feature = "reqwest")]
#[derive(Debug, Default, Clone)]
pub struct Reqwest(pub reqwest::Client);

#[cfg(feature = "reqwest")]
impl Deref for Reqwest {
	type Target = reqwest::Client;
	fn deref(&self) -> &Self::Target {
		&self.0
	}
}

#[cfg(feature = "blocking")]
pub use scooter::Blocking;
#[cfg(feature = "tokio")]
pub use scooter::Tokio;
