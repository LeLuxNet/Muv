#![allow(clippy::print_stdout, clippy::print_stderr)]

use std::{
	env::args,
	error::Error,
	fs::File,
	process::exit,
	sync::atomic::{AtomicU32, AtomicU64, Ordering},
};

use muv_osm_pbf::{
	data::{DataEntry, DataIter, DenseNodes},
	read::Reader,
};

fn main() -> Result<(), Box<dyn Error>> {
	let Some(path) = args().nth(1) else {
		eprintln!("usage: <path to pbf file>");
		exit(1);
	};

	let (r, _) = Reader::new_file(File::open(path)?)?;

	let loose_nodes = AtomicU64::default();
	let dense_nodes = AtomicU64::default();
	let ways = AtomicU32::default();
	let relations = AtomicU32::default();
	let changesets = AtomicU32::default();

	let blocks = r.par_for_each(|_, _, blob| {
		let data = blob.decompress()?;

		let mut local_loose_nodes = 0;
		let mut local_dense_nodes = 0;
		let mut local_ways = 0;
		let mut local_relations = 0;
		let mut local_changesets = 0;

		for entry in DataIter::new(&data) {
			match entry? {
				DataEntry::Node(_) => local_loose_nodes += 1,
				DataEntry::DenseNodes(n) => {
					local_dense_nodes += u64::try_from(DenseNodes::decode(n)?.count()).unwrap();
				}
				DataEntry::Way(_) => local_ways += 1,
				DataEntry::Relation(_) => local_relations += 1,
				DataEntry::ChangeSet(_) => local_changesets += 1,
			}
		}

		loose_nodes.fetch_add(local_loose_nodes, Ordering::Relaxed);
		dense_nodes.fetch_add(local_dense_nodes, Ordering::Relaxed);
		ways.fetch_add(local_ways, Ordering::Relaxed);
		relations.fetch_add(local_relations, Ordering::Relaxed);
		changesets.fetch_add(local_changesets, Ordering::Relaxed);

		Ok(())
	})?;

	println!("{:>12} blocks", blocks);
	println!(" ----------------------");
	let loose_nodes = loose_nodes.into_inner();
	let dense_nodes = dense_nodes.into_inner();
	println!("{:>12} nodes", loose_nodes + dense_nodes);
	println!("{loose_nodes:>12} - loose");
	println!("{dense_nodes:>12} - dense");
	println!("{:>12} ways", ways.into_inner());
	println!("{:>12} relations", relations.into_inner());
	println!("{:>12} changesets", changesets.into_inner());

	Ok(())
}
