use protozer0::{PbfReader, Tag, VarintIter, WireType};

use crate::{DeltaCoded, Error, Result};

pub struct DenseNodes<'a> {
	id: DeltaCoded<'a, i64, u64>,

	lat: DeltaCoded<'a, i64>,
	lon: DeltaCoded<'a, i64>,

	keys_vals: VarintIter<'a, u32>,
}

impl<'a> DenseNodes<'a> {
	pub fn decode(b: &'a [u8]) -> Result<Self> {
		let mut id = None;
		let mut lat = None;
		let mut lon = None;
		let mut keys_vals = None;

		let mut r = PbfReader::new(b);
		while let Some(tag) = r.next()? {
			match tag {
				tag if tag == Tag::new(1, WireType::Len) => id = Some(r.packed_sint64()?),
				tag if tag == Tag::new(8, WireType::Len) => lat = Some(r.packed_sint64()?),
				tag if tag == Tag::new(9, WireType::Len) => lon = Some(r.packed_sint64()?),
				tag if tag == Tag::new(10, WireType::Len) => keys_vals = Some(r.packed_uint32()?),
				_ => r.skip()?,
			}
		}

		let (Some(id), Some(lat), Some(lon), Some(keys_vals)) = (id, lat, lon, keys_vals) else {
			return Err(Error::MissingDenseNodesField);
		};

		Ok(Self {
			id: DeltaCoded::new(id),
			lat: DeltaCoded::new(lat),
			lon: DeltaCoded::new(lon),
			keys_vals,
		})
	}
}

impl<'a> DenseNodes<'a> {
	pub fn next<'b>(&'b mut self) -> Result<Option<DenseNode<'a, 'b>>> {
		let Some(id) = self.id.next().transpose()? else {
			return Ok(None);
		};

		let lat = self.lat.next().transpose()?;
		let lon = self.lon.next().transpose()?;

		let (Some(lat), Some(lon)) = (lat, lon) else {
			return Err(Error::MismatchDenseNodesLength);
		};

		Ok(Some(DenseNode {
			id,
			lat,
			lon,
			keys_vals: Some(&mut self.keys_vals),
		}))
	}

	#[must_use]
	pub fn count(self) -> usize {
		self.id.count()
	}
}

pub struct DenseNode<'a, 'b> {
	pub id: u64,
	pub lat: i64,
	pub lon: i64,
	keys_vals: Option<&'b mut VarintIter<'a, u32>>,
}

impl Iterator for DenseNode<'_, '_> {
	type Item = Result<(u32, u32)>;

	fn next(&mut self) -> Option<Self::Item> {
		let keys_vals = self.keys_vals.take()?;

		let key = match keys_vals.next() {
			Some(Ok(0)) | None => {
				return None;
			}
			Some(Ok(key)) => key,
			Some(Err(e)) => return Some(Err(e.into())),
		};

		let res = match keys_vals.next() {
			Some(Ok(val)) => Some(Ok((key, val))),
			Some(Err(e)) => Some(Err(e.into())),
			None => None,
		};

		self.keys_vals = Some(keys_vals);

		res
	}
}

impl Drop for DenseNode<'_, '_> {
	fn drop(&mut self) {
		let Some(keys_vals) = self.keys_vals.take() else {
			return;
		};

		for val in keys_vals {
			if matches!(val, Ok(0) | Err(_)) {
				break;
			}
		}
	}
}
