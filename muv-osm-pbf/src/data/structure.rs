use protozer0::{PbfReader, Tag, WireType};

use crate::{Error, Nanodegrees, Result, data::StringTable};

#[derive(Debug, Clone)]
pub struct Data<'a> {
	pub string_table: StringTable<'a>,

	pub granularity: Option<i32>,
	pub lat_offset: Option<i64>,
	pub lon_offset: Option<i64>,
}

impl<'a> Data<'a> {
	pub fn decode(data: &'a [u8]) -> Result<Self> {
		let mut r = PbfReader::new(data);

		let mut string_table = None;
		let mut granularity = None;
		let mut lat_offset = None;
		let mut lon_offset = None;

		while let Some(tag) = r.next()? {
			match tag {
				tag if tag == Tag::new(1, WireType::Len) => string_table = Some(r.message()?),
				tag if tag == Tag::new(17, WireType::Varint) => granularity = Some(r.int32()?),
				tag if tag == Tag::new(19, WireType::Varint) => lat_offset = Some(r.int64()?),
				tag if tag == Tag::new(20, WireType::Varint) => lon_offset = Some(r.int64()?),
				_ => r.skip()?,
			}
		}

		let Some(string_table) = string_table else {
			return Err(Error::MissingStringTable);
		};

		Ok(Self {
			string_table: StringTable { r: string_table },
			granularity,
			lat_offset,
			lon_offset,
		})
	}

	#[must_use]
	pub fn granularity(&self) -> i32 {
		self.granularity.unwrap_or(100)
	}
	#[must_use]
	pub fn lat_offset(&self) -> i64 {
		self.lat_offset.unwrap_or(0)
	}
	#[must_use]
	pub fn lon_offset(&self) -> i64 {
		self.lon_offset.unwrap_or(0)
	}

	#[must_use]
	pub fn lat_nanodegrees(&self, lat: i64) -> Nanodegrees {
		Nanodegrees(lat * i64::from(self.granularity()) + self.lat_offset())
	}
	#[must_use]
	pub fn lon_nanodegrees(&self, lon: i64) -> Nanodegrees {
		Nanodegrees(lon * i64::from(self.granularity()) + self.lon_offset())
	}

	#[must_use]
	#[allow(clippy::as_conversions, clippy::cast_precision_loss)]
	pub fn lat(&self, lat: i64) -> f64 {
		self.lat_nanodegrees(lat).0 as f64 / 1e9
	}
	#[must_use]
	#[allow(clippy::as_conversions, clippy::cast_precision_loss)]
	pub fn lon(&self, lon: i64) -> f64 {
		self.lon_nanodegrees(lon).0 as f64 / 1e9
	}
}

pub enum DataEntry<'a> {
	Node(&'a [u8]),
	DenseNodes(&'a [u8]),
	Way(&'a [u8]),
	Relation(&'a [u8]),
	ChangeSet(&'a [u8]),
}

pub struct DataIter<'a> {
	data_r: PbfReader<'a>,
	group_r: Option<PbfReader<'a>>,
}

impl<'a> DataIter<'a> {
	#[must_use]
	pub const fn new(data: &'a [u8]) -> Self {
		Self {
			data_r: PbfReader::new(data),
			group_r: None,
		}
	}

	fn next_group(&mut self) -> Result<Option<PbfReader<'a>>> {
		while let Some(tag) = self.data_r.next()? {
			if tag == Tag::new(2, WireType::Len) {
				let group = self.data_r.message()?;
				return Ok(Some(group));
			}

			self.data_r.skip()?;
		}

		Ok(None)
	}

	fn next_entry(&mut self) -> Result<Option<DataEntry<'a>>> {
		loop {
			if self.group_r.is_none() {
				if let Some(group) = self.next_group()? {
					self.group_r = Some(group);
				} else {
					return Ok(None);
				}
			}
			let group_r = self.group_r.as_mut().unwrap();

			while let Some(tag) = group_r.next()? {
				let entry = match tag {
					tag if tag == Tag::new(1, WireType::Len) => DataEntry::Node(group_r.bytes()?),
					tag if tag == Tag::new(2, WireType::Len) => {
						DataEntry::DenseNodes(group_r.bytes()?)
					}
					tag if tag == Tag::new(3, WireType::Len) => DataEntry::Way(group_r.bytes()?),
					tag if tag == Tag::new(4, WireType::Len) => {
						DataEntry::Relation(group_r.bytes()?)
					}
					tag if tag == Tag::new(5, WireType::Len) => {
						DataEntry::ChangeSet(group_r.bytes()?)
					}
					_ => {
						group_r.skip()?;
						continue;
					}
				};
				return Ok(Some(entry));
			}

			self.group_r = None;
		}
	}
}

impl<'a> Iterator for DataIter<'a> {
	type Item = Result<DataEntry<'a>>;

	fn next(&mut self) -> Option<Self::Item> {
		self.next_entry().transpose()
	}
}
