use protozer0::{PbfReader, Tag, VarintIter, WireType};

use crate::{DeltaCoded, Error, Result};

pub struct Way<'a> {
	pub id: u64,
	pub keys_vals: KeysVals<'a>,
	pub refs: DeltaCoded<'a, i64, u64>,
}

impl<'a> Way<'a> {
	pub fn decode(b: &'a [u8]) -> Result<Self> {
		let mut r = PbfReader::new(b);

		let mut id = None;
		let mut keys = None;
		let mut vals = None;
		let mut refs = None;

		while let Some(tag) = r.next()? {
			match tag {
				tag if tag == Tag::new(1, WireType::Varint) => id = Some(r.uint64()?),
				tag if tag == Tag::new(2, WireType::Len) => keys = Some(r.packed_uint32()?),
				tag if tag == Tag::new(3, WireType::Len) => vals = Some(r.packed_uint32()?),
				tag if tag == Tag::new(8, WireType::Len) => refs = Some(r.packed_sint64()?),
				_ => r.skip()?,
			}
		}

		let (Some(id), Some(refs)) = (id, refs) else {
			return Err(Error::MissingWayField);
		};

		Ok(Self {
			id,
			keys_vals: KeysVals {
				keys: keys.unwrap_or_default(),
				vals: vals.unwrap_or_default(),
			},
			refs: DeltaCoded::new(refs),
		})
	}
}

#[derive(Default, Clone)]
pub struct KeysVals<'a> {
	pub(crate) keys: VarintIter<'a, u32>,
	pub(crate) vals: VarintIter<'a, u32>,
}

impl Iterator for KeysVals<'_> {
	type Item = Result<(u32, u32)>;

	fn next(&mut self) -> Option<Self::Item> {
		let key = match self.keys.next()? {
			Ok(key) => key,
			Err(e) => return Some(Err(e.into())),
		};

		let val = match self.vals.next()? {
			Ok(val) => val,
			Err(e) => return Some(Err(e.into())),
		};

		Some(Ok((key, val)))
	}
}
