use std::{
	fmt::{self, Debug, Formatter},
	iter::FusedIterator,
};

use protozer0::{PbfReader, Tag, WireType};

use crate::{Error, Result};

#[derive(Clone)]
pub struct StringTable<'a> {
	pub(crate) r: PbfReader<'a>,
}

impl Debug for StringTable<'_> {
	fn fmt(&self, f: &mut Formatter<'_>) -> fmt::Result {
		f.debug_list().entries(self.clone()).finish()
	}
}

impl StringTable<'_> {
	fn consume_tag(&mut self) -> Option<Result<bool>> {
		let val = self.r.next().transpose()?;

		let tag = match val {
			Err(e) => return Some(Err(e.into())),
			Ok(tag) => tag,
		};

		if tag != Tag::new(1, WireType::Len) {
			return Some(Err(Error::UnknownStringTableTag(tag.field_number())));
		}

		Some(Ok(true))
	}
}

impl<'a> Iterator for StringTable<'a> {
	type Item = Result<&'a str>;

	fn next(&mut self) -> Option<Self::Item> {
		if let Err(e) = self.consume_tag()? {
			self.r.data = &[];
			return Some(Err(e));
		}

		Some(match self.r.string() {
			Ok(v) => Ok(v),
			Err(e) => {
				self.r.data = &[];
				Err(e.into())
			}
		})
	}

	fn nth(&mut self, n: usize) -> Option<Self::Item> {
		for _ in 0..n {
			if let Err(e) = self.consume_tag()? {
				self.r.data = &[];
				return Some(Err(e));
			}

			if let Err(e) = self.r.skip() {
				self.r.data = &[];
				return Some(Err(e.into()));
			}
		}

		self.next()
	}
}

impl FusedIterator for StringTable<'_> {}
