use protozer0::{PbfReader, Tag, VarintIter, WireType};

use crate::{DeltaCoded, Error, Result, data::KeysVals};

pub struct Relation<'a> {
	pub id: u64,
	pub keys_vals: KeysVals<'a>,
	pub members: MemberIter<'a>,
}

impl<'a> Relation<'a> {
	pub fn decode(b: &'a [u8]) -> Result<Self> {
		let mut r = PbfReader::new(b);

		let mut id = None;
		let mut keys = None;
		let mut vals = None;
		let mut members_role = None;
		let mut members_id = None;
		let mut members_type = None;

		while let Some(tag) = r.next()? {
			match tag {
				tag if tag == Tag::new(1, WireType::Varint) => id = Some(r.uint64()?),
				tag if tag == Tag::new(2, WireType::Len) => keys = Some(r.packed_uint32()?),
				tag if tag == Tag::new(3, WireType::Len) => vals = Some(r.packed_uint32()?),
				tag if tag == Tag::new(8, WireType::Len) => members_role = Some(r.packed_uint32()?),
				tag if tag == Tag::new(9, WireType::Len) => members_id = Some(r.packed_sint64()?),
				tag if tag == Tag::new(10, WireType::Len) => {
					members_type = Some(r.packed_uint32()?);
				}
				_ => r.skip()?,
			}
		}

		let Some(id) = id else {
			return Err(Error::MissingRelationField);
		};

		Ok(Relation {
			id,
			keys_vals: KeysVals {
				keys: keys.unwrap_or_default(),
				vals: vals.unwrap_or_default(),
			},
			members: MemberIter {
				types: members_type.unwrap_or_default(),
				ids: DeltaCoded::new(members_id.unwrap_or_default()),
				roles: members_role.unwrap_or_default(),
			},
		})
	}
}

pub enum MemberType {
	Node = 0,
	Way = 1,
	Relation = 2,
}

pub struct Member {
	pub r#type: MemberType,
	pub id: u64,
	pub role: u32,
}

pub struct MemberIter<'a> {
	types: VarintIter<'a, u32>,
	ids: DeltaCoded<'a, i64, u64>,
	roles: VarintIter<'a, u32>,
}

impl Iterator for MemberIter<'_> {
	type Item = Result<Member>;

	fn next(&mut self) -> Option<Self::Item> {
		let r#type = match self.types.next()? {
			Ok(0) => MemberType::Node,
			Ok(1) => MemberType::Way,
			Ok(2) => MemberType::Relation,
			Ok(n) => return Some(Err(Error::UnknownMemberType(n))),
			Err(e) => return Some(Err(e.into())),
		};

		let id = match self.ids.next()? {
			Ok(val) => val,
			Err(e) => return Some(Err(e)),
		};

		let role = match self.roles.next()? {
			Ok(val) => val,
			Err(e) => return Some(Err(e.into())),
		};

		Some(Ok(Member { r#type, id, role }))
	}
}
