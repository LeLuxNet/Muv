use std::{fs::File, num::NonZero};

#[cfg(unix)]
use std::{
	io::{self},
	os::unix::fs::FileExt,
};

#[cfg(any(not(unix), target_os = "linux"))]
use std::io::BufReader;

#[cfg(unix)]
use rustix::fs::{Advice, fadvise};

use crate::{
	Blob, Result,
	read::{ParStrategy, Strategy},
};

#[cfg(any(not(unix), target_os = "linux"))]
use crate::read::ReadSeekStrategy;

#[cfg(unix)]
use crate::read::{ReadAt, ReadAtStrategy};

#[cfg(unix)]
impl ReadAt for File {
	fn read_at(&self, buf: &mut [u8], offset: u64) -> io::Result<usize> {
		FileExt::read_at(self, buf, offset)
	}

	fn read_exact_at(&self, buf: &mut [u8], offset: u64) -> io::Result<()> {
		FileExt::read_exact_at(self, buf, offset)
	}
}

#[derive(Debug)]
pub struct FileStrategy(
	#[cfg(target_os = "linux")]
	RandomAccessStrategy<ReadSeekStrategy<BufReader<File>>, ReadAtStrategy<File>>,
	#[cfg(all(unix, not(target_os = "linux")))] ReadAtStrategy<File>,
	#[cfg(not(unix))] ReadSeekStrategy<BufReader<File>>,
);

impl FileStrategy {
	pub(crate) fn new(file: File) -> Self {
		#[cfg(unix)]
		{
			let _ = fadvise(&file, 0, None, Advice::Sequential);
		}

		#[cfg(target_os = "linux")]
		{
			let strategy = if linux::block_type(&file).is_none_or(BlockType::random_access) {
				RandomAccessStrategy::Random(ReadAtStrategy(file))
			} else {
				RandomAccessStrategy::Sequential(ReadSeekStrategy::new(BufReader::new(file)))
			};
			Self(strategy)
		}

		#[cfg(all(unix, not(target_os = "linux")))]
		{
			Self(ReadAtStrategy(file))
		}

		#[cfg(not(unix))]
		Self(ReadSeekStrategy::new(BufReader::new(file)))
	}
}

impl Strategy for FileStrategy {
	fn next(&mut self, buf: &mut Vec<u8>, offset: u64, header_phase: bool) -> Result<Option<u64>> {
		self.0.next(buf, offset, header_phase)
	}

	fn seek_to(&mut self, buf: &mut Vec<u8>, offset: u64) -> io::Result<()> {
		self.0.seek_to(buf, offset)
	}
}

impl ParStrategy for FileStrategy {
	fn par_for_each(
		&mut self,
		offset: u64,
		parallelism: NonZero<usize>,
		f: impl Fn(u32, u64, Blob) -> Result<()> + Send + Sync,
	) -> Result<u32> {
		self.0.par_for_each(offset, parallelism, f)
	}
}

#[cfg(target_os = "linux")]
#[derive(Debug)]
enum RandomAccessStrategy<S, R> {
	Sequential(S),
	Random(R),
}

#[cfg(target_os = "linux")]
impl<S: Strategy, R: Strategy> Strategy for RandomAccessStrategy<S, R> {
	fn next(&mut self, buf: &mut Vec<u8>, offset: u64, header_phase: bool) -> Result<Option<u64>> {
		match self {
			Self::Sequential(s) => s.next(buf, offset, header_phase),
			Self::Random(s) => s.next(buf, offset, header_phase),
		}
	}

	fn seek_to(&mut self, buf: &mut Vec<u8>, offset: u64) -> io::Result<()> {
		match self {
			Self::Sequential(s) => s.seek_to(buf, offset),
			Self::Random(s) => s.seek_to(buf, offset),
		}
	}
}

#[cfg(target_os = "linux")]
impl<S: ParStrategy, R: ParStrategy> ParStrategy for RandomAccessStrategy<S, R> {
	fn par_for_each(
		&mut self,
		offset: u64,
		parallelism: NonZero<usize>,
		f: impl Fn(u32, u64, Blob) -> Result<()> + Send + Sync,
	) -> Result<u32> {
		match self {
			Self::Sequential(s) => s.par_for_each(offset, parallelism, f),
			Self::Random(s) => s.par_for_each(offset, parallelism, f),
		}
	}
}

#[cfg(target_os = "linux")]
#[derive(Debug, Clone, Copy)]
enum BlockType {
	TmpFs,
	NonRotational,
	Rotational,
}

#[cfg(target_os = "linux")]
impl BlockType {
	const fn random_access(self) -> bool {
		match self {
			Self::TmpFs | Self::NonRotational => true,
			Self::Rotational => false,
		}
	}
}

#[cfg(target_os = "linux")]
mod linux {
	use std::{
		collections::HashMap,
		fs::{File, metadata, read_link},
		io::{BufRead, BufReader, Read},
		os::unix::fs::MetadataExt,
		path::{Path, PathBuf},
		sync::{LazyLock, Mutex},
	};

	use rustix::fs::{AtFlags, StatxFlags, major, minor, statx};

	use super::BlockType;

	pub(crate) fn block_type(file: &File) -> Option<BlockType> {
		let mut mounts = (*MOUNTS).as_ref()?.lock().unwrap();
		let stat = statx(file, "", AtFlags::EMPTY_PATH, StatxFlags::MNT_ID).ok()?;
		mounts.get_mut(&stat.stx_mnt_id)?.resolve_type()
	}

	static MOUNTS: LazyLock<Option<Mutex<HashMap<u64, MountInfo>>>> =
		LazyLock::new(|| Some(Mutex::new(load_mounts()?)));

	fn load_mounts() -> Option<HashMap<u64, MountInfo>> {
		let mut mountinfo = BufReader::new(File::open("/proc/self/mountinfo").ok()?);
		let mut line = String::new();
		let mut mounts = HashMap::new();

		while mountinfo.read_line(&mut line).ok()? != 0 {
			let (id, rest) = line.split_once(' ')?;
			let id = id.parse().ok()?;

			let (_, rest) = rest.split_once(" - ")?;
			let mut params = rest.split(' ');

			let fs_type = params.next()?;
			let source = params.next()?;

			let info = match fs_type {
				"tmpfs" => MountInfo::Type(BlockType::TmpFs),
				_ if source.starts_with('/') => MountInfo::Source(PathBuf::from(source)),
				_ => MountInfo::Unknown,
			};
			mounts.insert(id, info);

			line.clear();
		}

		Some(mounts)
	}

	#[derive(Debug)]
	enum MountInfo {
		Type(BlockType),
		Source(PathBuf),
		Unknown,
	}

	impl MountInfo {
		fn resolve_type(&mut self) -> Option<BlockType> {
			let source = match self {
				Self::Type(t) => return Some(*t),
				Self::Source(s) => s,
				Self::Unknown => return None,
			};

			let metadata = metadata(source).ok()?;
			let dev = metadata.rdev();

			let block_dir = format!("/sys/dev/block/{}:{}", major(dev), minor(dev));
			let device_dir = read_link(block_dir).ok()?;
			let absolute_device_dir = Path::new("/sys/dev/block").join(device_dir);

			let mut dir = &*absolute_device_dir;
			let mut full_path = PathBuf::new();
			for i in 0..3 {
				if i != 0 {
					dir = dir.parent()?;
				}

				full_path.clear();
				full_path.push(dir);
				full_path.push("queue/rotational");

				if let Ok(mut rot_file) = File::open(&full_path) {
					let mut buf = [0; 1];
					rot_file.read_exact(&mut buf).ok()?;
					let r#type = match buf[0] {
						b'0' => BlockType::NonRotational,
						b'1' => BlockType::Rotational,
						_ => return None,
					};
					*self = Self::Type(r#type);
					return Some(r#type);
				}
			}

			None
		}
	}
}
