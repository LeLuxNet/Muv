use std::{
	self,
	io::{self, ErrorKind, Read},
	num::NonZero,
};

use crate::{
	Blob, Error, Result,
	read::{ParStrategy, Strategy, decode_header, parallel},
};

#[derive(Debug)]
pub struct ReadStrategy<R>(pub(crate) R);

impl<R: Read> Strategy for ReadStrategy<R> {
	fn next(&mut self, buf: &mut Vec<u8>, offset: u64, header_phase: bool) -> Result<Option<u64>> {
		next_blob_body(&mut self.0, buf, offset, header_phase)
	}

	fn seek_to(&mut self, buf: &mut Vec<u8>, mut offset: u64) -> io::Result<()> {
		let len = offset.min(1024).try_into().unwrap();
		if buf.len() < len {
			buf.resize(len, 0);
		}

		while offset != 0 {
			let len = buf.len().min(offset.try_into().unwrap_or(usize::MAX));
			match self.0.read(&mut buf[..len])? {
				0 => {
					return Err(io::Error::new(
						ErrorKind::UnexpectedEof,
						"failed to fill buffer to seek",
					));
				}
				n => offset -= u64::try_from(n).unwrap(),
			}
		}

		Ok(())
	}
}

impl<R: Read + Send> ParStrategy for ReadStrategy<R> {
	fn par_for_each(
		&mut self,
		offset: u64,
		parallelism: NonZero<usize>,
		f: impl Fn(u32, u64, Blob) -> Result<()> + Send + Sync,
	) -> Result<u32> {
		parallel(offset, &mut self.0, parallelism, f, |mut state, buf| {
			let offset = state.offset;
			let Some(new_offset) = next_blob_body(&mut state.data, buf, offset, false)? else {
				return Ok(true);
			};

			state.index += 1;
			state.offset = new_offset;
			drop(state);

			Ok(false)
		})
	}
}

fn next_blob_body(
	r: &mut impl Read,
	buf: &mut Vec<u8>,
	mut offset: u64,
	header_phase: bool,
) -> Result<Option<u64>> {
	loop {
		let mut header_size_buf = [0; 4];
		if let Err(e) = r.read_exact(&mut header_size_buf) {
			return if e.kind() == ErrorKind::UnexpectedEof {
				Ok(None)
			} else {
				Err(Error::Read(e))
			};
		}
		offset += 4;
		let header_size = u32::from_be_bytes(header_size_buf);

		let Ok(header_size) = u16::try_from(header_size) else {
			return Err(Error::HeaderTooBig(header_size));
		};

		buf.resize(header_size.into(), 0);
		r.read_exact(buf).map_err(Error::Read)?;
		offset += u64::from(header_size);

		let (data_size, known) = decode_header(buf, header_phase)?;

		buf.resize(data_size.into(), 0);
		r.read_exact(buf).map_err(Error::Read)?;
		offset += u64::from(data_size);

		if known {
			return Ok(Some(offset));
		}
	}
}
