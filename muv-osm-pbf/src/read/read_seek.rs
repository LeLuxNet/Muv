use std::{
	io::{self, Read, Seek, SeekFrom},
	num::NonZero,
};

use crate::{
	Result,
	read::{ParStrategy, ReadStrategy, Strategy},
};

#[derive(Debug)]
pub struct ReadSeekStrategy<R>(ReadStrategy<R>);

impl<R> ReadSeekStrategy<R> {
	pub(crate) const fn new(r: R) -> Self {
		Self(ReadStrategy(r))
	}
}

impl<R: Read + Seek> Strategy for ReadSeekStrategy<R> {
	fn next(&mut self, buf: &mut Vec<u8>, offset: u64, header_phase: bool) -> Result<Option<u64>> {
		self.0.next(buf, offset, header_phase)
	}

	fn seek_to(&mut self, _buf: &mut Vec<u8>, offset: u64) -> io::Result<()> {
		self.0.0.seek(SeekFrom::Start(offset))?;
		Ok(())
	}
}

impl<R: Read + Send> ParStrategy for ReadSeekStrategy<R> {
	fn par_for_each(
		&mut self,
		offset: u64,
		parallelism: NonZero<usize>,
		f: impl Fn(u32, u64, crate::Blob) -> Result<()> + Send + Sync,
	) -> Result<u32> {
		self.0.par_for_each(offset, parallelism, f)
	}
}
