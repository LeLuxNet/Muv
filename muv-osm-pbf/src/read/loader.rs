use std::{
	iter::Peekable,
	slice::Iter,
	sync::atomic::{AtomicU64, Ordering},
};

use crate::{
	Error, Result,
	data::{Data, DataEntry, DataIter, DenseNode, DenseNodes, Relation, Way},
	read::{ParStrategy, Reader, Strategy, parallelism},
};

#[derive(Debug)]
pub struct Loader<S> {
	reader: Reader<S>,

	indexed: bool,

	node_index: Range<u64>,
	dense_nodes_index: Range<u64>,
	way_index: Range<u64>,
	relation_index: Range<u64>,
}

impl<S: Strategy + ParStrategy + Send> Loader<S> {
	pub fn new(reader: Reader<S>) -> Self {
		Self {
			reader,

			indexed: false,

			node_index: Range::default(),
			dense_nodes_index: Range::default(),
			way_index: Range::default(),
			relation_index: Range::default(),
		}
	}
}

#[doc(hidden)]
pub trait BlockLoad {
	type Output<'a>;
	type Iteration<'a>: ?Sized;

	fn process<'a>(&self, data: Data<'a>) -> Result<Option<Self::Output<'a>>>;
	fn iteration_ref<'a, 'b>(output: &'b Self::Output<'a>) -> &'b Self::Iteration<'a>;
}

#[doc(hidden)]
pub struct BlockVecLoad;

impl BlockLoad for BlockVecLoad {
	type Output<'a> = Vec<&'a str>;
	type Iteration<'a> = [&'a str];

	fn process<'a>(&self, data: Data<'a>) -> Result<Option<Self::Output<'a>>> {
		let res: Result<_> = data.string_table.collect();
		Ok(Some(res?))
	}

	fn iteration_ref<'a, 'b>(output: &'b Self::Output<'a>) -> &'b Self::Iteration<'a> {
		output
	}
}

impl<T: Fn(Data) -> Result<Option<R>>, R> BlockLoad for T {
	type Output<'a> = R;
	type Iteration<'a> = R;

	fn process<'a>(&self, data: Data<'a>) -> Result<Option<Self::Output<'a>>> {
		self(data)
	}

	fn iteration_ref<'a, 'b>(output: &'b Self::Output<'a>) -> &'b Self::Iteration<'a> {
		output
	}
}

impl<S: Strategy + ParStrategy + Send> Loader<S> {
	#[allow(clippy::type_complexity)]
	pub fn load(
		&mut self,
	) -> LoaderRun<
		S,
		BlockVecLoad,
		fn(Data, &[&str], ()) -> Result<()>,
		fn(Data, &[&str], DenseNode) -> Result<()>,
		fn(Data, &[&str], Way) -> Result<()>,
		fn(Data, &[&str], Relation) -> Result<()>,
	> {
		LoaderRun {
			loader: self,

			block: BlockVecLoad,

			node: None,
			dense_nodes: None,
			way: None,
			relation: None,
		}
	}

	#[allow(clippy::type_complexity)]
	pub fn load_with<BlockFn: Fn(Data) -> Result<Option<Block>> + Send + Sync, Block>(
		&mut self,
		block: BlockFn,
	) -> LoaderRun<
		S,
		BlockFn,
		fn(Data, &Block, ()) -> Result<()>,
		fn(Data, &Block, DenseNode) -> Result<()>,
		fn(Data, &Block, Way) -> Result<()>,
		fn(Data, &Block, Relation) -> Result<()>,
	> {
		LoaderRun {
			loader: self,

			block,

			node: None,
			dense_nodes: None,
			way: None,
			relation: None,
		}
	}
}

#[must_use]
pub struct LoaderRun<
	'a,
	S: Strategy + ParStrategy + Send,
	BlockFn,
	NodeFn,
	DenseNodeFn,
	WayFn,
	RelationFn,
> {
	loader: &'a mut Loader<S>,

	block: BlockFn,

	node: Option<NodeFn>,
	dense_nodes: Option<DenseNodeFn>,
	way: Option<WayFn>,
	relation: Option<RelationFn>,
}

impl<
	'a,
	S: Strategy + ParStrategy + Send,
	BlockFn: BlockLoad + Send + Sync,
	NodeFn: Fn(Data, &BlockFn::Iteration<'_>, ()) -> Result<()> + Send + Sync,
	DenseNodeFn: Fn(Data, &BlockFn::Iteration<'_>, DenseNode) -> Result<()> + Send + Sync,
	WayFn: Fn(Data, &BlockFn::Iteration<'_>, Way) -> Result<()> + Send + Sync,
	RelationFn: Fn(Data, &BlockFn::Iteration<'_>, Relation) -> Result<()> + Send + Sync,
> LoaderRun<'a, S, BlockFn, NodeFn, DenseNodeFn, WayFn, RelationFn>
{
	pub fn node<F: Fn(Data, &BlockFn::Iteration<'_>, ()) -> Result<()> + Send + Sync>(
		self,
		f: F,
	) -> LoaderRun<'a, S, BlockFn, F, DenseNodeFn, WayFn, RelationFn> {
		LoaderRun {
			loader: self.loader,

			block: self.block,

			node: Some(f),
			dense_nodes: self.dense_nodes,
			way: self.way,
			relation: self.relation,
		}
	}

	pub fn dense_node<
		F: Fn(Data, &BlockFn::Iteration<'_>, DenseNode) -> Result<()> + Send + Sync,
	>(
		self,
		f: F,
	) -> LoaderRun<'a, S, BlockFn, NodeFn, F, WayFn, RelationFn> {
		LoaderRun {
			loader: self.loader,

			block: self.block,

			node: self.node,
			dense_nodes: Some(f),
			way: self.way,
			relation: self.relation,
		}
	}

	pub fn way<F: Fn(Data, &BlockFn::Iteration<'_>, Way) -> Result<()> + Send + Sync>(
		self,
		f: F,
	) -> LoaderRun<'a, S, BlockFn, NodeFn, DenseNodeFn, F, RelationFn> {
		LoaderRun {
			loader: self.loader,

			block: self.block,

			node: self.node,
			dense_nodes: self.dense_nodes,
			way: Some(f),
			relation: self.relation,
		}
	}

	pub fn relation<F: Fn(Data, &BlockFn::Iteration<'_>, Relation) -> Result<()> + Send + Sync>(
		self,
		f: F,
	) -> LoaderRun<'a, S, BlockFn, NodeFn, DenseNodeFn, WayFn, F> {
		LoaderRun {
			loader: self.loader,

			block: self.block,

			node: self.node,
			dense_nodes: self.dense_nodes,
			way: self.way,
			relation: Some(f),
		}
	}

	pub fn run(mut self) -> Result<()> {
		if self.loader.indexed {
			self.run_indexed()?;
		} else {
			self.run_fresh()?;
		}

		self.node = None;
		self.dense_nodes = None;
		self.way = None;
		self.relation = None;

		Ok(())
	}

	fn run_fresh(&mut self) -> Result<()> {
		let node_index: Range<AtomicU64> = self.loader.node_index.into();
		let dense_nodes_index: Range<AtomicU64> = self.loader.dense_nodes_index.into();
		let way_index: Range<AtomicU64> = self.loader.way_index.into();
		let relation_index: Range<AtomicU64> = self.loader.relation_index.into();

		self.loader.reader.strategy.par_for_each(
			self.loader.reader.offset,
			parallelism(),
			|_, offset, blob| {
				let body = blob.decompress()?;
				let data = Data::decode(&body)?;
				let Some(block) = self.block.process(data.clone())? else {
					for entry in DataIter::new(&body) {
						match entry? {
							DataEntry::Node(_) => node_index.insert(offset),
							DataEntry::DenseNodes(_) => dense_nodes_index.insert(offset),
							DataEntry::Way(_) => way_index.insert(offset),
							DataEntry::Relation(_) => relation_index.insert(offset),
							DataEntry::ChangeSet(_) => {}
						}
					}

					return Ok(());
				};
				let block_ref = BlockFn::iteration_ref(&block);

				for entry in DataIter::new(&body) {
					match entry? {
						DataEntry::Node(_) => {
							node_index.insert(offset);
							todo!();
						}
						DataEntry::DenseNodes(dn) => {
							dense_nodes_index.insert(offset);
							if let Some(dense_nodes) = self.dense_nodes.as_ref() {
								let mut dn = DenseNodes::decode(dn)?;
								while let Some(n) = dn.next()? {
									dense_nodes(data.clone(), block_ref, n)?;
								}
							}
						}
						DataEntry::Way(w) => {
							way_index.insert(offset);
							if let Some(way) = self.way.as_ref() {
								way(data.clone(), block_ref, Way::decode(w)?)?;
							}
						}
						DataEntry::Relation(r) => {
							relation_index.insert(offset);
							if let Some(relation) = self.relation.as_ref() {
								relation(data.clone(), block_ref, Relation::decode(r)?)?;
							}
						}
						DataEntry::ChangeSet(_) => {}
					}
				}

				Ok(())
			},
		)?;

		self.loader.node_index = node_index.into_inner();
		self.loader.dense_nodes_index = dense_nodes_index.into_inner();
		self.loader.way_index = way_index.into_inner();
		self.loader.relation_index = relation_index.into_inner();

		self.loader.indexed = true;

		Ok(())
	}

	fn run_indexed(&mut self) -> Result<()> {
		let mut offsets = [Range::default(); 4];
		let mut offsets_len = 0;
		if self.node.is_some() {
			offsets[offsets_len] = self.loader.node_index;
			offsets_len += 1;
		}
		if self.dense_nodes.is_some() {
			offsets[offsets_len] = self.loader.dense_nodes_index;
			offsets_len += 1;
		}
		if self.way.is_some() {
			offsets[offsets_len] = self.loader.way_index;
			offsets_len += 1;
		}
		if self.relation.is_some() {
			offsets[offsets_len] = self.loader.relation_index;
			offsets_len += 1;
		}

		let offsets = MultiRange::merge(&mut offsets[..offsets_len]);

		for offset in offsets {
			self.loader.reader.seek_to(offset.start)?;
			let end = offset.end;

			let res = self.loader.reader.strategy.par_for_each(
				offset.start,
				parallelism(),
				|_, offset, blob| {
					if offset > end {
						return Err(Error::NoHeaderBlob);
					}

					let body = blob.decompress()?;
					let data = Data::decode(&body)?;
					let Some(block) = self.block.process(data.clone())? else {
						return Ok(());
					};
					let block_ref = BlockFn::iteration_ref(&block);

					for entry in DataIter::new(&body) {
						match entry? {
							DataEntry::Node(_) => {
								todo!()
							}
							DataEntry::DenseNodes(dn) => {
								if let Some(dense_nodes) = self.dense_nodes.as_ref() {
									let mut dn = DenseNodes::decode(dn)?;
									while let Some(n) = dn.next()? {
										dense_nodes(data.clone(), block_ref, n)?;
									}
								}
							}
							DataEntry::Way(w) => {
								if let Some(way) = self.way.as_ref() {
									way(data.clone(), block_ref, Way::decode(w)?)?;
								}
							}
							DataEntry::Relation(r) => {
								if let Some(relation) = self.relation.as_ref() {
									relation(data.clone(), block_ref, Relation::decode(r)?)?;
								}
							}
							DataEntry::ChangeSet(_) => {}
						}
					}

					Ok(())
				},
			);

			match res {
				Ok(_) | Err(Error::NoHeaderBlob) => {}
				Err(e) => return Err(e),
			}
		}

		Ok(())
	}
}

struct MultiRange<'a>(Peekable<Iter<'a, Range<u64>>>);

impl<'a> MultiRange<'a> {
	fn merge(ranges: &'a mut [Range<u64>]) -> Self {
		ranges.sort_unstable_by_key(|r| r.start);
		Self(ranges.iter().peekable())
	}
}

impl Iterator for MultiRange<'_> {
	type Item = Range<u64>;

	fn next(&mut self) -> Option<Self::Item> {
		let mut base = *self.0.next()?;
		while let Some(new) = self.0.next_if(|new| new.start <= base.end) {
			base.end = base.end.max(new.end);
		}
		Some(base)
	}
}

#[derive(Debug, Clone, Copy, PartialEq)]
struct Range<T> {
	start: T,
	end: T,
}

impl Default for Range<u64> {
	fn default() -> Self {
		Self {
			start: u64::MAX,
			end: 0,
		}
	}
}

impl From<Range<u64>> for Range<AtomicU64> {
	fn from(value: Range<u64>) -> Self {
		Self {
			start: AtomicU64::new(value.start),
			end: AtomicU64::new(value.end),
		}
	}
}

impl Range<AtomicU64> {
	pub fn insert(&self, n: u64) {
		self.start.fetch_min(n, Ordering::Relaxed);
		self.end.fetch_max(n, Ordering::Relaxed);
	}

	pub const fn into_inner(self) -> Range<u64> {
		Range {
			start: self.start.into_inner(),
			end: self.end.into_inner(),
		}
	}
}

#[cfg(test)]
mod tests {
	use super::{MultiRange, Range};

	#[test]
	fn multi_range() {
		let mut from = [
			Range { start: 0, end: 4 },
			Range { start: 8, end: 9 },
			Range { start: 9, end: 11 },
			Range { start: 3, end: 5 },
			Range { start: 13, end: 14 },
		];
		let to = [
			Range { start: 0, end: 5 },
			Range { start: 8, end: 11 },
			Range { start: 13, end: 14 },
		];
		assert_eq!(MultiRange::merge(&mut from).collect::<Vec<_>>(), to);
	}
}
