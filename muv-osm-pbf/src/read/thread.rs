use std::{
	num::NonZero,
	sync::{
		Mutex, MutexGuard, OnceLock,
		atomic::{AtomicBool, Ordering},
	},
	thread::{Builder, available_parallelism, scope},
};

use crate::{Blob, Result};

pub(crate) struct State<T> {
	pub(crate) index: u32,
	pub(crate) offset: u64,
	pub(crate) data: T,
}

pub(crate) fn parallelism() -> NonZero<usize> {
	available_parallelism().unwrap_or(NonZero::new(2).unwrap())
}

pub(crate) fn parallel<M: Send>(
	offset: u64,
	data: M,
	parallelism: NonZero<usize>,
	f: impl Fn(u32, u64, Blob) -> Result<()> + Send + Sync,
	inner: impl Fn(MutexGuard<State<M>>, &mut Vec<u8>) -> Result<bool> + Send + Sync,
) -> Result<u32> {
	let lock = Mutex::new(State {
		index: 0,
		offset,
		data,
	});

	let run = AtomicBool::new(true);
	let err = OnceLock::default();

	#[allow(clippy::significant_drop_tightening)]
	let worker = || {
		let mut buf = Vec::with_capacity(16 * 1024 * 1024);

		while run.load(Ordering::Acquire) {
			let state = lock.lock().unwrap();
			let index = state.index;
			let offset = state.offset;

			if inner(state, &mut buf)? {
				return Ok(());
			}

			let blob = Blob::decode(&buf)?;
			f(index, offset, blob)?;
		}

		Ok(())
	};

	let catch_worker = || {
		let res = worker();
		run.store(false, Ordering::Release);
		if let Err(e) = res {
			let _ = err.set(e);
		}
	};

	scope(|s| {
		for i in 0..parallelism.get() - 1 {
			if Builder::new()
				.name(format!("muv-osm-pbf #{i}"))
				.spawn_scoped(s, catch_worker)
				.is_err()
			{
				break;
			}
		}

		catch_worker();
	});

	match err.into_inner() {
		None => Ok(lock.into_inner().unwrap().index),
		Some(e) => Err(e),
	}
}
