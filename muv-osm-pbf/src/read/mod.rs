use std::{
	fs::File,
	io::{self, Read, Seek},
	num::NonZero,
};

use protozer0::{PbfReader, Tag, WireType};

use crate::{Blob, Error, Result, header::Header};

#[allow(clippy::module_inception)]
mod read;
pub use read::*;

mod read_seek;
pub use read_seek::*;

mod read_at;
pub use read_at::*;

mod file;
pub use file::*;

mod thread;
pub(crate) use thread::*;

mod loader;
pub use loader::*;

#[derive(Debug)]
pub struct Reader<S> {
	pub(crate) strategy: S,
	pub(crate) buf: Vec<u8>,
	pub(crate) offset: u64,
}

pub trait Strategy {
	fn next(&mut self, buf: &mut Vec<u8>, offset: u64, header_phase: bool) -> Result<Option<u64>>;

	fn seek_to(&mut self, buf: &mut Vec<u8>, offset: u64) -> io::Result<()>;
}

impl<S: Strategy + ?Sized> Strategy for &mut S {
	fn next(&mut self, buf: &mut Vec<u8>, offset: u64, header_phase: bool) -> Result<Option<u64>> {
		(**self).next(buf, offset, header_phase)
	}

	fn seek_to(&mut self, buf: &mut Vec<u8>, offset: u64) -> io::Result<()> {
		(**self).seek_to(buf, offset)
	}
}

pub trait ParStrategy {
	fn par_for_each(
		&mut self,
		offset: u64,
		parallelism: NonZero<usize>,
		f: impl Fn(u32, u64, Blob) -> Result<()> + Send + Sync,
	) -> Result<u32>;
}

impl<S: Strategy> Reader<S> {
	fn new(mut strategy: S) -> Result<(Self, Header)> {
		let mut buf = Vec::new();
		let Some(offset) = strategy.next(&mut buf, 0, true)? else {
			return Err(Error::NoHeaderBlob);
		};

		let blob = Blob::decode(&buf)?;
		let data = blob.decompress()?;
		let mut header = Header::decode(&data)?;

		if let Some(i) = header
			.required_features
			.iter()
			.position(|f| !matches!(&**f, "OsmSchema-V0.6" | "DenseNodes"))
		{
			let name = header.required_features.remove(i);
			return Err(Error::UnsupportedFeature(name));
		}

		let r = Self {
			strategy,
			buf,
			offset,
		};

		Ok((r, header))
	}
}

impl<R: Read> Reader<ReadStrategy<R>> {
	pub fn new_read(reader: R) -> Result<(Self, Header)> {
		Self::new(ReadStrategy(reader))
	}
}

impl<R: Read + Seek> Reader<ReadSeekStrategy<R>> {
	pub fn new_read_seek(reader: R) -> Result<(Self, Header)> {
		Self::new(ReadSeekStrategy::new(reader))
	}
}

impl<R: ReadAt> Reader<ReadAtStrategy<R>> {
	pub fn new_read_at(reader: R) -> Result<(Self, Header)> {
		Self::new(ReadAtStrategy(reader))
	}
}

impl Reader<FileStrategy> {
	pub fn new_file(file: File) -> Result<(Self, Header)> {
		Self::new(FileStrategy::new(file))
	}
}

impl<S: Strategy> Reader<S> {
	#[allow(clippy::should_implement_trait)]
	pub fn next(&mut self) -> Result<Option<(u64, Blob)>> {
		let offset = self.offset;
		let Some(new_offset) = self.strategy.next(&mut self.buf, offset, false)? else {
			return Ok(None);
		};
		self.offset = new_offset;

		let blob = Blob::decode(&self.buf)?;
		Ok(Some((offset, blob)))
	}

	pub fn seek_to(&mut self, offset: u64) -> Result<()> {
		self.strategy
			.seek_to(&mut self.buf, offset)
			.map_err(Error::Seek)?;
		self.offset = offset;
		Ok(())
	}
}

impl<S: ParStrategy> Reader<S> {
	pub fn par_for_each<F: Fn(u32, u64, Blob) -> Result<()> + Send + Sync>(
		self,
		f: F,
	) -> Result<u32> {
		self.par_for_each_with(parallelism(), f)
	}

	pub fn par_for_each_with<F: Fn(u32, u64, Blob) -> Result<()> + Send + Sync>(
		mut self,
		parallelism: NonZero<usize>,
		f: F,
	) -> Result<u32> {
		self.strategy.par_for_each(self.offset, parallelism, f)
	}
}

#[derive(Clone, Copy)]
pub(crate) struct DataSize(
	#[cfg(target_pointer_width = "16")] usize,
	#[cfg(not(target_pointer_width = "16"))] u32,
);

impl DataSize {
	#[allow(clippy::missing_const_for_fn)]
	fn new(size: u32) -> Option<Self> {
		#[cfg(target_pointer_width = "16")]
		let size = size.try_into().ok()?;

		if size > 32 * 1024 * 1024 {
			None
		} else {
			Some(Self(size))
		}
	}
}

impl From<DataSize> for usize {
	#[allow(clippy::as_conversions)]
	fn from(value: DataSize) -> Self {
		value.0 as Self
	}
}

impl From<DataSize> for u32 {
	#[allow(clippy::as_conversions)]
	fn from(value: DataSize) -> Self {
		value.0 as Self
	}
}

impl From<DataSize> for u64 {
	fn from(value: DataSize) -> Self {
		u32::from(value).into()
	}
}

pub(crate) fn decode_header(header: &[u8], header_phase: bool) -> Result<(DataSize, bool)> {
	let mut pr = PbfReader::new(header);

	let mut r#type = None;
	let mut data_size = None;
	while let Some(tag) = pr.next()? {
		match tag {
			tag if tag == Tag::new(1, WireType::Len) => r#type = Some(pr.bytes()?),
			tag if tag == Tag::new(3, WireType::Varint) => data_size = Some(pr.uint32()?),
			_ => pr.skip()?,
		}
	}

	let data_size = data_size.ok_or(Error::MissingBlobSize)?;
	let data_size = DataSize::new(data_size).ok_or(Error::DataTooBig)?;
	let r#type = r#type.ok_or(Error::MissingBlobType)?;

	let known = if r#type == b"OSMData" {
		if header_phase {
			return Err(Error::NoHeaderBlob);
		}
		true
	} else {
		header_phase && r#type == b"OSMHeader"
	};

	Ok((data_size, known))
}
