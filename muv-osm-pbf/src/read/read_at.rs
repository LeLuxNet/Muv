use std::{
	io::{self, Cursor, ErrorKind},
	num::NonZero,
};

use crate::{
	Blob, Error, Result,
	read::{DataSize, ParStrategy, Strategy, decode_header, parallel},
};

pub trait ReadAt {
	fn read_at(&self, buf: &mut [u8], offset: u64) -> io::Result<usize>;

	fn read_exact_at(&self, buf: &mut [u8], offset: u64) -> io::Result<()>;
}

impl ReadAt for Cursor<&[u8]> {
	fn read_at(&self, buf: &mut [u8], offset: u64) -> io::Result<usize> {
		let offset = offset.try_into().unwrap_or(usize::MAX);
		match self.get_ref().split_at_checked(offset) {
			Some((_, src)) => {
				let len = src.len().min(buf.len());
				buf[..len].copy_from_slice(&src[..len]);
				Ok(len)
			}
			None => Ok(0),
		}
	}

	/// ```
	/// use muv_osm_pbf::read::ReadAt;
	/// use std::io::Cursor;
	///
	/// let input: Cursor<&[u8]> = Cursor::new(&[1, 2, 4, 5, 7, 9, 10, 11]);
	///
	/// let mut output = [0; 4];
	/// input.read_exact_at(&mut output, 3).unwrap();
	/// assert_eq!(output, [5, 7, 9, 10]);
	///
	/// assert!(input.read_exact_at(&mut output, 5).is_err());
	/// ```
	fn read_exact_at(&self, buf: &mut [u8], offset: u64) -> io::Result<()> {
		let data = self.get_ref();
		let Some(offset) = offset
			.try_into()
			.ok()
			.filter(|offset| offset + buf.len() <= data.len())
		else {
			return Err(io::Error::new(
				io::ErrorKind::UnexpectedEof,
				"offset bigger than usize::MAX",
			));
		};

		buf.copy_from_slice(&data[offset..offset + buf.len()]);
		Ok(())
	}
}

#[derive(Debug)]
pub struct ReadAtStrategy<R>(pub(crate) R);

impl<R: ReadAt> Strategy for ReadAtStrategy<R> {
	fn next(&mut self, buf: &mut Vec<u8>, offset: u64, header_phase: bool) -> Result<Option<u64>> {
		let Some((mut data_offset, data_size)) =
			next_blob_body_offset(&self.0, buf, offset, header_phase)?
		else {
			return Ok(None);
		};

		buf.resize(data_size.into(), 0);
		self.0
			.read_exact_at(buf, data_offset)
			.map_err(Error::Read)?;
		data_offset += u64::from(data_size);

		Ok(Some(data_offset))
	}

	fn seek_to(&mut self, _buf: &mut Vec<u8>, _offset: u64) -> io::Result<()> {
		Ok(())
	}
}

impl<R: ReadAt + Send + Sync> ParStrategy for ReadAtStrategy<R> {
	fn par_for_each(
		&mut self,
		offset: u64,
		parallelism: NonZero<usize>,
		f: impl Fn(u32, u64, Blob) -> Result<()> + Send + Sync,
	) -> Result<u32> {
		parallel(offset, (), parallelism, f, |mut state, buf| {
			let Some((data_offset, data_size)) =
				next_blob_body_offset(&self.0, buf, state.offset, false)?
			else {
				return Ok(true);
			};

			state.index += 1;
			state.offset = data_offset + u64::from(data_size);
			drop(state);

			buf.resize(data_size.into(), 0);
			self.0
				.read_exact_at(buf, data_offset)
				.map_err(Error::Read)?;

			Ok(false)
		})
	}
}

fn next_blob_body_offset(
	r: &impl ReadAt,
	buf: &mut Vec<u8>,
	mut offset: u64,
	header_phase: bool,
) -> Result<Option<(u64, DataSize)>> {
	loop {
		let mut header_size_buf = [0; 4];
		if let Err(e) = r.read_exact_at(&mut header_size_buf, offset) {
			return if e.kind() == ErrorKind::UnexpectedEof {
				Ok(None)
			} else {
				Err(Error::Read(e))
			};
		}
		offset += 4;
		let header_size = u32::from_be_bytes(header_size_buf);

		let Ok(header_size) = u16::try_from(header_size) else {
			return Err(Error::HeaderTooBig(header_size));
		};

		buf.resize(header_size.into(), 0);
		r.read_exact_at(buf, offset).map_err(Error::Read)?;
		offset += u64::from(header_size);

		let (data_size, known) = decode_header(buf, header_phase)?;

		if known {
			return Ok(Some((offset, data_size)));
		}
		offset += u64::from(data_size);
	}
}
