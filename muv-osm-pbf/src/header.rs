use std::fmt::Debug;

use protozer0::{PbfReader, PbfWriter, Tag, WireType};

use crate::{Error, Nanodegrees, Result};

#[derive(Debug, Default, Clone, PartialEq, Eq)]
pub struct Header {
	pub bbox: Option<Bbox>,

	pub required_features: Vec<String>,
	pub optional_features: Vec<String>,

	pub writing_program: Option<String>,
	pub source: Option<String>,
}

impl Header {
	pub fn decode(data: &[u8]) -> Result<Self> {
		let mut r = PbfReader::new(data);

		let mut res = Self::default();

		while let Some(tag) = r.next()? {
			match tag {
				tag if tag == Tag::new(1, WireType::Len) => {
					res.bbox = Some(Bbox::decode(r.bytes()?)?);
				}

				tag if tag == Tag::new(4, WireType::Len) => {
					res.required_features.push(r.string()?.to_owned());
				}
				tag if tag == Tag::new(5, WireType::Len) => {
					res.optional_features.push(r.string()?.to_owned());
				}

				tag if tag == Tag::new(16, WireType::Len) => {
					res.writing_program = Some(r.string()?.to_owned());
				}
				tag if tag == Tag::new(17, WireType::Len) => {
					res.source = Some(r.string()?.to_owned());
				}

				_ => r.skip()?,
			}
		}

		Ok(res)
	}

	pub(crate) fn encode(&self, w: &mut PbfWriter) {
		if let Some(bbox) = self.bbox {
			w.submessage(1);
			bbox.encode(w);
			w.commit();
		}

		for feature in &self.required_features {
			w.string(4, feature);
		}
		for feature in &self.optional_features {
			w.string(5, feature);
		}

		if let Some(writing_program) = &self.writing_program {
			w.string(16, writing_program);
		}
		if let Some(source) = &self.source {
			w.string(16, source);
		}
	}
}

#[derive(Debug, Clone, Copy, PartialEq, Eq, PartialOrd, Ord, Hash)]
pub struct Bbox {
	pub left: Nanodegrees,
	pub right: Nanodegrees,
	pub top: Nanodegrees,
	pub bottom: Nanodegrees,
}

impl Bbox {
	pub fn decode(data: &[u8]) -> Result<Self> {
		let mut r = PbfReader::new(data);

		let mut left = None;
		let mut right = None;
		let mut top = None;
		let mut bottom = None;

		while let Some(tag) = r.next()? {
			match tag {
				tag if tag == Tag::new(1, WireType::Varint) => left = Some(r.sint64()?),
				tag if tag == Tag::new(2, WireType::Varint) => right = Some(r.sint64()?),
				tag if tag == Tag::new(3, WireType::Varint) => top = Some(r.sint64()?),
				tag if tag == Tag::new(4, WireType::Varint) => bottom = Some(r.sint64()?),
				_ => r.skip()?,
			}
		}

		let (Some(left), Some(right), Some(top), Some(bottom)) = (left, right, top, bottom) else {
			return Err(Error::MissingBboxSide);
		};

		Ok(Self {
			left: Nanodegrees(left),
			right: Nanodegrees(right),
			top: Nanodegrees(top),
			bottom: Nanodegrees(bottom),
		})
	}

	fn encode(&self, w: &mut PbfWriter) {
		w.sint64(1, self.left.0);
		w.sint64(2, self.right.0);
		w.sint64(3, self.bottom.0);
		w.sint64(4, self.top.0);
	}
}
