use std::{borrow::Cow, io::Write};

use protozer0::PbfWriter;

use crate::{Blob, Error, Result, header::Header};

pub struct Writer<W> {
	w: W,
	header_buf: Vec<u8>,
	body_buf: Vec<u8>,
}

impl<W: Write> Writer<W> {
	pub fn new(w: W, header: &Header) -> Result<Self> {
		let mut buf = Vec::new();
		let mut pw = PbfWriter::new(&mut buf);
		header.encode(&mut pw);

		let blob = Blob::Raw(Cow::Owned(buf.clone()));

		let mut sel = Self {
			w,
			header_buf: Vec::new(),
			body_buf: buf,
		};
		sel.write_with_type("OSMHeader", &blob)?;
		Ok(sel)
	}

	pub fn write(&mut self, blob: &Blob) -> Result<()> {
		self.write_with_type("OSMData", blob)
	}

	fn write_with_type(&mut self, r#type: &str, blob: &Blob) -> Result<()> {
		self.body_buf.clear();
		blob.encode(&mut self.body_buf);

		self.header_buf.clear();
		let mut w = PbfWriter::new(&mut self.header_buf);
		w.string(1, r#type);
		w.uint64(3, self.body_buf.len().try_into().unwrap());

		let n: u32 = self.header_buf.len().try_into().unwrap();
		self.w.write_all(&n.to_be_bytes()).map_err(Error::Write)?;

		self.w.write_all(&self.header_buf).map_err(Error::Write)?;
		self.w.write_all(&self.body_buf).map_err(Error::Write)?;

		Ok(())
	}
}
