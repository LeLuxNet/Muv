use std::borrow::Cow;

use protozer0::{PbfReader, PbfWriter, Tag, WireType};

use crate::{Error, Result};

#[cfg(feature = "zlib")]
use condense::deflate;
#[cfg(feature = "zlib")]
pub use condense::deflate::Level as ZlibLevel;

#[cfg(feature = "zstd")]
use condense::zstd;

#[derive(Debug)]
#[cfg_attr(
	not(all(feature = "zlib", feature = "lz4", feature = "zstd")),
	non_exhaustive
)]
pub enum Blob<'a> {
	Raw(Cow<'a, [u8]>),
	#[cfg(feature = "zlib")]
	Zlib {
		data: Cow<'a, [u8]>,
		len: usize,
	},
	#[cfg(feature = "lz4")]
	Lz4 {
		data: Cow<'a, [u8]>,
		len: usize,
	},
	#[cfg(feature = "zstd")]
	Zstd {
		data: Cow<'a, [u8]>,
		len: usize,
	},
}

impl<'a> Blob<'a> {
	pub(crate) fn decode(buf: &'a [u8]) -> Result<Self> {
		let mut r = PbfReader::new(buf);

		let mut raw_size = None;
		let mut blob = None;
		while let Some(tag) = r.next()? {
			blob = Some(match tag {
				tag if tag == Tag::new(1, WireType::Len) => Blob::Raw(Cow::Borrowed(r.bytes()?)),
				tag if tag == Tag::new(2, WireType::Varint) => {
					raw_size = Some(r.uint32()?);
					continue;
				}
				#[cfg(feature = "zlib")]
				tag if tag == Tag::new(3, WireType::Len) => Blob::Zlib {
					data: Cow::Borrowed(r.bytes()?),
					len: 0,
				},
				#[cfg(feature = "lz4")]
				tag if tag == Tag::new(6, WireType::Len) => Blob::Lz4 {
					data: Cow::Borrowed(r.bytes()?),
					len: 0,
				},
				#[cfg(feature = "zstd")]
				tag if tag == Tag::new(7, WireType::Len) => Blob::Zstd {
					data: Cow::Borrowed(r.bytes()?),
					len: 0,
				},
				tag => return Err(Error::UnknownCompression(tag.field_number())),
			});
		}

		let Some(blob) = blob else {
			return Err(Error::MissingData);
		};

		let raw_size = raw_size
			.map(usize::try_from)
			.transpose()
			.map_err(|_| Error::DataTooBig)?;

		let blob = match blob {
			Blob::Raw(_) => {
				let _ = raw_size;
				blob
			}
			#[cfg(feature = "zlib")]
			Blob::Zlib { data, len: _ } => Blob::Zlib {
				data,
				len: raw_size.ok_or(Error::MissingDataSize)?,
			},
			#[cfg(feature = "lz4")]
			Blob::Lz4 { data, len: _ } => Blob::Lz4 {
				data,
				len: raw_size.ok_or(Error::MissingDataSize)?,
			},
			#[cfg(feature = "zstd")]
			Blob::Zstd { data, len: _ } => Blob::Zstd {
				data,
				len: raw_size.ok_or(Error::MissingDataSize)?,
			},
		};

		if blob.decompressed_len() > 32 * 1024 * 1024 {
			return Err(Error::DataTooBig);
		}

		Ok(blob)
	}

	#[allow(unreachable_code)]
	pub(crate) fn encode(&self, buf: &mut Vec<u8>) {
		let mut w = PbfWriter::new(buf);

		#[allow(unused_variables)]
		let (field_number, data, len) = match self {
			Self::Raw(b) => {
				w.bytes(1, b);
				return;
			}
			#[cfg(feature = "zlib")]
			Self::Zlib { data, len } => (3, data, *len),
			#[cfg(feature = "lz4")]
			Self::Lz4 { data, len } => (6, data, *len),
			#[cfg(feature = "zstd")]
			Self::Zstd { data, len } => (7, data, *len),
		};

		w.uint64(2, len.try_into().unwrap());
		w.bytes(field_number, data);
	}

	#[must_use]
	pub fn into_owned(self) -> Blob<'static> {
		match self {
			Self::Raw(b) => Blob::Raw(b.into_owned().into()),
			#[cfg(feature = "zlib")]
			Self::Zlib { data, len } => Blob::Zlib {
				data: data.into_owned().into(),
				len,
			},
			#[cfg(feature = "lz4")]
			Self::Lz4 { data, len } => Blob::Lz4 {
				data: data.into_owned().into(),
				len,
			},
			#[cfg(feature = "zstd")]
			Self::Zstd { data, len } => Blob::Zstd {
				data: data.into_owned().into(),
				len,
			},
		}
	}

	#[must_use]
	pub fn compressed_len(&self) -> usize {
		match self {
			Self::Raw(b) => b.len(),
			#[cfg(feature = "zlib")]
			Self::Zlib { data, len: _ } => data.len(),
			#[cfg(feature = "lz4")]
			Self::Lz4 { data, len: _ } => data.len(),
			#[cfg(feature = "zstd")]
			Self::Zstd { data, len: _ } => data.len(),
		}
	}

	#[cfg(feature = "zlib")]
	#[must_use]
	pub fn compress_zlib(data: &[u8], level: ZlibLevel) -> Blob<'static> {
		let res = deflate::compress_zlib(data, (), level).unwrap();
		Blob::Zlib {
			data: Cow::Owned(res),
			len: data.len(),
		}
	}

	#[cfg(feature = "lz4")]
	#[must_use]
	pub fn compress_lz4(data: &[u8]) -> Blob<'static> {
		Blob::Lz4 {
			data: Cow::Owned(lz4_flex::compress(data)),
			len: data.len(),
		}
	}

	#[cfg(feature = "zstd")]
	pub fn compress_zstd(data: &[u8], level: i8) -> Result<Blob<'static>> {
		let res = zstd::compress(data, (), level)?;
		Ok(Blob::Zstd {
			data: Cow::Owned(res),
			len: data.len(),
		})
	}

	#[must_use]
	pub fn decompressed_len(&self) -> usize {
		match self {
			Self::Raw(b) => b.len(),
			#[cfg(feature = "zlib")]
			Self::Zlib { data: _, len } => *len,
			#[cfg(feature = "lz4")]
			Self::Lz4 { data: _, len } => *len,
			#[cfg(feature = "zstd")]
			Self::Zstd { data: _, len } => *len,
		}
	}

	pub fn decompress(&'a self) -> Result<Cow<'a, [u8]>> {
		Ok(match self {
			Self::Raw(b) => Cow::Borrowed(b),
			#[cfg(feature = "zlib")]
			Self::Zlib { data, len } => {
				let res = deflate::decompress_zlib(data, Vec::with_capacity(*len))?;
				Cow::Owned(res)
			}
			#[cfg(feature = "lz4")]
			Self::Lz4 { data, len } => {
				let res = lz4_flex::decompress(data, *len)?;
				Cow::Owned(res)
			}
			#[cfg(feature = "zstd")]
			Self::Zstd { data, len } => {
				let res = zstd::decompress(data, Vec::with_capacity(*len))?;
				Cow::Owned(res)
			}
		})
	}
}
