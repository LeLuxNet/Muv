use std::{
	error,
	fmt::{self, Debug, Display, Formatter},
	io,
	num::NonZero,
	result,
};

#[cfg(feature = "zlib")]
use condense::deflate;

#[cfg(feature = "zstd")]
use condense::zstd;

pub type Result<T> = result::Result<T, Error>;

#[derive(Debug)]
#[cfg_attr(
	not(all(feature = "zlib", feature = "lz4", feature = "zstd")),
	non_exhaustive
)]
pub enum Error {
	Read(io::Error),
	Write(io::Error),
	Seek(io::Error),
	Protozer0(protozer0::Error),

	HeaderTooBig(u32),

	NoHeaderBlob,
	UnsupportedFeature(String),
	MissingBboxSide,

	MissingBlobType,
	MissingBlobSize,

	MissingDataSize,
	MissingData,
	DataTooBig,
	UnknownCompression(NonZero<u32>),

	#[cfg(feature = "zlib")]
	Zlib(deflate::DecompressError),
	#[cfg(feature = "lz4")]
	Lz4(lz4_flex::block::DecompressError),
	#[cfg(feature = "zstd")]
	Zstd(zstd::Error),

	MissingStringTable,
	UnknownStringTableTag(NonZero<u32>),

	MissingDenseNodesField,
	MismatchDenseNodesLength,

	MissingWayField,
	MissingRelationField,

	UnknownMemberType(u32),

	NegativeId,
}

impl Display for Error {
	fn fmt(&self, f: &mut Formatter<'_>) -> fmt::Result {
		f.write_str(match self {
			Self::Read(_) => "failed to read",
			Self::Write(_) => "failed to write",
			Self::Seek(_) => "failed to seek",
			Self::Protozer0(_) => "failed to decode protobuf",

			Self::HeaderTooBig(_) => "header above 64KB",

			Self::NoHeaderBlob => "no header blob",
			Self::UnsupportedFeature(name) => {
				return write!(f, "required feature {name:?} unsupported");
			}
			Self::MissingBboxSide => "missing side in header bbox",

			Self::MissingBlobType => "missing blob type",
			Self::MissingBlobSize => "missing blob size",

			Self::MissingDataSize => "missing data size",
			Self::MissingData => "missing data",
			Self::DataTooBig => "data too big",
			Self::UnknownCompression(tag) => {
				return write!(f, "unknown compression with tag {tag}");
			}

			Self::Zlib(_) => "failed to decompress data using zlib",
			#[cfg(feature = "lz4")]
			Self::Lz4(_) => "failed to decompress data using lz4",
			#[cfg(feature = "zstd")]
			Self::Zstd(_) => "failed to decompress data using zstd",

			Self::MissingStringTable => "missing string table",
			Self::UnknownStringTableTag(tag) => {
				return write!(f, "unknown tag {tag} in string table");
			}

			Self::MissingDenseNodesField => "missing field in dense nodes",
			Self::MismatchDenseNodesLength => "packed fields length in dense nodes do not match",

			Self::MissingWayField => "missing field in way",
			Self::MissingRelationField => "missing field in relation",

			Self::UnknownMemberType(n) => return write!(f, "unknown relation member type {n}"),

			Self::NegativeId => "id is negative",
		})
	}
}

impl error::Error for Error {
	fn source(&self) -> Option<&(dyn error::Error + 'static)> {
		Some(match self {
			Self::Read(e) | Self::Write(e) | Self::Seek(e) => e,
			Self::Protozer0(e) => e,
			Self::Zlib(e) => e,
			#[cfg(feature = "lz4")]
			Self::Lz4(e) => e,
			#[cfg(feature = "zstd")]
			Self::Zstd(e) => e,
			_ => return None,
		})
	}
}

impl From<protozer0::Error> for Error {
	fn from(value: protozer0::Error) -> Self {
		Self::Protozer0(value)
	}
}

#[cfg(feature = "zlib")]
impl From<deflate::DecompressError> for Error {
	fn from(value: deflate::DecompressError) -> Self {
		Self::Zlib(value)
	}
}

#[cfg(feature = "lz4")]
impl From<lz4_flex::block::DecompressError> for Error {
	fn from(value: lz4_flex::block::DecompressError) -> Self {
		Self::Lz4(value)
	}
}

#[cfg(feature = "zstd")]
impl From<zstd::Error> for Error {
	fn from(value: zstd::Error) -> Self {
		Self::Zstd(value)
	}
}
