use std::fmt::{self, Debug, Formatter};

#[derive(Clone, Copy, PartialEq, Eq, PartialOrd, Ord, Hash)]
pub struct Nanodegrees(pub i64);

impl Debug for Nanodegrees {
	fn fmt(&self, f: &mut Formatter<'_>) -> fmt::Result {
		write!(
			f,
			"{}.{:0>9}",
			self.0 / 1_000_000_000,
			self.0 % 1_000_000_000
		)
	}
}
