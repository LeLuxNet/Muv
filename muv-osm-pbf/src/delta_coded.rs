use std::{marker::PhantomData, ops::AddAssign};

use protozer0::{Svarint, SvarintIter};

use crate::{Error, Result};

pub struct DeltaCoded<'a, T, I = T> {
	state: T,
	r: SvarintIter<'a, T>,
	item: PhantomData<I>,
}

impl<'a, T: Default, I> DeltaCoded<'a, T, I> {
	#[must_use]
	pub fn new(r: SvarintIter<'a, T>) -> Self {
		Self {
			state: T::default(),
			r,
			item: PhantomData,
		}
	}
}

impl<T: Svarint + Clone + AddAssign + TryInto<I>, I> Iterator for DeltaCoded<'_, T, I> {
	type Item = Result<I>;

	fn next(&mut self) -> Option<Self::Item> {
		Some(match self.r.next()? {
			Ok(delta) => {
				self.state += delta;
				if let Ok(item) = self.state.clone().try_into() {
					Ok(item)
				} else {
					Err(Error::NegativeId)
				}
			}
			Err(e) => Err(e.into()),
		})
	}

	fn size_hint(&self) -> (usize, Option<usize>) {
		self.r.size_hint()
	}

	fn count(self) -> usize {
		self.r.count()
	}
}
