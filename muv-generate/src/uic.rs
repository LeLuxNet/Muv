use std::{error::Error, fmt::Write, ops::RangeInclusive};

use rasn_compiler::{Compiler, prelude::RasnBackend};

use crate::utils::save_formatted;

const VERSIONS: [(u8, u8, u8); 3] = [(1, 3, 4), (2, 0, 2), (3, 0, 3)];

pub(crate) fn build() -> Result<(), Box<dyn Error>> {
	for (major, minor, patch) in VERSIONS {
		let url = format!(
			"https://raw.githubusercontent.com/UnionInternationalCheminsdeFer/UIC-barcode/refs/heads/master/misc/uicRailTicketData_v{major}.{minor}.{patch}.asn"
		);
		let input = ureq::get(&url).call()?.into_string()?;
		let mut comp = Converter::new(input);

		save_formatted(
			comp.output(),
			&format!("muv-transit-ticket/src/uic/gen_{major}_{minor}.rs"),
		)?;
	}

	Ok(())
}

struct Converter {
	input: String,
}

impl Converter {
	fn new(mut input: String) -> Self {
		input.pop();
		while input.pop().is_some_and(|c| c != '\n') {}

		Self { input }
	}

	fn output(mut self) -> String {
		self.input.push_str("END");

		let res = Compiler::<RasnBackend, _>::new()
			.add_asn_literal(self.input)
			.compile_to_string()
			.unwrap();

		let mut res = res.generated;
		res.pop();
		res.pop();

		let mod_start = res.find("pub mod ").unwrap();
		let mod_end = mod_start + res[mod_start..].find('{').unwrap() + 1;
		res.replace_range(mod_start..mod_end, "");

		res.replacen(
			"#[allow(",
			"#![allow(clippy::all, clippy::pedantic, clippy::nursery,",
			1,
		)
		.replacen("use lazy_static::lazy_static;", "", 1)
	}
}
