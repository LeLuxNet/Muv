use std::error::Error;

mod utils;

mod addr;
mod driving_side;
mod speed;
mod uic;

fn main() -> Result<(), Box<dyn Error>> {
	println!("addr");
	addr::build()?;

	println!("driving_side");
	driving_side::build()?;

	println!("speed");
	speed::build()?;

	println!("uic");
	uic::build()?;

	Ok(())
}
