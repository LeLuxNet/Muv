use proc_macro2::{Ident, Span, TokenStream};
use quote::{ToTokens, TokenStreamExt, quote};
use serde::Deserialize;
use std::{
	collections::{BTreeMap, HashMap, HashSet},
	error::Error,
	str::CharIndices,
};
use ureq::get;

use crate::utils::save_formatted;

#[derive(Deserialize)]
#[serde(untagged)]
enum Entry {
	#[allow(dead_code)]
	Template(String),
	Country(Country),
}

#[derive(Deserialize)]
#[allow(dead_code)]
struct Country {
	use_country: Option<String>,
	change_country: Option<String>,
	add_component: Option<String>,
	address_template: Option<String>,
	fallback_template: Option<String>,
	replace: Option<Vec<(String, String)>>,
	postformat_replace: Option<Vec<(String, String)>>,
}

#[derive(Debug)]
enum Context<'a> {
	Text(&'a str),
	Template(&'a str),
	Section(&'a str, Vec<Context<'a>>),
}

fn parse_mustache<'a>(chars: &mut CharIndices<'a>) -> Vec<Context<'a>> {
	let mut res = Vec::new();

	let mut parens_matched = false;

	loop {
		let s = chars.as_str();
		let Some((i, c)) = chars.next() else {
			return res;
		};

		res.push(if c == '{' {
			if !parens_matched {
				chars.next().unwrap();
			}

			let (start_i, start_c) = chars.next().unwrap();

			let (end_i, _) = chars.find(|(_, c)| c == &'}').unwrap();
			chars.next().unwrap();
			if start_c == '{' {
				chars.next().unwrap();
			}
			parens_matched = false;

			let name = &s[start_i - i + usize::from(start_c == '{' || start_c == '#')..end_i - i];
			#[allow(clippy::wildcard_in_or_patterns)]
			match start_c {
				'#' => Context::Section(name, parse_mustache(chars)),
				'/' => return res,
				'{' | _ => Context::Template(name),
			}
		} else {
			let Some((end_i, _)) = chars.find(|(_, c)| c == &'{') else {
				res.push(Context::Text(s));
				return res;
			};
			parens_matched = true;
			Context::Text(&s[..end_i - i])
		});
	}
}

fn get_addr_identifier(s: &str) -> TokenStream {
	match s {
		"house" => quote! { housename },
		"house_number" => quote! { housenumber },
		"road" => quote! { street },
		"state_code" => quote! { state },
		"county_code" => quote! { county },
		_ => Ident::new(s, Span::call_site()).into_token_stream(),
	}
}

fn write_text(s: &str, fmt: &TokenStream, res: &mut TokenStream) -> Result<(), Box<dyn Error>> {
	if s.is_empty() {
		return Ok(());
	}
	if s.contains(['{', '}']) {
		Err(format!("text '{s}' contains a {{ or }}"))?
	}
	res.append_all(quote! { #fmt.write_str(#s)?; });
	Ok(())
}

fn replace_field(
	replace: Option<&Vec<(String, String)>>,
	fmt: &TokenStream,
	name: &str,
	ident: &TokenStream,
) -> TokenStream {
	let Some(replace) = replace else {
		return quote! { #fmt.write_str(#ident)?; };
	};

	let mut arms = TokenStream::new();
	for (key, val) in replace {
		if let Some(key) = key.strip_prefix(name).and_then(|s| s.strip_prefix('=')) {
			let key = key.replace('\\', "");
			arms.append_all(if let Some(key_suffix) = key.strip_suffix('$') {
				let key_suffix_len = key_suffix.len();
				let push_suffix = if val.is_empty() {
					quote!()
				} else {
					quote! { ?; #fmt.write_str(#val) }
				};
				quote! { _ if #ident.ends_with(#key_suffix) => {
					#fmt.write_str(&#ident[..#ident.len()-#key_suffix_len])
					#push_suffix
				}, }
			} else {
				quote! { #key => #fmt.write_str(#val), }
			});
		}
	}
	if arms.is_empty() {
		return quote! { #fmt.write_str(#ident)?; };
	}

	quote! { match #ident {
		#arms
		_ => #fmt.write_str(#ident),
	}?; }
}

fn execute_template(
	addr: &TokenStream,
	replace: Option<&Vec<(String, String)>>,
	t: Context,
	fmt: &TokenStream,
	res: &mut TokenStream,
) -> Result<(), Box<dyn Error>> {
	match t {
		Context::Template(name) => {
			let ident = get_addr_identifier(name);
			let body = replace_field(replace, fmt, name, &ident);
			res.append_all(quote! {
				if let Some(#ident) = #addr.#ident {
					#body
				}
			});
		}
		Context::Text(t) => write_text(t, fmt, res)?,
		Context::Section(name, body) => match name {
			"first" => {
				let mut first = true;

				let mut res_some = TokenStream::new();
				let mut res_capture = TokenStream::new();
				let mut res_body = TokenStream::new();
				let mut capture_count = 0;

				let body_last = body.len() - 1;

				let mut seen_somes = HashSet::new();

				for (i, part) in body.into_iter().enumerate() {
					match part {
						Context::Template(name) => {
							if capture_count > 0 {
								res_some.append_all(quote! {,});
								res_capture.append_all(quote! {,});
							}
							capture_count += 1;

							let ident = get_addr_identifier(name);
							res_some.append_all(quote! { Some(#ident) });
							res_capture.append_all(quote! { #addr.#ident });
							res_body.append_all(replace_field(replace, fmt, name, &ident));
						}
						Context::Text(t) => {
							if let Some((before, after)) = t.split_once("||") {
								write_text(before.trim(), fmt, &mut res_body)?;

								if seen_somes.insert(res_some.to_string()) {
									if first {
										first = false;
										res.append_all(quote! { if let });
									} else {
										res.append_all(quote! { else if let });
									}

									if capture_count > 1 {
										res.append_all(quote! { (#res_some) = (#res_capture) });
									} else {
										res.append_all(quote! { #res_some = #res_capture });
									}

									res.append_all(quote! { { #res_body } });
								}

								capture_count = 0;
								res_some = TokenStream::new();
								res_capture = TokenStream::new();
								res_body = TokenStream::new();

								write_text(after.trim(), fmt, &mut res_body)?;
							} else if (i != 0 && i != body_last) || !t.trim().is_empty() {
								write_text(t, fmt, &mut res_body)?;
							}
						}
						Context::Section(_, _) => panic!(),
					}
				}

				if !res_body.is_empty() && !seen_somes.contains(&res_some.to_string()) {
					if first {
						res.append_all(quote! { if let });
					} else {
						res.append_all(quote! { else if let });
					}

					if capture_count > 1 {
						res.append_all(quote! { (#res_some) = (#res_capture) });
					} else {
						res.append_all(quote! { #res_some = #res_capture });
					}

					res.append_all(quote! { { #res_body } });
				}
			}
			_ => panic!(),
		},
	}
	Ok(())
}

const SOURCE: &str = "https://raw.githubusercontent.com/OpenCageData/address-formatting/master/conf/countries/worldwide.yaml";

pub fn build() -> Result<(), Box<dyn Error>> {
	let entries: BTreeMap<String, Entry> =
		serde_yaml::from_reader(get(SOURCE).call()?.into_reader())?;
	println!("\tloaded file");

	let mut ast = quote! {
		use std::fmt::{Formatter, Result};
		use crate::addr::Addr;
	};
	let fmt = quote!(f);
	let addr = quote!(addr);

	let mut match_case = TokenStream::new();

	let mut reuse: HashMap<String, Ident> = HashMap::new();

	for (mut name, entry) in entries {
		let Entry::Country(country) = entry else {
			continue;
		};

		let Some(template) = &country.address_template else {
			continue;
		};
		let template = template.trim();
		let ctx = parse_mustache(&mut template.char_indices());

		let mut res = TokenStream::new();
		for ctx in ctx {
			execute_template(&addr, country.replace.as_ref(), ctx, &fmt, &mut res)?;
		}

		match_case.append_all(if name == "default" {
			quote! { _ }
		} else {
			quote! { Some(#name) }
		});

		let res_str = res.to_string();
		if let Some(ident) = reuse.get(&res_str) {
			match_case.append_all(quote! { => #ident, });
			continue;
		}

		name.make_ascii_lowercase();
		let ident = Ident::new(&format!("format_{name}"), Span::call_site());
		reuse.insert(res_str, ident.clone());
		ast.append_all(
			quote! { fn #ident(#addr: &Addr, #fmt: &mut Formatter) -> Result {
				#res
				Ok(())
			}},
		);

		match_case.append_all(quote! { => #ident, });
	}

	ast.append_all(quote! {
		#[allow(clippy::match_same_arms, clippy::too_many_lines)]
		pub(crate) fn format(#addr: &Addr, #fmt: &mut Formatter) -> Result {
			(match #addr.country { #match_case })(#addr, #fmt)
		}
	});
	println!("\tcomputed ast");

	save_formatted(ast, "muv-osm/src/addr/format.rs")
}
