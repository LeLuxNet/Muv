use muv_mediawiki::{Blocking, sparql::Api, wikibase::Q};
use proc_macro2::TokenStream;
use quote::{ToTokens, quote};
use std::{collections::BTreeMap, error::Error};

use serde::Deserialize;

use crate::utils::save_formatted;

#[derive(Debug, Clone, Copy, PartialEq, Deserialize)]
enum Side {
	Left,
	Right,
}

const QUERY: &str = r#"
SELECT ?area ?iso_code ?driving_side WHERE {
    ?area wdt:P297 ?iso_code.
    OPTIONAL { ?area wdt:P1622 ?driving_side. }
    FILTER(NOT EXISTS { ?area wdt:P576 ?abolished. })
}"#;

const USER_AGENT: &str = "muv-osm-generate/0.1";

pub(crate) fn build() -> Result<(), Box<dyn Error>> {
	let data =
		Api::<Blocking>::new(USER_AGENT, "https://query.wikidata.org/sparql").select(QUERY)?;

	let mut sides = BTreeMap::new();
	for mut b in data.results.bindings {
		let area = b.remove("area").unwrap().value;
		let iso_code = b.remove("iso_code").unwrap().value;

		let Some(driving_side) = b.get("driving_side") else {
			// Exclude areas without any roads
			if !matches!(&*iso_code, "AQ" | "BV" | "CP" | "HM") {
				println!("\tmissing {iso_code} ({area})");
			}
			continue;
		};

		let driving_side = match driving_side
			.value
			.strip_prefix("http://www.wikidata.org/entity/")
			.unwrap()
			.parse()?
		{
			Q(13196750) => Side::Left,
			Q(14565199) => Side::Right,
			value => {
				println!("\tinvalid driving side {value} ({area})");
				continue;
			}
		};

		sides.insert(iso_code, driving_side);
	}

	let mut case_left = TokenStream::new();
	let mut case_right = TokenStream::new();

	for (iso_code, side) in sides {
		let case = match side {
			Side::Left => &mut case_left,
			Side::Right => &mut case_right,
		};

		if !case.is_empty() {
			case.extend(quote!( | ));
		}
		case.extend(iso_code.into_token_stream());
	}

	let ast = quote! {
		use crate::lanes::Side;

		pub(crate) fn driving_side(region: &str) -> Option<Side> {
			match region {
				#case_left => Some(Side::Left),
				#case_right => Some(Side::Right),
				_ => None,
			}
		}
	};

	save_formatted(ast, "muv-osm/src/lanes/highway/driving_side.rs")
}
