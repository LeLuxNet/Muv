use std::{error::Error, fmt::Debug, io::stdout, path::PathBuf, result};

use clap::{Args, Parser, ValueEnum};
use element::Id;
use serde::Serialize;

mod element;
mod lanes;
mod lines;
mod recompress;
mod service;
mod tags;
mod transit;

#[derive(Debug, Parser)]
enum Command {
	Pbf {
		#[clap(subcommand)]
		command: PbfCommand,
	},

	Osm {
		#[arg(short, long, global = true, help = "Change the input format")]
		input: Option<OsmFormat>,
		#[arg(short, long, global = true, help = "Change the output format")]
		output: Option<OsmFormat>,

		#[clap(subcommand)]
		command: OsmCommand,
	},

	Transit {
		#[arg(short, long, help = "Change the input format")]
		input: TransitFormat,

		#[arg(short, long)]
		paths: Vec<PathBuf>,

		#[clap(subcommand)]
		command: TransitCommand,
	},
}

#[derive(Debug, Clone, Copy, Parser, ValueEnum)]
enum OsmFormat {
	Json,
	Yaml,
}

#[derive(Debug, Clone, Copy, Parser, ValueEnum)]
enum TransitFormat {
	Gtfs,
	GtfsRt,
	Netex,
	VmtHafas,
	OebbHafas,
	Amtrak,
	Viarail,
}

impl OsmFormat {
	fn write_to_output(&self, v: &impl Serialize) -> Result<()> {
		match self {
			Self::Json => serde_json::to_writer_pretty(stdout().lock(), v)?,
			Self::Yaml => serde_yaml::to_writer(stdout().lock(), v)?,
		};
		Ok(())
	}
}

#[derive(Debug, Parser)]
enum PbfCommand {
	#[command(about = "Recompress pbf file")]
	Recompress {
		#[clap(flatten)]
		compression: Compression,

		input: PathBuf,
		output: PathBuf,
	},
}

#[derive(Debug, Clone, Copy, Args)]
#[group(required = true, multiple = false)]
struct Compression {
	#[arg(long, help = "Don't compress output")]
	raw: bool,
	#[arg(
        long,
        require_equals = true,
        value_name = "LEVEL",
        help = "Compress to the zlib format with an optional compression level (defaults to 6)",
        default_missing_value = "6",
        num_args(0..=1),
        value_parser = clap::value_parser!(u8).range(0..=12),
    )]
	zlib: Option<u8>,
	#[arg(long, help = "Compress to LZ4")]
	lz4: bool,
	#[arg(
        long,
        require_equals = true,
        value_name = "LEVEL",
        help = "compress to zstd with an optional compression level (defaults to 3)",
        default_missing_value = "3",
        num_args(0..=1),
        value_parser = clap::value_parser!(i8).range(-7..=22),
    )]
	zstd: Option<i8>,
}

#[derive(Debug, Parser)]
enum OsmCommand {
	#[command(about = "Format tags")]
	Tags {
		#[clap(flatten)]
		id: Id,
	},

	#[command(about = "Parse lanes of a way")]
	Lanes {
		#[clap(flatten)]
		id: Id,

		#[arg(short = 'R', long, help = "Region codes the lanes are parsed with")]
		regions: Vec<String>,
	},
}

#[derive(Debug, Parser)]
enum TransitCommand {
	Lines,
	Service,
}

type Result<T> = result::Result<T, Box<dyn Error>>;

fn main() -> Result<()> {
	let args = Command::parse();

	match args {
		Command::Pbf { command } => match command {
			PbfCommand::Recompress {
				compression,
				input,
				output,
			} => recompress::run(compression, &input, &output),
		},
		Command::Osm {
			input,
			output,
			command,
		} => match command {
			OsmCommand::Tags { id } => tags::run(id, input, output),
			OsmCommand::Lanes { id, regions } => lanes::run(id, input, output, regions),
		},
		Command::Transit {
			input,
			paths,
			command,
		} => match command {
			TransitCommand::Lines => lines::run(input, &paths),
			TransitCommand::Service => service::run(input, &paths),
		},
	}
}
