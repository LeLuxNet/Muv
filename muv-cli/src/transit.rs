use std::{
	collections::HashSet,
	fs::{File, read},
	io::BufReader,
	path::PathBuf,
};

use chrono::{TimeDelta, Utc};
use muv_transit::{
	Blocking, Network, Vehicle, amtrak, gtfs,
	gtfs_rt::{self, Message},
	hafas::{self, Client},
	netex, viarail,
};

use crate::{Result, TransitFormat};

fn load_hafas(config: &'static hafas::Config, paths: &[PathBuf], network: &mut Network) {
	let client: Client<Blocking> = config.client();
	let mut jids = HashSet::new();

	for path in paths {
		let trip = client
			.station_board(
				hafas::StationBoardType::Departure,
				hafas::Location::new_name(path.display().to_string()),
				config.time(&Utc::now()),
				TimeDelta::hours(6),
			)
			.unwrap();

		jids.extend(trip.jny_l.into_iter().map(|j| j.jid));
	}

	for jid in jids {
		let jd = client.journey_details(&jid).unwrap();
		jd.insert_into(network, config.timezone, config.cls_map.from);
	}
}

pub fn load(input: TransitFormat, paths: &[PathBuf]) -> Result<(Network, Vec<Vehicle>)> {
	let mut network = Network::default();
	let mut vehicles = Vec::new();

	match input {
		TransitFormat::Gtfs => {
			for path in paths {
				let f = File::open(path)?;
				let r = gtfs::DatasetReader::new(f)?;
				r.insert_into(&mut network, false)?;
			}
		}
		TransitFormat::GtfsRt => {
			for path in paths {
				let b = read(path)?;
				let m = gtfs_rt::FeedMessage(gtfs_rt::structure::FeedMessage::decode(&*b)?);
				vehicles.extend(m.vehicles(""));
			}
		}
		TransitFormat::Netex => {
			let mut converter = netex::Converter::new(&mut network);
			for path in paths {
				let f = File::open(path)?;
				let pd: netex::PublicationDelivery = quick_xml::de::from_reader(BufReader::new(f))?;
				pd.insert_into(&mut converter);
			}
		}
		TransitFormat::VmtHafas => load_hafas(&hafas::VMT, paths, &mut network),
		TransitFormat::OebbHafas => load_hafas(&hafas::OEBB, paths, &mut network),
		TransitFormat::Amtrak => {
			vehicles.extend(
				amtrak::Maps::<Blocking>::default()
					.trains_data()?
					.vehicles(),
			);
		}
		TransitFormat::Viarail => {
			vehicles.extend(
				viarail::TsiMobile::<Blocking>::default()
					.all_data()?
					.vehicles(),
			);
		}
	}

	Ok((network, vehicles))
}
