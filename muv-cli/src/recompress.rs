use std::{
	cmp::Ordering, collections::BinaryHeap, fs::File, io::BufWriter, path::Path, sync::Mutex,
};

use muv_osm_pbf::{Blob, ZlibLevel, read::Reader, write::Writer};

use crate::{Compression, Result};

struct IndexBlob {
	index: u32,
	blob: Blob<'static>,
}

impl PartialEq for IndexBlob {
	fn eq(&self, other: &Self) -> bool {
		self.index == other.index
	}
}

impl Eq for IndexBlob {}

impl PartialOrd for IndexBlob {
	fn partial_cmp(&self, other: &Self) -> Option<Ordering> {
		Some(self.cmp(other))
	}
}

impl Ord for IndexBlob {
	fn cmp(&self, other: &Self) -> Ordering {
		other.index.cmp(&self.index)
	}
}

struct State<W> {
	next_index: u32,
	queue: BinaryHeap<IndexBlob>,
	writer: Writer<W>,
}

pub(crate) fn run(compression: Compression, from: &Path, to: &Path) -> Result<()> {
	let from_file = File::open(from)?;
	let to_file = File::create(to)?;

	let (r, header) = Reader::new_file(from_file)?;
	let w = Writer::new(BufWriter::new(to_file), &header)?;

	let state = Mutex::new(State {
		next_index: 0,
		queue: BinaryHeap::new(),
		writer: w,
	});

	r.par_for_each(|index, _, blob| {
		let blob = blob.decompress()?;

		let blob = if compression.raw {
			Blob::Raw(blob).into_owned()
		} else if let Some(zlib) = compression.zlib {
			Blob::compress_zlib(&blob, ZlibLevel::new(zlib).unwrap())
		} else if compression.lz4 {
			Blob::compress_lz4(&blob)
		} else if let Some(zstd) = compression.zstd {
			Blob::compress_zstd(&blob, zstd)?
		} else {
			unreachable!("missing compression level")
		};

		let mut state = state.lock().unwrap();
		if state.next_index == index {
			state.writer.write(&blob)?;
			state.next_index += 1;
		} else {
			state.queue.push(IndexBlob { index, blob });
		}

		while state
			.queue
			.peek()
			.is_some_and(|ib| state.next_index == ib.index)
		{
			let blob = state.queue.pop().unwrap().blob;
			state.writer.write(&blob)?;
			state.next_index += 1;
		}
		drop(state);

		Ok(())
	})?;

	let queue = state.into_inner().unwrap().queue;
	assert_eq!(queue.len(), 0);

	Ok(())
}
