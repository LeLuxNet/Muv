use std::path::PathBuf;

use chrono::Local;
use muv_transit::Board;

use crate::{Result, TransitFormat, transit::load};

pub fn run(input: TransitFormat, paths: &[PathBuf]) -> Result<()> {
	let (network, _) = load(input, paths)?;

	let now = Local::now();
	let today = now.date_naive();

	for r in network.routes.into_values() {
		if !network.services.get(&r.service).unwrap().allowed(today) {
			continue;
		}

		if r.stops.first().unwrap().departure.unwrap().on(today) > now {
			continue;
		}
		if r.stops.last().unwrap().arrival.unwrap().on(today) < now {
			continue;
		}

		for rs in r.stops {
			let stop = network.stops.get(&rs.id).unwrap();

			println!();
			println!("{}", stop.name.as_ref().unwrap());

			if rs.board == Board::Yes {
				print!("b");
			} else {
				print!(" ");
			}
			if let Some(arrival) = rs.arrival {
				print!(" {} ({})", arrival.time, arrival.tz);
			}
			println!();

			if rs.alight {
				print!("a");
			} else {
				print!(" ");
			}
			if let Some(departure) = rs.departure {
				print!(" {} ({})", departure.time, departure.tz);
			}
			println!();
		}

		println!();
		println!("---");
	}

	println!("{now}");

	Ok(())
}
