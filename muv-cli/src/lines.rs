use std::{collections::HashSet, io::stdout, path::PathBuf};

use geojson::{Feature, FeatureCollection, Value};

use crate::{Result, TransitFormat, transit::load};

pub fn run(input: TransitFormat, paths: &[PathBuf]) -> Result<()> {
	let (network, vehicles) = load(input, paths)?;

	let mut features = Vec::new();

	let mut seen = HashSet::new();
	eprintln!("/{}", network.routes.len());
	for (route_id, route) in network.routes.into_iter().enumerate().map(|(i, v)| {
		eprintln!("{i}");
		v
	}) {
		let Some(line_id) = route.line else {
			continue;
		};
		let line = network.lines.get(&line_id).unwrap();

		let shape_id = route.shape;

		let line_string: Vec<_> = if let Some(shape_id) = shape_id {
			let shape = network.shapes.get(&shape_id).unwrap();
			if !seen.insert((line_id, shape_id)) {
				continue;
			}

			shape.iter().map(|loc| vec![loc.lon, loc.lat]).collect()
		} else {
			if !seen.insert((line_id, route_id)) {
				continue;
			}

			route
				.stops
				.into_iter()
				.map(|stop| {
					let loc = network.stops.get(&stop.id).unwrap().location.unwrap();
					vec![loc.lon, loc.lat]
				})
				.collect()
		};

		if !line_string.is_empty() {
			let properties = [
				("name".into(), line.name.to_owned().into()),
				(
					"stroke".into(),
					line.color
						.as_ref()
						.map_or_else(String::new, |color| format!("#{color}"))
						.into(),
				),
			];
			features.push(Feature {
				bbox: None,
				geometry: Some(Value::LineString(line_string).into()),
				id: None,
				properties: Some(properties.into_iter().collect()),
				foreign_members: None,
			});
		}
	}

	for (stop_id, stop) in network.stops {
		let Some(loc) = stop.location else {
			continue;
		};

		let properties = [
			("id".into(), stop_id.into()),
			("name".into(), stop.name.into()),
			("code".into(), stop.code.into()),
			("ifopt".into(), stop.ifopt.into()),
			("uic".into(), stop.uic.into()),
		];
		features.push(Feature {
			bbox: None,
			geometry: Some(Value::Point(vec![loc.lon, loc.lat]).into()),
			id: None,
			properties: Some(properties.into_iter().collect()),
			foreign_members: None,
		});
	}

	for vehicle in vehicles {
		let Some(loc) = vehicle.location else {
			continue;
		};

		let properties = [
			("id".into(), vehicle.id.to_string().into()),
			("code".into(), vehicle.code.into()),
			("lineName".into(), vehicle.line_name.into()),
		];
		features.push(Feature {
			bbox: None,
			geometry: Some(Value::Point(vec![loc.lon, loc.lat]).into()),
			id: None,
			properties: Some(properties.into_iter().collect()),
			foreign_members: None,
		});
	}

	let collection = FeatureCollection {
		bbox: None,
		features,
		foreign_members: None,
	};

	serde_json::to_writer_pretty(stdout().lock(), &collection)?;

	Ok(())
}
