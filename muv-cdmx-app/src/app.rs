use std::{
	error,
	fmt::{self, Display, Formatter},
	result,
};

use scooter::Agent;

use crate::gcm::{CheckinError, RegisterError};

#[cfg(any(feature = "blocking", feature = "tokio"))]
use crate::gcm::Gcm;
#[cfg(any(feature = "blocking", feature = "tokio"))]
use scooter::Request;
#[cfg(any(feature = "blocking", feature = "tokio"))]
use serde::{Deserialize, de::DeserializeOwned};

#[cfg(any(feature = "blocking", feature = "tokio"))]
pub(crate) const APP: &str = "mx.gob.cdmx.adip.apps";
#[cfg(any(feature = "blocking", feature = "tokio"))]
pub(crate) const PROJECT: u64 = 911_152_330_019;

#[cfg(any(feature = "blocking", feature = "tokio"))]
const BASE_URL: &str = "https://apiapp.cdmx.gob.mx/index.php/";
#[cfg(any(feature = "blocking", feature = "tokio"))]
const BASE_QUERY: &str =
	"user_id=&os=Android&os_version=35&model=sdk_gphone64_x86_64&brand=google&lat=&lng=";

#[derive(Debug, Default)]
pub struct App<A> {
	pub agent: Agent<A>,
	pub device_id: Option<String>,
	pub token: Option<String>,
}

#[cfg(feature = "blocking")]
impl App<crate::Blocking> {
	fn checkin_and_register(&mut self) -> Result<()> {
		let gcm = Gcm {
			agent: self.agent.clone(),
		};
		let checkin = gcm.checkin(None)?;

		for _ in 0..3 {
			match gcm.register(APP, PROJECT, checkin) {
				Ok(token) => {
					self.device_id = Some(token);
					return Ok(());
				}
				Err(RegisterError::Generic) => {}
				Err(e) => Err(e)?,
			}
		}

		Err(RegisterError::Generic)?
	}

	fn maybe_auth_req<D: DeserializeOwned>(&mut self, path: &str, query: &str) -> Result<D> {
		if self.device_id.is_none() {
			self.checkin_and_register()?;
		}

		self.res(&self.req(path, query).send()?.bytes()?)
	}

	pub(crate) fn auth_req<D: DeserializeOwned>(&mut self, path: &str, query: &str) -> Result<D> {
		if self.token.is_none() {
			let () = self.maybe_auth_req("v1/Users/Token", "")?;
		}
		self.maybe_auth_req(path, query)
	}
}

#[cfg(feature = "tokio")]
impl App<crate::Tokio> {
	async fn checkin_and_register(&mut self) -> Result<()> {
		let gcm = Gcm {
			agent: self.agent.clone(),
		};
		let checkin = gcm.checkin(None).await?;

		for _ in 0..3 {
			match gcm.register(APP, PROJECT, checkin).await {
				Ok(token) => {
					self.device_id = Some(token);
					return Ok(());
				}
				Err(RegisterError::Generic) => {}
				Err(e) => Err(e)?,
			}
		}

		Err(RegisterError::Generic)?
	}

	async fn maybe_auth_req<D: DeserializeOwned>(&mut self, path: &str, query: &str) -> Result<D> {
		if self.device_id.is_none() {
			self.checkin_and_register().await?;
		}

		self.res(&self.req(path, query).send().await?.bytes()?)
	}

	pub(crate) async fn auth_req<D: DeserializeOwned>(
		&mut self,
		path: &str,
		query: &str,
	) -> Result<D> {
		if self.token.is_none() {
			let () = self.maybe_auth_req("v1/Users/Token", "").await?;
		}
		self.maybe_auth_req(path, query).await
	}
}

#[cfg(any(feature = "blocking", feature = "tokio"))]
impl<A: Clone> App<A> {
	fn req(&self, path: &str, query: &str) -> Request<A> {
		let device_token = self.device_id.as_ref().unwrap();
		let url = format!("{BASE_URL}{path}?{BASE_QUERY}&device_id={device_token}&{query}");
		dbg!(&url);
		let mut req = self.agent.get(url).debug(path.replace('/', "_"), query);
		if let Some(token) = &self.token {
			req = req.bearer(token);
		}
		req
	}

	fn res<D: DeserializeOwned>(&mut self, body: &[u8]) -> Result<D> {
		let full_err = match serde_json::from_slice(body) {
			Ok(FullRes {
				code: 200,
				message: _,
				response,
			}) => {
				if let Some(token) = response.token {
					self.token = Some(token);
				}
				return Ok(response.data);
			}
			Ok(FullRes {
				code,
				message,
				response: _,
			}) => {
				return Err(Error::Response { code, message });
			}
			Err(e) => e,
		};

		let json_err = match serde_json::from_slice(body) {
			Ok(CodeRes {
				code: 200,
				message: _,
			}) => full_err,
			Ok(CodeRes { code, message }) => return Err(Error::Response { code, message }),
			Err(e) => e,
		};

		Err(Error::Request(scooter::Error::Json(json_err)))
	}
}

#[cfg(any(feature = "blocking", feature = "tokio"))]
#[derive(Deserialize)]
struct FullRes<T> {
	code: u16,
	message: String,
	response: InnerRes<T>,
}

#[cfg(any(feature = "blocking", feature = "tokio"))]
#[derive(Deserialize)]
struct InnerRes<T> {
	#[serde(flatten)]
	data: T,
	token: Option<String>,
}

#[cfg(any(feature = "blocking", feature = "tokio"))]
#[derive(Deserialize)]
struct CodeRes {
	code: u16,
	message: String,
}

pub type Result<T> = result::Result<T, Error>;

#[derive(Debug)]
pub enum Error {
	Request(scooter::Error),
	Response { code: u16, message: String },

	Checkin(CheckinError),
	Register(RegisterError),
}

impl Display for Error {
	fn fmt(&self, f: &mut Formatter<'_>) -> fmt::Result {
		match self {
			Self::Request(_) => f.write_str("api request failed"),
			Self::Response { code, message } => write!(f, "{message} (code {code})"),

			Self::Checkin(_) => f.write_str("mandatory gcm checkin failed"),
			Self::Register(_) => f.write_str("mandatory gcm register failed"),
		}
	}
}

impl error::Error for Error {
	fn source(&self) -> Option<&(dyn error::Error + 'static)> {
		match self {
			Self::Request(e) => Some(e),
			Self::Response { .. } => None,
			Self::Checkin(e) => Some(e),
			Self::Register(e) => Some(e),
		}
	}
}

impl From<scooter::Error> for Error {
	fn from(value: scooter::Error) -> Self {
		Self::Request(value)
	}
}

impl From<CheckinError> for Error {
	fn from(value: CheckinError) -> Self {
		Self::Checkin(value)
	}
}

impl From<RegisterError> for Error {
	fn from(value: RegisterError) -> Self {
		Self::Register(value)
	}
}
