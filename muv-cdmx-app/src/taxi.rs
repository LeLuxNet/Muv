use serde::Deserialize;

use crate::Coordinate;

#[cfg(any(feature = "blocking", feature = "tokio"))]
use crate::{App, Result};

#[cfg(feature = "blocking")]
impl App<crate::Blocking> {
	pub fn taxi_driver_nearby(&mut self, coord: Coordinate) -> Result<ResponseDriver> {
		self.auth_req(
			"v2/Taxi/Driver/Nearby",
			&format!("lat={}&lng={}", coord.lat, coord.lng),
		)
	}
}

#[cfg(feature = "tokio")]
impl App<crate::Tokio> {
	pub async fn taxi_driver_nearby(&mut self, coord: Coordinate) -> Result<ResponseDriver> {
		self.auth_req(
			"v2/Taxi/Driver/Nearby",
			&format!("lat={}&lng={}", coord.lat, coord.lng),
		)
		.await
	}
}

#[derive(Debug, Deserialize)]
pub struct ResponseDriver {
	pub connected_driver: Option<bool>,
	pub drivers: Vec<Driver>,
	pub fee: Fee,
	pub pdf: Option<String>,
	pub vehicle_data: Option<Vehicle>,
}

#[derive(Debug, Deserialize)]
pub struct Driver {
	pub curp: Option<String>,
	pub driver_location: Option<Coordinate>,
	pub name: Option<String>,
	pub orientation: f64,
	pub percentage_canceled: Option<String>,
	pub phone: Option<String>,
	pub photo_url: Option<String>,
	pub plate: String,
	pub request_cancelled_count: Option<u32>,
	pub request_count: Option<u32>,
	pub score: Option<String>,
	pub trips_count: Option<u32>,
}

#[derive(Debug, Deserialize)]
pub struct Fee {
	pub description: String,
	pub extra: String,
}

#[derive(Debug, Deserialize)]
pub struct Vehicle {}
