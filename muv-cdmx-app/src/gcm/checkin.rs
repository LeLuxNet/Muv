use std::{
	error::Error,
	fmt::{self, Display, Formatter},
};

#[cfg(any(feature = "blocking", feature = "tokio"))]
use crate::gcm::Gcm;
#[cfg(any(feature = "blocking", feature = "tokio"))]
use protozer0::{PbfReader, PbfWriter, Tag, WireType};
#[cfg(any(feature = "blocking", feature = "tokio"))]
use scooter::{Agent, Request};

#[cfg(feature = "blocking")]
impl Gcm<crate::Blocking> {
	pub fn checkin(&self, known: Option<Checkin>) -> Result<Checkin, CheckinError> {
		res_body(&req(&self.agent, known).send()?.bytes()?)
	}
}

#[cfg(feature = "tokio")]
impl Gcm<crate::Tokio> {
	pub async fn checkin(&self, known: Option<Checkin>) -> Result<Checkin, CheckinError> {
		res_body(&req(&self.agent, known).send().await?.bytes()?)
	}
}

#[cfg(any(feature = "blocking", feature = "tokio"))]
fn req<A: Clone>(agent: &Agent<A>, known: Option<Checkin>) -> Request<A> {
	agent
		.post("https://android.clients.google.com/checkin")
		.content_type("application/x-protobuf")
		.bytes(req_body(known))
}

#[cfg(any(feature = "blocking", feature = "tokio"))]
fn req_body(known: Option<Checkin>) -> Vec<u8> {
	let mut req = Vec::new();
	let mut w = PbfWriter::new(&mut req);

	if let Some(known) = known {
		w.uint64(2, known.android_id);
		w.fixed64(13, known.security_token);
	}
	w.int32(14, 2); // version
	w.submessage(4); // checkin
	w.commit();

	req
}

#[cfg(any(feature = "blocking", feature = "tokio"))]
fn res_body(body: &[u8]) -> Result<Checkin, CheckinError> {
	let mut ok = None;
	let mut android_id = None;
	let mut security_token = None;

	let mut r = PbfReader::new(body);
	while let Some(tag) = r.next()? {
		match tag {
			tag if tag == Tag::new(1, WireType::Varint) => ok = Some(r.bool()?),
			tag if tag == Tag::new(7, WireType::I64) => android_id = Some(r.fixed64()?),
			tag if tag == Tag::new(8, WireType::I64) => security_token = Some(r.fixed64()?),
			_ => r.skip().unwrap(),
		}
	}

	match ok {
		None => return Err(CheckinError::MissingOk),
		Some(false) => return Err(CheckinError::NotOk),
		Some(true) => {}
	}

	let android_id = android_id.ok_or(CheckinError::MissingAndroidId)?;
	let security_token = security_token.ok_or(CheckinError::MissingAndroidId)?;

	Ok(Checkin {
		android_id,
		security_token,
	})
}

#[derive(Debug, Clone, Copy, PartialEq, Eq, Hash)]
pub struct Checkin {
	pub android_id: u64,
	pub security_token: u64,
}

#[derive(Debug)]
pub enum CheckinError {
	Request(scooter::Error),
	Protobuf(protozer0::Error),

	MissingOk,
	NotOk,

	MissingAndroidId,
	MissingSecurityToken,
}

impl Display for CheckinError {
	fn fmt(&self, f: &mut Formatter<'_>) -> fmt::Result {
		f.write_str(match self {
			Self::Request(_) => "request failed",
			Self::Protobuf(_) => "decoding protobuf failed",

			Self::MissingOk => "missing required field `ok` in response",
			Self::NotOk => "request was not ok",

			Self::MissingAndroidId => "missing android id in response",
			Self::MissingSecurityToken => "missing security token in response",
		})
	}
}

impl Error for CheckinError {
	fn source(&self) -> Option<&(dyn Error + 'static)> {
		match self {
			Self::Request(e) => Some(e),
			Self::Protobuf(e) => Some(e),
			Self::MissingOk | Self::NotOk | Self::MissingAndroidId | Self::MissingSecurityToken => {
				None
			}
		}
	}
}

impl From<scooter::Error> for CheckinError {
	fn from(value: scooter::Error) -> Self {
		Self::Request(value)
	}
}

impl From<protozer0::Error> for CheckinError {
	fn from(value: protozer0::Error) -> Self {
		Self::Protobuf(value)
	}
}

#[cfg(test)]
#[cfg(feature = "blocking")]
mod test {
	use crate::{Blocking, gcm::Gcm};

	#[test]
	fn revalidate() {
		let gcm = Gcm::<Blocking>::default();

		let first = gcm.checkin(None).unwrap();
		let second = gcm.checkin(Some(first)).unwrap();
		assert_eq!(first, second);
	}
}
