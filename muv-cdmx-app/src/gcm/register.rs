use std::{
	error::Error,
	fmt::{self, Display, Formatter},
};

#[cfg(any(feature = "blocking", feature = "tokio"))]
use crate::gcm::{Checkin, Gcm};
#[cfg(any(feature = "blocking", feature = "tokio"))]
use scooter::{Agent, Request, url};

#[cfg(feature = "blocking")]
impl Gcm<crate::Blocking> {
	pub fn register(
		&self,
		app: &str,
		project: u64,
		checkin: Checkin,
	) -> Result<String, RegisterError> {
		res(&req(&self.agent, app, project, checkin).send()?.text()?)
	}
}

#[cfg(feature = "tokio")]
impl Gcm<crate::Tokio> {
	pub async fn register(
		&self,
		app: &str,
		project: u64,
		checkin: Checkin,
	) -> Result<String, RegisterError> {
		res(&req(&self.agent, app, project, checkin)
			.send()
			.await?
			.text()?)
	}
}

#[cfg(any(feature = "blocking", feature = "tokio"))]
fn req<A: Clone>(
	agent: &Agent<A>,
	app: &str,
	project: u64,
	checkin: Checkin,
) -> Request<'static, A> {
	agent
		.post("https://android.apis.google.com/c2dm/register3")
		.header(
			"Authorization",
			format!("AidLogin {}:{}", checkin.android_id, checkin.security_token),
		)
		.content_type("application/x-www-form-urlencoded")
		.bytes(Vec::from(url!(
			"app={}&sender={:?}&X-scope=*&device={:?}",
			app,
			project,
			checkin.android_id,
		)))
}

#[cfg(any(feature = "blocking", feature = "tokio"))]
fn res(s: &str) -> Result<String, RegisterError> {
	for (key, val) in s.split('&').filter_map(|s| s.split_once('=')) {
		match key {
			"token" => return Ok(val.to_owned()),
			"Error" => {
				return Err(match val {
					"PHONE_REGISTRATION_ERROR" => RegisterError::Generic,
					"INVALID_SENDER" => RegisterError::InvalidSender,
					_ => RegisterError::Custom(val.to_owned()),
				});
			}
			_ => {}
		}
	}

	Err(RegisterError::MissingToken)
}

#[derive(Debug)]
pub enum RegisterError {
	Request(scooter::Error),
	MissingToken,

	Generic,
	InvalidSender,
	Custom(String),
}

impl Display for RegisterError {
	fn fmt(&self, f: &mut Formatter<'_>) -> fmt::Result {
		f.write_str(match self {
			Self::Request(_) => "request failed",
			Self::MissingToken => "missing token in response",

			Self::Generic => "generic registration error (PHONE_REGISTRATION_ERROR)",
			Self::InvalidSender => "invalid sender provided",
			Self::Custom(s) => s,
		})
	}
}

impl Error for RegisterError {
	fn source(&self) -> Option<&(dyn Error + 'static)> {
		match self {
			Self::Request(e) => Some(e),
			Self::MissingToken | Self::Generic | Self::InvalidSender | Self::Custom(_) => None,
		}
	}
}

impl From<scooter::Error> for RegisterError {
	fn from(value: scooter::Error) -> Self {
		Self::Request(value)
	}
}

#[cfg(feature = "blocking")]
#[cfg(test)]
mod test {
	use std::{thread::sleep, time::Duration};

	use scooter::Blocking;

	use crate::{APP, PROJECT, gcm::Gcm};

	#[test]
	fn checkin_and_register() {
		let gcm = Gcm::<Blocking>::default();

		let checkin = gcm.checkin(None).unwrap();
		sleep(Duration::from_millis(500));
		gcm.register(APP, PROJECT, checkin).unwrap();
	}
}
