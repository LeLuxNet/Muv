use scooter::Agent;

mod checkin;
pub use checkin::*;

mod register;
pub use register::*;

#[derive(Debug, Default, Clone)]
pub struct Gcm<A> {
	pub agent: Agent<A>,
}
