use serde::{Deserialize, Serialize};

#[derive(Debug, Clone, Copy, PartialEq, Serialize, Deserialize)]
pub struct Coordinate {
	pub lat: f64,
	pub lng: f64,
}

impl Default for Coordinate {
	fn default() -> Self {
		Self {
			lat: 19.431_968,
			lng: -99.133_333,
		}
	}
}
