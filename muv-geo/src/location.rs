use std::{
	f64,
	fmt::{self, Display, Formatter},
};

use geographiclib_rs::{Geodesic, InverseGeodesic};

#[cfg(feature = "serde")]
use serde::{Deserialize, Serialize};

const SEMI_MAJOR_AXIS: f64 = 6_378_137.0;

/// WGS 84 coordinates location
#[derive(Debug, Clone, Copy, PartialEq)]
#[cfg_attr(feature = "serde", derive(Serialize, Deserialize))]
pub struct Location {
	pub lat: f64,
	pub lon: f64,
}

impl Display for Location {
	fn fmt(&self, f: &mut Formatter<'_>) -> fmt::Result {
		write!(f, "{}, {}", self.lat, self.lon)
	}
}

impl Location {
	#[must_use]
	pub const fn new(lat: f64, lon: f64) -> Self {
		Self { lat, lon }
	}

	/// ```
	/// # use muv_geo::Location;
	/// assert_eq!(
	/// 	Location::new(160.0, 160.0).canonicalize(),
	/// 	Location::new(20.0, 160.0)
	/// );
	/// assert_eq!(
	/// 	Location::new(400.0, 400.0).canonicalize(),
	/// 	Location::new(40.0, 40.0)
	/// );
	/// assert_eq!(
	/// 	Location::new(280.0, -200.0).canonicalize(),
	/// 	Location::new(-80.0, 160.0)
	/// );
	/// ```
	pub fn canonicalize(self) -> Self {
		let lat = ((self.lat - 90.0).rem_euclid(360.0) - 180.0).abs() - 90.0;
		let lon = (self.lon + 180.0).rem_euclid(360.0) - 180.0;
		Self { lat, lon }
	}

	/// ```
	/// # use muv_geo::Location;
	/// let loc = Location::new(31.325672, 120.616086);
	///
	/// let (x, y) = loc.to_web_mercator();
	/// assert_eq!(x, 13426921.274997693);
	/// assert_eq!(y, 3675116.470582768);
	/// ```
	pub fn to_web_mercator(self) -> (f64, f64) {
		let x = self.lon.to_radians() * SEMI_MAJOR_AXIS;
		let lat = (self.lat + 90.0).to_radians() / 2.0;
		let y = lat.tan().ln() * SEMI_MAJOR_AXIS;
		(x, y)
	}
}

impl Location {
	/// Calculates the geodesic distance in meters between two locations
	/// using the [algorithm given by Karney](https://arxiv.org/pdf/1109.4448).
	///
	/// ```
	/// # use muv_geo::Location;
	/// let from = Location::new(19.05840, -98.30190);
	/// let to = Location::new(50.92884, 11.58460);
	///
	/// let distance = from.distance(to);
	/// assert_eq!(distance, 9_698_197.296_924_35);
	/// ```
	#[must_use]
	pub fn distance(self, other: Self) -> f64 {
		Geodesic::wgs84().inverse(self.lat, self.lon, other.lat, other.lon)
	}

	/// Calculates the linear distance in meters between two locations
	/// using the [haversine formula](https://en.wikipedia.org/wiki/Haversine_formula)
	///
	/// ```
	/// # use muv_geo::Location;
	/// let from = Location::new(50.36317, -4.14211);
	/// let to = Location::new(50.64093, 3.04454);
	/// # assert!((from.distance(to) - 510_600.611_174_951_6).abs() < 1e-9);
	///
	/// let difference = from.linear_distance(to) - from.distance(to);
	/// assert!(difference.abs() < 1_500.0);
	/// ```
	#[must_use]
	pub fn linear_distance(self, other: Self) -> f64 {
		let lat1 = self.lat.to_radians();
		let lon1 = self.lon.to_radians();
		let lat2 = other.lat.to_radians();
		let lon2 = other.lon.to_radians();

		fn double_hav(a: f64, b: f64) -> f64 {
			1.0 - (b - a).cos()
		}
		let lat_hav = double_hav(lat1, lat2);
		let lon_hav = double_hav(lon1, lon2);
		let num = lat_hav + lat1.cos() * lat2.cos() * lon_hav;
		2.0 * SEMI_MAJOR_AXIS * (num / 2.0).sqrt().asin()
	}
}

impl From<[f64; 2]> for Location {
	fn from([lat, lon]: [f64; 2]) -> Self {
		Self { lat, lon }
	}
}

impl From<(f64, f64)> for Location {
	fn from((lat, lon): (f64, f64)) -> Self {
		Self { lat, lon }
	}
}

#[cfg(feature = "geo-types")]
impl From<geo_types::Coord> for Location {
	fn from(value: geo_types::Coord) -> Self {
		Self {
			lat: value.y,
			lon: value.x,
		}
	}
}

#[cfg(feature = "geo-types")]
impl From<geo_types::Point> for Location {
	fn from(value: geo_types::Point) -> Self {
		value.0.into()
	}
}

#[cfg(feature = "geojson")]
impl From<Location> for geojson::Value {
	fn from(value: Location) -> Self {
		Self::Point(vec![value.lon, value.lat])
	}
}

#[cfg(feature = "proj")]
impl proj::Coord<f64> for Location {
	fn x(&self) -> f64 {
		self.lon
	}
	fn y(&self) -> f64 {
		self.lat
	}

	fn from_xy(x: f64, y: f64) -> Self {
		Self { lat: y, lon: x }
	}
}

#[cfg(feature = "wkt")]
impl wkt::ToWkt<f64> for Location {
	fn to_wkt(&self) -> wkt::Wkt<f64> {
		let coord = wkt::types::Coord {
			x: self.lon,
			y: self.lat,
			z: None,
			m: None,
		};
		wkt::Wkt::Point(wkt::types::Point(Some(coord)))
	}
}
