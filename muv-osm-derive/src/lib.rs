use std::collections::BTreeMap;

use proc_macro2::{Ident, Literal, Span, TokenStream};
use quote::{ToTokens, TokenStreamExt, quote};
use syn::{
	Data, DataEnum, DeriveInput, Error, LitStr, Meta, Token, parse_macro_input,
	punctuated::Punctuated,
};

fn pascal_to_snake(s: &str) -> String {
	let mut res = String::new();
	for c in s.chars() {
		if c.is_uppercase() {
			if !res.is_empty() {
				res.push('_');
			}
			res.extend(c.to_lowercase());
		} else {
			res.push(c)
		}
	}
	res
}

struct EnumVariant {
	ident: Ident,
	aliases: Vec<(Span, String)>,
}

fn parse_enum(data: DataEnum) -> syn::Result<Vec<EnumVariant>> {
	data.variants
		.into_iter()
		.map(|variant| {
			if !variant.fields.is_empty() {
				return Err(Error::new_spanned(
					variant.fields,
					"#[derive(FromOsmStr)] does not support enum fields",
				));
			}

			let ident = variant.ident;

			let attr = variant.attrs.into_iter().find(
				|attr| matches! (&attr.meta, Meta::List(meta) if meta.path.is_ident("osm_str")),
			);

			let aliases = if let Some(attr) = attr {
				let Ok(meta) =
					attr.parse_args_with(Punctuated::<LitStr, Token![|]>::parse_terminated)
				else {
					return Err(Error::new_spanned(
						attr,
						"expected string literals seperated by `|` pipes",
					));
				};

				meta.into_iter().map(|s| (s.span(), s.value())).collect()
			} else {
				vec![(ident.span(), pascal_to_snake(&ident.to_string()))]
			};

			Ok(EnumVariant { ident, aliases })
		})
		.collect()
}

#[derive(Default)]
struct Trie {
	data: Option<Ident>,
	children: BTreeMap<u8, Trie>,
}

impl Trie {
	fn insert(&mut self, s: &[u8], data: Ident) -> Option<Ident> {
		let mut node = self;
		for b in s {
			let normalized_b = if b.is_ascii() {
				b.to_ascii_lowercase()
			} else {
				*b
			};
			node = node.children.entry(normalized_b).or_default();
		}
		node.data.replace(data)
	}

	fn if_chain(&self, b: TokenStream, mut index: usize) -> TokenStream {
		let mut same_cond = TokenStream::new();

		let mut node = self;
		while node.children.len() == 1 {
			if !same_cond.is_empty() {
				same_cond.append_all(quote! { && });
			}

			let ch;
			(ch, node) = node.children.iter().next().unwrap();
			let indext = Literal::usize_unsuffixed(index);
			same_cond.append_all(case_ignore_eq(quote! { #b[#indext] }, *ch));
			index += 1;
		}

		let mut res = TokenStream::new();
		if node.children.is_empty() {
			let data = node.data.as_ref().unwrap();
			res = quote! { return Some(Self::#data) };
		} else {
			for (ch, child) in &node.children {
				if !res.is_empty() {
					res.append_all(quote! { else });
				}
				let indext = Literal::usize_unsuffixed(index);
				let cond = case_ignore_eq(quote! { #b[#indext] }, *ch);
				let body = child.if_chain(b.clone(), index + 1);
				res.append_all(quote! { if #cond { #body } });
			}
		}

		if !same_cond.is_empty() {
			res = quote! { if #same_cond { #res } };
		}

		res
	}
}

fn case_ignore_eq(left: TokenStream, right: u8) -> TokenStream {
	let right_tok = Literal::byte_character(right);

	let right_upper = right.to_ascii_uppercase();
	if right_upper == right {
		quote! { #left == #right_tok }
	} else {
		let right_upper_tok = Literal::byte_character(right_upper);
		quote! { (#left == #right_tok || #left == #right_upper_tok) }
	}
}

#[proc_macro_derive(FromOsmStr, attributes(osm_str))]
pub fn from_derive(t: proc_macro::TokenStream) -> proc_macro::TokenStream {
	let input: DeriveInput = parse_macro_input!(t);
	match from_derive_inner(input) {
		Ok(tokens) => tokens.into(),
		Err(err) => err.to_compile_error().into(),
	}
}

fn from_derive_inner(input: DeriveInput) -> syn::Result<TokenStream> {
	let Data::Enum(data) = input.data else {
		return Err(Error::new(
			Span::call_site(),
			"#[derive(FromOsmStr)] can only be used on C-style enums",
		));
	};

	let data = parse_enum(data)?;
	let ident = input.ident;

	let mut forest: BTreeMap<usize, Trie> = BTreeMap::new();
	for variant in data {
		let ident = variant.ident;
		for (span, value) in variant.aliases {
			let old = forest
				.entry(value.len())
				.or_default()
				.insert(value.as_bytes(), ident.clone());
			if old.is_some() && old.as_ref() != Some(&ident) {
				return Err(Error::new(
					span,
					format!("conflicting duplicate name \"{value}\""),
				));
			}
		}
	}

	let bytes = quote! { b };

	let mut arms = TokenStream::new();
	for (len, trie) in forest {
		let body = trie.if_chain(bytes.clone(), 0);
		arms.append_all(quote! { #len => #body });
	}

	Ok(quote! {
		#[automatically_derived]
		impl FromOsmStr for #ident {
			fn from_osm_str(s: &str) -> Option<Self> {
				let #bytes = s.as_bytes();
				match #bytes.len() {
					#arms
					_ => {}
				}
				None
			}
		}
	})
}

#[proc_macro_derive(ToOsmStr, attributes(osm_str))]
pub fn to_derive(t: proc_macro::TokenStream) -> proc_macro::TokenStream {
	let input: DeriveInput = parse_macro_input!(t);
	match to_derive_inner(input) {
		Ok(tokens) => tokens.into(),
		Err(err) => err.to_compile_error().into(),
	}
}

fn to_derive_inner(input: DeriveInput) -> syn::Result<TokenStream> {
	let Data::Enum(data) = input.data else {
		return Err(Error::new(
			Span::call_site(),
			"#[derive(ToOsmStr)] can only be used on C-style enums",
		));
	};

	let data = parse_enum(data)?;
	let ident = input.ident;

	let cases: TokenStream = data
		.into_iter()
		.filter_map(|variant| {
			let ident = variant.ident;
			let value = variant.aliases.into_iter().next()?.1.into_token_stream();
			Some(quote!(Self::#ident => #value,))
		})
		.collect();

	Ok(quote! {
		#[automatically_derived]
		impl ToOsmStr for #ident {
			fn to_osm_str(self) -> Option<std::borrow::Cow<'static, str>> {
				Some(match self {
					#cases
					_ => return None
				}
				.into())
			}
		}
	})
}
