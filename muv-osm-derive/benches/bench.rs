use std::{
	collections::{BTreeMap, HashMap},
	env::temp_dir,
	ffi::{c_char, c_uchar},
	fs::{File, remove_file},
	hash::BuildHasher,
	io::{Write, stderr},
	mem,
	path::PathBuf,
	process::{Command, Stdio},
	sync::Mutex,
};

use criterion::{BenchmarkId, Criterion, black_box, criterion_group, criterion_main};
use libloading::Library;
use muv_osm_derive::FromOsmStr;
use muv_osm_pbf::read::{Loader, Reader};
use phf::phf_map;
use rustc_hash1::FxHashMap;

fn load_pbf(name: &str) -> Vec<Vec<(String, String)>> {
	let path: PathBuf = [
		env!("CARGO_MANIFEST_DIR"),
		"benches",
		&format!("{}.osm.pbf", name),
	]
	.iter()
	.collect();

	let (r, _) = Reader::new_file(File::open(path).unwrap()).unwrap();
	let mut l = Loader::new(r);
	let tags: Mutex<Vec<_>> = Mutex::default();

	l.load()
		.way(|_, strings, w| {
			let t = w
				.keys_vals
				.into_iter()
				.map(|kv| {
					let (k, v) = kv?;
					let k = strings[usize::try_from(k).unwrap()].to_owned();
					let v = strings[usize::try_from(v).unwrap()].to_owned();
					Ok((k, v))
				})
				.collect::<muv_osm_pbf::Result<_>>()?;
			tags.lock().unwrap().push(t);
			Ok(())
		})
		.run()
		.unwrap();

	tags.into_inner().unwrap()
}

trait Table<T> {
	fn get(&self, key: &str) -> Option<T>;
}

impl<T: Copy> Table<T> for Vec<(&str, T)> {
	fn get(&self, key: &str) -> Option<T> {
		Some(self.iter().find(|(k, _)| &key == k)?.1)
	}
}

struct SortedVec<'a, T>(Vec<(&'a str, T)>);

impl<'a, T> FromIterator<(&'a str, T)> for SortedVec<'a, T> {
	fn from_iter<I: IntoIterator<Item = (&'a str, T)>>(iter: I) -> Self {
		let mut v: Vec<_> = iter.into_iter().collect();
		v.sort_unstable_by(|(k1, _), (k2, _)| k1.cmp(k2));
		Self(v)
	}
}

impl<'a, T: Copy> Table<T> for SortedVec<'a, T> {
	fn get(&self, key: &str) -> Option<T> {
		let i = self.0.binary_search_by(|(k, _)| k.cmp(&key)).ok()?;
		Some(self.0[i].1)
	}
}

impl<T: Copy, S: BuildHasher> Table<T> for HashMap<&str, T, S> {
	fn get(&self, key: &str) -> Option<T> {
		self.get(key).copied()
	}
}

impl<T: Copy> Table<T> for BTreeMap<&str, T> {
	fn get(&self, key: &str) -> Option<T> {
		self.get(key).copied()
	}
}

impl<T: Copy> Table<T> for phf::Map<&str, T> {
	fn get(&self, key: &str) -> Option<T> {
		self.get(key).copied()
	}
}

trait FromOsmStr: Sized {
	fn from_osm_str(s: &str) -> Option<Self>;
}

#[repr(C)]
#[derive(Debug)]
struct GperfRes {
	name: *const c_char,
	index: c_uchar,
}

struct Gperf {
	path: PathBuf,
	in_word_set: unsafe extern "C" fn(*const c_char, usize) -> *const GperfRes,
}

impl<'a> FromIterator<&'a str> for Gperf {
	fn from_iter<T: IntoIterator<Item = &'a str>>(iter: T) -> Self {
		let gperf = Command::new("gperf")
			.args(&["-I", "-l", "-C", "-t", "--ignore-case"])
			.stdin(Stdio::piped())
			.stdout(Stdio::piped())
			.stderr(stderr())
			.spawn()
			.unwrap();

		let path = temp_dir().join("muv_osm_derive_bench");

		let mut comp = Command::new("gcc")
			.args([
				"-x",
				"c",
				"-shared",
				"-fPIC",
				"-O3",
				"-march=native",
				"-",
				"-o",
			])
			.arg(path.as_os_str())
			.stdin(gperf.stdout.unwrap())
			.stdout(stderr())
			.stderr(stderr())
			.spawn()
			.unwrap();

		let mut stdin = gperf.stdin.unwrap();
		write!(
			stdin,
			"struct res {{ char *name; unsigned char index; }};\n%%\n"
		)
		.unwrap();
		for (i, token) in iter.into_iter().enumerate() {
			write!(stdin, "{token},{i}\n").unwrap();
		}
		drop(stdin);

		comp.wait().unwrap();

		let lib = unsafe { Library::new(&path) }.unwrap();
		let in_word_set = *unsafe { lib.get(b"in_word_set\0") }.unwrap();
		mem::forget(lib);

		Self { path, in_word_set }
	}
}

impl Gperf {
	fn find(&self, b: &str) -> Option<u8> {
		let ptr = b.as_ptr() as *const c_char;
		let res = unsafe { (self.in_word_set)(ptr, b.len()) };
		if res.is_null() {
			None
		} else {
			Some(unsafe { res.read() }.index)
		}
	}
}

impl Drop for Gperf {
	fn drop(&mut self) {
		let _ = remove_file(&self.path);
	}
}

macro_rules! bench_tables {
    ($c:ident $name:literal in $data:ident $($key:literal => $val:ident,)+) => {{
        let mut group = $c.benchmark_group($name);

        #[derive(Debug, Clone, Copy, PartialEq, FromOsmStr, enum_utils::FromStr)]
        #[enumeration(case_insensitive)]
        enum Val {
            $(
                #[osm_str($key)]
                #[enumeration(rename = $key)]
                $val,
            )+
        }

        struct Match;
        impl Table<Val> for Match {
            fn get(&self, key: &str) -> Option<Val> {
                Some(match key {
                    $($key => Val::$val,)+
                    _ => return None
                })
            }
        }

        struct Derive;
        impl Table<Val> for Derive {
            fn get(&self, key: &str) -> Option<Val> {
                Val::from_osm_str(key)
            }
        }

        struct EnumUtils;
        impl Table<Val> for EnumUtils {
            fn get(&self, key: &str) -> Option<Val> {
                key.parse().ok()
            }
        }

        struct Hashify;
        impl Table<Val> for Hashify {
            fn get(&self, key: &str) -> Option<Val> {
                hashify::map! {
                    key.as_bytes(),
                    Val,
                    $($key => Val::$val,)+
                }.copied()
            }
        }

        struct HashifyTiny;
        impl Table<Val> for HashifyTiny {
            fn get(&self, key: &str) -> Option<Val> {
                hashify::tiny_map! {
                    key.as_bytes(),
                    $($key => Val::$val,)+
                }
            }
        }

        struct HashifyIgnoreCase;
        impl Table<Val> for HashifyIgnoreCase {
            fn get(&self, key: &str) -> Option<Val> {
                hashify::map_ignore_case! {
                    key.as_bytes(),
                    Val,
                    $($key => Val::$val,)+
                }.copied()
            }
        }

        struct HashifyTinyIgnoreCase;
        impl Table<Val> for HashifyTinyIgnoreCase {
            fn get(&self, key: &str) -> Option<Val> {
                hashify::tiny_map_ignore_case! {
                    key.as_bytes(),
                    $($key => Val::$val,)+
                }
            }
        }

        struct EnumGperf(Gperf);
        impl Table<Val> for EnumGperf {
            fn get(&self, key: &str) -> Option<Val> {
                self.0.find(key).map(|k| unsafe { mem::transmute(k) })
            }
        }

        let values = [$(($key, Val::$val)),+];

        let vec: Vec<_> = values.into_iter().collect();
        let gperf = EnumGperf([ $($key),+ ].into_iter().collect());
        let sorted_vec: SortedVec<_> = values.into_iter().collect();
        let hash_map: HashMap<_, _> = values.into_iter().collect();
        let rustc_hash_map: rustc_hash::FxHashMap<_, _> = values.into_iter().collect();
        let fx_hash_map: FxHashMap<_, _> = values.into_iter().collect();
        let gxhash_map: gxhash::HashMap<_, _> = values.into_iter().collect();
        let ahash_map: HashMap<_, _, ahash::RandomState> = values.into_iter().collect();
        let foldhash_fast_map: HashMap<_, _, foldhash::fast::RandomState> = values.into_iter().collect();
        let foldhash_quality_map: HashMap<_, _, foldhash::quality::RandomState> = values.into_iter().collect();
        let btree_map: BTreeMap<_, _> = values.into_iter().collect();
        let phf = phf_map! { $($key => Val::$val,)+ };

        let tables = [
            ("derive", Box::new(Derive) as Box<dyn Table<_>>),
            ("gperf", Box::new(gperf)),
            ("match", Box::new(Match)),
            ("enum_utils::FromStr", Box::new(EnumUtils)),
            ("hashify::map", Box::new(Hashify)),
            ("hashify::tiny_map", Box::new(HashifyTiny)),
            ("hashify::map_ignore_case", Box::new(HashifyIgnoreCase)),
            ("hashify::tiny_map_ignore_case", Box::new(HashifyTinyIgnoreCase)),
            ("std::Vec::find", Box::new(vec)),
            ("std::Vec::binary_search", Box::new(sorted_vec)),
            ("std::HashMap", Box::new(hash_map)),
            ("rustc-hash", Box::new(rustc_hash_map)),
            ("FxHashMap", Box::new(fx_hash_map)),
            ("gxhash", Box::new(gxhash_map)),
            ("ahash", Box::new(ahash_map)),
            ("foldhash::fast", Box::new(foldhash_fast_map)),
            ("foldhash::quality", Box::new(foldhash_quality_map)),
            ("std::BTreeMap", Box::new(btree_map)),
            ("phf::Map", Box::new(phf)),
        ];

        for (name, table) in tables.iter() {
            for (key, val) in values.iter() {
                assert_eq!(table.get(key), Some(*val), "{name}");
            }
        }

        let mismatching: Vec<_> = $data
            .iter()
            .flatten()
            .map(|(key, _)| key.as_str())
            .filter(|key| Match.get(key).is_none())
            .collect();

        for percent in (0..=100).step_by(50) {
            let split = values.len() * percent / 100;
            let mix: Vec<_> = values[split..]
                .iter()
                .map(|(key, _)| key)
                .chain(&mismatching[..split])
                .collect();

            for (name, table) in &tables {
                group.bench_with_input(BenchmarkId::new(*name, percent), &mix,
                    |b, mix| b.iter(|| mix.iter().for_each(|key| {
                        black_box(table.get(black_box(key)));
                    })));
            }
        }

        group.finish();
    }};
}

fn bench(c: &mut Criterion) {
	let data = load_pbf("bremen");

	bench_tables! { c "TMode" in data
		"foot" => Foot,
		"dog" => Dog,
		"ski" => Ski,
		"inline_skates" => InlineSkates,
		"horse" => Horse,

		"vehicle" => Vehicle,
		"bicycle" => Bicycle,
		"electric_bicycle" => ElectricBicycle,

		"kick_scooter" => KickScooter,
		"carriage" => Carriage,
		"cycle_rickshaw" => CycleRickshaw,
		"hand_cart" => HandCart,
		"trailer" => Trailer,
		"caravan" => Caravan,

		"motor_vehicle" => MotorVehicle,
		"motorcycle" => Motorcycle,
		"moped" => Moped,
		"speed_pedelec" => SpeedPedelec,
		"mofa" => Mofa,
		"small_electric_vehicle" => SmallElectricVehicle,

		"double_tracked_motor_vehicle" => DoubleTrackedMotorVehicle,
		"motorcar" => Motorcar,
		"motorhome" => Motorhome,
		"tourist_bus" => TouristBus,
		"coach" => Coach,
		"goods" => Goods,
		"hgv" => Hgv,
		"hgv_articulated" => HgvArticulated,
		"bdouble" => BDouble,
		"agricultural" => Agricultural,
		"auto_rickshaw" => AutoRickshaw,
		"nev" => Nev,
		"golf_cart" => GolfCart,
		"atv" => Atv,
		"ohv" => Ohv,
		"snowmobile" => Snowmobile,

		"psv" => Psv,
		"bus" => Bus,
		"taxi" => Taxi,
		"minibus" => Minibus,
		"share_taxi" => ShareTaxi,
		"hov" => Hov,
		"carpool" => Carpool,
		"car_sharing" => CarSharing,
		"emergency" => Emergency,
		"hazmat" => Hazmat,
		"disabled" => Disabled,
	};
}

criterion_group!(benches, bench);
criterion_main!(benches);
