#![no_main]

use std::io::Cursor;

use libfuzzer_sys::fuzz_target;
use muv_osm::format::pbf::{Data, DataEntry, DataIter, DenseNodes, Reader, Result};

fn run(input: &[u8]) -> Result<()> {
	let mut r = Reader::new(Cursor::new(input))?;

	while let Some(blob) = r.next()? {
		let b = blob.decompress()?;

		let data = Data::decode(&b)?;
		let _ = data.string_table.last();

		for entry in DataIter::new(&b) {
			match entry? {
				DataEntry::DenseNodes(d) => {
					let mut dn = DenseNodes::decode(d)?;
					while dn.next()?.is_some() {}
				}
				DataEntry::Node(_)
				| DataEntry::Way(_)
				| DataEntry::Relation(_)
				| DataEntry::ChangeSet(_) => {}
			}
		}
	}

	Ok(())
}

fuzz_target!(|input: &[u8]| {
	let _ = run(input);
});
