#![no_main]

use libfuzzer_sys::{
	arbitrary::{self, Arbitrary},
	fuzz_target,
};
use muv_osm::{lanes::lanes, Tag};

#[derive(Debug, Arbitrary)]
struct Input<'a> {
	tag: Tag<'a>,
	regions: Vec<String>,
}

fuzz_target!(|input: Input| {
	if let Some(lanes) = lanes(&input.tag, &input.regions) {
		let double_lanes = lanes.lanes.len() * 2;
		assert!(double_lanes >= usize::from(lanes.centre));
		assert!(double_lanes >= usize::from(lanes.centre_line));
	};
});
