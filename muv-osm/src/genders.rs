use crate::{FromOsm, FromOsmStr, Taglike, get_tag, simple_bitflags};

simple_bitflags!(Genders: u8 {
	Unisex(unisex) = 1 << 0,
	Male(male)     = 1 << 1,
	Female(female) = 1 << 2,
});

impl Genders {
	#[inline]
	fn new(unisex: bool, male: bool, female: bool) -> Self {
		let mut res = Self::default();
		if unisex {
			res |= Self::Unisex;
		}
		if male {
			res |= Self::Male;
		}
		if female {
			res |= Self::Female;
		}
		res
	}
}

impl<'a, T: Taglike<'a>> FromOsm<'a, T> for Genders {
	fn from_osm(tag: T) -> Option<Self> {
		let male = get_tag!(tag, male).and_then(bool::from_osm_str);
		let female = get_tag!(tag, female).and_then(bool::from_osm_str);
		let unisex = get_tag!(tag, unisex).and_then(bool::from_osm_str);
		if male.is_none() && female.is_none() && unisex.is_none() {
			return None;
		}

		let segregated = get_tag!(tag, gender_segregated)
			.and_then(bool::from_osm_str)
			.unwrap_or_default();

		let unisex = unisex.unwrap_or_default();
		if segregated {
			Some(Self::new(
				false,
				male.unwrap_or(unisex),
				female.unwrap_or(unisex),
			))
		} else {
			Some(Self::new(
				unisex,
				male.unwrap_or_default(),
				female.unwrap_or_default(),
			))
		}
	}
}
