#![cfg_attr(docsrs, feature(doc_auto_cfg))]

pub use muv_geo::Location;
pub use muv_osm_derive::{FromOsmStr, ToOsmStr};

#[warn(missing_docs)]
mod access;
pub use access::*;

mod tmode;
pub use tmode::*;

#[warn(missing_docs)]
mod hierarchy;
pub use hierarchy::*;

mod colour;
pub use colour::*;

#[cfg(feature = "conditional")]
pub mod conditional;
#[cfg(feature = "conditional")]
#[doc(inline)]
pub use conditional::Conditional;

mod parse;
pub use parse::*;

mod bitflags;
pub(crate) use bitflags::*;

mod surface;
pub use surface::*;

mod tags;
pub use tags::*;

mod lifecycle;
pub use lifecycle::*;

pub mod units;

mod vehicle;
pub use vehicle::*;

mod name;
pub use name::*;

mod genders;
pub use genders::*;

#[cfg(feature = "conditional")]
mod restriction;
#[cfg(feature = "conditional")]
pub use restriction::*;

#[cfg(feature = "addr")]
pub mod addr;

#[cfg(feature = "lanes")]
pub mod lanes;

// TODO
// #[cfg(feature = "tree")]
// pub mod tree;
