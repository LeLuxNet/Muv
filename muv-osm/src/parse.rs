use std::{borrow::Cow, collections::BTreeMap};

#[cfg(feature = "mediawiki")]
use muv_mediawiki::wikibase::Q;

use crate::Taglike;

/// Parse a string into an OSM value
///
/// The trait is most of the time derived for enums using the derive macro.
/// ```
/// # use muv_osm::FromOsmStr;
/// #[derive(Debug, PartialEq, FromOsmStr)]
/// enum Lit {
/// 	Yes,
/// 	Limited,
/// 	#[osm_str("24/7")]
/// 	TwentyfourSeven,
/// 	StreetLight,
/// }
///
/// assert_eq!(Lit::from_osm_str("yes"), Some(Lit::Yes));
/// assert_eq!(Lit::from_osm_str("Limited"), Some(Lit::Limited));
/// assert_eq!(Lit::from_osm_str("24/7"), Some(Lit::TwentyfourSeven));
/// assert_eq!(Lit::from_osm_str("street_light"), Some(Lit::StreetLight));
/// assert_eq!(Lit::from_osm_str("unknown"), None);
/// ```
pub trait FromOsmStr: Sized {
	fn from_osm_str(s: &str) -> Option<Self>;
}

pub trait FromOsm<'a, T: Taglike<'a>>: Sized {
	fn from_osm(tag: T) -> Option<Self>;
}

pub trait ToOsmStr {
	fn to_osm_str(self) -> Option<Cow<'static, str>>;
}

impl FromOsmStr for bool {
	fn from_osm_str(s: &str) -> Option<Self> {
		match s {
			"yes" => Some(true),
			"no" => Some(false),
			_ => None,
		}
	}
}

impl ToOsmStr for bool {
	fn to_osm_str(self) -> Option<Cow<'static, str>> {
		Some(if self { "yes" } else { "no" }.into())
	}
}

impl FromOsmStr for u32 {
	fn from_osm_str(s: &str) -> Option<Self> {
		s.parse().ok()
	}
}

impl ToOsmStr for u32 {
	fn to_osm_str(self) -> Option<Cow<'static, str>> {
		Some(self.to_string().into())
	}
}

impl FromOsmStr for String {
	fn from_osm_str(s: &str) -> Option<Self> {
		Some(s.to_owned())
	}
}

impl ToOsmStr for String {
	fn to_osm_str(self) -> Option<Cow<'static, str>> {
		Some(self.into())
	}
}

#[cfg(feature = "mediawiki")]
impl FromOsmStr for Q {
	fn from_osm_str(s: &str) -> Option<Self> {
		s.parse().ok()
	}
}

#[cfg(feature = "mediawiki")]
impl ToOsmStr for Q {
	fn to_osm_str(self) -> Option<Cow<'static, str>> {
		Some(self.to_string().into())
	}
}

impl FromOsmStr for Vec<String> {
	fn from_osm_str(s: &str) -> Option<Self> {
		Some(SeperatedValueIter::new(s).map(Cow::into_owned).collect())
	}
}

impl ToOsmStr for Vec<String> {
	fn to_osm_str(self) -> Option<Cow<'static, str>> {
		if self.is_empty() {
			return Some(String::new().into());
		}

		let rest_values = &self[1..];
		let len = rest_values.iter().map(String::len).sum::<usize>() + rest_values.len();

		let mut values = self.into_iter();
		let mut res = values.next().unwrap();
		if res.contains(';') {
			res = res.replace(';', ";;");
		}

		res.reserve_exact(len);

		for part in values {
			res.push(';');

			let mut escape_parts = part.split(';');
			res.push_str(escape_parts.next().unwrap());
			for escape_part in escape_parts {
				res.push_str(";;");
				res.push_str(escape_part);
			}
		}

		Some(Cow::Owned(res))
	}
}

/// Iterate through [semicolon seperated values](https://wiki.openstreetmap.org/wiki/Semi-colon_value_separator)
///
/// Seperated values are a string containing semicolons `;`.
/// ```
/// use muv_osm::SeperatedValueIter;
/// use std::borrow::Cow;
///
/// let mut iter = SeperatedValueIter::new("cash;card;voucher");
///
/// assert_eq!(iter.next(), Some(Cow::Borrowed("cash")));
/// assert_eq!(iter.next(), Some(Cow::Borrowed("card")));
/// assert_eq!(iter.next(), Some(Cow::Borrowed("voucher")));
/// assert_eq!(iter.next(), None);
/// ```
///
/// To write a literal semicolon inside a seperated value a second semicolon `;;` can be used to escape.
/// ```
/// # use muv_osm::SeperatedValueIter;
/// # use std::borrow::Cow;
/// let mut iter = SeperatedValueIter::new("hack;;space");
///
/// assert_eq!(iter.next(), Some(Cow::Owned("hack;space".into())));
/// assert_eq!(iter.next(), None);
/// ```
#[must_use]
pub struct SeperatedValueIter<'a> {
	s: Option<&'a str>,
}

impl<'a> SeperatedValueIter<'a> {
	pub const fn new(s: &'a str) -> Self {
		Self { s: Some(s) }
	}
}

impl<'a> Iterator for SeperatedValueIter<'a> {
	type Item = Cow<'a, str>;

	fn next(&mut self) -> Option<Self::Item> {
		let s = self.s?;

		let Some((value_part, rest)) = s.split_once(';') else {
			self.s = None;
			return Some(Cow::Borrowed(s));
		};

		let Some(mut next_part) = rest.strip_prefix(';') else {
			self.s = Some(rest);
			return Some(Cow::Borrowed(value_part));
		};

		let mut value = String::from(value_part);

		loop {
			value.push(';');

			let Some((value_part, rest)) = next_part.split_once(';') else {
				self.s = None;
				value.push_str(next_part);
				return Some(Cow::Owned(value));
			};

			value.push_str(value_part);

			let Some(new_part) = rest.strip_prefix(';') else {
				self.s = Some(rest);
				return Some(Cow::Owned(value));
			};
			next_part = new_part;
		}
	}
}

impl<'a, T: Taglike<'a>, F: FromOsm<'a, T>> FromOsm<'a, T> for BTreeMap<&'a str, F> {
	fn from_osm(tag: T) -> Option<Self> {
		tag.iter_subtags()
			.map(|(key, val)| F::from_osm(val).map(|val| (key, val)))
			.collect()
	}
}

macro_rules! simple_enum {
    ($name:ident $(($($derive:ident),+))? $(#[$($attr:tt)*])* { $($content:tt)+ }) => {
        $(#[$($attr)*])*
        #[derive(Debug, Clone, Copy, PartialEq, Eq, PartialOrd, Ord, Hash, $crate::FromOsmStr, $crate::ToOsmStr, $($($derive),+)?)]
        #[cfg_attr(feature = "serde", derive(serde::Serialize, serde::Deserialize))]
        #[cfg_attr(feature = "wasm-bindgen", wasm_bindgen::prelude::wasm_bindgen)]
        pub enum $name {
            $($content)+
        }
    };
}
pub(crate) use simple_enum;

#[cfg(test)]
mod tests {
	use crate::{SeperatedValueIter, ToOsmStr};

	#[test]
	fn from_seperated_value() {
		assert_eq!(SeperatedValueIter::new("").collect::<Vec<_>>(), vec![""]);
		assert_eq!(
			SeperatedValueIter::new(";abc;def;").collect::<Vec<_>>(),
			vec!["", "abc", "def", ""]
		);
		assert_eq!(
			SeperatedValueIter::new(";;abc;def;;").collect::<Vec<_>>(),
			vec![";abc", "def;"]
		);
		assert_eq!(
			SeperatedValueIter::new("abc;;def;;ghi;jkl").collect::<Vec<_>>(),
			vec!["abc;def;ghi", "jkl"]
		);
		assert_eq!(
			SeperatedValueIter::new("abc;;;def").collect::<Vec<_>>(),
			vec!["abc;", "def"]
		);
		assert_eq!(
			SeperatedValueIter::new("abc;;;;;;def").collect::<Vec<_>>(),
			vec!["abc;;;def"]
		);
	}

	#[test]
	fn to_seperated_value() {
		assert_eq!(vec![].to_osm_str(), Some("".into()));
		assert_eq!(vec![String::new()].to_osm_str(), Some("".into()));
		assert_eq!(
			vec![String::from("abc"), String::from("def")].to_osm_str(),
			Some("abc;def".into())
		);
		assert_eq!(
			vec![String::from(";abc"), String::from("def;")].to_osm_str(),
			Some(";;abc;def;;".into())
		);
		assert_eq!(
			vec![String::from("abc;def;ghi"), String::from("jkl;mno")].to_osm_str(),
			Some("abc;;def;;ghi;jkl;;mno".into())
		);
	}
}
