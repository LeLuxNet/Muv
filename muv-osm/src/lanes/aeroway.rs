use crate::{
	AccessLevel, FromOsmStr, Lifecycle, TMode, TModes, Tag,
	lanes::{
		Lane, Lanes,
		travel::{TravelLane, TravelLaneDirection},
	},
};

#[derive(Clone, FromOsmStr)]
enum AerowayClassification {
	Runway,
	Taxiway,
	Stopway,
}

impl AerowayClassification {
	const fn access_level(&self) -> AccessLevel {
		match self {
			Self::Taxiway | Self::Runway => AccessLevel::Yes,
			Self::Stopway => AccessLevel::Escape,
		}
	}
}

/// Parse tags of an aeroway into lanes
///
/// Returns [`None`] in case the tags aren't an aeroway
#[must_use]
pub fn aeroway_lanes(tags: &Tag) -> Option<Lanes> {
	let (lifecycle, class, _) = Lifecycle::from_osm::<AerowayClassification>(tags, "aeroway")?;

	let access = TModes::from([
		(TMode::All, AccessLevel::No.into()),
		(TMode::Aeroplane, class?.access_level().into()),
	]);

	let travel_lane = TravelLaneDirection::with_access(access);

	let mut lane: Lane = TravelLane::both(travel_lane).into();
	lane = lane.add_parse(tags);

	Some(Lanes::new_single(lane, lifecycle))
}
