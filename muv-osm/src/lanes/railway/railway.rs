use crate::{
	AccessLevel, FromOsm, FromOsmStr, Lifecycle, TMode, TModes, Tag, get_tag,
	lanes::{
		Lane, Lanes,
		travel::{TravelLane, TravelLaneDirection},
	},
	units::{Distance, Quantity},
};

#[derive(Clone, FromOsmStr)]
enum RailwayClassification {
	Abandoned,
	Construction,
	Disused,
	Funicular,
	LightRail,
	Miniature,
	Monorail,
	NarrowGauge,
	Rail,
	Subway,
	Tram,
}

impl RailwayClassification {
	const fn transportation_mode(&self) -> Option<TMode> {
		Some(match self {
			Self::Rail => TMode::Train,
			Self::Monorail => TMode::Monorail,
			Self::Funicular => TMode::Funicular,
			Self::LightRail => TMode::LightRail,
			Self::Subway => TMode::Subway,
			Self::Tram => TMode::Tram,
			_ => return None,
		})
	}
}

/// Parse tags of a railway into lanes
///
/// Returns [`None`] in case the tags aren't a railway
#[must_use]
pub fn railway_lanes(tags: &Tag) -> Option<Lanes> {
	let (lifecycle, class, railway) =
		Lifecycle::from_osm::<RailwayClassification>(tags, "railway")?;

	let mode = class?.transportation_mode()?;

	let mut access = TModes::all(AccessLevel::No.into());
	access.set(mode, AccessLevel::Yes.into());

	let mut travel_lane = TravelLaneDirection::with_access(access);

	if let Some(maxspeed) = get_tag!(tags, maxspeed)
		.and_then(Quantity::from_osm_str)
		.map(Into::into)
	{
		travel_lane.maxspeed.set(TMode::All, maxspeed);
	}

	if let Some(tags) = get_tag!(tags, loading_gauge:) {
		if let Some(maxheight) = get_tag!(tags, maxheight)
			.and_then(|s| Quantity::from_osm_str_with_unit(s, Distance::Millimetre))
		{
			travel_lane.maxheight.set(TMode::All, maxheight.into());
		}
		if let Some(maxwidth) = get_tag!(tags, maxwidth)
			.and_then(|s| Quantity::from_osm_str_with_unit(s, Distance::Millimetre))
		{
			travel_lane.maxwidth.set(TMode::All, maxwidth.into());
		}
	}

	let mut travel_lane = TravelLane::both(travel_lane);
	match get_tag!(railway, preferred_direction) {
		Some("forward") => {
			travel_lane
				.forward
				.access
				.set(mode, AccessLevel::Designated.into());
		}
		Some("backward") => {
			travel_lane
				.backward
				.access
				.set(mode, AccessLevel::Designated.into());
		}
		_ => {}
	}

	match get_tag!(railway, bidirectional) {
		Some("signals") => {
			travel_lane
				.backward
				.access
				.set(mode, AccessLevel::Permit.into());
		}
		Some("possible") => {
			travel_lane
				.backward
				.access
				.set(mode, AccessLevel::No.into());
		}
		_ => {}
	}

	let mut lane: Lane = travel_lane.into();

	if let Some(track_widths) = get_tag!(tags, gauge).and_then(|s| {
		s.split(';')
			.map(|s| Quantity::from_osm_str_with_unit(s, Distance::Millimetre))
			.collect()
	}) {
		lane.track_widths = track_widths;
	}

	lane.electrifications = FromOsm::from_osm(tags).unwrap_or_default();

	Some(Lanes::new_single(lane, lifecycle))
}

#[cfg(test)]
mod tests {
	use crate::{
		TMode,
		lanes::{Electrification, ElectrificationSystem, railway_lanes, travel::TravelLane},
		new_tag, quantity,
		units::{Frequency, Speed, Voltage},
	};

	#[test]
	fn railway() {
		let tags = new_tag! {
			railway = "rail",
			maxspeed = "120",
			loading_gauge:height = "5600",
			loading_gauge:width = "4100",
			electrified = "contact_line",
			voltage = "25000",
			frequency = "50",
		};

		let lanes = railway_lanes(&tags).unwrap();
		let lane = &lanes.lanes[0];
		let travel_lane: &TravelLane = (&lane.variant).try_into().unwrap();

		assert_eq!(lane.electrifications, vec![Electrification {
			system: ElectrificationSystem::OverheadLine,
			voltage: Some(quantity!(25 Voltage::Kilovolts)),
			frequency: Some(quantity!(50 Frequency::Hertz)),
		}]);

		assert_eq!(
			travel_lane.forward.maxspeed.get(TMode::Train),
			Some(&quantity!(120 Speed::KilometresPerHour.into()).into())
		);
	}
}
