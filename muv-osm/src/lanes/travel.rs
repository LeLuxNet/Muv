use std::{collections::btree_map::Entry, mem::replace};

use crate::{
	AccessLevel, Conditional, FromOsm, FromOsmStr, TMode, TModes, Taglike, ToOsmStr,
	conditional::{MatchSituation, Restriction},
	get_tag,
	lanes::{Lane, LaneVariant, MAX_DIRECTION_LANES, MAX_LANES, Side, highway::RepeatN},
	simple_bitflags, simple_enum,
	units::{Distance, Quantity, WalkSpeed, Weight},
};

#[cfg(feature = "lanes-default-speeds")]
use crate::lanes::highway::speed_default;

#[cfg(feature = "serde")]
use serde::{Deserialize, Serialize};

/// Lane used for traveling
///
/// This contrasts with [`ParkingLane`](`super::parking::ParkingLane`) which is used for parking vehicles.
/// A `TravelLane` consists of a forward and backward direction.
#[derive(Debug, Clone, PartialEq)]
#[cfg_attr(feature = "serde", derive(Serialize, Deserialize))]
pub struct TravelLane {
	pub forward: TravelLaneDirection,
	pub backward: TravelLaneDirection,
}

impl TryFrom<LaneVariant> for TravelLane {
	type Error = ();
	fn try_from(value: LaneVariant) -> Result<Self, Self::Error> {
		match value {
			LaneVariant::Travel(t) => Ok(*t),
			LaneVariant::Parking(_) => Err(()),
		}
	}
}
impl<'a> TryFrom<&'a mut LaneVariant> for &'a mut TravelLane {
	type Error = ();
	fn try_from(value: &'a mut LaneVariant) -> Result<Self, Self::Error> {
		match value {
			LaneVariant::Travel(t) => Ok(t),
			LaneVariant::Parking(_) => Err(()),
		}
	}
}
impl<'a> TryFrom<&'a LaneVariant> for &'a TravelLane {
	type Error = ();
	fn try_from(value: &'a LaneVariant) -> Result<Self, Self::Error> {
		match value {
			LaneVariant::Travel(t) => Ok(t),
			LaneVariant::Parking(_) => Err(()),
		}
	}
}

impl From<TravelLane> for LaneVariant {
	fn from(value: TravelLane) -> Self {
		Self::Travel(Box::new(value))
	}
}

impl TravelLane {
	/// Construct a travel lane with the same [`TravelLaneDirection`] being used in both
	/// the [forward](`TravelLane::forward`) and [backward](`TravelLane::backward`) direction.
	#[must_use]
	pub fn both(lane: TravelLaneDirection) -> Self {
		Self {
			forward: lane.clone(),
			backward: lane,
		}
	}
}

simple_bitflags!(Turn: u16
#[warn(missing_docs)]
/// Bitflag of turn indications often displayed by white arrows on the pavement
///
/// [OSM Wiki](https://wiki.openstreetmap.org/wiki/Key:turn)
{
	/// Sharp turn to the left
	///
	/// ```text
	///  /|
	/// v |
	/// ```
	SharpLeft(sharp_left)        = 1 << 0,
	/// Turn to the left
	///
	/// ```text
	/// <-\
	///   |
	/// ```
	Left(left)                   = 1 << 1,
	/// Slight turn to the left
	///
	/// ```text
	/// ^
	///  \
	///   |
	/// ```
	SlightLeft(sharp_left)       = 1 << 2,
	/// Merge to the left
	///
	/// ```text
	/// ^
	/// |\
	/// ||
	/// ```
	MergeToLeft(merge_to_left)   = 1 << 3,

	/// Forward/through
	///
	/// ```text
	/// ^
	/// |
	/// ```
	Through(through)             = 1 << 4,

	/// Merge to the right
	///
	/// ```text
	///  ^
	/// /|
	/// ||
	/// ```
	MergeToRight(merge_to_right) = 1 << 5,
	/// Slight turn to the right
	///
	/// ```text
	///   ^
	///  /
	/// |
	/// ```
	SlightRight(slight_right)    = 1 << 6,
	/// Turn to the right
	///
	/// ```text
	/// /->
	/// |
	/// ```
	Right(right)                 = 1 << 7,
	/// Sharp turn to the right
	///
	/// ```text
	/// |\
	/// | v
	/// ```
	SharpRight(sharp_right)      = 1 << 8,

	/// U-turn
	///
	/// ```text
	///  _
	/// / \
	/// v |
	/// ```
	Reverse(reverse)             = 1 << 9,
});

#[warn(missing_docs)]
impl Turn {
	/// Sharp turn ([`SharpLeft`](`Turn::SharpLeft`) or [`SharpRight`](`Turn::SharpRight`)) to the side passed
	#[must_use]
	pub const fn sharp(side: Side) -> Self {
		match side {
			Side::Left => Self::SharpLeft,
			Side::Right => Self::SharpRight,
		}
	}

	/// Turn ([`Left`](`Turn::Left`) or [`Right`](`Turn::Right`)) to the side passed
	#[must_use]
	pub const fn normal(side: Side) -> Self {
		match side {
			Side::Left => Self::Left,
			Side::Right => Self::Right,
		}
	}

	/// Slight turn ([`SlightLeft`](`Turn::SlightLeft`) or [`SlightRight`](`Turn::SlightRight`)) to the side passed
	#[must_use]
	pub const fn slight(side: Side) -> Self {
		match side {
			Side::Left => Self::SlightLeft,
			Side::Right => Self::SlightRight,
		}
	}

	/// Merge ([`MergeToLeft`](`Turn::MergeToLeft`) or [`MergeToRight`](`Turn::MergeToRight`)) to the side passed
	#[must_use]
	pub const fn merge_to(side: Side) -> Self {
		match side {
			Side::Left => Self::MergeToLeft,
			Side::Right => Self::MergeToRight,
		}
	}
}

simple_enum!(PriorityRoad {
	Yes,
	Designated,
	#[osm_str("yes_unposted")]
	Unposted,
	#[osm_str("no" | "end")]
	No,
});

simple_enum!(Change {
	#[osm_str("yes")]
	Both,
	#[osm_str("not_left" | "only_right")]
	Right,
	#[osm_str("not_right" | "only_left")]
	Left,
	No,
});

simple_enum!(Overtaking {
	#[osm_str("yes" | "both")]
	Yes,
	Caution,
	#[osm_str("no" | "no_overtaking_of_multitrack_vehicles")]
	No,
});

impl From<bool> for Overtaking {
	fn from(value: bool) -> Self {
		if value { Self::Yes } else { Self::No }
	}
}

impl From<Overtaking> for bool {
	fn from(value: Overtaking) -> Self {
		value != Overtaking::No
	}
}

#[derive(Debug, Clone, PartialEq)]
#[cfg_attr(feature = "serde", derive(Serialize, Deserialize))]
pub struct TravelLaneDirection {
	#[cfg_attr(feature = "serde", serde(skip_serializing_if = "TModes::is_empty"))]
	pub access: TModes<Conditional<AccessLevel>>,

	/// Indicated turn directions often displayed by white arrows on the pavement
	///
	/// [OSM Wiki](https://wiki.openstreetmap.org/wiki/Key:turn)
	#[cfg_attr(feature = "serde", serde(skip_serializing_if = "TModes::is_empty"))]
	pub turn: TModes<Conditional<Turn>>,
	#[cfg_attr(feature = "serde", serde(skip_serializing_if = "TModes::is_empty"))]
	pub change: TModes<Conditional<Change>>,
	/// Overtaking restrictions
	///
	/// It consists of two `TModes<Conditional<T>>` components.
	/// The outer component represents the vehicle *overtaking*.
	/// The inner component represents the vehicle *being overtaken*.
	#[cfg_attr(feature = "serde", serde(skip_serializing_if = "TModes::is_empty"))]
	pub overtaking: TModes<Conditional<TModes<Conditional<Overtaking>>>>,

	#[cfg_attr(feature = "serde", serde(skip_serializing_if = "TModes::is_empty"))]
	pub priority_road: TModes<Conditional<PriorityRoad>>,

	#[cfg_attr(feature = "serde", serde(skip_serializing_if = "TModes::is_empty"))]
	pub minspeed: TModes<Conditional<Quantity<WalkSpeed>>>,
	/// Highest allowed speed according to signs and local laws
	///
	/// [OSM Wiki](https://wiki.openstreetmap.org/wiki/Key:maxspeed)
	#[cfg_attr(feature = "serde", serde(skip_serializing_if = "TModes::is_empty"))]
	pub maxspeed: TModes<Conditional<Quantity<WalkSpeed>>>,

	#[cfg_attr(feature = "serde", serde(skip_serializing_if = "TModes::is_empty"))]
	pub maxweight: TModes<Conditional<Quantity<Weight>>>,
	#[cfg_attr(feature = "serde", serde(skip_serializing_if = "TModes::is_empty"))]
	pub maxaxleload: TModes<Conditional<Quantity<Weight>>>,
	#[cfg_attr(feature = "serde", serde(skip_serializing_if = "TModes::is_empty"))]
	pub maxlength: TModes<Conditional<Quantity<Distance>>>,
	#[cfg_attr(feature = "serde", serde(skip_serializing_if = "TModes::is_empty"))]
	pub maxwidth: TModes<Conditional<Quantity<Distance>>>,
	#[cfg_attr(feature = "serde", serde(skip_serializing_if = "TModes::is_empty"))]
	pub maxheight: TModes<Conditional<Quantity<Distance>>>,
	#[cfg_attr(feature = "serde", serde(skip_serializing_if = "TModes::is_empty"))]
	pub maxdraught: TModes<Conditional<Quantity<Distance>>>,
}

impl TravelLaneDirection {
	pub(crate) fn default_empty() -> Self {
		Self {
			access: TModes::default(),

			turn: TModes::default(),
			change: TModes::default(),
			overtaking: TModes::default(),

			priority_road: TModes::default(),

			minspeed: TModes::default(),
			maxspeed: TModes::default(),

			maxweight: TModes::default(),
			maxaxleload: TModes::default(),
			maxlength: TModes::default(),
			maxwidth: TModes::default(),
			maxheight: TModes::default(),
			maxdraught: TModes::default(),
		}
	}

	#[must_use]
	pub fn with_access(access: TModes<Conditional<AccessLevel>>) -> Self {
		Self {
			access,

			turn: TModes::default(),
			change: TModes::default(),
			overtaking: TModes::default(),

			priority_road: TModes::default(),

			minspeed: TModes::default(),
			maxspeed: TModes::default(),

			maxweight: TModes::default(),
			maxaxleload: TModes::default(),
			maxlength: TModes::default(),
			maxwidth: TModes::default(),
			maxheight: TModes::default(),
			maxdraught: TModes::default(),
		}
	}

	#[must_use]
	pub fn matches(&self, situation: &MatchSituation) -> bool {
		matches_metric(&self.maxweight, situation, &situation.vehicle.weight)
			&& matches_metric(&self.maxaxleload, situation, &situation.vehicle.axleload)
			&& matches_metric(&self.maxlength, situation, &situation.vehicle.length)
			&& matches_metric(&self.maxwidth, situation, &situation.vehicle.width)
			&& matches_metric(&self.maxheight, situation, &situation.vehicle.height)
			&& matches_metric(&self.maxdraught, situation, &situation.vehicle.draught)
	}
}

fn matches_metric<T: PartialOrd>(
	data: &TModes<Conditional<T>>,
	situation: &MatchSituation,
	value: &Option<T>,
) -> bool {
	if let (Some(border_value), Some(value)) = (
		data.get(situation.vehicle.mode)
			.and_then(|conditional| conditional.evaluate(situation)),
		value,
	) {
		border_value >= value
	} else {
		true
	}
}

#[derive(Debug, Clone)]
pub(crate) enum ParseDirectionLanes {
	All(Box<Lane>, u8, bool),
	Individual(Vec<Lane>),
}

impl ParseDirectionLanes {
	pub(crate) fn mut_each(&mut self, mut f: impl FnMut(&mut Lane)) {
		match self {
			Self::All(lane, _, dirty) => {
				f(lane);
				*dirty = true;
			}
			Self::Individual(lanes) => lanes.iter_mut().for_each(f),
		};
	}

	pub(crate) fn len(&self) -> u8 {
		match self {
			Self::All(_, count, _) => *count,
			Self::Individual(lanes) => lanes.len().try_into().unwrap(),
		}
	}

	pub(crate) fn extend_vec(self, vec: &mut Vec<Lane>) -> u8 {
		let limit = usize::from(MAX_LANES)
			.saturating_sub(vec.len())
			.try_into()
			.unwrap();
		match self {
			Self::All(lane, count, _) => {
				let count = count.min(limit);
				vec.extend(RepeatN {
					item: Some(*lane),
					count: count.into(),
				});
				count
			}
			Self::Individual(lanes) => {
				let len = lanes.len();
				if len <= limit.into() {
					vec.extend(lanes);
					len.try_into().unwrap()
				} else {
					vec.extend(lanes.into_iter().take(limit.into()));
					limit
				}
			}
		}
	}
}

const fn allowed_on_platform(mode: TMode) -> bool {
	!matches!(
		mode,
		// psv children
		TMode::Bus | TMode::Taxi | TMode::Minibus | TMode::ShareTaxi | TMode::SchoolBus | TMode::TruckBus |
        // rail based children
        TMode::Train | TMode::Monorail | TMode::Funicular | TMode::LightRail | TMode::Subway | TMode::Tram
	)
}

pub(crate) struct AllParser<Tl> {
	pub(crate) dirty: bool,
	pub(crate) direction: fn(Tl) -> Option<Tl>,
}

impl<'a, Tl: Taglike<'a>> AllParser<Tl> {
	#[inline]
	fn mode<T>(
		&mut self,
		tags: Tl,
		value: &mut TModes<Conditional<T>>,
		mode: TMode,
		parse: fn(&str) -> Option<T>,
	) {
		if let Some(val) =
			(self.direction)(tags).and_then(|tags| Conditional::parse_with(tags, parse))
		{
			value.set(mode, val);
			self.dirty = true;
		}
	}

	#[inline]
	pub(crate) fn access(
		&mut self,
		tags_access: Option<Tl>,
		tags: Tl,
		platform: bool,
		value: &mut TModes<Conditional<AccessLevel>>,
	) {
		if let Some(access) = tags_access
			.and_then(|tags| Conditional::<AccessLevel>::from_osm((self.direction)(tags)?))
		{
			for (_, access_default) in value.iter_mut() {
				*access_default = access_default.merge(&access, |default, global| {
					if default == &AccessLevel::No || global == &AccessLevel::Yes {
						*default
					} else {
						*global
					}
				});
			}
		}

		for (mode, tags) in TMode::iter_tags(tags) {
			if !platform || allowed_on_platform(mode) {
				self.mode(tags, value, mode, AccessLevel::from_osm_str);
			}
		}
	}

	#[inline]
	pub(crate) fn with_parse<T>(
		&mut self,
		tags: Option<Tl>,
		value: &mut TModes<Conditional<T>>,
		parse: fn(&str) -> Option<T>,
	) {
		if let Some(tags) = tags {
			for (mode, tags) in TMode::iter_tags_with_all(tags) {
				self.mode(tags, value, mode, parse);
			}
		}
	}

	#[inline]
	pub(crate) fn base<T: FromOsmStr>(
		&mut self,
		tags: Option<Tl>,
		value: &mut TModes<Conditional<T>>,
	) {
		self.with_parse(tags, value, T::from_osm_str);
	}

	pub(crate) fn maxspeed(&mut self, tags: Option<Tl>, travel: &mut TravelLaneDirection) {
		if let Some((_, legal_maxspeed)) = tags.and_then(|tags| {
			let (country, class) = (self.direction)(tags)?.value()?.split_once(':')?;
			speed_default::default(country, class)
		}) {
			self.maxspeed_with_data(legal_maxspeed, travel);
		}
	}

	pub(crate) fn maxspeed_with_data(
		&mut self,
		legal: TModes<Conditional<Quantity<WalkSpeed>>>,
		travel: &mut TravelLaneDirection,
	) {
		for (mode, mut legal_value) in legal {
			let legal_base = legal_value.base().copied();

			let parent_base = mode
				.iter_hierarchy()
				.skip(1)
				.find_map(|mode| travel.maxspeed.get_raw(mode)?.base())
				.copied();

			let mut self_entry = travel.maxspeed.entry(mode);
			let self_value = if let Entry::Occupied(self_value) = &mut self_entry {
				Some(self_value.get_mut())
			} else {
				None
			};
			let self_base = self_value.as_ref().and_then(|v| v.base().copied());

			let has_new_base = self_base.is_none()
				&& legal_base.as_ref().is_some_and(|legal_base| {
					parent_base
						.as_ref()
						.map_or(true, |parent_base| parent_base > legal_base)
				});

			if let Some(self_value) = self_value {
				let has_self_conditionals = self_value.len() != usize::from(self_base.is_some());
				if has_self_conditionals {
					if has_new_base {
						self_value
							.0
							.insert(0, legal_value.into_iter().next().unwrap());
						self.dirty = true;
					}
					continue;
				}
			}

			let new_base = if has_new_base {
				legal_base.is_some()
			} else {
				self_base.is_some()
			};
			let base = if has_new_base {
				legal_base
			} else {
				self_base.or(parent_base)
			};

			if let Some(base) = base {
				legal_value.0.retain(|legal_r| {
					if legal_r.conditions.is_empty() {
						new_base
					} else {
						base > legal_r.value
					}
				});

				if let Some(self_base) = self_base {
					if legal_base.is_some() {
						legal_value.0.get_mut(0).unwrap().value = self_base;
					} else {
						legal_value.0.insert(0, Restriction {
							value: self_base,
							conditions: Vec::new(),
						});
					}
				}
			}

			if legal_value.is_empty() {
				if let Entry::Occupied(e) = self_entry {
					self.dirty = true;
					e.remove();
				}
			} else {
				match self_entry {
					Entry::Vacant(e) => {
						e.insert(legal_value);
					}
					Entry::Occupied(mut e) => {
						e.insert(legal_value);
					}
				}
				self.dirty = true;
			}
		}
	}
}

pub(crate) struct IndividualParser<Tl> {
	pub(crate) lanes: ParseDirectionLanes,
	pub(crate) lanes_reversed: bool,
	pub(crate) direction: fn(Tl) -> Option<Tl>,
}

impl<'a, Tl: Taglike<'a>> IndividualParser<Tl> {
	fn with_lanes(&mut self, count: Option<usize>, modify: impl FnOnce(&mut [Lane], bool)) {
		let count = count.map(|count| count.min(MAX_DIRECTION_LANES.into()));

		// Set self.lanes to an actual value again before function exit (excluding panics).
		let lanes = replace(&mut self.lanes, ParseDirectionLanes::Individual(Vec::new()));

		let mut lanes_list = match lanes {
			ParseDirectionLanes::All(lane, base_count, _) => {
				let count = count.unwrap_or_else(|| base_count.into());
				vec![*lane; count]
			}
			ParseDirectionLanes::Individual(lanes_list) => {
				if Some(lanes_list.len()) == count || count.is_none() {
					lanes_list
				} else {
					self.lanes = ParseDirectionLanes::Individual(lanes_list);
					return;
				}
			}
		};

		modify(&mut lanes_list, self.lanes_reversed);

		self.lanes = ParseDirectionLanes::Individual(lanes_list);
	}

	pub(crate) fn mask_outer(&mut self, tags: Tl, mode: TMode, key: &str) {
		if let Some(count) = get_tag!(tags, lanes:)
			.and_then(|tags| (self.direction)(tags.get(key)?)?.value()?.parse().ok())
		{
			self.with_lanes(None, |lanes, lanes_reversed| {
				if lanes_reversed {
					for lane in lanes.iter_mut().take(count) {
						let travel = lane.unwrap_travel();
						if travel.access.get_raw(mode).is_none() {
							travel.access = TModes::from([
								(TMode::All, AccessLevel::No.into()),
								(mode, AccessLevel::Designated.into()),
							]);
						}
					}
				} else {
					for lane in lanes.iter_mut().rev().take(count) {
						let travel = lane.unwrap_travel();
						if travel.access.get_raw(mode).is_none() {
							travel.access = TModes::from([
								(TMode::All, AccessLevel::No.into()),
								(mode, AccessLevel::Designated.into()),
							]);
						}
					}
				}
			});
		}
	}

	#[inline]
	fn direction_conditional<T: 'a>(
		&mut self,
		tags: Option<Tl>,
		parse: fn(&str) -> Option<T>,
		get_mut: impl Fn(&mut Lane) -> &mut Conditional<T>,
	) {
		let Some(conditional_lanes) = tags.and_then(|tags| {
			Conditional::parse_with((self.direction)(get_tag!(tags, lanes:)?)?, Some)
		}) else {
			return;
		};

		let Some(first_lane_str) = conditional_lanes.first().map(|r| r.value) else {
			return;
		};
		let lanes_count = first_lane_str.split('|').count();

		self.with_lanes(Some(lanes_count), |lanes_list, lanes_reversed| {
			for restriction in conditional_lanes {
				if lanes_reversed {
					for (value, lane) in restriction.value.rsplit('|').zip(lanes_list.iter_mut()) {
						if let Some(value) = parse(value) {
							if restriction.conditions.is_empty() {
								get_mut(lane).0.clear();
							}
							get_mut(lane).0.push(Restriction {
								value,
								conditions: restriction.conditions.clone(),
							});
						}
					}
				} else {
					for (value, lane) in restriction.value.split('|').zip(lanes_list.iter_mut()) {
						if let Some(value) = parse(value) {
							if restriction.conditions.is_empty() {
								get_mut(lane).0.clear();
							}
							get_mut(lane).0.push(Restriction {
								value,
								conditions: restriction.conditions.clone(),
							});
						}
					}
				}
			}
		});
	}

	#[inline]
	pub(crate) fn direction<T: 'a>(
		&mut self,
		tags: Option<Tl>,
		parse: fn(&str) -> Option<T>,
		set: impl Fn(&mut Lane, T),
	) {
		let Some(lane_str) =
			tags.and_then(|tags| (self.direction)(get_tag!(tags, lanes:)?)?.value())
		else {
			return;
		};

		let lanes_count = lane_str.split('|').count();

		self.with_lanes(Some(lanes_count), |lanes_list, lanes_reversed| {
			if lanes_reversed {
				for (lane_value, lane) in lane_str.rsplit('|').zip(lanes_list.iter_mut()) {
					if let Some(lane_value) = parse(lane_value) {
						set(lane, lane_value);
					}
				}
			} else {
				for (lane_value, lane) in lane_str.split('|').zip(lanes_list.iter_mut()) {
					if let Some(lane_value) = parse(lane_value) {
						set(lane, lane_value);
					}
				}
			}
		});
	}

	#[inline]
	fn mode<T: 'a>(
		&mut self,
		mode: TMode,
		tags: Tl,
		parse: fn(&str) -> Option<T>,
		get_value: fn(&mut Lane) -> &mut TModes<Conditional<T>>,
	) {
		self.direction_conditional(Some(tags), parse, |lane| {
			get_value(lane).entry(mode).or_default()
		});
	}

	#[inline]
	pub(crate) fn access(
		&mut self,
		tags_access: Option<Tl>,
		tags: Tl,
		platform: bool,
		get_value: fn(&mut Lane) -> &mut TModes<Conditional<AccessLevel>>,
	) {
		if let Some(tags_access) = tags_access {
			self.mode(
				TMode::LandBased,
				tags_access,
				AccessLevel::from_osm_str,
				get_value,
			);
		}

		for (mode, tags) in
			TMode::iter_tags(tags).filter(|(mode, _)| !platform || allowed_on_platform(*mode))
		{
			self.mode(mode, tags, AccessLevel::from_osm_str, get_value);
		}
	}

	#[inline]
	pub(crate) fn with_parse<T: 'a>(
		&mut self,
		tags: Option<Tl>,
		get_value: fn(&mut Lane) -> &mut TModes<Conditional<T>>,
		parse: fn(&str) -> Option<T>,
	) {
		let Some(tags) = tags else {
			return;
		};

		for (mode, tags) in TMode::iter_tags_with_all(tags) {
			self.mode(mode, tags, parse, get_value);
		}
	}

	#[inline]
	pub(crate) fn base<T: FromOsmStr + 'a>(
		&mut self,
		tags: Option<Tl>,
		get_value: fn(&mut Lane) -> &mut TModes<Conditional<T>>,
	) {
		self.with_parse(tags, get_value, T::from_osm_str);
	}
}

#[cfg(test)]
mod tests {
	use crate::{
		AccessLevel, TMode,
		conditional::Restriction,
		lanes::{lanes, travel::TravelLane},
		new_tag, quantity,
		units::{Number, Quantity, Speed},
	};

	#[cfg(feature = "serde")]
	use crate::lanes::travel::Turn;

	#[test]
	fn lanes_conditional() {
		let tags = new_tag! {
			highway = "secondary",
			bus:lanes:forward = "no|yes",
			bus:lanes:backward = "no|yes",
			bus:lanes:forward:conditional = "(yes|designated) @ (Mo-Fr 06:00-09:00)",
			bus:lanes:backward:conditional = "(yes|designated) @ (Mo-Fr 14:00-18:00)",
		};

		let lanes = lanes(&tags, &["DE"]).unwrap();

		assert_eq!(lanes.lanes.len(), 4);

		let lane0: &TravelLane = (&lanes.lanes[0].variant).try_into().unwrap();
		let lane0bus = lane0.backward.access.get(TMode::Bus).unwrap();
		assert_eq!(lane0bus.len(), 2);
		assert_eq!(lane0bus[0].value, AccessLevel::Yes);
		assert_eq!(lane0bus[0].conditions.len(), 0);
		assert_eq!(lane0bus[1].value, AccessLevel::Designated);
	}

	#[test]
	fn maxspeed_type_above() {
		let tags = new_tag! {
			highway = "primary",
			maxspeed:type = "FR:rural",
			maxspeed = "100",
		};

		let lanes = lanes(&tags, &["FR"]).unwrap();

		let lane0: &TravelLane = (&lanes.lanes[0].variant).try_into().unwrap();

		assert_eq!(
			lane0
				.backward
				.maxspeed
				.get(TMode::All)
				.and_then(|s| s.first()),
			Some(&Restriction {
				value: quantity!(100 Speed::KilometresPerHour.into()),
				conditions: Vec::new(),
			})
		);
		assert_eq!(
			lane0
				.backward
				.maxspeed
				.get(TMode::Hgv)
				.and_then(|s| s.first()),
			Some(&Restriction {
				value: Quantity::new(Number::new(90.0), Speed::KilometresPerHour.into()),
				conditions: Vec::new(),
			})
		);
	}

	#[test]
	fn maxspeed_type_below() {
		let tags = new_tag! {
			highway = "motorway",
			maxspeed = "10",
		};

		let lanes = lanes(&tags, &["DE"]).unwrap();

		let lane0: &TravelLane = (&lanes.lanes[0].variant).try_into().unwrap();

		assert_eq!(
			lane0.forward.maxspeed.get(TMode::All),
			Some(&quantity!(10 Speed::KilometresPerHour.into()).into())
		);
		assert_eq!(lane0.forward.maxspeed.get_raw(TMode::Bus), None);
		assert_eq!(lane0.forward.maxspeed.get_raw(TMode::Hgv), None);
	}

	#[test]
	#[cfg(feature = "serde")]
	fn serde() {
		use serde_test::{Configure, Token, assert_tokens};

		let turn = Turn::SharpLeft | Turn::Left | Turn::SlightRight;

		assert_tokens(&turn.readable(), &[
			Token::Seq { len: None },
			Token::Str("SharpLeft"),
			Token::Str("Left"),
			Token::Str("SlightRight"),
			Token::SeqEnd,
		]);
		assert_tokens(&turn.compact(), &[Token::U16(0b0100_0011)]);
	}
}
