use std::iter::once;

use crate::{
	AccessLevel, Conditional, FromOsm, FromOsmStr, TMode, TModes, Taglike, get_tag,
	lanes::{LaneVariant, Side, highway::HighwayClassification, travel::ParseDirectionLanes},
};

#[derive(Debug, PartialEq, Clone, Copy, FromOsmStr)]
pub(crate) enum OnewayDirection {
	#[osm_str("yes" | "true" | "1")]
	Forward,
	#[osm_str("reverse" | "-1")]
	Backward,
	#[osm_str("no" | "false" | "0")]
	Both,
	#[osm_str()]
	BothWays,
	Alternating,
	Reversible,
}

impl OnewayDirection {
	#[inline]
	const fn in_direction(self, forward: bool) -> bool {
		match self {
			Self::Forward => forward,
			Self::Backward => !forward,
			Self::Both | Self::BothWays | Self::Alternating => true,
			Self::Reversible => false,
		}
	}

	#[inline]
	pub(crate) const fn in_direction_ever(self, forward: bool) -> bool {
		match self {
			Self::Forward => forward,
			Self::Backward => !forward,
			Self::Both | Self::BothWays | Self::Alternating | Self::Reversible => true,
		}
	}

	pub(crate) const fn flip(self) -> Self {
		match self {
			Self::Forward => Self::Backward,
			Self::Backward => Self::Forward,
			_ => self,
		}
	}
}

pub(crate) fn forward_on(oneway: Option<OnewayDirection>, driving_side: Side, side: Side) -> bool {
	if oneway == Some(OnewayDirection::Backward) {
		driving_side != side
	} else {
		driving_side == side
	}
}

fn implies_oneway<'a, T: Taglike<'a>>(class: Option<HighwayClassification>, tags: T) -> bool {
	class == Some(HighwayClassification::Motorway(false))
		|| matches!(get_tag!(tags, junction), Some("circular" | "roundabout"))
}

pub(crate) fn parse_oneway<'a, T: Taglike<'a>>(
	class: Option<HighwayClassification>,
	tags: T,
	oneway_tags: Option<T>,
	oneway_default: OnewayDirection,
) -> (
	TModes<Conditional<OnewayDirection>>,
	Option<OnewayDirection>,
) {
	let (oneway_vehicle, oneway_default) =
		if let Some(oneway_vehicle) = oneway_tags.and_then(Conditional::from_osm) {
			let oneway_default = oneway_vehicle
				.first()
				.filter(|r| r.conditions.is_empty())
				.map(|r| r.value);
			(oneway_vehicle, oneway_default)
		} else {
			let oneway_default = if implies_oneway(class, tags) {
				OnewayDirection::Forward
			} else {
				oneway_default
			};
			(oneway_default.into(), Some(oneway_default))
		};

	let oneway = if let Some(oneway_tags) = oneway_tags {
		once((TMode::Vehicle, oneway_vehicle))
			.chain(
				TMode::iter_tags(oneway_tags)
					.filter_map(|(mode, tags)| Some((mode, Conditional::from_osm(tags)?))),
			)
			.collect()
	} else {
		TModes::from([(TMode::Vehicle, oneway_vehicle)])
	};

	(oneway, oneway_default)
}

fn apply_oneway_mask<'a>(
	access: &'a Conditional<AccessLevel>,
	oneway: &'a Conditional<OnewayDirection>,
	forward: bool,
) -> Conditional<AccessLevel> {
	access.merge(oneway, |access, oneway| {
		if oneway.in_direction(forward) {
			*access
		} else {
			AccessLevel::No
		}
	})
}

pub(crate) fn add_oneway_direction(
	oneway: &TModes<Conditional<OnewayDirection>>,
	lanes: &mut ParseDirectionLanes,
	forward: bool,
) {
	// optimization to skip `oneway=no`
	if oneway.len() == 1 {
		if let Some(vehicle) = oneway.get_raw(TMode::Vehicle) {
			if vehicle.len() == 1
				&& vehicle[0].conditions.is_empty()
				&& vehicle[0].value == OnewayDirection::Both
			{
				return;
			}
		}
	}

	lanes.mut_each(|lane| {
		let travel = match &mut lane.variant {
			LaneVariant::Travel(t) if forward => &mut t.forward,
			LaneVariant::Travel(t) => &mut t.backward,
			LaneVariant::Parking(_) => unreachable!(),
		};
		travel.access = travel.access.zip(oneway, |access, oneway| {
			if let Some(oneway) = oneway {
				Some(apply_oneway_mask(access?, oneway, forward))
			} else {
				access.cloned()
			}
		});
	});
}

pub(crate) fn ever_allowed_oneway(oneway: &TModes<Conditional<OnewayDirection>>) -> (bool, bool) {
	let mut forward = false;
	let mut backward = false;
	for restriction in oneway.iter().flat_map(|(_, c)| c.iter()) {
		forward = forward || restriction.value.in_direction(true);
		backward = backward || restriction.value.in_direction(false);
	}
	(forward, backward)
}

#[cfg(test)]
mod tests {
	use crate::{
		AccessLevel, TMode,
		lanes::{lanes, travel::TravelLane},
		new_tag,
	};

	#[test]
	fn layered_oneway() {
		let tags = new_tag! {
			highway = "cycleway",
			oneway = "yes"
		};

		let lanes = lanes(&tags, &["DE"]).unwrap();
		assert_eq!(lanes.lanes.len(), 1);

		let travel_lane: &TravelLane = (&lanes.lanes[0].variant).try_into().unwrap();

		assert_eq!(
			travel_lane.forward.access.get(TMode::Bicycle),
			Some(&AccessLevel::Designated.into())
		);
	}

	#[test]
	fn roundabout() {
		let tags = new_tag! {
			highway = "residential",
			junction = "roundabout"
		};

		let lanes = lanes(&tags, &["DE"]).unwrap();
		assert_eq!(lanes.lanes.len(), 1);

		let travel_lane: &TravelLane = (&lanes.lanes[0].variant).try_into().unwrap();

		assert_eq!(
			travel_lane.forward.access.get(TMode::Motorcar),
			Some(&AccessLevel::Yes.into())
		);
	}
}
