use std::mem::replace;

use crate::{
	AccessLevel, Conditional, FromOsmStr, TMode, TModes, Taglike, get_tag,
	lanes::{
		Lane, LaneVariant, Side,
		highway::{
			HighwayClassification, OnewayDirection, Placement, access, add_oneway_direction,
			ever_allowed_oneway, forward_on, parse_direction,
		},
		parking::ParkingOrientation,
		travel::{ParseDirectionLanes, TravelLane, TravelLaneDirection, Turn},
	},
	units::{Distance, Number, Quantity},
};

pub(crate) struct BaseLanes<'a, T: Taglike<'a>, R: AsRef<str>> {
	pub(crate) regions: &'a [R],
	pub(crate) driving_side: Side,

	pub(crate) tags: T,
	pub(crate) lanes: &'a mut Vec<Lane>,
	pub(crate) is_sidepath: bool,
	pub(crate) return_center: bool,
	pub(crate) segregated: Option<bool>,

	pub(crate) class: Option<HighwayClassification>,
	pub(crate) oneway: &'a TModes<Conditional<OnewayDirection>>,
	pub(crate) oneway_vehicle: Option<OnewayDirection>,
	pub(crate) platform: bool,
	pub(crate) has_sidewalk: bool,
	pub(crate) busway_forward: u8,
	pub(crate) busway_backward: u8,
}

impl<'a, T: Taglike<'a>, R: AsRef<str>> BaseLanes<'a, T, R> {
	#[allow(clippy::too_many_lines)]
	pub(crate) fn add(mut self) -> (u8, u8) {
		let mut access = self.class.map_or_else(access::highway, |class| {
			let motorroad = get_tag!(self.tags, motorroad) == Some("yes");
			class.default_access(self.regions, self.tags, motorroad)
		});
		if self.has_sidewalk {
			access.set(TMode::Foot, AccessLevel::UseSidepath.into());
		}

		let mut lane = Lane::travel_wrap(TravelLaneDirection::with_access(access));
		lane.is_sidepath = self.is_sidepath
			|| (self.class == Some(HighwayClassification::Footway)
				&& get_tag!(self.tags, footway) == Some("sidewalk"))
			|| get_tag!(self.tags, is_sidepath) == Some("yes");

		#[cfg(feature = "lanes-default-speeds")]
		let mut maxspeed_default = self
			.class
			.and_then(|highway_class| highway_class.default_speed(self.regions))
			.map(|(minspeed, maxspeed)| {
				let travel = lane.unwrap_travel();
				travel.minspeed = minspeed;
				maxspeed
			});
		#[cfg(not(feature = "lanes-default-speeds"))]
		let mut maxspeed_default = None;

		let lanes_start = self.lanes.len();
		let segregated_index = if self.segregated == Some(true)
			&& forward_on(self.oneway_vehicle, self.driving_side, Side::Left)
		{
			let len = self.lanes.len();
			self.lanes
				.push(TravelLane::both(TravelLaneDirection::default_empty()).into());
			Some(len)
		} else {
			None
		};
		let segregated_len = u8::from(self.segregated == Some(true));

		let lanes_tag = get_tag!(self.tags, lanes:);
		let lanes_count: Option<u8> = lanes_tag.and_then(|tags| tags.value()?.parse().ok());

		let one_lanes = parse_direction(
			lane,
			lanes_count.unwrap_or_default(),
			self.tags,
			false,
			self.platform,
			|dir| Some(dir),
			|| maxspeed_default.clone(),
		);

		let placement_tags = get_tag!(self.tags, placement:);

		let lane = match one_lanes {
			ParseDirectionLanes::All(lane, _, _) => *lane,
			mut one_lanes @ ParseDirectionLanes::Individual(_) => {
				let (ever_forward, ever_backward) = ever_allowed_oneway(self.oneway);
				one_lanes.mut_each(|lane| lane.travel_wrap_build(ever_forward, ever_backward));

				add_oneway_direction(self.oneway, &mut one_lanes, true);
				add_oneway_direction(self.oneway, &mut one_lanes, false);

				if let Some(segregated) = self.segregated {
					self.apply_segregated(lanes_start, segregated_index, segregated);
				}

				let lanes_len = one_lanes.extend_vec(self.lanes) + segregated_len;

				return (
					lanes_len,
					placement_tags
						.filter(|_| self.return_center)
						.and_then(|tags| Placement::from_osm_str(tags.value()?)?.to_center_index())
						.unwrap_or(lanes_len),
				);
			}
		};

		let both_ways_count = lanes_tag
			.and_then(|tags| get_tag!(tags, both_ways)?.parse().ok())
			.unwrap_or(0);
		let mut both_ways = parse_direction(
			lane.clone(),
			both_ways_count,
			self.tags,
			false,
			self.platform,
			|dir| get_tag!(dir, both_ways:),
			|| maxspeed_default.clone(),
		);

		let cloned_lane = if let ParseDirectionLanes::All(mut both_ways_lane, _, dirty) = both_ways
		{
			if both_ways_count == 0
				&& get_tag!(self.tags, centre_turn_lane)
					.or_else(|| get_tag!(self.tags, center_turn_lane))
					== Some("yes")
			{
				both_ways_lane
					.unwrap_travel()
					.turn
					.set(TMode::All, Turn::normal(!self.driving_side).into());
				both_ways = ParseDirectionLanes::All(both_ways_lane, 1, true);
				lane.clone()
			} else if !dirty && both_ways_count == 0 {
				both_ways = ParseDirectionLanes::Individual(Vec::new());
				*both_ways_lane
			} else {
				both_ways = ParseDirectionLanes::All(both_ways_lane, both_ways_count, dirty);
				lane.clone()
			}
		} else {
			lane.clone()
		};

		let forward_count = lanes_tag.and_then(|tags| get_tag!(tags, forward)?.parse().ok());
		let backward_count = lanes_tag.and_then(|tags| get_tag!(tags, backward)?.parse().ok());

		let mut merge = !matches!(self.oneway_vehicle, None | Some(OnewayDirection::Both));

		let (forward_count, backward_count) = match (lanes_count, forward_count, backward_count) {
			(_, Some(forward), Some(backward)) => {
				if self.oneway_vehicle != Some(OnewayDirection::Reversible) {
					merge = false;
				}
				(forward, backward)
			}
			(Some(lanes), Some(forward), None) => (
				forward,
				lanes
					.saturating_sub(forward)
					.saturating_sub(self.busway_forward)
					.saturating_sub(both_ways_count),
			),
			(Some(lanes), None, Some(backward)) => (
				lanes
					.saturating_sub(backward)
					.saturating_sub(self.busway_backward)
					.saturating_sub(both_ways_count),
				backward,
			),
			(Some(lanes), None, None) if merge && both_ways_count == 0 => {
				let lanes = lanes
					.saturating_sub(self.busway_forward)
					.saturating_sub(self.busway_backward);
				(lanes, lanes)
			}
			(lanes, None, None) => {
				let lanes = if let Some(lanes) = lanes {
					lanes
						.saturating_sub(self.busway_forward)
						.saturating_sub(self.busway_backward)
						.saturating_sub(both_ways_count)
				} else if matches!(get_tag!(self.tags, priority), Some("forward" | "backward")) {
					1
				} else {
					2
				};

				let direction = lanes / 2;
				if lanes == 1 {
					merge = true;
				}
				(direction + (lanes & 1), direction.max(1))
			}
			(None, forward, backward) => (forward.unwrap_or(1), backward.unwrap_or(1)),
		};

		let mut forward = parse_direction(
			cloned_lane,
			forward_count,
			self.tags,
			false,
			self.platform,
			|dir| get_tag!(dir, forward:),
			|| maxspeed_default.clone(),
		);

		let mut backward = parse_direction(
			lane,
			backward_count,
			self.tags,
			true,
			self.platform,
			|dir| get_tag!(dir, backward:),
			|| maxspeed_default.take(),
		);

		let (centre_line, centre) =
			if merge && forward.len() == backward.len() && both_ways.len() == 0 {
				let mut all_lanes = glue_parse_direction_lanes(forward, backward);

				add_oneway_direction(self.oneway, &mut all_lanes, true);
				add_oneway_direction(self.oneway, &mut all_lanes, false);

				let centre = all_lanes.extend_vec(self.lanes) + segregated_len;

				// TODO: two_way_placement
				(centre, centre)
			} else {
				forward.mut_each(|lane| lane.travel_wrap_build(true, false));
				both_ways.mut_each(|lane| lane.travel_wrap_build(true, true));
				backward.mut_each(|lane| lane.travel_wrap_build(false, true));

				add_oneway_direction(self.oneway, &mut forward, true);
				add_oneway_direction(self.oneway, &mut backward, false);

				let (left, right) = match self.driving_side {
					Side::Left => (forward, backward),
					Side::Right => (backward, forward),
				};

				let left_len = left.extend_vec(self.lanes);
				let both_ways_len = both_ways.extend_vec(self.lanes);
				let right_len = right.extend_vec(self.lanes);

				let centre_line = left_len * 2 + both_ways_len;
				let centre = if self.return_center {
					get_two_way_placement(
						placement_tags,
						self.driving_side,
						left_len,
						both_ways_len,
						right_len,
					)
				} else {
					centre_line
				};

				(centre_line, centre)
			};

		if let Some(segregated) = self.segregated {
			self.apply_segregated(lanes_start, segregated_index, segregated);
		}

		if self.is_sidepath {
			if let Some(width) = get_tag!(self.tags, width).and_then(Quantity::from_osm_str) {
				distribute_full_width(&mut self.lanes[lanes_start..], width);
			}
		}

		(centre_line, centre)
	}

	fn apply_segregated(
		&mut self,
		start_index: usize,
		segregated_index: Option<usize>,
		segregated: bool,
	) {
		if segregated {
			self.add_segregated(start_index, segregated_index);
		} else {
			self.remove_segregated(start_index);
		}
	}

	fn add_segregated(&mut self, start_index: usize, segregated_index: Option<usize>) {
		let mut forward = None;
		let mut backward = None;
		let mut is_sidepath = false;
		for lane in &mut self.lanes[start_index..] {
			let LaneVariant::Travel(travel) = &mut lane.variant else {
				continue;
			};

			forward = travel
				.forward
				.access
				.set(TMode::Foot, AccessLevel::No.into())
				.or(forward);
			backward = travel
				.backward
				.access
				.set(TMode::Foot, AccessLevel::No.into())
				.or(backward);
			is_sidepath = is_sidepath || lane.is_sidepath;
		}

		// TODO: Remove once access masking is in place
		if forward == Some(AccessLevel::No.into()) {
			forward = None;
		}
		if backward == Some(AccessLevel::No.into()) {
			backward = None;
		}

		let forward = TravelLaneDirection::with_access(TModes::from([
			(TMode::All, AccessLevel::No.into()),
			(
				TMode::Foot,
				forward.unwrap_or_else(|| AccessLevel::Yes.into()),
			),
		]));
		let backward = TravelLaneDirection::with_access(TModes::from([
			(TMode::All, AccessLevel::No.into()),
			(
				TMode::Foot,
				backward.unwrap_or_else(|| AccessLevel::Yes.into()),
			),
		]));

		let mut lane: Lane = TravelLane { forward, backward }.into();
		lane.is_sidepath = is_sidepath;
		if let Some(tags) = get_tag!(self.tags, footway:) {
			lane = lane.add_parse(tags);
		}

		if let Some(segregated_index) = segregated_index {
			self.lanes[segregated_index] = lane;
		} else {
			self.lanes.push(lane);
		}
	}

	fn remove_segregated(&mut self, start_index: usize) {
		for lane in &mut self.lanes[start_index..] {
			if let LaneVariant::Travel(lane) = &mut lane.variant {
				lane.forward
					.access
					.set(TMode::Foot, AccessLevel::Yes.into());
				lane.backward
					.access
					.set(TMode::Foot, AccessLevel::Yes.into());
			}
		}
	}
}

#[inline]
fn get_two_way_placement<'a, T: Taglike<'a>>(
	tags: Option<T>,
	driving_side: Side,
	left_len: u8,
	both_ways_len: u8,
	right_len: u8,
) -> u8 {
	if let Some(center) =
		tags.and_then(|tags| Placement::from_osm_str(get_tag!(tags, forward)?)?.to_center_index())
	{
		return match driving_side {
			Side::Left => center,
			Side::Right => (right_len + both_ways_len) * 2 + center,
		};
	}

	if let Some(center) =
		tags.and_then(|tags| Placement::from_osm_str(get_tag!(tags, backward)?)?.to_center_index())
	{
		return match driving_side {
			Side::Left => (left_len + both_ways_len + right_len) * 2 - center,
			Side::Right => left_len * 2 - center,
		};
	}

	left_len * 2 + both_ways_len
}

fn glue_parse_direction_lanes(
	forward: ParseDirectionLanes,
	backward: ParseDirectionLanes,
) -> ParseDirectionLanes {
	match (forward, backward) {
		(
			ParseDirectionLanes::All(mut forward_lane, count, _),
			ParseDirectionLanes::All(backward_lane, _, _),
		) => {
			<&mut TravelLane>::try_from(&mut forward_lane.variant)
				.unwrap()
				.backward = TravelLane::try_from(backward_lane.variant).unwrap().forward;
			ParseDirectionLanes::All(forward_lane, count, false)
		}
		(
			ParseDirectionLanes::Individual(mut forward_lanes),
			ParseDirectionLanes::All(backward_lane, _, _),
		) => {
			let backward = TravelLane::try_from(backward_lane.variant).unwrap().forward;
			for forward_lane in &mut forward_lanes {
				<&mut TravelLane>::try_from(&mut forward_lane.variant)
					.unwrap()
					.backward = backward.clone();
			}
			ParseDirectionLanes::Individual(forward_lanes)
		}
		(
			ParseDirectionLanes::All(forward_lane, _, _),
			ParseDirectionLanes::Individual(mut backward_lanes),
		) => {
			let forward = TravelLane::try_from(forward_lane.variant).unwrap().forward;
			for backward_lane in &mut backward_lanes {
				let lane = <&mut TravelLane>::try_from(&mut backward_lane.variant).unwrap();
				lane.backward = replace(&mut lane.forward, forward.clone());
			}
			ParseDirectionLanes::Individual(backward_lanes)
		}
		(
			ParseDirectionLanes::Individual(mut forward_lanes),
			ParseDirectionLanes::Individual(backward_lanes),
		) => {
			forward_lanes
				.iter_mut()
				.zip(backward_lanes.into_iter().rev())
				.for_each(|(forward_lane, backward_lane)| {
					<&mut TravelLane>::try_from(&mut forward_lane.variant)
						.unwrap()
						.backward = TravelLane::try_from(backward_lane.variant).unwrap().forward;
				});
			ParseDirectionLanes::Individual(forward_lanes)
		}
	}
}

fn ever_allowed(t: &TravelLane, mode: TMode) -> bool {
	t.forward
		.access
		.get(mode)
		.is_some_and(|c| c.iter().any(|r| r.value != AccessLevel::No))
		|| t.backward
			.access
			.get(mode)
			.is_some_and(|c| c.iter().any(|r| r.value != AccessLevel::No))
}

pub(crate) fn distribute_full_width(lanes: &mut [Lane], mut full_width: Quantity<Distance>) {
	let weights: Vec<u16> = lanes
		.iter()
		.filter_map(|lane| {
			if let Some(width) = lane.width {
				full_width -= &width;
				return None;
			}

			Some(match &lane.variant {
				LaneVariant::Travel(t)
					if ever_allowed(t, TMode::Motorcar)
						|| ever_allowed(t, TMode::Bus)
						|| ever_allowed(t, TMode::Taxi) =>
				{
					2
				}
				LaneVariant::Travel(_) => 1,
				LaneVariant::Parking(p) => match p.orientation {
					Some(ParkingOrientation::Parallel) | None => 2,
					Some(ParkingOrientation::Diagonal | ParkingOrientation::Perpendicular) => 4,
				},
			})
		})
		.collect();

	let full_weights: Number = weights.iter().sum::<u16>().into();
	for (lane, weight) in lanes
		.iter_mut()
		.filter(|lane| lane.width.is_none())
		.zip(weights.into_iter())
	{
		lane.width = Some(full_width * weight.into() / full_weights);
	}
}

#[cfg(test)]
mod tests {
	use crate::{
		TModes,
		lanes::{
			lanes,
			travel::{TravelLane, Turn},
		},
		new_tag,
	};

	#[test]
	fn sidepath() {
		let tags = new_tag! {
			highway = "residential",
			sidewalk = "both",
		};

		let lanes = lanes(&tags, &["NL"]).unwrap();
		assert_eq!(lanes.lanes.len(), 4);

		assert_eq!(lanes.lanes[0].is_sidepath, true);
		assert_eq!(lanes.lanes[1].is_sidepath, false);
		assert_eq!(lanes.lanes[2].is_sidepath, false);
		assert_eq!(lanes.lanes[3].is_sidepath, true);
	}

	#[test]
	fn sidewalk_sidepath() {
		let tags = new_tag! {
			highway = "footway",
			footway = "sidewalk",
		};

		let lanes = lanes(&tags, &["GB"]).unwrap();
		assert_eq!(lanes.lanes.len(), 1);

		assert_eq!(lanes.lanes[0].is_sidepath, true);
	}

	#[test]
	fn cycleway_sidepath() {
		let tags = new_tag! {
			highway = "cycleway",
			is_sidepath = "yes",
		};

		let lanes = lanes(&tags, &["BE"]).unwrap();
		assert_eq!(lanes.lanes.len(), 1);

		assert_eq!(lanes.lanes[0].is_sidepath, true);
	}

	#[test]
	fn cycleway_segregated_sidepath() {
		let tags = new_tag! {
			highway = "residential",
			cycleway:left = "track",
			cycleway:left:segregated = "yes",
		};

		let lanes = lanes(&tags, &["BE"]).unwrap();
		assert_eq!(lanes.lanes.len(), 4);

		assert_eq!(lanes.lanes[0].is_sidepath, true);
		assert_eq!(lanes.lanes[1].is_sidepath, true);
		assert_eq!(lanes.lanes[2].is_sidepath, false);
		assert_eq!(lanes.lanes[3].is_sidepath, false);
	}

	#[test]
	fn no_duplicated_turns() {
		let tags = new_tag! {
			highway = "secondary_link",
			oneway = "yes",
			lanes = "2",
			turn:lanes = "left|right",
		};

		let lanes = lanes(&tags, &["US"]).unwrap();
		assert_eq!(lanes.lanes.len(), 2);

		let travel0: &TravelLane = (&lanes.lanes[0].variant).try_into().unwrap();
		assert_eq!(travel0.forward.turn, TModes::all(Turn::Left.into()));
		assert_eq!(travel0.backward.turn, TModes::default());

		let travel1: &TravelLane = (&lanes.lanes[1].variant).try_into().unwrap();
		assert_eq!(travel1.forward.turn, TModes::all(Turn::Right.into()));
		assert_eq!(travel1.backward.turn, TModes::default());
	}
}
