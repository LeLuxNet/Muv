use crate::{AccessLevel, TMode, TModes, Taglike, conditional::Conditional, get_tag};

#[inline]
pub(crate) fn motorway() -> TModes<Conditional<AccessLevel>> {
	TModes::from([
		(TMode::All, AccessLevel::No.into()),
		(TMode::LandBased, AccessLevel::Yes.into()),
		(TMode::Motorcar, AccessLevel::Designated.into()),
		(TMode::Motorcycle, AccessLevel::Designated.into()),
		(TMode::Goods, AccessLevel::Designated.into()),
		(TMode::Hgv, AccessLevel::Designated.into()),
		(TMode::Psv, AccessLevel::Designated.into()),
		(TMode::Moped, AccessLevel::No.into()),
		(TMode::Horse, AccessLevel::No.into()),
		(TMode::Bicycle, AccessLevel::No.into()),
		(TMode::Foot, AccessLevel::No.into()),
	])
}

#[inline]
pub(crate) fn highway() -> TModes<Conditional<AccessLevel>> {
	TModes::from([
		(TMode::All, AccessLevel::No.into()),
		(TMode::LandBased, AccessLevel::Yes.into()),
	])
}

#[inline]
pub(crate) fn path() -> TModes<Conditional<AccessLevel>> {
	TModes::from([
		(TMode::All, AccessLevel::No.into()),
		(TMode::Foot, AccessLevel::Yes.into()),
		(TMode::Horse, AccessLevel::Yes.into()),
		(TMode::Bicycle, AccessLevel::Yes.into()),
	])
}

#[inline]
pub(crate) fn bridleway() -> TModes<Conditional<AccessLevel>> {
	TModes::from([
		(TMode::All, AccessLevel::No.into()),
		(TMode::Horse, AccessLevel::Designated.into()),
	])
}

#[inline]
pub(crate) fn cycleway() -> TModes<Conditional<AccessLevel>> {
	TModes::from([
		(TMode::All, AccessLevel::No.into()),
		(TMode::Bicycle, AccessLevel::Designated.into()),
	])
}

#[inline]
pub(crate) fn footway() -> TModes<Conditional<AccessLevel>> {
	TModes::from([
		(TMode::All, AccessLevel::No.into()),
		(TMode::Foot, AccessLevel::Designated.into()),
	])
}

#[inline]
pub(crate) fn steps<'a, T: Taglike<'a>>(tags: T) -> TModes<Conditional<AccessLevel>> {
	let mut access = TModes::from([
		(TMode::All, AccessLevel::No.into()),
		(TMode::Foot, AccessLevel::Yes.into()),
	]);

	let mut ramp_wheelchair = false;
	let mut ramp_stroller = false;
	let mut ramp_bicycle = false;

	let ramp_tags = get_tag!(tags, ramp:);
	if let Some(ramp_tags) = ramp_tags {
		if get_tag!(ramp_tags, wheelchair) == Some("yes") {
			ramp_wheelchair = true;
		} else if get_tag!(ramp_tags, stroller) == Some("yes") {
			ramp_stroller = true;
		} else if matches!(get_tag!(ramp_tags, bicycle), Some("yes" | "automatic")) {
			ramp_bicycle = true;
		} else if matches!(get_tag!(ramp_tags, luggage), Some("yes" | "automatic")) {
		} else if ramp_tags.value() == Some("yes") {
			ramp_stroller = true;
		}
	}

	if ramp_stroller || ramp_wheelchair {
		access.set(TMode::Stroller, AccessLevel::Yes.into());
	}
	if ramp_bicycle || ramp_stroller || ramp_wheelchair {
		access.set(TMode::Bicycle, AccessLevel::Dismount.into());
	}

	access
}

#[inline]
pub(crate) fn busway() -> TModes<Conditional<AccessLevel>> {
	TModes::from([
		(TMode::All, AccessLevel::No.into()),
		(TMode::Bus, AccessLevel::Designated.into()),
		(TMode::Emergency, AccessLevel::Yes.into()),
	])
}

#[inline]
pub(crate) fn bus_guideway() -> TModes<Conditional<AccessLevel>> {
	TModes::from([
		(TMode::All, AccessLevel::No.into()),
		(TMode::Bus, AccessLevel::Designated.into()),
	])
}

#[inline]
pub(crate) fn shoulder() -> TModes<Conditional<AccessLevel>> {
	TModes::from([
		(TMode::All, AccessLevel::No.into()),
		(TMode::Foot, AccessLevel::Yes.into()),
	])
}

#[inline]
pub(crate) fn escape() -> TModes<Conditional<AccessLevel>> {
	TModes::from([
		(TMode::All, AccessLevel::No.into()),
		(TMode::LandBased, AccessLevel::Escape.into()),
	])
}

#[inline]
pub(crate) fn raceway() -> TModes<Conditional<AccessLevel>> {
	// TODO
	TModes::from([(TMode::All, AccessLevel::No.into())])
}

pub(crate) fn cyclestreet(
	regions: &[impl AsRef<str>],
	bicycle_road: bool,
) -> TModes<Conditional<AccessLevel>> {
	let mut access = TModes::from([
		(TMode::All, AccessLevel::No.into()),
		(TMode::LandBased, AccessLevel::Yes.into()),
		(TMode::Bicycle, AccessLevel::Designated.into()),
	]);

	if !regions
		.iter()
		.any(|region| cyclestreet_region(region.as_ref(), &mut access))
		&& bicycle_road
	{
		access.set(TMode::Vehicle, AccessLevel::Destination.into());
	}

	access
}

pub(crate) fn cyclestreet_region(
	region: &str,
	access: &mut TModes<Conditional<AccessLevel>>,
) -> bool {
	match region {
		"AT" => {
			access.set(TMode::Vehicle, AccessLevel::Destination.into());
			access.set(TMode::Psv, AccessLevel::Yes.into());
			access.set(TMode::Emergency, AccessLevel::Yes.into());
		}
		"BE" => {
			access.set(TMode::Mofa, AccessLevel::Designated.into());
		}
		"CH" | "ES" | "FI" | "FR" | "LU" | "NL" | "RU" | "SE" | "US" => {}
		"CZ" => {
			access.set(TMode::Vehicle, AccessLevel::No.into());
		}
		"DE" => {
			access.set(TMode::Vehicle, AccessLevel::No.into());
			access.set(TMode::SmallElectricVehicle, AccessLevel::Yes.into());
		}
		"DK" => {
			access.set(TMode::Vehicle, AccessLevel::No.into());
			access.set(TMode::Mofa, AccessLevel::Designated.into());
		}
		"EE" => {
			access.set(TMode::LandBased, AccessLevel::No.into());
			access.set(TMode::Foot, AccessLevel::Yes.into());
			access.set(TMode::Mofa, AccessLevel::Designated.into());
			access.set(TMode::Moped, AccessLevel::Designated.into());
			access.set(TMode::SmallElectricVehicle, AccessLevel::Designated.into());
		}
		_ => return false,
	};
	true
}

#[cfg(test)]
mod tests {
	use crate::{
		AccessLevel, TMode,
		lanes::{assert_access, highway::access::steps, lanes},
		new_tag,
	};

	#[test]
	fn prevent_override_cascade() {
		let tags = new_tag! {
			highway = "footway",
			access = "permissive",
		};

		let lanes = lanes(&tags, &["PT"]).unwrap();
		assert_eq!(lanes.lanes.len(), 1);

		assert_access(
			&lanes.lanes[0],
			TMode::Foot,
			AccessLevel::Permissive,
			AccessLevel::Permissive,
		);
		assert_access(
			&lanes.lanes[0],
			TMode::Motorcar,
			AccessLevel::No,
			AccessLevel::No,
		);
	}

	#[test]
	fn cascade_access_no() {
		let tags = new_tag! {
			highway = "residential",
			access = "no",
			bicycle = "designated",
		};

		let lanes = lanes(&tags, &["RU"]).unwrap();
		assert_eq!(lanes.lanes.len(), 2);

		assert_access(
			&lanes.lanes[0],
			TMode::Foot,
			AccessLevel::No,
			AccessLevel::No,
		);
		assert_access(
			&lanes.lanes[0],
			TMode::Motorcar,
			AccessLevel::No,
			AccessLevel::No,
		);
		assert_access(
			&lanes.lanes[0],
			TMode::Bicycle,
			AccessLevel::No,
			AccessLevel::Designated,
		);
	}

	#[test]
	fn skip_access_yes() {
		let tags = new_tag! {
			highway = "cycleway",
			access = "yes",
		};

		let lanes = lanes(&tags, &["DE"]).unwrap();
		assert_eq!(lanes.lanes.len(), 1);

		assert_access(
			&lanes.lanes[0],
			TMode::Motorcar,
			AccessLevel::No,
			AccessLevel::No,
		);
		assert_access(
			&lanes.lanes[0],
			TMode::Bicycle,
			AccessLevel::Designated,
			AccessLevel::Designated,
		);
	}

	#[test]
	fn bicycle_ramp() {
		let tags = new_tag! {
			ramp:stroller = "yes",
		};

		let access = steps(&tags);

		assert_eq!(access.get(TMode::Stroller), Some(&AccessLevel::Yes.into()));
		assert_eq!(
			access.get(TMode::Bicycle),
			Some(&AccessLevel::Dismount.into())
		);
	}
}
