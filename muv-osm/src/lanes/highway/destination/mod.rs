use crate::Colour;

#[cfg(feature = "serde")]
use serde::{Deserialize, Serialize};

mod symbol;
pub use symbol::*;

#[derive(Debug, Default, Clone, PartialEq, Eq)]
#[cfg_attr(feature = "serde", derive(Serialize, Deserialize))]
pub struct Destination {
	pub r#ref: Vec<String>,
	pub int_ref: Vec<String>,
	pub lines: Vec<DestinationLine>,
}

#[derive(Debug, Default, Clone, PartialEq, Eq)]
#[cfg_attr(feature = "serde", derive(Serialize, Deserialize))]
pub struct DestinationLine {
	pub name: String,
	pub symbol: Vec<DestinationSymbol>,
	pub colour: Option<Colour>,
}
