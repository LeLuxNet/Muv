use crate::{
	Conditional, FromOsm, FromOsmStr, Surface, SurfaceKind, TMode, TModes, Taglike, get_tag,
	lanes::{
		Lane, MAX_DIRECTION_LANES, Smoothness,
		highway::traffic_signs,
		travel::{AllParser, IndividualParser, Overtaking, ParseDirectionLanes},
	},
	units::{Quantity, WalkSpeed},
};

#[allow(clippy::too_many_lines)]
pub(crate) fn parse_direction<'a, T: Taglike<'a>>(
	mut lane: Lane,
	lane_count: u8,
	tags: T,
	lanes_reversed: bool,
	platform: bool,
	direction: fn(T) -> Option<T>,
	maxspeed_default: impl FnOnce() -> Option<TModes<Conditional<Quantity<WalkSpeed>>>>,
) -> ParseDirectionLanes {
	let traffic_sign = get_tag!(tags, traffic_sign:);

	let access = get_tag!(tags, access:);

	let turn = get_tag!(tags, turn:);
	let change = get_tag!(tags, change:);
	let overtaking = get_tag!(tags, overtaking:);

	let priority_road = get_tag!(tags, priority_road:);

	let maxspeed = get_tag!(tags, maxspeed:);
	let minspeed = get_tag!(tags, minspeed:);

	let maxspeed_type = maxspeed.and_then(|tags| get_tag!(tags, type:));
	let source_maxspeed = get_tag!(tags, source:maxspeed:);
	let zone = get_tag!(tags, zone:);
	let zone_maxspeed = zone.and_then(|tags| get_tag!(tags, maxspeed:));
	let zone_traffic = zone.and_then(|tags| get_tag!(tags, traffic:));
	let maxspeed_source = maxspeed.and_then(|tags| get_tag!(tags, source:));

	let maxweight = get_tag!(tags, maxweight:);
	let maxaxleload = get_tag!(tags, maxaxleload:);
	let maxlength = get_tag!(tags, maxlength:);
	let maxwidth = get_tag!(tags, maxwidth:);
	let maxheight = get_tag!(tags, maxheight:);
	let maxdraught = get_tag!(tags, maxdraught:);

	let surface = get_tag!(tags, surface:);
	let smoothness = get_tag!(tags, smoothness:);
	let width = get_tag!(tags, width:);

	let travel = lane.unwrap_travel();
	let mut all = AllParser {
		dirty: false,
		direction,
	};

	all.access(access, tags, platform, &mut travel.access);

	all.base(turn, &mut travel.turn);
	all.base(change, &mut travel.change);
	all.with_parse(overtaking, &mut travel.overtaking, parse_overtaking); // TODO: Support overtaking=forward/backward
	all.with_parse(
		traffic_sign,
		&mut travel.overtaking,
		traffic_signs::overtaking,
	);

	all.base(priority_road, &mut travel.priority_road);

	all.base(zone_maxspeed, &mut travel.maxspeed);
	all.base(maxspeed, &mut travel.maxspeed);
	all.base(minspeed, &mut travel.minspeed);
	all.maxspeed(maxspeed_type, travel);
	all.maxspeed(source_maxspeed, travel);
	all.maxspeed(maxspeed, travel);
	all.maxspeed(zone_traffic, travel);
	all.maxspeed(maxspeed_source, travel);
	if let Some(maxspeed_default) = maxspeed_default() {
		all.maxspeed_with_data(maxspeed_default, travel);
	}

	all.base(maxweight, &mut travel.maxweight);
	all.base(maxaxleload, &mut travel.maxaxleload);
	all.base(maxlength, &mut travel.maxlength);
	all.base(maxwidth, &mut travel.maxwidth);
	all.base(maxheight, &mut travel.maxheight);
	all.base(maxdraught, &mut travel.maxheight);

	if let Some(surface) = surface.and_then(Surface::from_osm) {
		lane.surface = Some(surface);
		all.dirty = true;
	}
	if let Some(smoothness) = smoothness.and_then(|tags| Smoothness::from_osm_str(tags.value()?)) {
		lane.smoothness = Some(smoothness);
		all.dirty = true;
	}

	let lanes = ParseDirectionLanes::All(
		Box::new(lane),
		lane_count.min(MAX_DIRECTION_LANES),
		all.dirty,
	);
	let mut individual = IndividualParser {
		lanes,
		lanes_reversed,
		direction,
	};

	individual.access(access, tags, platform, |lane| {
		&mut lane.unwrap_travel().access
	});
	individual.mask_outer(tags, TMode::Psv, "psv");
	individual.mask_outer(tags, TMode::Bus, "bus");

	individual.base(turn, |lane| &mut lane.unwrap_travel().turn);
	individual.base(change, |lane| &mut lane.unwrap_travel().change);
	individual.with_parse(
		overtaking,
		|lane| &mut lane.unwrap_travel().overtaking,
		parse_overtaking,
	);
	individual.with_parse(
		traffic_sign,
		|lane| &mut lane.unwrap_travel().overtaking,
		traffic_signs::overtaking,
	);

	individual.base(priority_road, |lane| {
		&mut lane.unwrap_travel().priority_road
	});

	// TODO: maxspeed_type and source_maxspeed per lane
	individual.base(maxspeed, |lane| &mut lane.unwrap_travel().maxspeed);
	individual.base(minspeed, |lane| &mut lane.unwrap_travel().minspeed);

	individual.base(maxweight, |lane| &mut lane.unwrap_travel().maxweight);
	individual.base(maxaxleload, |lane| &mut lane.unwrap_travel().maxaxleload);
	individual.base(maxlength, |lane| &mut lane.unwrap_travel().maxlength);
	individual.base(maxwidth, |lane| &mut lane.unwrap_travel().maxwidth);
	individual.base(maxheight, |lane| &mut lane.unwrap_travel().maxheight);
	individual.base(maxdraught, |lane| &mut lane.unwrap_travel().maxdraught);

	individual.direction(width, Quantity::from_osm_str, |lane, val| {
		lane.width = Some(val);
	});
	individual.direction(surface, SurfaceKind::from_osm_str, |lane, val| {
		lane.surface = Some(val.into());
	});
	individual.direction(smoothness, Smoothness::from_osm_str, |lane, val| {
		lane.smoothness = Some(val);
	});

	individual.lanes
}

#[allow(clippy::unnecessary_wraps)]
pub(crate) fn parse_overtaking(s: &str) -> Option<TModes<Conditional<Overtaking>>> {
	Some(match Overtaking::from_osm_str(s) {
		Some(v @ (Overtaking::Yes | Overtaking::Caution)) => TModes::all(v.into()),
		v => {
			let mut res = TModes::from([
				(TMode::All, Overtaking::Yes.into()),
				(TMode::DoubleTrackedMotorVehicle, Overtaking::No.into()),
			]);
			if v.is_none() {
				if let Some(mode) = TMode::from_osm_str(s.strip_prefix("only_").unwrap_or(s)) {
					res.set(mode, Overtaking::Yes.into());
				}
			}
			res
		}
	})
}
