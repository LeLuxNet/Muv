pub mod parking;
pub mod travel;

pub mod highway;
#[doc(inline)]
pub use highway::highway_lanes;

pub mod railway;
#[doc(inline)]
pub use railway::railway_lanes;

#[warn(missing_docs)]
mod waterway;
pub use waterway::*;

#[warn(missing_docs)]
mod aeroway;
pub use aeroway::*;

#[allow(clippy::module_inception)]
mod lanes;
pub use lanes::*;

mod lane;
pub use lane::*;

mod tags;
pub(crate) use tags::*;
