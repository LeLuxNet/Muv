use crate::{
	TMode,
	units::{Distance, Quantity, Weight},
};

#[derive(Debug)]
pub struct Vehicle {
	pub mode: TMode,

	pub wheels: Option<u32>,
	pub axles: Option<u32>,
	pub occupants: Option<u32>,
	pub seats: Option<u32>,

	pub weight: Option<Quantity<Weight>>,
	pub weightrating: Option<Quantity<Weight>>,
	pub weightcapacity: Option<Quantity<Weight>>,
	pub emptyweight: Option<Quantity<Weight>>,
	pub axleload: Option<Quantity<Weight>>,
	pub length: Option<Quantity<Distance>>,
	pub width: Option<Quantity<Distance>>,
	pub height: Option<Quantity<Distance>>,
	pub draught: Option<Quantity<Distance>>,

	pub trailers: Option<u32>,
	pub trailerweight: Option<Quantity<Weight>>,
	pub caravan: Option<bool>,
	pub articulated: Option<bool>,
}

impl Vehicle {
	#[inline]
	#[must_use]
	pub const fn new(mode: TMode) -> Self {
		Self {
			mode,

			wheels: None,
			axles: None,
			occupants: None,
			seats: None,

			weight: None,
			weightrating: None,
			weightcapacity: None,
			emptyweight: None,
			axleload: None,
			length: None,
			width: None,
			height: None,
			draught: None,

			trailers: None,
			trailerweight: None,
			caravan: None,
			articulated: None,
		}
	}
}
