use std::{
	borrow::Cow,
	cmp::Ordering,
	fmt::Display,
	ops::{Add, AddAssign, Div, DivAssign, Mul, MulAssign, Sub, SubAssign},
};

#[cfg(feature = "serde")]
use serde::{Deserialize, Serialize};

use crate::{FromOsmStr, ToOsmStr, units::Number};

/// Physical quantity
///
/// Quantities are not [converted](Quantity::to) to one unit when parsing to prevent losing precision and allowing to
/// display the units the same way they are on, for example, the sign.
///
/// [OSM Wiki](https://wiki.openstreetmap.org/wiki/Map_features/Units)
#[derive(Debug, Clone, Copy)]
#[cfg_attr(feature = "serde", derive(Serialize, Deserialize))]
pub struct Quantity<U> {
	pub value: Number,
	pub unit: U,
}

impl<U> Quantity<U> {
	/// Creates a new instance of the unit
	///
	/// ```
	/// use muv_osm::{
	/// 	n,
	/// 	units::{Distance, Quantity, Speed},
	/// };
	///
	/// Quantity::new(n!(2.5), Distance::Metre);
	/// Quantity::new(n!(30), Speed::MilesPerHour);
	/// ```
	#[inline]
	#[must_use]
	pub const fn new(value: Number, unit: U) -> Self {
		Self { value, unit }
	}
}

#[macro_export]
macro_rules! quantity {
	($value:literal $unit:expr) => {
		$crate::units::Quantity::new($crate::n!($value), $unit)
	};
}

impl<U: Display> Display for Quantity<U> {
	fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
		write!(f, "{} {}", self.value, self.unit)
	}
}

pub trait PartialUnit: Sized + PartialEq {
	#[must_use]
	fn partial_default() -> Option<Self> {
		None
	}

	#[must_use]
	fn from_special_quantity_osm_str(s: &str) -> Option<Quantity<Self>> {
		_ = s;
		None
	}

	#[must_use]
	fn partial_convert(&self, to: &Self, value: Number) -> Option<Number>;
}

pub trait Unit: PartialUnit {
	#[must_use]
	fn convert(&self, to: &Self, value: Number) -> Number;
}

impl<U: PartialUnit> Quantity<U> {
	/// Tries to convert the unit and it's value
	///
	/// ```
	/// use muv_osm::{quantity, units::{Duration, Quantity}};
	///
	/// let duration = quantity!(30 Duration::Minute);
	/// let duration_min = duration.partial_to(Duration::Hour);
	/// assert_eq!(duration_min, Some(quantity!(0.5 Duration::Hour)));
	///
	/// let duration_days = duration.partial_to(Duration::Day);
	/// assert_eq!(duration_days, None);
	/// ```
	pub fn partial_to(&self, other: U) -> Option<Self> {
		Some(Self {
			value: self.unit.partial_convert(&other, self.value)?,
			unit: other,
		})
	}
}

impl<U: PartialUnit> PartialEq for Quantity<U> {
	fn eq(&self, other: &Self) -> bool {
		if self.unit == other.unit {
			self.value == other.value
		} else {
			self.unit.partial_convert(&other.unit, self.value) == Some(other.value)
		}
	}
}

impl<U: Unit + Eq> Eq for Quantity<U> {}

impl<U: Unit> Quantity<U> {
	/// Converts the unit and it's value
	///
	/// ```
	/// use muv_osm::{quantity, units::{Quantity, Speed}};
	///
	/// let speed = quantity!(15 Speed::MetresPerSecond);
	/// let speed_kmh = speed.to(Speed::KilometresPerHour);
	///
	/// assert_eq!(speed_kmh, quantity!(54 Speed::KilometresPerHour));
	/// ```
	#[inline]
	#[must_use]
	pub fn to(&self, other: U) -> Self {
		Self {
			value: self.unit.convert(&other, self.value),
			unit: other,
		}
	}
}

impl<U: PartialUnit> PartialOrd for Quantity<U> {
	fn partial_cmp(&self, other: &Self) -> Option<Ordering> {
		if self.unit == other.unit {
			self.value.partial_cmp(&other.value)
		} else {
			self.unit
				.partial_convert(&other.unit, other.value)?
				.partial_cmp(&other.value)
		}
	}
}

impl<U: Unit + Ord> Ord for Quantity<U> {
	fn cmp(&self, other: &Self) -> Ordering {
		if self.unit == other.unit {
			self.value.cmp(&other.value)
		} else {
			self.unit
				.convert(&other.unit, other.value)
				.cmp(&other.value)
		}
	}
}

impl<U: PartialUnit + FromOsmStr> FromOsmStr for Quantity<U> {
	fn from_osm_str(s: &str) -> Option<Self> {
		Self::from_osm_str_with_unit_fn(s, U::partial_default)
	}
}

impl<U: PartialUnit + FromOsmStr> Quantity<U> {
	/// Parses a unit with a different default if no unit is given
	///
	/// ```
	/// use muv_osm::{FromOsmStr, quantity, units::{Duration, Quantity}};
	///
	/// assert_eq!(
	///     Quantity::from_osm_str("24 h"),
	///     Some(quantity!(24 Duration::Hour)),
	/// );
	/// assert_eq!(
	///     Quantity::from_osm_str("10"),
	///     Some(quantity!(10 Duration::Second)),
	/// );
	/// assert_eq!(
	///     Quantity::from_osm_str_with_unit("7 days", Duration::Hour),
	///     Some(quantity!(7 Duration::Day)),
	/// );
	/// assert_eq!(
	///     Quantity::from_osm_str_with_unit("6.2", Duration::Hour),
	///     Some(quantity!(6.2 Duration::Hour)),
	/// );
	/// ```
	pub fn from_osm_str_with_unit(s: &str, unit: U) -> Option<Self> {
		Self::from_osm_str_with_unit_fn(s, || Some(unit))
	}

	fn from_osm_str_with_unit_fn(s: &str, unit_fn: impl FnOnce() -> Option<U>) -> Option<Self> {
		Self::from_osm_str_with_unit_fn_base(s, unit_fn)
			.or_else(|| U::from_special_quantity_osm_str(s))
	}

	pub(crate) fn from_osm_str_with_unit_fn_base(
		s: &str,
		unit_fn: impl FnOnce() -> Option<U>,
	) -> Option<Self> {
		let (value, mut unit_str) = Number::parse_start(s)?;
		unit_str = unit_str.trim_start();
		let unit = if unit_str.is_empty() {
			unit_fn()
		} else {
			U::from_osm_str(unit_str)
		}?;
		Some(Self { value, unit })
	}
}

impl<U: PartialUnit + ToOsmStr> ToOsmStr for Quantity<U> {
	fn to_osm_str(self) -> Option<std::borrow::Cow<'static, str>> {
		self.to_osm_str_with_unit(U::partial_default())
	}
}

impl<U: ToOsmStr> Quantity<U> {
	#[allow(clippy::wrong_self_convention)]
	pub fn to_osm_str_without_unit(self) -> Option<Cow<'static, str>> {
		Some(format!("{} {}", self.value, self.unit.to_osm_str()?).into())
	}
}

impl<U: ToOsmStr + PartialEq> Quantity<U> {
	#[allow(clippy::wrong_self_convention)]
	pub fn to_osm_str_with_unit(self, unit: Option<U>) -> Option<Cow<'static, str>> {
		if unit.is_some_and(|unit| unit == self.unit) {
			Some(self.value.to_string().into())
		} else {
			self.to_osm_str_without_unit()
		}
	}
}

impl<U: Unit> Add<&Self> for Quantity<U> {
	type Output = Self;

	fn add(mut self, rhs: &Self) -> Self::Output {
		self.value += rhs.unit.convert(&self.unit, rhs.value);
		self
	}
}

impl<U: Unit> AddAssign<&Self> for Quantity<U> {
	fn add_assign(&mut self, rhs: &Self) {
		self.value += rhs.unit.convert(&self.unit, rhs.value);
	}
}

impl<U: Unit> Sub<&Self> for Quantity<U> {
	type Output = Self;

	fn sub(mut self, rhs: &Self) -> Self::Output {
		self.value -= rhs.unit.convert(&self.unit, rhs.value);
		self
	}
}

impl<U: Unit> SubAssign<&Self> for Quantity<U> {
	fn sub_assign(&mut self, rhs: &Self) {
		self.value -= rhs.unit.convert(&self.unit, rhs.value);
	}
}

impl<U> Mul<Number> for Quantity<U> {
	type Output = Self;

	fn mul(mut self, rhs: Number) -> Self::Output {
		self.value *= rhs;
		self
	}
}

impl<U> MulAssign<Number> for Quantity<U> {
	fn mul_assign(&mut self, rhs: Number) {
		self.value *= rhs;
	}
}

impl<U> Div<Number> for Quantity<U> {
	type Output = Self;

	fn div(mut self, rhs: Number) -> Self::Output {
		self.value /= rhs;
		self
	}
}

impl<U> DivAssign<Number> for Quantity<U> {
	fn div_assign(&mut self, rhs: Number) {
		self.value /= rhs;
	}
}

#[cfg(test)]
mod tests {
	use crate::{
		FromOsmStr, ToOsmStr,
		units::{Distance, Duration, Quantity, Speed},
	};

	#[test]
	fn from_osm_str() {
		assert_eq!(
			Quantity::from_osm_str("50"),
			Some(quantity!(50 Distance::Metre))
		);
		assert_eq!(
			Quantity::from_osm_str("50nmi"),
			Some(quantity!(50 Distance::NauticalMile))
		);
		assert_eq!(
			Quantity::from_osm_str("40 km"),
			Some(quantity!(40 Distance::Kilometre))
		);
		assert_eq!(
			Quantity::from_osm_str("20.5km"),
			Some(quantity!(20.5 Distance::Kilometre))
		);
		assert_eq!(
			Quantity::from_osm_str("-12.3'"),
			Some(quantity!(-12.3 Distance::Feet))
		);
	}

	#[test]
	fn into_osm_str() {
		assert_eq!(
			quantity!(20 Speed::KilometresPerHour).to_osm_str(),
			Some("20".into())
		);
		assert_eq!(
			quantity!(12.5 Distance::NauticalMile).to_osm_str(),
			Some("12.5 nmi".into())
		);
		assert_eq!(
			quantity!(40 Duration::Second).to_osm_str_with_unit(Some(Duration::Hour)),
			Some("40 s".into())
		);
		assert_eq!(
			quantity!(2 Duration::Hour).to_osm_str_with_unit(Some(Duration::Hour)),
			Some("2".into())
		);
		assert_eq!(
			quantity!(30 Speed::KilometresPerHour).to_osm_str_without_unit(),
			Some("30 km/h".into())
		);
	}
}
