use std::{
	error::Error,
	fmt::{self, Debug, Display},
	str,
	str::FromStr,
};

use crate::{
	FromOsmStr, ToOsmStr,
	units::{Number, PartialUnit},
};

#[cfg(feature = "serde")]
use serde::{Deserialize, Deserializer, Serialize, Serializer, de::Visitor};

#[derive(Clone, Copy, PartialEq, Eq, Hash)]
pub struct Currency(pub(crate) [u8; 3]);

impl Debug for Currency {
	fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
		write!(
			f,
			"Currency(\"{}{}{}\")",
			char::from(self.0[0]),
			char::from(self.0[1]),
			char::from(self.0[2])
		)
	}
}

impl Display for Currency {
	fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
		write!(
			f,
			"{}{}{}",
			char::from(self.0[0]),
			char::from(self.0[1]),
			char::from(self.0[2])
		)
	}
}

impl PartialUnit for Currency {
	fn partial_convert(&self, _to: &Self, _value: Number) -> Option<Number> {
		None
	}
}

#[derive(Debug, Clone, Copy, PartialEq, Eq, Hash)]
pub struct ParseCurrencyError;

impl Display for ParseCurrencyError {
	fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> fmt::Result {
		f.write_str("failed to parse 3 digit currency code")
	}
}

impl Error for ParseCurrencyError {}

impl FromStr for Currency {
	type Err = ParseCurrencyError;

	fn from_str(s: &str) -> Result<Self, Self::Err> {
		Self::from_osm_str(s).ok_or(ParseCurrencyError)
	}
}

impl FromOsmStr for Currency {
	fn from_osm_str(s: &str) -> Option<Self> {
		let b = s.as_bytes();
		match b {
			[a @ b'A'..=b'Z', b @ b'A'..=b'Z', c @ b'A'..=b'Z'] => Some(Self([*a, *b, *c])),
			_ => None,
		}
	}
}

impl ToOsmStr for Currency {
	fn to_osm_str(self) -> Option<std::borrow::Cow<'static, str>> {
		str::from_utf8(self.0.as_slice())
			.ok()
			.map(|s| s.to_owned().into())
	}
}

#[cfg(feature = "serde")]
impl Currency {
	fn to_compact_u16(self) -> u16 {
		u16::from(self.0[0] - b'A') << 10
			| u16::from(self.0[1] - b'A') << 5
			| u16::from(self.0[2] - b'A')
	}

	fn from_compact_u16(v: u16) -> Self {
		Self([
			u8::try_from(v >> 10 & 0b11111).unwrap() + b'A',
			u8::try_from(v >> 5 & 0b11111).unwrap() + b'A',
			u8::try_from(v & 0b11111).unwrap() + b'A',
		])
	}
}

#[cfg(feature = "serde")]
impl Serialize for Currency {
	fn serialize<S>(&self, serializer: S) -> Result<S::Ok, S::Error>
	where
		S: Serializer,
	{
		if serializer.is_human_readable() {
			self.to_string().serialize(serializer)
		} else {
			self.to_compact_u16().serialize(serializer)
		}
	}
}

#[cfg(feature = "serde")]
struct CurrencyVisitor;

#[cfg(feature = "serde")]
impl<'de> Visitor<'de> for CurrencyVisitor {
	type Value = Currency;

	fn expecting(&self, formatter: &mut std::fmt::Formatter) -> std::fmt::Result {
		formatter.write_str("3 letter uppercase currency code")
	}

	fn visit_str<E>(self, v: &str) -> Result<Self::Value, E>
	where
		E: serde::de::Error,
	{
		v.parse().map_err(serde::de::Error::custom)
	}
}

#[cfg(feature = "serde")]
impl<'de> Deserialize<'de> for Currency {
	fn deserialize<D>(deserializer: D) -> Result<Self, D::Error>
	where
		D: Deserializer<'de>,
	{
		if deserializer.is_human_readable() {
			deserializer.deserialize_str(CurrencyVisitor)
		} else {
			u16::deserialize(deserializer).map(Self::from_compact_u16)
		}
	}
}

#[cfg(test)]
mod tests {
	use crate::{
		FromOsmStr, ToOsmStr, quantity,
		units::{Currency, Quantity},
	};

	#[test]
	fn display() {
		assert_eq!(Currency([b'E', b'U', b'R']).to_string(), "EUR");
	}

	#[test]
	fn from_osm_str() {
		assert_eq!(
			Currency::from_osm_str("EUR"),
			Some(Currency([b'E', b'U', b'R']))
		);
		assert_eq!(
			Currency::from_osm_str("CAD"),
			Some(Currency([b'C', b'A', b'D']))
		);
		assert_eq!(Currency::from_osm_str("US$"), None);
		assert_eq!(Currency::from_osm_str("Hello"), None);
	}

	#[test]
	fn into_osm_str() {
		assert_eq!(
			Currency([b'J', b'P', b'Y']).to_osm_str(),
			Some("JPY".into())
		);
	}

	#[test]
	fn unit_from_osm_str() {
		assert_eq!(
			Quantity::from_osm_str("0.50 EUR"),
			Some(quantity!(
				0.50
				Currency([b'E', b'U', b'R'])
			))
		);
	}

	#[test]
	#[cfg(feature = "serde")]
	fn serde() {
		use serde_test::{Configure, Token, assert_tokens};

		let currency = Currency::from_osm_str("EUR").unwrap();

		assert_tokens(&currency.readable(), &[Token::Str("EUR")]);
		assert_tokens(&currency.compact(), &[Token::U16(0b_00100_10100_10001)]);
	}
}
