use std::fmt::{self, Debug, Display, Formatter};

use crate::{FromOsm, Taglike, get_tag};

mod format;

#[derive(Debug, Default, PartialEq, Eq)]
pub struct Addr<'a> {
	pub(crate) attention: Option<&'a str>, // tmp

	pub unit: Option<&'a str>,
	pub housename: Option<&'a str>,
	pub housenumber: Option<&'a str>,
	pub street: Option<&'a str>,
	pub residential: Option<&'a str>,
	pub quarter: Option<&'a str>,
	pub neighbourhood: Option<&'a str>,
	pub subdistrict: Option<&'a str>,
	pub district: Option<&'a str>,
	pub suburb: Option<&'a str>,
	pub postcode: Option<&'a str>,
	pub place: Option<&'a str>,
	pub postal_city: Option<&'a str>,
	pub city_district: Option<&'a str>,
	pub city: Option<&'a str>,
	pub town: Option<&'a str>,
	pub village: Option<&'a str>,
	pub hamlet: Option<&'a str>,
	pub municipality: Option<&'a str>,
	pub county: Option<&'a str>,
	pub archipelago: Option<&'a str>,
	pub island: Option<&'a str>,
	pub province: Option<&'a str>,
	pub state_district: Option<&'a str>,
	pub state: Option<&'a str>,
	pub region: Option<&'a str>,
	pub country: Option<&'a str>,
	pub continent: Option<&'a str>,
}

impl Display for Addr<'_> {
	fn fmt(&self, f: &mut Formatter<'_>) -> fmt::Result {
		format::format(self, f)
	}
}

impl<'a, T: Taglike<'a>> FromOsm<'a, T> for Addr<'a> {
	fn from_osm(tag: T) -> Option<Self> {
		Some(Self {
			attention: None,
			unit: get_tag!(tag, unit),
			housename: get_tag!(tag, housename),
			housenumber: get_tag!(tag, housenumber),
			street: get_tag!(tag, street),
			residential: get_tag!(tag, residential),
			quarter: get_tag!(tag, quarter),
			neighbourhood: get_tag!(tag, neighborhood),
			subdistrict: get_tag!(tag, subdistrict),
			district: get_tag!(tag, district),
			suburb: get_tag!(tag, suburb),
			postcode: get_tag!(tag, postcode),
			place: get_tag!(tag, place),
			postal_city: get_tag!(tag, postal_city),
			city_district: get_tag!(tag, city_district),
			city: get_tag!(tag, city),
			town: get_tag!(tag, town),
			village: get_tag!(tag, village),
			hamlet: get_tag!(tag, hamlet),
			municipality: get_tag!(tag, municipality),
			county: get_tag!(tag, county),
			archipelago: get_tag!(tag, archipelago),
			island: get_tag!(tag, island),
			province: get_tag!(tag, province),
			state_district: get_tag!(tag, state_district),
			state: get_tag!(tag, state),
			region: get_tag!(tag, region),
			country: get_tag!(tag, country),
			continent: get_tag!(tag, continent),
		})
	}
}

#[cfg(test)]
mod tests {
	use crate::addr::Addr;

	#[test]
	#[ignore]
	fn format_de() {
		let addr = Addr {
			housename: Some("Elbphilharmonie"),
			housenumber: Some("1"),
			street: Some("Platz der Deutschen Einheit"),
			postcode: Some("20457"),
			city: Some("Hamburg"),
			country: Some("DE"),
			..Default::default()
		};
		assert_eq!(
			addr.to_string(),
			"Elbphilharmonie\nPlatz der Deutschen Einheit 1\n20457 Hamburg"
		);
	}

	#[test]
	#[ignore]
	fn format_es() {
		let addr = Addr {
			housenumber: Some("401"),
			street: Some("Carrer de Mallorca"),
			postcode: Some("08001"),
			city: Some("Barcelona"),
			country: Some("ES"),
			..Default::default()
		};

		assert_eq!(addr.to_string(), "Carrer de Mallorca, 401\n08001 Barcelona");
	}
}
