use std::{
	borrow::Cow,
	collections::{HashMap, hash_map::Iter},
	iter::Map,
};

use rustc_hash::FxBuildHasher;

#[cfg(feature = "arbitrary")]
use arbitrary::Arbitrary;

pub type TagHasher = FxBuildHasher;
const TAG_HASHER: TagHasher = FxBuildHasher;

/// Tree of OSM tags (keys and values)
///
/// ```
/// use muv_osm::{Tag, new_tag};
/// use std::{borrow::Cow, collections::HashMap};
///
/// let tags = new_tag! {
/// 	amenity = "fire_station",
/// 	operator = "Mountain City services",
/// 	operator:type = "public",
/// };
///
/// let tree = Tag {
/// 	value: None,
/// 	subtags: HashMap::from_iter([
/// 		(Cow::Borrowed("amenity"), Tag {
/// 			value: Some(Cow::Borrowed("fire_station")),
/// 			subtags: HashMap::default(),
/// 		}),
/// 		(Cow::Borrowed("operator"), Tag {
/// 			value: Some(Cow::Borrowed("Mountain City services")),
/// 			subtags: HashMap::from_iter([(Cow::Borrowed("type"), Tag {
/// 				value: Some(Cow::Borrowed("public")),
/// 				subtags: HashMap::default(),
/// 			})]),
/// 		}),
/// 	]),
/// };
///
/// assert_eq!(tags, tree);
/// ```
#[derive(Debug, Default, Clone, PartialEq, Eq)]
#[cfg_attr(feature = "arbitrary", derive(Arbitrary))]
pub struct Tag<'a> {
	pub value: Option<Cow<'a, str>>,
	pub subtags: HashMap<Cow<'a, str>, Tag<'a>, TagHasher>,
}

impl Tag<'_> {
	#[must_use]
	pub fn new() -> Self {
		Self {
			value: None,
			subtags: HashMap::with_hasher(TAG_HASHER),
		}
	}

	#[must_use]
	pub(crate) fn with_capacity(capacity: usize) -> Self {
		Self {
			value: None,
			subtags: HashMap::with_capacity_and_hasher(capacity, TAG_HASHER),
		}
	}
}

impl<'a> Tag<'a> {
	pub fn set<K, V>(&mut self, key: K, val: V)
	where
		K: Into<Cow<'a, str>>,
		V: Into<Cow<'a, str>>,
	{
		self.subtags.entry(key.into()).or_default().value = Some(val.into());
	}

	/// Converts all contained `Cow::Borrowed` into `Cow::Owned`.
	///
	/// ```
	/// use muv_osm::Tag;
	/// use std::{borrow::Cow, collections::HashMap};
	///
	/// let tag = Tag {
	/// 	value: Some(Cow::Borrowed("asphalt")),
	/// 	subtags: HashMap::from_iter([(Cow::Borrowed("colour"), Tag {
	/// 		value: Some(Cow::Borrowed("red")),
	/// 		subtags: HashMap::default(),
	/// 	})]),
	/// };
	///
	/// let owned_tag = Tag {
	/// 	value: Some(Cow::Owned(String::from("asphalt"))),
	/// 	subtags: HashMap::from_iter([(Cow::Owned("colour".into()), Tag {
	/// 		value: Some(Cow::Owned(String::from("red"))),
	/// 		subtags: HashMap::default(),
	/// 	})]),
	/// };
	///
	/// assert_eq!(tag.into_owned(), owned_tag);
	/// ```
	#[must_use]
	pub fn into_owned(self) -> Tag<'static> {
		let value = self.value.map(|val| val.into_owned().into());
		let subtags: HashMap<_, Tag<'static>, TagHasher> = self
			.subtags
			.into_iter()
			.map(|(key, subtags)| (key.into_owned().into(), subtags.into_owned()))
			.collect();
		Tag { value, subtags }
	}
}

pub trait Taglike<'a>: Copy + 'a {
	type Iter: Iterator<Item = (&'a str, Self)>;

	#[must_use]
	fn value(self) -> Option<&'a str>;

	#[must_use]
	fn get(self, key: &str) -> Option<Self>;

	#[must_use]
	fn get_value(self, key: &str) -> Option<&'a str> {
		self.get(key)?.value()
	}

	#[must_use]
	fn iter_subtags(self) -> Self::Iter;
}

impl<'a> Taglike<'a> for &'a Tag<'a> {
	type Iter = Map<
		Iter<'a, Cow<'a, str>, Tag<'a>>,
		fn((&'a Cow<'a, str>, &'a Tag<'a>)) -> (&'a str, &'a Tag<'a>),
	>;

	fn value(self) -> Option<&'a str> {
		self.value.as_deref()
	}

	fn get(self, key: &str) -> Option<Self> {
		self.subtags.get(key)
	}

	fn iter_subtags(self) -> Self::Iter {
		self.subtags.iter().map(|(k, v)| (k, v))
	}
}

impl<'a, I> From<I> for Tag<'a>
where
	I: Into<Cow<'a, str>>,
{
	fn from(value: I) -> Self {
		Self {
			value: Some(value.into()),
			subtags: HashMap::default(),
		}
	}
}

impl<'a> Tag<'a> {
	#[inline]
	pub(crate) fn insert_raw(
		&mut self,
		key: impl Into<Cow<'a, str>>,
		val: impl Into<Cow<'a, str>>,
	) {
		let mut tag = self;
		let key: Cow<_> = key.into();
		for part in key.as_ref().split(':') {
			tag = tag.subtags.entry(part.to_owned().into()).or_default();
		}
		tag.value = Some(val.into());
	}

	fn fold<B, F: FnMut(B, (Cow<'a, str>, Cow<'a, str>)) -> B>(
		self,
		mut acc: B,
		f: &mut F,
		name: Cow<'a, str>,
	) -> B {
		for (key, tag) in self.subtags {
			let sub_name = if name.is_empty() {
				key
			} else {
				Cow::Owned(format!("{name}:{key}"))
			};
			acc = tag.fold(acc, f, sub_name);
		}

		if let Some(value) = self.value {
			acc = f(acc, (name, value));
		}

		acc
	}
}

impl<'a, K, V> FromIterator<(K, V)> for Tag<'a>
where
	K: Into<Cow<'a, str>>,
	V: Into<Cow<'a, str>>,
{
	fn from_iter<T: IntoIterator<Item = (K, V)>>(iter: T) -> Self {
		let iter = iter.into_iter();
		let (lower, _) = iter.size_hint();
		let mut tag = Tag::with_capacity(lower / 2);
		for (key, val) in iter {
			tag.insert_raw(key, val);
		}
		tag
	}
}

impl<'a> IntoIterator for Tag<'a> {
	type Item = (Cow<'a, str>, Cow<'a, str>);
	type IntoIter = IntoTagsIter<'a>;

	fn into_iter(self) -> Self::IntoIter {
		IntoTagsIter { tags: self }
	}
}

pub struct IntoTagsIter<'a> {
	tags: Tag<'a>,
}

impl<'a> Iterator for IntoTagsIter<'a> {
	type Item = (Cow<'a, str>, Cow<'a, str>);

	fn next(&mut self) -> Option<Self::Item> {
		todo!()
	}

	fn fold<B, F>(self, init: B, mut f: F) -> B
	where
		Self: Sized,
		F: FnMut(B, Self::Item) -> B,
	{
		self.tags.fold(init, &mut f, Cow::Borrowed(""))
	}
}

#[cfg(test)]
mod tests {
	use std::borrow::Cow;

	use crate::{Tag, new_tag};

	#[test]
	fn parse_tags() {
		let list = vec![
			("highway", "primary"),
			("maxspeed", "70"),
			("maxspeed:bus", "60"),
			("maxspeed:hgv", "50"),
		];

		let tags: Tag = list.into_iter().collect();

		assert_eq!(tags, Tag {
			value: None,
			subtags: [
				("highway".into(), "primary".into()),
				("maxspeed".into(), Tag {
					value: Some("70".into()),
					subtags: [("bus".into(), "60".into()), ("hgv".into(), "50".into())]
						.into_iter()
						.collect()
				})
			]
			.into_iter()
			.collect()
		});
	}

	#[test]
	fn collect() {
		let tags = new_tag! {
			highway = "primary",
			maxspeed = "70",
			maxspeed:bus = "60",
			maxspeed:hgv = "50",
		};

		// let mut list: Vec<_> = tags.into_iter().collect(); // TODO
		let mut list = Vec::new();
		tags.into_iter().for_each(|e| list.push(e));

		list.sort_unstable();

		assert_eq!(list, vec![
			(Cow::Borrowed("highway"), Cow::Borrowed("primary")),
			(Cow::Borrowed("maxspeed"), Cow::Borrowed("70")),
			(Cow::Borrowed("maxspeed:bus"), Cow::Borrowed("60")),
			(Cow::Borrowed("maxspeed:hgv"), Cow::Borrowed("50")),
		]);
	}
}
