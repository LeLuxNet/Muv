use serde::{
	Deserialize, Deserializer, Serialize, Serializer,
	de::{self, MapAccess, Visitor},
	ser::SerializeMap,
};

use std::{borrow::Cow, fmt};

use crate::{Tag, Taglike};

impl Tag<'_> {
	fn len(&self) -> usize {
		usize::from(self.value.is_some()) + self.subtags.values().map(Tag::len).sum::<usize>()
	}

	fn serialize_to_map<S>(&self, map: &mut S, name: &str) -> Result<(), S::Error>
	where
		S: SerializeMap,
	{
		if let Some(value) = self.value() {
			map.serialize_entry(name, &value)?;
		}
		for (key, tag) in &self.subtags {
			if name.is_empty() {
				tag.serialize_to_map(map, key)
			} else {
				tag.serialize_to_map(map, format!("{name}:{key}").as_str())
			}?;
		}
		Ok(())
	}
}

impl Serialize for Tag<'_> {
	fn serialize<S>(&self, serializer: S) -> Result<S::Ok, S::Error>
	where
		S: Serializer,
	{
		let mut map = serializer.serialize_map(Some(self.len()))?;
		self.serialize_to_map(&mut map, "")?;
		map.end()
	}
}

struct CowStr<'de>(Cow<'de, str>);

impl<'de> Deserialize<'de> for CowStr<'de> {
	fn deserialize<D>(deserializer: D) -> Result<Self, D::Error>
	where
		D: Deserializer<'de>,
	{
		deserializer.deserialize_str(CowStrVisitor).map(Self)
	}
}

struct CowStrVisitor;

impl<'de> Visitor<'de> for CowStrVisitor {
	type Value = Cow<'de, str>;

	fn expecting(&self, formatter: &mut fmt::Formatter) -> fmt::Result {
		formatter.write_str("a string")
	}

	fn visit_borrowed_str<E>(self, v: &'de str) -> Result<Self::Value, E>
	where
		E: de::Error,
	{
		Ok(v.into())
	}

	fn visit_str<E>(self, v: &str) -> Result<Self::Value, E>
	where
		E: de::Error,
	{
		Ok(v.to_owned().into())
	}

	fn visit_string<E>(self, v: String) -> Result<Self::Value, E>
	where
		E: de::Error,
	{
		Ok(v.into())
	}
}

struct TagVisitor;

impl<'de> Visitor<'de> for TagVisitor {
	type Value = Tag<'de>;

	fn expecting(&self, formatter: &mut fmt::Formatter) -> fmt::Result {
		formatter.write_str("a map with strings as keys and values")
	}

	fn visit_map<A>(self, mut map: A) -> Result<Self::Value, A::Error>
	where
		A: MapAccess<'de>,
	{
		let mut tag = map
			.size_hint()
			.map_or_else(Tag::new, |c| Tag::with_capacity(c / 2));
		while let Some((key, val)) = map.next_entry::<CowStr<'de>, CowStr<'de>>()? {
			tag.insert_raw(key.0, val.0);
		}
		Ok(tag)
	}
}

impl<'de> Deserialize<'de> for Tag<'de> {
	fn deserialize<D>(deserializer: D) -> Result<Self, D::Error>
	where
		D: Deserializer<'de>,
	{
		deserializer.deserialize_map(TagVisitor)
	}
}

struct OwnedTagVisitor;

impl<'de> Visitor<'de> for OwnedTagVisitor {
	type Value = Tag<'static>;

	fn expecting(&self, formatter: &mut fmt::Formatter) -> fmt::Result {
		formatter.write_str("a map with strings as keys and values")
	}

	fn visit_map<A>(self, mut map: A) -> Result<Self::Value, A::Error>
	where
		A: MapAccess<'de>,
	{
		let mut tag = map
			.size_hint()
			.map_or_else(Tag::new, |c| Tag::with_capacity(c / 2));
		while let Some((key, val)) = map.next_entry::<Cow<'static, str>, Cow<'static, str>>()? {
			tag.insert_raw(key, val);
		}
		Ok(tag)
	}
}

impl Tag<'static> {
	#[allow(clippy::missing_errors_doc)]
	pub fn deserialize_owned<'de, D>(deserializer: D) -> Result<Self, D::Error>
	where
		D: Deserializer<'de>,
	{
		deserializer.deserialize_map(OwnedTagVisitor)
	}
}

#[cfg(test)]
mod tests {
	use crate::{Tag, new_tag};
	use serde::{Deserialize, Serialize};
	use serde_test::{Token, assert_tokens};

	#[test]
	fn serde() {
		let tags = new_tag! {
			highway = "residential",
			lit = "yes",
		};

		assert_tokens(&tags, &[
			Token::Map { len: Some(2) },
			Token::Str("highway"),
			Token::Str("residential"),
			Token::Str("lit"),
			Token::Str("yes"),
			Token::MapEnd,
		]);
	}

	#[test]
	fn serde_owned() {
		#[derive(Debug, PartialEq, Serialize, Deserialize)]
		struct Data {
			#[serde(deserialize_with = "Tag::deserialize_owned")]
			tags: Tag<'static>,
		}

		let tags = new_tag! {
			shop = "bakery",
		};

		assert_tokens(&Data { tags }, &[
			Token::Struct {
				name: "Data",
				len: 1,
			},
			Token::Str("tags"),
			Token::Map { len: Some(1) },
			Token::Str("shop"),
			Token::Str("bakery"),
			Token::MapEnd,
			Token::StructEnd,
		]);
	}
}
