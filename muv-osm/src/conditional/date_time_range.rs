use std::{
	borrow::Cow,
	fmt::{self, Debug, Display, Formatter},
	str::FromStr,
};

use muv_geo::Location;
use opening_hours::OpeningHours;
use opening_hours_syntax::Error;

use crate::{FromOsmStr, ToOsmStr, conditional::MatchDateTime};

#[cfg(feature = "conditional-localized")]
use opening_hours::{
	Context,
	localization::{Coordinates, Localize, TzLocation},
};

#[cfg(feature = "serde")]
use serde::{
	Deserialize, Deserializer, Serialize, Serializer,
	de::{self, Visitor},
};

#[derive(Clone)]
pub struct DateTimeRange {
	oh: OpeningHours,
	s: String,
}

pub type DateTimeRangeError = Error;

impl FromStr for DateTimeRange {
	type Err = DateTimeRangeError;
	fn from_str(s: &str) -> Result<Self, Self::Err> {
		Ok(Self {
			oh: OpeningHours::parse(s)?,
			s: s.to_owned(),
		})
	}
}

impl TryFrom<String> for DateTimeRange {
	type Error = DateTimeRangeError;
	fn try_from(value: String) -> Result<Self, Self::Error> {
		Ok(Self {
			oh: OpeningHours::parse(&value)?,
			s: value,
		})
	}
}

impl FromOsmStr for DateTimeRange {
	fn from_osm_str(s: &str) -> Option<Self> {
		s.parse().ok()
	}
}

impl ToOsmStr for DateTimeRange {
	fn to_osm_str(self) -> Option<Cow<'static, str>> {
		Some(self.s.into())
	}
}

impl Display for DateTimeRange {
	fn fmt(&self, f: &mut Formatter<'_>) -> fmt::Result {
		write!(f, "{}", self.s)
	}
}

impl Debug for DateTimeRange {
	fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> fmt::Result {
		write!(f, "{}", self.s)
	}
}

impl PartialEq for DateTimeRange {
	fn eq(&self, other: &Self) -> bool {
		self.s == other.s
	}
}

#[cfg(feature = "serde")]
impl Serialize for DateTimeRange {
	fn serialize<S: Serializer>(&self, serializer: S) -> Result<S::Ok, S::Error> {
		self.s.serialize(serializer)
	}
}

#[cfg(feature = "serde")]
struct DateTimeRangeVisitor;

#[cfg(feature = "serde")]
impl<'de> Visitor<'de> for DateTimeRangeVisitor {
	type Value = DateTimeRange;

	fn expecting(&self, formatter: &mut Formatter) -> fmt::Result {
		formatter.write_str("a datetime range in opening_hours format")
	}

	fn visit_string<E: de::Error>(self, s: String) -> Result<Self::Value, E> {
		DateTimeRange::try_from(s).map_err(de::Error::custom)
	}

	fn visit_str<E: de::Error>(self, s: &str) -> Result<Self::Value, E> {
		DateTimeRange::from_str(s).map_err(de::Error::custom)
	}
}

#[cfg(feature = "serde")]
impl<'de> Deserialize<'de> for DateTimeRange {
	fn deserialize<D: Deserializer<'de>>(deserializer: D) -> Result<Self, D::Error> {
		deserializer.deserialize_string(DateTimeRangeVisitor)
	}
}

impl DateTimeRange {
	#[must_use]
	pub fn matches(&self, date_time: MatchDateTime, location: Location) -> bool {
		#[cfg(not(feature = "conditional-localized"))]
		{
			let _ = location;
			let MatchDateTime::Local(local) = date_time;
			self.oh.is_open(local)
		}

		#[cfg(feature = "conditional-localized")]
		{
			let loc = location.canonicalize();
			let coords = Coordinates::new(loc.lat, loc.lon).unwrap();
			let localize = TzLocation::from_coords(coords);

			let time_with_tz = match date_time {
				MatchDateTime::Local(local) => localize.datetime(local),
				MatchDateTime::Timezone(tz) => tz,
			};

			let ctx = Context::default().with_locale(localize);
			let coord_oh = self.oh.clone().with_context(ctx);
			coord_oh.is_open(time_with_tz)
		}
	}
}
