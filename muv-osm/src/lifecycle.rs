use crate::{FromOsmStr, Tag, Taglike, ToOsmStr, simple_enum};

simple_enum!(Lifecycle(Default) {
	Proposed,
	Planned,
	/// In construction
	Construction,
	/// The state of most everyday objects
	#[default]
	#[osm_str()]
	Normal,
	/// Currently not used, but can easily be reactivated
	Disused,
	/// Fallen into serious disrepair needing serious effort to reactivate
	Abandoned,
	/// Ruins of the object remain
	Ruins,
	/// The feature is removed but it's historical effect is still visible
	///
	/// One example of this could be a former railway, that now only shows as a line without
	/// buildings stretching through a city.
	Razed,
	Demolished,
	Destroyed,
	Removed,
});

impl Lifecycle {
	pub fn from_osm<'a, T: FromOsmStr>(
		tags: &'a Tag<'a>,
		key: &str,
	) -> Option<(Self, Option<T>, &'a Tag<'a>)> {
		let Some((base, base_val)) = tags.get(key).and_then(|base| Some((base, base.value()?)))
		else {
			// prefix method
			return tags
				.subtags
				.iter()
				.find_map(|(lifecycle_str, lifecycle_base)| {
					let lifecycle = Self::from_osm_str(lifecycle_str)?;
					let base = lifecycle_base.get(key)?;
					Some((lifecycle, T::from_osm_str(base.value()?), base))
				});
		};

		if let Some(lifecycle) = Self::from_osm_str(base_val) {
			// value method
			let val = tags.get_value(base_val).and_then(T::from_osm_str);
			Some((lifecycle, val, base))
		} else {
			// yes method
			let lifecycle = tags
				.subtags
				.iter()
				.filter(|(_, value)| {
					value
						.value()
						.is_some_and(|value| bool::from_osm_str(value).unwrap_or_default())
				})
				.find_map(|(lifecycle_str, _)| Self::from_osm_str(lifecycle_str))
				.unwrap_or_default();
			Some((lifecycle, T::from_osm_str(base_val), base))
		}
	}
}

#[cfg(test)]
mod tests {
	use crate::{FromOsmStr, Lifecycle, get_tag, new_tag};

	#[derive(Debug, PartialEq, FromOsmStr)]
	enum HighwayClassification {
		Primary,
		Secondary,
		Tertiary,
	}

	#[test]
	fn value() {
		let tags = new_tag! {
			highway = "construction",
			construction = "secondary",
		};

		let (lifecycle, value, base) = Lifecycle::from_osm(&tags, "highway").unwrap();

		assert_eq!(lifecycle, Lifecycle::Construction);
		assert_eq!(base, get_tag!(tags, highway:).unwrap());
		assert_eq!(value, Some(HighwayClassification::Secondary));
	}

	#[test]
	fn prefix() {
		let tags = new_tag! {
			razed:highway = "tertiary",
		};

		let (lifecycle, value, base) = Lifecycle::from_osm(&tags, "highway").unwrap();

		assert_eq!(lifecycle, Lifecycle::Razed);
		assert_eq!(base, get_tag!(tags, razed:highway:).unwrap());
		assert_eq!(value, Some(HighwayClassification::Tertiary));
	}

	#[test]
	fn yes() {
		let tags = new_tag! {
			highway = "primary",
			abandoned = "yes",
		};

		let (lifecycle, value, base) = Lifecycle::from_osm(&tags, "highway").unwrap();

		assert_eq!(lifecycle, Lifecycle::Abandoned);
		assert_eq!(base, get_tag!(tags, highway:).unwrap());
		assert_eq!(value, Some(HighwayClassification::Primary));
	}
}
