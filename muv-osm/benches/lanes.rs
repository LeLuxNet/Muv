use std::{fs::File, path::PathBuf};

use criterion::{BatchSize, Criterion, black_box, criterion_group, criterion_main};
use muv_osm::{Tag, get_tag, lanes::highway_lanes, reader::Reader};

fn load_pbf(name: &str) -> Vec<Vec<(String, String)>> {
	let path: PathBuf = [
		env!("CARGO_MANIFEST_DIR"),
		"benches",
		&format!("{}.osm.pbf", name),
	]
	.iter()
	.collect();

	let r = Reader::new(File::open(path).unwrap());
	let mut tags = Vec::new();

	r.for_each(|e| {
		if let Element::Way(e) = e {
			tags.push(e.tags.map(|(k, v)| (k.to_owned(), v.to_owned())).collect());
		}
	})
	.unwrap();

	tags
}

#[cfg(feature = "lanes")]
fn bench(c: &mut Criterion) {
	let data = load_pbf("bremen");
	let data = &data[..1_000];

	let data_tags: Vec<Tag> = data
		.iter()
		.map(|tags| tags.iter().map(|(k, v)| (k.as_str(), v.as_str())).collect())
		.collect();

	{
		c.bench_function("Tag::from_iter", |b| {
			b.iter(|| {
				data.iter().for_each(|tags| {
					black_box(
						tags.iter()
							.map(|(k, v)| (k.as_str(), v.as_str()))
							.collect::<Tag>(),
					);
				})
			})
		});

		c.bench_function("Tag::get_value name", |b| {
			b.iter(|| {
				data_tags.iter().for_each(|tags| {
					black_box(get_tag!(tags, name));
				})
			})
		});
		c.bench_function("Tag::get_value oneway:bicycle", |b| {
			b.iter(|| {
				data_tags.iter().for_each(|tags| {
					black_box(get_tag!(tags, oneway: bicycle));
				})
			})
		});

		c.bench_function("Tag::into_iter", |b| {
			b.iter_batched(
				|| data_tags.clone(),
				|data| {
					data.into_iter().for_each(|tags| {
						tags.into_iter().for_each(|(k, v)| {
							black_box((k, v));
						});
					})
				},
				BatchSize::SmallInput,
			)
		});
	}

	{
		c.bench_function("highway_lanes", |b| {
			b.iter(|| {
				data_tags
					.iter()
					.map(|tags| {
						highway_lanes(tags, &["DE"])
							.unwrap()
							.lanes
							.into_iter()
							.for_each(|l| {
								black_box(l);
							})
					})
					.count()
			})
		});
	}
}

criterion_group!(benches, bench);
criterion_main!(benches);
