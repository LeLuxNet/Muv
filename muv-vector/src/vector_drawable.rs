use std::fmt::{self, Write};

use crate::{Cap, Graphic, Join, Rule};

pub fn write<W: Write>(mut w: W, graphic: &Graphic) -> fmt::Result {
	#[allow(clippy::float_cmp)]
	{
		assert_eq!(graphic.view.top, 0.0);
		assert_eq!(graphic.view.left, 0.0);
	}

	let width = graphic.view.width();
	let height = graphic.view.height();

	writeln!(
		w,
		r#"<vector xmlns:android="http://schemas.android.com/apk/res/android" android:width="{width}dp" android:height="{height}dp" android:viewportWidth="{width}" android:viewportHeight="{height}">"#,
	)?;

	for path in &graphic.children {
		write!(w, r#"<path android:pathData="{}""#, path.commands)?;

		if let Some(stroke) = path.stroke {
			let (r, g, b) = stroke.color.to_rgb8();
			write!(
				w,
				r##" android:strokeColor="#{r:02x}{g:02x}{b:02x}" android:strokeWidth="{}""##,
				stroke.width,
			)?;

			match stroke.cap {
				Cap::Butt => {}
				Cap::Round => write!(w, r#" android:strokeLineCap="round""#)?,
				Cap::Square => write!(w, r#" android:strokeLineCap="square""#)?,
			}

			match stroke.join {
				Join::Miter { limit: 4.0 } => {}
				Join::Miter { limit } => write!(w, r#" android:strokeMiterLimit="{limit}""#)?,
				Join::Round => write!(w, r#" android:strokeLineJoin="round""#)?,
				Join::Bevel => write!(w, r#" android:strokeLineJoin="bevel""#)?,
			}
		}

		if let Some(fill) = path.fill {
			let (r, g, b) = fill.color.to_rgb8();
			write!(w, r##" android:fillColor="#{r:02x}{g:02x}{b:02x}""##)?;

			match fill.rule {
				Rule::NonZero => {}
				Rule::EvenOdd => write!(w, r#" android:fillType="evenOdd""#)?,
			}
		}

		writeln!(w, "/>")?;
	}

	write!(w, "</vector>")
}
