use std::io::Read;

use mupdf::{
	ColorParams, Colorspace, Device, LineCap, LineJoin, Matrix, NativeDevice, PathWalker, Rect,
	StrokeState, Text,
};

use crate::{Bbox, Cap, Color, Command, Commands, Fill, Graphic, Join, Path, Point, Rule, Stroke};

pub struct Document(mupdf::Document);

impl Document {
	pub fn from_reader<R: Read>(mut r: R, password: Option<&str>) -> Self {
		let mut data = Vec::new();
		r.read_to_end(&mut data).unwrap();

		let mut doc = mupdf::Document::from_bytes(&data, "").unwrap();
		if let Some(password) = password {
			doc.authenticate(password).unwrap();
		}

		Self(doc)
	}

	#[must_use]
	pub fn page(&self, n: u32) -> Option<Page> {
		self.0.load_page(n.try_into().unwrap()).ok().map(Page)
	}
}

pub struct Page(mupdf::Page);

impl Page {
	#[must_use]
	pub fn to_graphic(&self) -> Graphic {
		let mut path_dev = PathDevice::default();
		let path = Device::from_native(&mut path_dev).unwrap();
		self.0.run(&path, &Matrix::default()).unwrap();

		Graphic {
			view: self.0.bounds().unwrap().into(),
			children: path_dev.paths,
		}
	}
}

impl From<Rect> for Bbox {
	fn from(value: Rect) -> Self {
		Self {
			top: value.y0,
			bottom: value.y1,
			left: value.x0,
			right: value.x1,
		}
	}
}

#[derive(Default)]
struct ToPath {
	commands: Commands,
}

impl PathWalker for ToPath {
	fn move_to(&mut self, x: f32, y: f32) {
		self.commands.push(Command::MoveTo { p: Point { x, y } });
	}

	fn line_to(&mut self, x: f32, y: f32) {
		self.commands.push(Command::LineTo { p: Point { x, y } });
	}

	fn curve_to(&mut self, cx1: f32, cy1: f32, cx2: f32, cy2: f32, ex: f32, ey: f32) {
		self.commands.push(Command::CubicBezierTo {
			c1: Point { x: cx1, y: cy1 },
			c2: Point { x: cx2, y: cy2 },
			p: Point { x: ex, y: ey },
		});
	}

	fn close(&mut self) {
		self.commands.push(Command::Close);
	}
}

fn to_commands(path: &mupdf::Path) -> Commands {
	let mut walker = ToPath::default();
	path.walk(&mut walker).unwrap();
	walker.commands
}

#[derive(Default)]
struct PathDevice {
	paths: Vec<Path>,
}

impl NativeDevice for PathDevice {
	fn fill_path(
		&mut self,
		path: &mupdf::Path,
		even_odd: bool,
		cmt: Matrix,
		color_space: &Colorspace,
		color: &[f32],
		_alpha: f32,
		cp: ColorParams,
	) {
		let mut path = path.clone();
		path.transform(&cmt).unwrap();

		self.paths.push(Path {
			commands: to_commands(&path),
			stroke: None,
			fill: Some(Fill {
				color: map_color(color_space, color, cp),
				rule: if even_odd {
					Rule::EvenOdd
				} else {
					Rule::NonZero
				},
			}),
			text: None,
		});
	}

	fn stroke_path(
		&mut self,
		path: &mupdf::Path,
		stroke_state: &StrokeState,
		cmt: Matrix,
		color_space: &Colorspace,
		color: &[f32],
		_alpha: f32,
		cp: ColorParams,
	) {
		let mut path = path.clone();
		path.transform(&cmt).unwrap();

		self.paths.push(Path {
			commands: to_commands(&path),
			stroke: Some(Stroke {
				color: map_color(color_space, color, cp),
				width: stroke_state.line_width() * cmt.expansion(),
				cap: stroke_state.start_cap().into(),
				join: match stroke_state.line_join() {
					LineJoin::Miter | LineJoin::MiterXps => Join::Miter {
						limit: stroke_state.miter_limit(),
					},
					LineJoin::Round => Join::Round,
					LineJoin::Bevel => Join::Bevel,
				},
			}),
			fill: None,
			text: None,
		});
	}

	fn fill_text(
		&mut self,
		text: &Text,
		cmt: Matrix,
		color_space: &Colorspace,
		color: &[f32],
		_alpha: f32,
		cp: ColorParams,
	) {
		for span in text.spans() {
			let font = span.font();
			let trm = span.trm();

			for item in span.items() {
				let mut trm = trm.clone();
				trm.e = item.x();
				trm.f = item.y();
				trm.concat(cmt.clone());

				let text = char::from_u32(item.ucs().try_into().unwrap()).unwrap();

				let outline = font.outline_glyph_with_ctm(item.gid(), &trm).unwrap();
				if let Some(outline) = outline {
					let path = Path {
						commands: to_commands(&outline),
						stroke: None,
						fill: Some(Fill {
							color: map_color(color_space, color, cp),
							rule: Rule::NonZero,
						}),
						text: Some(text),
					};
					self.paths.push(path);
				} else {
					eprintln!("failed to render {text:?} in font {:?}", font.name());
				}
			}
		}
	}
}

impl From<LineCap> for Cap {
	fn from(value: LineCap) -> Self {
		match value {
			LineCap::Butt | LineCap::Triangle => Self::Butt,
			LineCap::Round => Self::Round,
			LineCap::Square => Self::Square,
		}
	}
}

fn map_color(color_space: &Colorspace, color: &[f32], color_params: ColorParams) -> Color {
	let device_rgb = Colorspace::device_rgb();
	if color_space == &device_rgb {
		Color::Rgb {
			r: color[0],
			g: color[1],
			b: color[2],
		}
	} else {
		let mut out = [0.0; 3];
		color_space
			.convert_color_into(color, &device_rgb, &mut out, None, color_params)
			.unwrap();
		Color::Rgb {
			r: out[0],
			g: out[1],
			b: out[2],
		}
	}
}
