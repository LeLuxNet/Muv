use std::fmt::{self, Write};

use crate::{Cap, Graphic, Join, Rule};

pub fn write<W: Write>(mut w: W, graphic: &Graphic) -> fmt::Result {
	let width = graphic.view.width();
	let height = graphic.view.height();

	writeln!(
		w,
		r#"<svg version="1.1" xmlns="http://www.w3.org/2000/svg" width="{width}" height="{height}" viewBox="{} {} {width} {height}">"#,
		graphic.view.left, graphic.view.top,
	)?;

	for path in &graphic.children {
		write!(w, r#"<path d="{}""#, path.commands)?;

		if let Some(stroke) = path.stroke {
			let (r, g, b) = stroke.color.to_rgb8();
			write!(
				w,
				r##" stroke="#{r:02x}{g:02x}{b:02x}" stroke-width="{}""##,
				stroke.width,
			)?;

			match stroke.cap {
				Cap::Butt => {}
				Cap::Round => write!(w, r#" stroke-linecap="round""#)?,
				Cap::Square => write!(w, r#" stroke-linecap="square""#)?,
			}

			match stroke.join {
				Join::Miter { limit: 4.0 } => {}
				Join::Miter { limit } => write!(w, r#" stroke-miterlimit="{limit}""#)?,
				Join::Round => write!(w, r#" stroke-linejoin="round""#)?,
				Join::Bevel => write!(w, r#" stroke-linejoin="bevel""#)?,
			}
		}

		if let Some(fill) = path.fill {
			let (r, g, b) = fill.color.to_rgb8();
			write!(w, r##" fill="#{r:02x}{g:02x}{b:02x}""##)?;

			match fill.rule {
				Rule::NonZero => {}
				Rule::EvenOdd => write!(w, r#" fill-rule="evenodd""#)?,
			}
		} else {
			write!(w, r#" fill="none""#)?;
		}

		if let Some(text) = path.text {
			write!(w, r#" data-text="{text}""#)?;
		}

		writeln!(w, "/>")?;
	}

	write!(w, "</svg>")
}
