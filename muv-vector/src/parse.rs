use crate::{Command, Point};

pub struct CommandsParser<'a> {
	s: &'a str,
	implied_command: Option<CommandType>,

	last_move_pos: Point,
	last_c_pos: Option<Point>,
	pos: Point,
}

impl<'a> CommandsParser<'a> {
	#[must_use]
	pub const fn new(s: &'a str) -> Self {
		Self {
			s,
			implied_command: None,

			last_move_pos: Point { x: 0.0, y: 0.0 },
			last_c_pos: None,
			pos: Point { x: 0.0, y: 0.0 },
		}
	}

	fn coord(&mut self) -> Option<f32> {
		let s = self.s.trim_ascii_start();

		let mut i = 0;
		if matches!(s.bytes().next()?, b'+' | b'-') {
			i += 1;
		}

		let mut seen_point = false;
		for b in s[i..].bytes() {
			if b.is_ascii_digit() {
				i += 1;
				continue;
			}
			if b == b'.' && !seen_point {
				i += 1;
				seen_point = true;
				continue;
			}
			break;
		}
		if i == 0 {
			return None;
		}

		let c = s[..i].parse().unwrap();
		self.s = s[i..].trim_ascii_start();

		if self.s.bytes().next() == Some(b',') {
			self.s = &self.s[1..];
		}

		Some(c)
	}

	fn coord_pair(&mut self) -> Option<Point> {
		let x = self.coord()?;
		let y = self.coord()?;
		Some(Point { x, y })
	}
}

impl Iterator for CommandsParser<'_> {
	type Item = Command;

	#[allow(clippy::too_many_lines)]
	fn next(&mut self) -> Option<Self::Item> {
		self.s = self.s.trim_ascii_start();

		let command_b = self.s.as_bytes().first()?;
		let command = if command_b.is_ascii_alphabetic() {
			let c = CommandType::parse(*command_b).unwrap();
			self.s = &self.s[1..];
			c
		} else {
			self.implied_command.unwrap()
		};

		self.implied_command = Some(command);
		Some(match command {
			CommandType::MoveToAbs => {
				self.pos = self.coord_pair().unwrap();
				self.last_move_pos = self.pos;
				self.implied_command = Some(CommandType::LineToAbs);
				Command::MoveTo { p: self.pos }
			}
			CommandType::MoveToRel => {
				let offset = self.coord_pair().unwrap();
				self.pos += offset;
				self.last_move_pos = self.pos;
				self.implied_command = Some(CommandType::LineToRel);
				Command::MoveTo { p: self.pos }
			}

			CommandType::LineToAbs => {
				self.pos = self.coord_pair().unwrap();
				Command::LineTo { p: self.pos }
			}
			CommandType::LineToRel => {
				let offset = self.coord_pair().unwrap();
				self.pos += offset;
				Command::LineTo { p: self.pos }
			}

			CommandType::HorizontalLineToAbs => {
				self.pos.x = self.coord().unwrap();
				Command::LineTo { p: self.pos }
			}
			CommandType::HorizontalLineToRel => {
				self.pos.x += self.coord().unwrap();
				Command::LineTo { p: self.pos }
			}

			CommandType::VerticalLineToAbs => {
				self.pos.y = self.coord().unwrap();
				Command::LineTo { p: self.pos }
			}
			CommandType::VerticalLineToRel => {
				self.pos.y += self.coord().unwrap();
				Command::LineTo { p: self.pos }
			}

			CommandType::CurveToAbs => {
				let c1 = self.coord_pair().unwrap();
				let c2 = self.coord_pair().unwrap();
				self.last_c_pos = Some(c2);
				self.pos = self.coord_pair().unwrap();
				Command::CubicBezierTo {
					c1,
					c2,
					p: self.pos,
				}
			}
			CommandType::CurveToRel => {
				let c1 = self.pos + self.coord_pair().unwrap();
				let c2 = self.pos + self.coord_pair().unwrap();
				self.last_c_pos = Some(c2);
				let offset = self.coord_pair().unwrap();
				self.pos += offset;
				Command::CubicBezierTo {
					c1,
					c2,
					p: self.pos,
				}
			}

			CommandType::SmoothCurveToAbs => {
				let c1 = self
					.last_c_pos
					.filter(|_| self.implied_command.unwrap().is_curve())
					.map_or(self.pos, |c2| self.pos * 2.0 - c2);
				let c2 = self.coord_pair().unwrap();
				self.last_c_pos = Some(c2);
				self.pos = self.coord_pair().unwrap();
				Command::CubicBezierTo {
					c1,
					c2,
					p: self.pos,
				}
			}
			CommandType::SmoothCurveToRel => {
				let c1 = self
					.last_c_pos
					.filter(|_| self.implied_command.unwrap().is_curve())
					.map_or(self.pos, |c2| self.pos * 2.0 - c2);
				let c2 = self.pos + self.coord_pair().unwrap();
				self.last_c_pos = Some(c2);
				let offset = self.coord_pair().unwrap();
				self.pos += offset;
				Command::CubicBezierTo {
					c1,
					c2,
					p: self.pos,
				}
			}

			CommandType::QuadraticCurveToAbs => {
				let origin = self.pos;
				let c = self.coord_pair().unwrap();
				self.last_c_pos = Some(c);
				self.pos = self.coord_pair().unwrap();
				Command::quadratic_bezier_to(origin, c, self.pos)
			}
			CommandType::QuadraticCurveToRel => {
				let origin = self.pos;
				let c = self.pos + self.coord_pair().unwrap();
				self.last_c_pos = Some(c);
				let offset = self.coord_pair().unwrap();
				self.pos += offset;
				Command::quadratic_bezier_to(origin, c, self.pos)
			}

			CommandType::SmoothQuadraticCurveToAbs => {
				let origin = self.pos;
				let c = self
					.last_c_pos
					.filter(|_| self.implied_command.unwrap().is_quadratic_curve())
					.map_or(self.pos, |c| self.pos * 2.0 - c);
				self.last_c_pos = Some(c);
				self.pos = self.coord_pair().unwrap();
				Command::quadratic_bezier_to(origin, c, self.pos)
			}
			CommandType::SmoothQuadraticCurveToRel => {
				let origin = self.pos;
				let c = self
					.last_c_pos
					.filter(|_| self.implied_command.unwrap().is_quadratic_curve())
					.map_or(self.pos, |c| self.pos * 2.0 - c);
				self.last_c_pos = Some(c);
				let offset = self.coord_pair().unwrap();
				self.pos += offset;
				Command::quadratic_bezier_to(origin, c, self.pos)
			}

			CommandType::Close => {
				self.pos = self.last_move_pos;
				Command::Close
			}
		})
	}
}

#[derive(Clone, Copy)]
#[repr(u8)]
enum CommandType {
	MoveToAbs = b'M',
	MoveToRel = b'm',

	LineToAbs = b'L',
	LineToRel = b'l',

	HorizontalLineToAbs = b'H',
	HorizontalLineToRel = b'h',

	VerticalLineToAbs = b'V',
	VerticalLineToRel = b'v',

	CurveToAbs = b'C',
	CurveToRel = b'c',

	SmoothCurveToAbs = b'S',
	SmoothCurveToRel = b's',

	QuadraticCurveToAbs = b'Q',
	QuadraticCurveToRel = b'q',

	SmoothQuadraticCurveToAbs = b'T',
	SmoothQuadraticCurveToRel = b't',

	Close = b'z',
}

impl CommandType {
	const fn parse(b: u8) -> Option<Self> {
		Some(match b {
			b'M' => Self::MoveToAbs,
			b'm' => Self::MoveToRel,

			b'L' => Self::LineToAbs,
			b'l' => Self::LineToRel,

			b'H' => Self::HorizontalLineToAbs,
			b'h' => Self::HorizontalLineToRel,

			b'V' => Self::VerticalLineToAbs,
			b'v' => Self::VerticalLineToRel,

			b'C' => Self::CurveToAbs,
			b'c' => Self::CurveToRel,

			b'S' => Self::SmoothCurveToAbs,
			b's' => Self::SmoothCurveToRel,

			b'Q' => Self::QuadraticCurveToAbs,
			b'q' => Self::QuadraticCurveToRel,

			b'T' => Self::SmoothQuadraticCurveToAbs,
			b't' => Self::SmoothQuadraticCurveToRel,

			b'Z' | b'z' => Self::Close,

			_ => return None,
		})
	}

	const fn is_curve(self) -> bool {
		matches!(
			self,
			Self::CurveToAbs | Self::CurveToRel | Self::SmoothCurveToAbs | Self::SmoothCurveToRel
		)
	}

	const fn is_quadratic_curve(self) -> bool {
		matches!(
			self,
			Self::QuadraticCurveToAbs
				| Self::QuadraticCurveToRel
				| Self::SmoothQuadraticCurveToAbs
				| Self::SmoothQuadraticCurveToRel
		)
	}
}

#[cfg(test)]
mod tests {
	use crate::{Command, CommandsParser, Point};

	fn assert_parse(paths: &[&str], expected: &[Command]) {
		for path in paths {
			let got: Vec<_> = CommandsParser::new(path).collect();
			assert_eq!(expected, &*got);
		}
	}

	#[test]
	fn triangle() {
		assert_parse(
			&[
				"M 100 100 L 300 100 L 200 300 z",
				"M 100 100 l 200 0 l -100 200 z",
				"M   100 , 100  L  300   100  L   200  ,300   z",
				"M 100 100 300 100 200 300 z",
				"M 100,100 300,100 200,300 z",
				"M100,100 300,100 200,300z",
			],
			&[
				Command::MoveTo {
					p: Point { x: 100.0, y: 100.0 },
				},
				Command::LineTo {
					p: Point { x: 300.0, y: 100.0 },
				},
				Command::LineTo {
					p: Point { x: 200.0, y: 300.0 },
				},
				Command::Close,
			],
		);
	}

	#[test]
	fn cubic() {
		assert_parse(
			&[
				"M100,200 C100,100 250,100 250,200 S400,300 400,200",
				"M100,200 C100,100,250,100,250,200 S400,300 400,200",
				"M100,200 c0,-100 150,-100 150,0 s150,100 150,0",
			],
			&[
				Command::MoveTo {
					p: Point { x: 100.0, y: 200.0 },
				},
				Command::CubicBezierTo {
					c1: Point { x: 100.0, y: 100.0 },
					c2: Point { x: 250.0, y: 100.0 },
					p: Point { x: 250.0, y: 200.0 },
				},
				Command::CubicBezierTo {
					c1: Point { x: 250.0, y: 300.0 },
					c2: Point { x: 400.0, y: 300.0 },
					p: Point { x: 400.0, y: 200.0 },
				},
			],
		);
	}

	#[test]
	fn quadratic() {
		assert_parse(
			&[
				"M200,300 Q400,50 600,300 T1000,300",
				"M200,300 Q400,50,600,300 Q800,550 1000,300",
				"M200,300 q200,-250 400,0 T1000,300",
				"M200,300 q200,-250 400,0 t400,0",
			],
			&[
				Command::MoveTo {
					p: Point { x: 200.0, y: 300.0 },
				},
				Command::quadratic_bezier_to(
					Point { x: 200.0, y: 300.0 },
					Point { x: 400.0, y: 50.0 },
					Point { x: 600.0, y: 300.0 },
				),
				Command::quadratic_bezier_to(
					Point { x: 600.0, y: 300.0 },
					Point { x: 800.0, y: 550.0 },
					Point {
						x: 1000.0,
						y: 300.0,
					},
				),
			],
		);
	}

	#[test]
	fn line_to() {
		assert_parse(
			&[
				"M 100,100 L 100,200 L 200,200 L 250,200",
				"M 100,100 v 100 h 100 h 50",
				"M 100,100 v 100 h 100 50",
				"M 100,100 v 100 h 100,50",
			],
			&[
				Command::MoveTo {
					p: Point { x: 100.0, y: 100.0 },
				},
				Command::LineTo {
					p: Point { x: 100.0, y: 200.0 },
				},
				Command::LineTo {
					p: Point { x: 200.0, y: 200.0 },
				},
				Command::LineTo {
					p: Point { x: 250.0, y: 200.0 },
				},
			],
		);
	}

	#[test]
	fn numbers() {
		assert_parse(
			&[
				"M -4.5 0.3 L 4 -10",
				"M -4.50 .3 4.00 -10",
				"M -4.50.3 4-10",
			],
			&[
				Command::MoveTo {
					p: Point { x: -4.5, y: 0.3 },
				},
				Command::LineTo {
					p: Point { x: 4.0, y: -10.0 },
				},
			],
		);
	}
}
