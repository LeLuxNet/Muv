use std::ops::{Add, AddAssign, Mul, MulAssign, Sub, SubAssign};

#[cfg(feature = "serde")]
use serde::{Deserialize, Serialize};

#[cfg(feature = "svg")]
pub mod svg;

#[cfg(feature = "vector-drawable")]
pub mod vector_drawable;

#[cfg(feature = "pdf")]
pub mod pdf;

#[cfg(feature = "iced")]
pub mod iced;

#[cfg(feature = "tiny-skia")]
pub mod tiny_skia;

mod command;
pub use command::*;

mod parse;
pub use parse::*;

#[derive(Debug, Clone, PartialEq)]
#[cfg_attr(feature = "serde", derive(Serialize, Deserialize))]
pub struct Graphic {
	pub view: Bbox,
	pub children: Vec<Path>,
}

impl Graphic {
	pub fn translate(&mut self, delta: Point) {
		self.view.top += delta.y;
		self.view.bottom += delta.y;
		self.view.left += delta.x;
		self.view.right += delta.x;

		for c in &mut self.children {
			c.translate(delta);
		}
	}
}

#[derive(Debug, Clone, PartialEq)]
#[cfg_attr(feature = "serde", derive(Serialize, Deserialize))]
pub struct Path {
	pub commands: Commands,
	pub stroke: Option<Stroke>,
	pub fill: Option<Fill>,
	pub text: Option<char>,
}

impl Path {
	pub fn translate(&mut self, delta: Point) {
		for cmd in &mut self.commands {
			match cmd {
				Command::MoveTo { p } | Command::LineTo { p } => *p += delta,
				Command::CubicBezierTo { c1, c2, p } => {
					*c1 += delta;
					*c2 += delta;
					*p += delta;
				}
				Command::Close => {}
			}
		}
	}
}

#[derive(Debug, Clone, Copy, PartialEq)]
#[cfg_attr(feature = "serde", derive(Serialize, Deserialize))]
pub struct Stroke {
	pub color: Color,
	pub width: f32,
	pub cap: Cap,
	pub join: Join,
}

#[derive(Debug, Clone, Copy, PartialEq)]
#[cfg_attr(feature = "serde", derive(Serialize, Deserialize))]
pub struct Fill {
	pub color: Color,
	pub rule: Rule,
}

#[derive(Debug, Clone, Copy, PartialEq)]
#[cfg_attr(feature = "serde", derive(Serialize, Deserialize))]
pub enum Color {
	Rgb { r: f32, g: f32, b: f32 },
}

impl Color {
	#[must_use]
	#[allow(clippy::missing_const_for_fn)]
	pub fn to_rgb(self) -> (f32, f32, f32) {
		match self {
			Self::Rgb { r, g, b } => (r, g, b),
		}
	}

	#[must_use]
	pub fn to_rgb8(self) -> (u8, u8, u8) {
		let (r, g, b) = self.to_rgb();
		(to_u8(r), to_u8(g), to_u8(b))
	}
}

#[allow(
	clippy::as_conversions,
	clippy::cast_possible_truncation,
	clippy::cast_sign_loss
)]
fn to_u8(n: f32) -> u8 {
	(n.clamp(0.0, 1.0) * 255.0).round() as u8
}

#[derive(Debug, Clone, Copy, PartialEq)]
#[cfg_attr(feature = "serde", derive(Serialize, Deserialize))]
pub struct Point {
	pub x: f32,
	pub y: f32,
}

impl Point {
	#[must_use]
	pub const fn new(x: f32, y: f32) -> Self {
		Self { x, y }
	}
}

impl Add for Point {
	type Output = Self;
	fn add(mut self, rhs: Self) -> Self::Output {
		self += rhs;
		self
	}
}

impl AddAssign for Point {
	fn add_assign(&mut self, rhs: Self) {
		self.x += rhs.x;
		self.y += rhs.y;
	}
}

impl Sub for Point {
	type Output = Self;
	fn sub(mut self, rhs: Self) -> Self::Output {
		self -= rhs;
		self
	}
}

impl SubAssign for Point {
	fn sub_assign(&mut self, rhs: Self) {
		self.x -= rhs.x;
		self.y -= rhs.y;
	}
}

impl Mul<f32> for Point {
	type Output = Self;
	fn mul(mut self, rhs: f32) -> Self::Output {
		self *= rhs;
		self
	}
}

impl MulAssign<f32> for Point {
	fn mul_assign(&mut self, rhs: f32) {
		self.x *= rhs;
		self.y *= rhs;
	}
}

#[derive(Debug, Clone, Copy, PartialEq)]
#[cfg_attr(feature = "serde", derive(Serialize, Deserialize))]
pub struct Bbox {
	pub top: f32,
	pub bottom: f32,
	pub left: f32,
	pub right: f32,
}

impl Default for Bbox {
	fn default() -> Self {
		Self {
			top: f32::INFINITY,
			bottom: -f32::INFINITY,
			left: f32::INFINITY,
			right: -f32::INFINITY,
		}
	}
}

impl Bbox {
	#[must_use]
	pub const fn width(self) -> f32 {
		(self.right - self.left).max(0.0)
	}
	#[must_use]
	pub const fn height(self) -> f32 {
		(self.bottom - self.top).max(0.0)
	}

	#[must_use]
	pub const fn area(self) -> f32 {
		self.width() * self.height()
	}

	#[must_use]
	pub const fn inside(self, p: Point) -> bool {
		self.top <= p.y && self.bottom >= p.y && self.left <= p.x && self.right >= p.y
	}

	#[must_use]
	pub const fn contains(self, other: Self) -> bool {
		self.top <= other.top
			&& self.bottom >= other.bottom
			&& self.left <= other.left
			&& self.right >= other.right
	}

	#[must_use]
	pub const fn union(self, other: Self) -> Self {
		Self {
			top: self.top.min(other.top),
			bottom: self.bottom.max(other.bottom),
			left: self.left.min(other.left),
			right: self.right.max(other.right),
		}
	}

	#[must_use]
	pub const fn insert(self, p: Point) -> Self {
		Self {
			top: self.top.min(p.y),
			bottom: self.bottom.max(p.y),
			left: self.left.min(p.x),
			right: self.right.max(p.x),
		}
	}
}

#[derive(Debug, Clone, Copy, PartialEq, Eq, PartialOrd, Ord, Hash)]
#[cfg_attr(feature = "serde", derive(Serialize, Deserialize))]
pub enum Cap {
	Butt,
	Round,
	Square,
}

#[derive(Debug, Clone, Copy, PartialEq, PartialOrd)]
#[cfg_attr(feature = "serde", derive(Serialize, Deserialize))]
pub enum Join {
	Miter { limit: f32 },
	Round,
	Bevel,
}

#[derive(Debug, Clone, Copy, PartialEq, Eq, PartialOrd, Ord, Hash)]
#[cfg_attr(feature = "serde", derive(Serialize, Deserialize))]
pub enum Rule {
	EvenOdd,
	NonZero,
}
