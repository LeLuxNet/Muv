use tiny_skia::{Color, FillRule, LineCap, LineJoin, Paint, Path, PathBuilder, Shader, Stroke};

use crate::{Cap, Command, Commands, Join, Rule};

impl From<crate::Color> for Color {
	fn from(value: crate::Color) -> Self {
		let (r, g, b) = value.to_rgb();
		Self::from_rgba(r, g, b, 1.0).unwrap()
	}
}

impl From<crate::Color> for Shader<'_> {
	fn from(value: crate::Color) -> Self {
		Self::SolidColor(value.into())
	}
}

impl From<crate::Color> for Paint<'_> {
	fn from(value: crate::Color) -> Self {
		Self {
			shader: value.into(),
			..Default::default()
		}
	}
}

impl From<Rule> for FillRule {
	fn from(value: Rule) -> Self {
		match value {
			Rule::EvenOdd => Self::EvenOdd,
			Rule::NonZero => Self::Winding,
		}
	}
}

impl From<Cap> for LineCap {
	fn from(value: Cap) -> Self {
		match value {
			Cap::Butt => Self::Butt,
			Cap::Round => Self::Round,
			Cap::Square => Self::Square,
		}
	}
}

impl From<Join> for LineJoin {
	fn from(value: Join) -> Self {
		match value {
			Join::Miter { limit: _ } => Self::Miter,
			Join::Round => Self::Round,
			Join::Bevel => Self::Bevel,
		}
	}
}

impl From<&crate::Stroke> for Stroke {
	fn from(value: &crate::Stroke) -> Self {
		Self {
			width: value.width,
			miter_limit: match value.join {
				Join::Miter { limit } => limit,
				_ => 0.0,
			},
			line_cap: value.cap.into(),
			line_join: value.join.into(),
			dash: None,
		}
	}
}

impl From<&Commands> for Path {
	fn from(value: &Commands) -> Self {
		let mut pb = PathBuilder::new();
		for cmd in value {
			match cmd {
				Command::MoveTo { p } => pb.move_to(p.x, p.y),
				Command::LineTo { p } => pb.line_to(p.x, p.y),
				Command::CubicBezierTo { c1, c2, p } => {
					pb.cubic_to(c1.x, c1.y, c2.x, c2.y, p.x, p.y);
				}
				Command::Close => pb.close(),
			}
		}
		pb.finish().unwrap()
	}
}
