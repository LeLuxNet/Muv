use crate::{Cap, Color, Fill, Join, Point, Rule, Stroke};

use iced_graphics::geometry::{self, fill};

impl From<Point> for iced_core::Point {
	fn from(value: Point) -> Self {
		Self {
			x: value.x,
			y: value.y,
		}
	}
}

impl From<Stroke> for geometry::Stroke<'_> {
	fn from(value: Stroke) -> Self {
		Self {
			style: value.color.into(),
			width: value.width,
			line_cap: value.cap.into(),
			line_join: value.join.into(),
			line_dash: geometry::LineDash::default(),
		}
	}
}

impl From<Fill> for fill::Fill {
	fn from(value: Fill) -> Self {
		Self {
			style: value.color.into(),
			rule: value.rule.into(),
		}
	}
}

impl From<Rule> for fill::Rule {
	fn from(value: Rule) -> Self {
		match value {
			Rule::EvenOdd => Self::EvenOdd,
			Rule::NonZero => Self::NonZero,
		}
	}
}

impl From<Color> for geometry::Style {
	fn from(value: Color) -> Self {
		Self::Solid(value.into())
	}
}

impl From<Color> for iced_core::Color {
	fn from(value: Color) -> Self {
		let (r, g, b) = value.to_rgb();
		Self::from_rgb(r, g, b)
	}
}

impl From<Cap> for geometry::LineCap {
	fn from(value: Cap) -> Self {
		match value {
			Cap::Butt => Self::Butt,
			Cap::Round => Self::Round,
			Cap::Square => Self::Square,
		}
	}
}

impl From<Join> for geometry::LineJoin {
	fn from(value: Join) -> Self {
		match value {
			Join::Miter { limit: _ } => Self::Miter,
			Join::Round => Self::Round,
			Join::Bevel => Self::Bevel,
		}
	}
}
