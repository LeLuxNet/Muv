use std::{
	fmt::{self, Display, Formatter},
	iter::FusedIterator,
	slice::{Iter, IterMut},
	vec::IntoIter,
};

#[cfg(feature = "serde")]
use serde::{Deserialize, Serialize};

use crate::Point;

#[derive(Debug, Clone, Copy, PartialEq)]
#[cfg_attr(feature = "serde", derive(Serialize, Deserialize))]
pub enum Command {
	MoveTo { p: Point },
	LineTo { p: Point },
	CubicBezierTo { c1: Point, c2: Point, p: Point },
	Close,
}

impl Command {
	/// ```
	/// use muv_vector::{Command, Point};
	///
	/// assert_eq!(
	/// 	Command::quadratic_bezier_to(
	/// 		Point::new(1.0, -1.0),
	/// 		Point::new(7.0, 2.0),
	/// 		Point::new(4.0, 11.0),
	/// 	),
	/// 	Command::CubicBezierTo {
	/// 		c1: Point::new(5.0, 1.0),
	/// 		c2: Point::new(6.0, 5.0),
	/// 		p: Point::new(4.0, 11.0),
	/// 	}
	/// );
	/// ```
	#[must_use]
	pub fn quadratic_bezier_to(origin: Point, c: Point, p: Point) -> Self {
		const TWO_THIRDS: f32 = 2.0 / 3.0;
		Self::CubicBezierTo {
			c1: (c - origin) * TWO_THIRDS + origin,
			c2: (c - p) * TWO_THIRDS + p,
			p,
		}
	}
}

impl Command {
	#[must_use]
	pub const fn last(self) -> Option<Point> {
		match self {
			Self::MoveTo { p } | Self::LineTo { p } | Self::CubicBezierTo { c1: _, c2: _, p } => {
				Some(p)
			}
			Self::Close => None,
		}
	}
}

/// ```
/// use muv_vector::{Command, Point};
///
/// assert_eq!(format!("{}", Command::Close), "Z");
///
/// let cmd = Command::MoveTo {
/// 	p: Point::new(-5.2, 12.358),
/// };
/// assert_eq!(format!("{cmd:.2}"), "M-5.20,12.36");
///
/// let cmd = Command::CubicBezierTo {
/// 	c1: Point::new(0.0, 1.6),
/// 	c2: Point::new(1.3, 4.2),
/// 	p: Point::new(3.2, 7.0),
/// };
/// assert_eq!(format!("{cmd:#}"), "C 0,1.6 1.3,4.2 3.2,7");
/// ```
impl Display for Command {
	fn fmt(&self, f: &mut Formatter<'_>) -> fmt::Result {
		f.write_str(match self {
			Self::MoveTo { .. } => "M",
			Self::LineTo { .. } => "L",
			Self::CubicBezierTo { .. } => "C",
			Self::Close => "Z",
		})?;

		#[allow(clippy::items_after_statements)]
		fn fmt_point(f: &mut Formatter, p: Point) -> fmt::Result {
			p.x.fmt(f)?;
			f.write_str(",")?;
			p.y.fmt(f)
		}

		match *self {
			Self::MoveTo { p } | Self::LineTo { p } => {
				if f.alternate() {
					f.write_str(" ")?;
				}
				fmt_point(f, p)
			}
			Self::CubicBezierTo { c1, c2, p } => {
				if f.alternate() {
					f.write_str(" ")?;
				}
				fmt_point(f, c1)?;
				f.write_str(" ")?;
				fmt_point(f, c2)?;
				f.write_str(" ")?;
				fmt_point(f, p)
			}
			Self::Close => Ok(()),
		}
	}
}

#[derive(Debug, Default, Clone, PartialEq)]
#[cfg_attr(feature = "serde", derive(Serialize, Deserialize))]
pub struct Commands(Vec<Command>);

impl Commands {
	pub fn push(&mut self, cmd: Command) {
		self.0.push(cmd);
	}

	pub fn pop(&mut self) -> Option<Command> {
		self.0.pop()
	}

	#[must_use]
	pub fn len(&self) -> usize {
		self.0.len()
	}

	#[must_use]
	pub fn is_empty(&self) -> bool {
		self.0.is_empty()
	}
}

impl Display for Commands {
	fn fmt(&self, f: &mut Formatter<'_>) -> fmt::Result {
		for (i, cmd) in self.iter().enumerate() {
			if f.alternate() && i != 0 {
				f.write_str(" ")?;
			}
			Display::fmt(&cmd, f)?;
		}
		Ok(())
	}
}

impl Commands {
	pub fn iter(&self) -> IterCommands<'_> {
		self.into_iter()
	}

	pub fn iter_mut(&mut self) -> IterMutCommands<'_> {
		self.into_iter()
	}
}

impl IntoIterator for Commands {
	type Item = Command;
	type IntoIter = IntoCommands;

	fn into_iter(self) -> Self::IntoIter {
		IntoCommands(self.0.into_iter())
	}
}

#[must_use]
pub struct IntoCommands(IntoIter<Command>);

impl Iterator for IntoCommands {
	type Item = Command;
	fn next(&mut self) -> Option<Self::Item> {
		self.0.next()
	}

	fn size_hint(&self) -> (usize, Option<usize>) {
		self.0.size_hint()
	}
}

impl ExactSizeIterator for IntoCommands {}

impl FusedIterator for IntoCommands {}

#[must_use]
pub struct IterCommands<'a>(Iter<'a, Command>);

impl<'a> IntoIterator for &'a Commands {
	type Item = Command;
	type IntoIter = IterCommands<'a>;

	fn into_iter(self) -> Self::IntoIter {
		IterCommands(self.0.iter())
	}
}

impl Iterator for IterCommands<'_> {
	type Item = Command;
	fn next(&mut self) -> Option<Self::Item> {
		self.0.next().copied()
	}

	fn size_hint(&self) -> (usize, Option<usize>) {
		self.0.size_hint()
	}
}

impl ExactSizeIterator for IterCommands<'_> {}

impl FusedIterator for IterCommands<'_> {}

#[must_use]
pub struct IterMutCommands<'a>(IterMut<'a, Command>);

impl<'a> IntoIterator for &'a mut Commands {
	type Item = &'a mut Command;
	type IntoIter = IterMutCommands<'a>;

	fn into_iter(self) -> Self::IntoIter {
		IterMutCommands(self.0.iter_mut())
	}
}

impl<'a> Iterator for IterMutCommands<'a> {
	type Item = &'a mut Command;
	fn next(&mut self) -> Option<Self::Item> {
		self.0.next()
	}

	fn size_hint(&self) -> (usize, Option<usize>) {
		self.0.size_hint()
	}
}

impl ExactSizeIterator for IterMutCommands<'_> {}

impl FusedIterator for IterMutCommands<'_> {}

impl FromIterator<Command> for Commands {
	fn from_iter<T: IntoIterator<Item = Command>>(iter: T) -> Self {
		Self(iter.into_iter().collect())
	}
}
